/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test suite for the '<em><b>Sspvis_emf</b></em>'
 * model. <!-- end-user-doc -->
 * 
 * @generated
 */
public class Sspvis_emfAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Sspvis_emfAllTests("Sspvis_emf Tests");
		suite.addTest(SSPVisEMFTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Sspvis_emfAllTests(String name) {
		super(name);
	}

} // Sspvis_emfAllTests
