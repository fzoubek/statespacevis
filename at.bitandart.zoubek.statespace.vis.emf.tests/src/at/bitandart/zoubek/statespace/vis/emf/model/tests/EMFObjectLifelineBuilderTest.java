/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.tests;

import at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder;
import at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>EMF Object Lifeline Builder</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
 * <em>Build Lifelines</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class EMFObjectLifelineBuilderTest extends TestCase {

	/**
	 * The fixture for this EMF Object Lifeline Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EMFObjectLifelineBuilder fixture = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EMFObjectLifelineBuilderTest.class);
	}

	/**
	 * Constructs a new EMF Object Lifeline Builder test case with the given
	 * name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EMFObjectLifelineBuilderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this EMF Object Lifeline Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(EMFObjectLifelineBuilder fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this EMF Object Lifeline Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EMFObjectLifelineBuilder getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SSPVisEMFFactory.eINSTANCE.createEMFObjectLifelineBuilder());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * <em>Build Lifelines</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * @generated
	 */
	public void testBuildLifelines__ModelHistory() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} // EMFObjectLifelineBuilderTest
