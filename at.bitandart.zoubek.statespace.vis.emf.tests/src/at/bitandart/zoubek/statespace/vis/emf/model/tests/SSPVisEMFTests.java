/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test suite for the '<em><b>model</b></em>' package.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class SSPVisEMFTests extends TestSuite {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new SSPVisEMFTests("model Tests");
		suite.addTestSuite(EMFObjectLifelineBuilderTest.class);
		suite.addTestSuite(EMFCompareHistoryBuilderTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SSPVisEMFTests(String name) {
		super(name);
	}

} // SSPVisEMFTests
