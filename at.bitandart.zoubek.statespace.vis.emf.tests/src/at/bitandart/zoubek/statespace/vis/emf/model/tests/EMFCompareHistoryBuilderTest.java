/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.tests;

import at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>EMF Compare History Builder</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
 * <em>Update Object History</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class EMFCompareHistoryBuilderTest extends TestCase {

	/**
	 * The fixture for this EMF Compare History Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EMFCompareHistoryBuilder fixture = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EMFCompareHistoryBuilderTest.class);
	}

	/**
	 * Constructs a new EMF Compare History Builder test case with the given
	 * name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EMFCompareHistoryBuilderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this EMF Compare History Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(EMFCompareHistoryBuilder fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this EMF Compare History Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EMFCompareHistoryBuilder getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SSPVisEMFFactory.eINSTANCE.createEMFCompareHistoryBuilder());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * <em>Update Object History</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * @generated
	 */
	public void testUpdateObjectHistory__Tree_ModelHistory() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} // EMFCompareHistoryBuilderTest
