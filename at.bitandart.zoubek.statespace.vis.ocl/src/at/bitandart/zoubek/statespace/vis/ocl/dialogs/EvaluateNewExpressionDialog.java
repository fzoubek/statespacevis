/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.ocl.dialogs;

import java.io.File;
import java.io.FileFilter;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

/**
 * A dialog that collects all necessary data for a new evaluation of an ocl
 * expression for state space path visualization
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class EvaluateNewExpressionDialog extends TitleAreaDialog {

	private String	oclExpression;
	private File	baseModelDir;
	private String	ecoreFilename;
	private String	henshinFilename;
	private String	initFilename;
	private Text	oclExpressionInput;
	private Text	baseModelDirInput;
	private Text	ecoreFilenameInput;
	private Text	henshinFilenameInput;
	private Text	initFilenameInput;
	private Button	selectEcoreFileButton;
	private Button	selectHenshinFileButton;
	private Button	selectInitFileButton;

	public EvaluateNewExpressionDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	public void create() {
		super.create();
		setTitle("Evaluate new expression");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout(3, false);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		container.setLayout(layout);

		// ocl expression
		Label oclExpressionLabel = new Label(container, SWT.NONE);
		oclExpressionLabel.setText("OCL Expression:");

		GridData gdOCLExpressionInput = new GridData();
		gdOCLExpressionInput.grabExcessHorizontalSpace = true;
		gdOCLExpressionInput.horizontalAlignment = GridData.FILL;
		gdOCLExpressionInput.horizontalSpan = 2;

		oclExpressionInput = new Text(container, SWT.BORDER);
		oclExpressionInput.setLayoutData(gdOCLExpressionInput);

		// base model directory
		Label baseModelDirLabel = new Label(container, SWT.NONE);
		baseModelDirLabel.setText("Base model directory:");

		GridData gdBaseModelDirInput = new GridData();
		gdBaseModelDirInput.grabExcessHorizontalSpace = true;
		gdBaseModelDirInput.horizontalAlignment = GridData.FILL;

		baseModelDirInput = new Text(container, SWT.BORDER);
		baseModelDirInput.setLayoutData(gdBaseModelDirInput);
		baseModelDirInput.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				File modelDir = new File(baseModelDirInput.getText());
				boolean isValidDir = modelDir.exists()
										&& modelDir.isDirectory();
				selectEcoreFileButton.setEnabled(isValidDir);
				selectHenshinFileButton.setEnabled(isValidDir);
				selectInitFileButton.setEnabled(isValidDir);
			}
		});

		Button selectBaseModelDirButton = new Button(container, SWT.PUSH);
		selectBaseModelDirButton.setText("select...");
		selectBaseModelDirButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(getShell());
				dialog.setFilterPath(baseModelDirInput.getText());
				String dirPath = dialog.open();
				if (dirPath != null) {
					baseModelDirInput.setText(dirPath);
				}
			}
		});

		// ecore file name
		Label ecoreFilenameLabel = new Label(container, SWT.NONE);
		ecoreFilenameLabel.setText("Ecore file name:");

		GridData gdEcoreFilenameInput = new GridData();
		gdEcoreFilenameInput.grabExcessHorizontalSpace = true;
		gdEcoreFilenameInput.horizontalAlignment = GridData.FILL;

		ecoreFilenameInput = new Text(container, SWT.BORDER);
		ecoreFilenameInput.setLayoutData(gdEcoreFilenameInput);

		selectEcoreFileButton = new Button(container, SWT.PUSH);
		selectEcoreFileButton.setText("select...");
		selectEcoreFileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				File modelDir = new File(baseModelDirInput.getText());

				if (modelDir.exists() && modelDir.isDirectory()) {

					ElementListSelectionDialog dialog = new ElementListSelectionDialog(	getShell(),
																						new LabelProvider() {
																							@Override
																							public String getText(
																									Object element) {
																								if (element instanceof File) {
																									return ((File) element).getName();
																								}
																								return super.getText(element);
																							}
																						});
					dialog.setMultipleSelection(false);
					dialog.setElements(modelDir.listFiles(new FileFilter() {

						@Override
						public boolean accept(File pathname) {
							return pathname.isFile();
						}

					}));
					if (dialog.open() == Window.OK) {
						Object[] result = dialog.getResult();
						if (result != null
							&& result.length > 0 && result[0] instanceof File) {
							ecoreFilenameInput.setText(((File) result[0]).getName());
						}
					}
				}
			}
		});
		selectEcoreFileButton.setEnabled(false);

		// henshin file name
		Label henshinFilenameLabel = new Label(container, SWT.NONE);
		henshinFilenameLabel.setText("Henshin file name:");

		GridData gdHenshinFilenameInput = new GridData();
		gdHenshinFilenameInput.grabExcessHorizontalSpace = true;
		gdHenshinFilenameInput.horizontalAlignment = GridData.FILL;

		henshinFilenameInput = new Text(container, SWT.BORDER);
		henshinFilenameInput.setLayoutData(gdHenshinFilenameInput);

		selectHenshinFileButton = new Button(container, SWT.PUSH);
		selectHenshinFileButton.setText("select...");
		selectHenshinFileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				File modelDir = new File(baseModelDirInput.getText());

				if (modelDir.exists() && modelDir.isDirectory()) {

					ElementListSelectionDialog dialog = new ElementListSelectionDialog(	getShell(),
																						new LabelProvider() {
																							@Override
																							public String getText(
																									Object element) {
																								if (element instanceof File) {
																									return ((File) element).getName();
																								}
																								return super.getText(element);
																							}
																						});
					dialog.setMultipleSelection(false);
					dialog.setElements(modelDir.listFiles(new FileFilter() {

						@Override
						public boolean accept(File pathname) {
							return pathname.isFile();
						}

					}));
					if (dialog.open() == Window.OK) {
						Object[] result = dialog.getResult();
						if (result != null
							&& result.length > 0 && result[0] instanceof File) {
							henshinFilenameInput.setText(((File) result[0]).getName());
						}
					}
				}
			}
		});
		selectHenshinFileButton.setEnabled(false);

		// init file name
		Label initFilenameLabel = new Label(container, SWT.NONE);
		initFilenameLabel.setText("Init file name:");

		GridData gdInitFilenameInput = new GridData();
		gdInitFilenameInput.grabExcessHorizontalSpace = true;
		gdInitFilenameInput.horizontalAlignment = GridData.FILL;

		initFilenameInput = new Text(container, SWT.BORDER);
		initFilenameInput.setLayoutData(gdInitFilenameInput);

		selectInitFileButton = new Button(container, SWT.PUSH);
		selectInitFileButton.setText("select...");
		selectInitFileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				File modelDir = new File(baseModelDirInput.getText());

				if (modelDir.exists() && modelDir.isDirectory()) {

					ElementListSelectionDialog dialog = new ElementListSelectionDialog(	getShell(),
																						new LabelProvider() {
																							@Override
																							public String getText(
																									Object element) {
																								if (element instanceof File) {
																									return ((File) element).getName();
																								}
																								return super.getText(element);
																							}
																						});
					dialog.setMultipleSelection(false);
					dialog.setElements(modelDir.listFiles(new FileFilter() {

						@Override
						public boolean accept(File pathname) {
							return pathname.isFile();
						}

					}));
					if (dialog.open() == Window.OK) {
						Object[] result = dialog.getResult();
						if (result != null
							&& result.length > 0 && result[0] instanceof File) {
							initFilenameInput.setText(((File) result[0]).getName());
						}
					}
				}
			}
		});
		selectInitFileButton.setEnabled(false);

		return area;
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		oclExpression = oclExpressionInput.getText();
		baseModelDir = new File(baseModelDirInput.getText());
		ecoreFilename = ecoreFilenameInput.getText();
		henshinFilename = henshinFilenameInput.getText();
		initFilename = initFilenameInput.getText();
		super.okPressed();
	}

	/**
	 * 
	 * @return the selected model directory or null if the dialog has been
	 *         canceled
	 */
	public File getBaseModelDirectory() {
		return baseModelDir;
	}

	/**
	 * 
	 * @return the ecore file name or null if the dialog has been canceled
	 * 
	 */
	public String getEcoreFilename() {
		return ecoreFilename;
	}

	/**
	 * 
	 * @return the henshin file name or null if the dialog has been canceled
	 * 
	 */
	public String getHenshinFilename() {
		return henshinFilename;
	}

	/**
	 * 
	 * @return the init filename or null if the dialog has been canceled
	 * 
	 */
	public String getInitFilename() {
		return initFilename;
	}

	/**
	 * 
	 * @return the ocl expression or null if the dialog has been canceled
	 * 
	 */
	public String getOCLExpression() {
		return oclExpression;
	}

}
