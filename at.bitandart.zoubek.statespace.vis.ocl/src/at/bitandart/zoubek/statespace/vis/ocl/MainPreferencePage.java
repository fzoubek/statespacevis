package at.bitandart.zoubek.statespace.vis.ocl;
/**
 * 
 */


import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class MainPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	/**
	 * Creates a new preference page with an empty title and no image.
	 */
	public MainPreferencePage() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(SSPVisOCLPlugin.getInstance()
												.getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {
		addField(new ColorFieldEditor(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_TRUE,
										"OCL lifeline color for value = true:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_FALSE,
										"OCL lifeline color for value = false:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_UNKNOWN,
										"OCL lifeline color for an unknown value:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_TRUE,
										"OCL lifeline selection color for value = true:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_FALSE,
										"OCL lifeline selection color for value = false:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_UNKNOWN,
										"OCL lifeline selection color for an unknown value:",
										getFieldEditorParent()));

	}

}
