/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.ocl.gef;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import at.bitandart.zoubek.statespace.vis.ocl.gef.edit.parts.OCLLifelineEP;
import at.bitandart.zoubek.statespace.vis.ocl.gef.edit.parts.OCLValueEP;
import at.bitandart.zoubek.statespace.vis.ocl.gef.edit.parts.OCLValueTransitionEP;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * a {@link EditPartFactory} that creates ocl edit parts.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OCLEditPartFactory implements EditPartFactory {
	
	private static final Logger LOG = Logger.getLogger(OCLEditPartFactory.class.getCanonicalName());

	@Override
	public EditPart createEditPart(EditPart contextEditPart, Object model) {
		LOG.log(Level.FINE, "Searching fo edit part for the object {0}...", model);
		EditPart editPart = null;
		if (model instanceof OCLLifeline) {
			editPart = new OCLLifelineEP();
			editPart.setModel(model);
		}
		if (model instanceof TreeNode) {
			LOG.log(Level.FINER, "Model of context edit part is {0}", contextEditPart.getModel());
			if (contextEditPart.getModel() instanceof OCLLifeline) {
				editPart = new OCLValueTransitionEP((OCLLifeline) contextEditPart.getModel());
				editPart.setModel(model);
			}
		}
		if (model instanceof TreeTransition) {
			LOG.log(Level.FINER, "Model of parent context edit part is {0}", contextEditPart.getParent().getModel());
			if (contextEditPart.getParent().getModel() instanceof OCLLifeline) {
				editPart = new OCLValueEP((OCLLifeline) contextEditPart.getParent()
																		.getModel());
				editPart.setModel(model);
			}

		}
		if(editPart != null){
			LOG.log(Level.FINE, "Created edit part {1} for the object {0}", new Object[]{ model, editPart});
		}else{
			LOG.log(Level.FINE, "No edit part found for the object {0}", model);
		}
		return editPart;
	}
}
