package at.bitandart.zoubek.statespace.vis.ocl.handlers;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.modelevolution.mc.stateconvert.api.Expression;
import org.modelevolution.mc.stateconvert.api.MainApi;
import org.osgi.framework.Bundle;

import at.bitandart.zoubek.statespace.vis.editors.StateSpacePathVisEditor;
import at.bitandart.zoubek.statespace.vis.editors.inputs.SSPVisEditorInput;
import at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFFactoryImpl;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineBuilder;
import at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisFactoryImpl;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl;

public class EvaluateNewExpressionRunnable implements IRunnableWithProgress {

	private static final Logger		LOG	= Logger.getLogger(EvaluateNewExpressionRunnable.class.getCanonicalName());

	/**
	 * the workbench window used to open the editor
	 */
	private final IWorkbenchWindow	window;
	/**
	 * the initial ocl expression
	 */
	private String					expression;
	/**
	 * the directory containing all models
	 */
	private File					modelDir;
	/**
	 * the ecore file name
	 */
	private String					ecoreFilename;
	/**
	 * the henshin file name
	 */
	private String					henshinFilename;
	/**
	 * the file name of the inital model snapshot?
	 */
	private String					initFilename;

	/**
	 * 
	 * @param window
	 *            the workbench window used to open the editor
	 * @param expression
	 *            the initial ocl expression
	 * @param relModelDir
	 *            the relative path to the model directory within the
	 *            org.eclipsem.modelevolution.stateconvertplug plugin
	 * @param ecoreFilename
	 *            the ecore file name
	 * @param henshinFilename
	 *            the henshin file name
	 * @param initFilename
	 *            the file name of the initial model snapshot?
	 */
	public EvaluateNewExpressionRunnable(IWorkbenchWindow window,
			String expression, String relModelDir, String ecoreFilename,
			String henshinFilename, String initFilename) {
		this.window = window;
		this.expression = expression;
		this.ecoreFilename = ecoreFilename;
		this.henshinFilename = henshinFilename;
		this.initFilename = initFilename;

		// resolve model base directory
		Bundle bundle = Platform.getBundle("org.eclipsem.modelevolution.stateconvertplug");
		URL bundleDir = bundle.getEntry("");
		URI modelDir = null;
		try {
			modelDir = new URI(FileLocator.resolve(bundleDir).toURI()
								+ relModelDir);
			LOG.log(Level.FINER,
					"Bundle directory URI: {0}",
					FileLocator.resolve(bundleDir).toURI().toString());

		} catch (URISyntaxException | IOException e) {
			LOG.log(Level.SEVERE,
					"Error resolving model base directory: "
							+ e.getLocalizedMessage(),
					e);
		}

		this.modelDir = new File(modelDir);
	}

	/**
	 * 
	 * @param window
	 *            the workbench window used to open the editor
	 * @param expression
	 *            the initial ocl expression
	 * @param modelDir
	 * @param ecoreFilename
	 *            the ecore file name
	 * @param henshinFilename
	 *            the henshin file name
	 * @param initFilename
	 *            the file name of the initial model snapshot?
	 */
	public EvaluateNewExpressionRunnable(IWorkbenchWindow window,
			String expression, File modelDir, String ecoreFilename,
			String henshinFilename, String initFilename) {
		this.window = window;
		this.expression = expression;
		this.ecoreFilename = ecoreFilename;
		this.henshinFilename = henshinFilename;
		this.initFilename = initFilename;
		this.modelDir = modelDir;
	}

	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {

		monitor.beginTask("evaluating \"" + expression + "\"", 4);
		monitor.subTask("Initializing model...");

		StatespacevisPackageImpl.init();
		StatespacevisFactory modelFactory = StatespacevisFactoryImpl.eINSTANCE;

		final at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis model = modelFactory.createStateSpacePathVis();

		model.setModelHistoryBuilder(SSPVisEMFFactoryImpl.eINSTANCE.createEMFCompareHistoryBuilder());
		model.getLifelineBuilders()
				.add(SSPVisEMFFactoryImpl.eINSTANCE.createEMFObjectLifelineBuilder());
		OCLLifelineBuilder oclLifelineBuilder = SSPVisOCLFactory.eINSTANCE.createOCLLifelineBuilder();
		model.getLifelineBuilders().add(oclLifelineBuilder);

		try {
			monitor.subTask("locating model data...");
			monitor.worked(1);

			monitor.subTask("evaluating expression...");
			monitor.worked(1);

			Expression expr = MainApi.evaluate(	expression,
												modelDir,
												ecoreFilename,
												henshinFilename,
												initFilename);

			monitor.subTask("Updating editor model...");
			monitor.worked(1);

			model.setStatespace(expr.getStateSpace().getStateSpace());
			Tree<State, StateSpaceTransition> tree = modelFactory.createTree();
			TreeNode<State, StateSpaceTransition> mainNode = expr.getStateSpace()
																	.getMainNode();
			tree.setRootNode(mainNode);
			model.setStatePathTree(tree);
			List<Expression> expressions = new ArrayList<>();
			expressions.add(expr);
			oclLifelineBuilder.setExpressions(expressions);

			monitor.worked(1);

			window.getWorkbench().getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					try {
						window.getActivePage()
								.openEditor(new SSPVisEditorInput(model),
											StateSpacePathVisEditor.EDITOR_ID);
					} catch (PartInitException e) {
						MessageDialog.openError(window.getShell(),
												"Couldn't open editor",
												"Couldn't open the state space visualization editor, see message log for further details");
						e.printStackTrace();
					}
				}
			});

		} catch (IOException e) {
			MessageDialog.openError(window.getShell(),
									"Couldn't open editor",
									"Couldn't open the state space path visualization editor, see message log for further details");
			// TODO Auto-generated catch block
			LOG.log(Level.SEVERE,
					"Error during expression evaluation: "
							+ e.getLocalizedMessage(),
					e);
		}
	}
}