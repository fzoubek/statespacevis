/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.ocl.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.Label;
import org.eclipse.emf.ecore.util.EContentAdapter;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.LifelineEP;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;

/**
 * Edit part for an OCL lifeline
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OCLLifelineEP extends LifelineEP {

	private static Logger	LOG	= Logger.getLogger(OCLLifelineEP.class.getCanonicalName());

	/**
	 * 
	 * @return the OCL lifeline instance or null if the model is not an instance
	 *         of <code>ObjectLifeline</code>
	 */
	public OCLLifeline getOCLLifeline() {
		Object model = getModel();
		if (model instanceof OCLLifeline) {
			return (OCLLifeline) model;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
	}

	@Override
	protected List<?> getModelChildren() {
		OCLLifeline model = (OCLLifeline) getModel();
		List<Object> children = new ArrayList<>();
		children.addAll(model.getOclValues().getAllNodes());
		return children;
	}

	@Override
	protected void updateLabel(Label label, Lifeline lifeline) {
		OCLLifeline oclLifeline = (OCLLifeline) lifeline;
		LOG.log(Level.FINE, "Updating label...");
		String labelString = "";
		if (lifeline != null) {
			labelString = oclLifeline.getExpression();
		}
		label.setText(labelString);
	}

	@Override
	protected Tree<?, ?> getLifelineTree(Lifeline lifeline) {
		return ((OCLLifeline) lifeline).getOclValues();
	}
	
	@Override
	protected void registerModelUpdateListener() {
		super.registerModelUpdateListener();
		// register also to tree node changes
		OCLLifeline objectLifeline = getOCLLifeline();
		if (objectLifeline != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<?, ?> node : objectLifeline.getOclValues().getAllNodes()) {
				node.eAdapters().add(modelUpdateListener);
			}
		}
	}
	
	@Override
	protected void notifyModelChanged() {
		super.notifyModelChanged();
		OCLLifeline objectLifeline = getOCLLifeline();
		// register also to new tree node changes
		if (objectLifeline != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<?, ?> node : objectLifeline.getOclValues().getAllNodes()) {
				if(!node.eAdapters().contains(modelUpdateListener)){
					node.eAdapters().add(modelUpdateListener);
				}
			}
		}
	}

	@Override
	protected void unregisterModelUpdateListener() {
		super.registerModelUpdateListener();
		// unregister also to tree node change listener
		OCLLifeline objectLifeline = getOCLLifeline();
		if (objectLifeline != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<?, ?> node : objectLifeline.getOclValues().getAllNodes()) {
				node.eAdapters().remove(modelUpdateListener);
			}
		}
	}
}
