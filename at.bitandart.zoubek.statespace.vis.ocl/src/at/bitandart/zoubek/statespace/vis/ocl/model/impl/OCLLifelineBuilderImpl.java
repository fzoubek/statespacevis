/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model.impl;

import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineBuilder;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue;
import at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLFactory;
import at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.modelevolution.mc.stateconvert.api.Expression;
import org.modelevolution.mc.stateconvert.api.Value;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>OCL Lifeline Builder</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class OCLLifelineBuilderImpl extends MinimalEObjectImpl.Container implements OCLLifelineBuilder {

	/**
	 * default logger
	 */
	private static final Logger	LOG	= Logger.getLogger(OCLLifelineBuilderImpl.class.getCanonicalName());

	/**
	 * the expressions used to create the lifelines
	 */
	private List<Expression>	expressions;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected OCLLifelineBuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SSPVisOCLPackage.Literals.OCL_LIFELINE_BUILDER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<Lifeline> buildLifelines(
			Tree<State, StateSpaceTransition> stateTree,
			ModelHistory modelHistory) {
		LOG.fine("building ocl lifelines...");
		EList<Lifeline> lifelines = new BasicEList<Lifeline>();
		StatespacevisFactory sspvisFactory = StatespacevisFactory.eINSTANCE;
		SSPVisOCLFactory oclFactory = SSPVisOCLFactory.eINSTANCE;
		for (Expression expression : expressions) {
			lifelines.add(buildOCLLifelineFromExpression(	stateTree,
															expression,
															oclFactory,
															sspvisFactory));
		}
		LOG.fine("completed building ocl lifelines");
		return lifelines;
	}

	private OCLLifeline buildOCLLifelineFromExpression(
			Tree<State, StateSpaceTransition> stateTree, Expression expression,
			SSPVisOCLFactory oclFactory, StatespacevisFactory sspvisFactory) {
		LOG.log(Level.FINE,
				"Building ocl lifeline for expression {0}...",
				expression.getExpression());

		OCLLifeline oclLifeline = oclFactory.createOCLLifeline();
		oclLifeline.setCollapsed(false);
		oclLifeline.setExpression(expression.getExpression());
		oclLifeline.setVisible(true);
		oclLifeline.setParentLifeline(null);

		Tree<Void, OCLValue> values = sspvisFactory.createTree();
		oclLifeline.setOclValues(values);

		LOG.log(Level.FINE, "Building ocl lifeline values...");

		Tree<Value<?>, Void> expressionValues = expression.getValues(stateTree);
		TreeNode<Void, OCLValue> rootNode = sspvisFactory.createTreeNode();
		convertToOCLValues(	rootNode,
							expressionValues.getRootNode(),
							sspvisFactory,
							oclFactory);
		values.setRootNode(rootNode);
		

		LOG.log(Level.FINE, "Building child ocl lifelines for {0}...", oclLifeline);
		
		for(Expression subExpression : expression.getSubexpressions()){
			oclLifeline.getChildLifelines().add(buildOCLLifelineFromExpression(stateTree, subExpression, oclFactory, sspvisFactory));
		}

		LOG.log(Level.FINE, "Finished building child ocl lifelines for {0}", oclLifeline);
		
		return oclLifeline;
	}

	/**
	 * convert a TreeNode and all its child tree nodes from {@link Value}s to
	 * TreeNodes containing {@link OCLValue}s
	 * 
	 * @param rootNode
	 *            the node to start with
	 * @param sspvisFactory
	 *            the factory of the main SSPVis model to use
	 * @param oclFactory
	 *            the factory of the OCL model to use
	 */
	private void convertToOCLValues(TreeNode<Void, OCLValue> prevNode,
			TreeNode<Value<?>, Void> rootNode,
			StatespacevisFactory sspvisFactory, SSPVisOCLFactory oclFactory) {
		LOG.log(Level.FINE, "Converting node {0} to ocl values", rootNode);
		if (rootNode.getChildTransitions().isEmpty()) {
			LOG.log(Level.FINE, "Reached last leaf, creating leaf node");
			TreeNode<Void, OCLValue> nextNode = sspvisFactory.createTreeNode();
			TreeTransition<Void, OCLValue> transition = sspvisFactory.createTreeTransition();
			transition.setPreviousNode(prevNode);
			transition.setNextNode(nextNode);

			Value<?> exprValue = rootNode.getNodeValue();
			OCLValue value = oclFactory.createOCLValue();
			if(exprValue != null){
				value.setClazz(exprValue.getClazz());
				value.setKnown(exprValue.isKnown());
				value.setValue(exprValue.getValue());
			}else{
				value.setClazz(Object.class);
				value.setKnown(false);
				value.setValue(null);
			}

			LOG.log(Level.FINEST, "Setting OCLValue: {0}", value);
			transition.setTransitionValue(value);
		} else {
			for (TreeTransition<Value<?>, Void> childTransition : rootNode.getChildTransitions()) {
				LOG.log(Level.FINER,
						"Creating node for transition {0}",
						childTransition);
				TreeNode<Void, OCLValue> nextNode = sspvisFactory.createTreeNode();
				TreeTransition<Void, OCLValue> transition = sspvisFactory.createTreeTransition();
				transition.setPreviousNode(prevNode);
				transition.setNextNode(nextNode);

				Value<?> exprValue = rootNode.getNodeValue();
				OCLValue value = oclFactory.createOCLValue();
				value.setClazz(exprValue.getClazz());
				value.setKnown(exprValue.isKnown());
				value.setValue(exprValue.getValue());

				LOG.log(Level.FINEST, "Setting OCLValue: {0}", value);
				transition.setTransitionValue(value);

				convertToOCLValues(	nextNode,
									childTransition.getNextNode(),
									sspvisFactory,
									oclFactory);
			}
		}
		LOG.log(Level.FINE,
				"Finished converting node {0} to ocl values",
				rootNode);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SSPVisOCLPackage.OCL_LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY:
				return buildLifelines(	(Tree<State, StateSpaceTransition>) arguments.get(0),
										(ModelHistory) arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * @return the expressions
	 */
	public List<Expression> getExpressions() {
		return expressions;
	}

	/**
	 * @param expressions
	 *            the expressions to set
	 */
	public void setExpressions(List<Expression> expressions) {
		this.expressions = expressions;
	}

} //OCLLifelineBuilderImpl
