/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OCL Lifeline Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLLifelineTransition()
 * @model
 * @generated
 */
public interface OCLLifelineTransition extends EObject {
} // OCLLifelineTransition
