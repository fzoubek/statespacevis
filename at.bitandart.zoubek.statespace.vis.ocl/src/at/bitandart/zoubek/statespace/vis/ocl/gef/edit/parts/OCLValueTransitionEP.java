/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.ocl.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.figures.ObjectTransitionNodeFigure;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.BaseEditPart;
import at.bitandart.zoubek.statespace.vis.gef.edit.policies.ColoredSelectionFeedbackPolicy;
import at.bitandart.zoubek.statespace.vis.layout.TreeLayoutMarker;
import at.bitandart.zoubek.statespace.vis.ocl.SSPVisOCLPlugin;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Edit part that represents a state space transition in an Object Lifeline
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OCLValueTransitionEP extends BaseEditPart {

	private static final Logger				LOG						= Logger.getLogger(OCLValueTransitionEP.class.getCanonicalName());

	private OCLLifeline						parentLifeline			= null;

	/**
	 * figure color for "true" values
	 */
	private Color							trueColor				= null;

	/**
	 * figure color for "false" values
	 */
	private Color							falseColor				= null;

	/**
	 * figure color for unknown values
	 */
	private Color							unknownColor			= null;

	/**
	 * figure color for selected "true" values
	 */
	private Color							trueSelectedColor		= null;

	/**
	 * figure color for selected "false" values
	 */
	private Color							falseSelectedColor		= null;

	/**
	 * figure color for selected unknown values
	 */
	private Color							unknownSelectedColor	= null;

	/**
	 * the preference listener
	 */
	private IPropertyChangeListener			preferenceListener		= null;

	private ColoredSelectionFeedbackPolicy	selectionPolicy;

	public OCLValueTransitionEP(OCLLifeline parentLifeline) {
		this.parentLifeline = parentLifeline;

		// add preference listener
		preferenceListener = new IPropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {
				LOG.log(Level.FINE, "Processing property update...");
				if (event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_TRUE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_FALSE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_UNKNOWN)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_TRUE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_FALSE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_UNKNOWN)) {
					updateColorsFromPreferences();
					refresh();
				} else {
					LOG.log(Level.FINE, "Nothing to update");
				}
				LOG.log(Level.FINE, "Property update processed");
			}
		};
		SSPVisOCLPlugin.getInstance().getPreferenceStore()
						.addPropertyChangeListener(preferenceListener);
		selectionPolicy = new ColoredSelectionFeedbackPolicy(	ColorConstants.black,
																ColorConstants.black,
																ColorConstants.gray,
																ColorConstants.gray); // default values, will be set to the correct values by updateColorsFormPreferences()
		// load colors
		updateColorsFromPreferences();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		LOG.log(Level.FINE, "Creating figure...");
		float dim = (parentLifeline.getParentLifeline() == null)
				? 16.0f : 12.0f;
		ObjectTransitionNodeFigure figure = new ObjectTransitionNodeFigure();
		figure.setPreferredSize(new PrecisionDimension(16, dim));
		figure.setChangedDiameter(dim - 2);
		updateFigureColor(figure, getOCLValueTransitionTreeNode());
		figure.setNullWidth(1.0f);
		figure.setNotNullWidth((parentLifeline.getParentLifeline() == null)
				? 5.0f : 3.0f);
		LOG.log(Level.FINE, "Figure created");
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		LOG.log(Level.FINE, "Refreshing visuals...");

		// create Layout constraint
		TreeNode<Void, OCLValue> node = getOCLValueTransitionTreeNode();
		Map<?, ?> ePRegistry = getRoot().getViewer().getEditPartRegistry();

		TreeLayoutMarker marker = new TreeLayoutMarker();
		marker.setLeaf(node.getChildTransitions().isEmpty());
		if (node.getParentTransition() != null) {
			TreeTransition<Void, OCLValue> transition = node.getParentTransition();
			TreeNode<Void, OCLValue> prevNode = transition.getPreviousNode();
			if (ePRegistry.containsKey(prevNode)) {
				GraphicalEditPart gEP = (GraphicalEditPart) ePRegistry.get(prevNode);
				marker.setPrevFigure(gEP.getFigure());
			}
		}

		((GraphicalEditPart) this.getParent()).setLayoutConstraint(	this,
																	getFigure(),
																	marker);

		// update figure

		ObjectTransitionNodeFigure figure = (ObjectTransitionNodeFigure) getFigure();

		// update figure colors
		updateFigureColor(figure, node);

		// update figure state
		List<TreeTransition<Void, OCLValue>> transitions = node.getChildTransitions();
		Object prevValue = null;
		Object nextValue = null;
		ObjectTransitionNodeFigure.FigureState stateToSet = ObjectTransitionNodeFigure.FigureState.NOTNULL;
		if (!transitions.isEmpty()) {
			// all values of outgoing transitions must have be the same -> retrieve it from the first
			nextValue = transitions.get(0).getTransitionValue().getValue();
		} else {
			figure.setConnectedToRightSide(false);
		}
		if (node.getParentTransition() != null) {
			prevValue = node.getParentTransition().getTransitionValue()
							.getValue();
		} else {
			figure.setConnectedToLeftSide(false);
		}
		LOG.log(Level.FINEST, "prevValue: {0}", prevValue);
		LOG.log(Level.FINEST, "nextValue: {0}", nextValue);

		if (nextValue != null && prevValue != null) {
			// Node with incoming and outgoing transitions, since we have object instances, determine the state based on equality

			if (prevValue.equals(nextValue)) {
				LOG.log(Level.FINE, "objects values are equal");
				stateToSet = ObjectTransitionNodeFigure.FigureState.NOTNULL;
				figure.setConnectedToLeftSide(true);
				figure.setConnectedToRightSide(true);
			} else {
				LOG.log(Level.FINE, "objects values are not equal");
				stateToSet = ObjectTransitionNodeFigure.FigureState.CHANGED;
				figure.setConnectedToLeftSide(false);
				figure.setConnectedToRightSide(true);
			}

		}

		figure.setState(stateToSet);

		super.refreshVisuals();
		LOG.log(Level.FINE, "Visuals refreshed");
	}

	/**
	 * updates the figure colors based on the given node with the current edit
	 * parts colors
	 * 
	 * @param figure
	 * @param node
	 */
	private void updateFigureColor(ObjectTransitionNodeFigure figure,
			TreeNode<Void, OCLValue> node) {
		LOG.fine("Updating figure colors...");
		OCLValue prevOCLValue = null;
		OCLValue nextOCLValue = null;
		if (node.getParentTransition() != null) {
			prevOCLValue = node.getParentTransition().getTransitionValue();
		}
		if (!node.getChildTransitions().isEmpty()) {
			nextOCLValue = node.getChildTransitions().get(0)
								.getTransitionValue();
		}
		Color figureColor = unknownColor;
		Color selectionColor = unknownSelectedColor;
		if (nextOCLValue != null && nextOCLValue.getValue() instanceof Boolean) {
			// no previous ocl value -> choose the inverse value color
			Boolean value = (Boolean) nextOCLValue.getValue();
			if (value.booleanValue()) {
				figureColor = trueColor;
				selectionColor = trueSelectedColor;
			} else {
				figureColor = falseColor;
				selectionColor = falseSelectedColor;
			}
		} else if (prevOCLValue != null) {
			if (prevOCLValue.getValue() instanceof Boolean) {
				Boolean value = (Boolean) prevOCLValue.getValue();
				if (value.booleanValue()) {
					figureColor = trueColor;
					selectionColor = trueSelectedColor;
				} else {
					figureColor = falseColor;
					selectionColor = falseSelectedColor;
				}
			}
		}

		figure.setBackgroundColor(figureColor);
		figure.setForegroundColor(figureColor);
		if (selectionPolicy != null) {
			selectionPolicy.setDefaultForegroundColor(figureColor);
			selectionPolicy.setDefaultBackgroundColor(figureColor);
			selectionPolicy.setSelectedForegroundColor(selectionColor);
			selectionPolicy.setSelectedBackgroundColor(selectionColor);
		}
		LOG.fine("Finished updating figure colors");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, selectionPolicy);
	}

	@Override
	protected List<?> getModelSourceConnections() {
		@SuppressWarnings("unchecked")
		TreeNode<StateSpaceTransition, ObjectLifelineState> model = (TreeNode<StateSpaceTransition, ObjectLifelineState>) getModel();
		List<Object> sourceConnections = new ArrayList<>();
		sourceConnections.addAll(model.getChildTransitions());
		return sourceConnections;
	}

	@Override
	protected List<?> getModelTargetConnections() {
		@SuppressWarnings("unchecked")
		TreeNode<StateSpaceTransition, ObjectLifelineState> model = (TreeNode<StateSpaceTransition, ObjectLifelineState>) getModel();
		List<Object> targetConnections = new ArrayList<>();
		if (model.getParentTransition() != null) {
			targetConnections.add(model.getParentTransition());
		}
		return targetConnections;
	}

	@Override
	public void deactivate() {
		super.deactivate();
		disposeColors();
		SSPVisOCLPlugin.getInstance().getPreferenceStore()
						.removePropertyChangeListener(preferenceListener);
	}

	private void disposeColors() {
		LOG.log(Level.FINE, "Disposing old colors...");
		for (Color color : new Color[] { trueColor, falseColor, unknownColor,
				trueSelectedColor, falseSelectedColor, unknownSelectedColor }) {
			if (color != null) {
				color.dispose();
			}
		}
		LOG.log(Level.FINE, "colors disposed...");
	}

	/**
	 * updates the colors used for the figures based on the current preferences
	 */
	private void updateColorsFromPreferences() {
		LOG.log(Level.FINE, "Updating color from preferences...");

		// dispose previous colors
		disposeColors();

		// unknown color
		String prefColor = SSPVisOCLPlugin.getInstance()
											.getPreferenceStore()
											.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_UNKNOWN);
		LOG.log(Level.FINER, "Unknown color: {0}", prefColor);
		unknownColor = new Color(	Display.getDefault(),
									StringConverter.asRGB(prefColor));
		// true color
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_TRUE);
		LOG.log(Level.FINER, "True color: {0}", prefColor);
		trueColor = new Color(	Display.getDefault(),
								StringConverter.asRGB(prefColor));
		// false color
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_FALSE);
		LOG.log(Level.FINER, "False color: {0}", prefColor);
		falseColor = new Color(	Display.getDefault(),
								StringConverter.asRGB(prefColor));

		// selected color unkown
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_UNKNOWN);
		LOG.log(Level.FINER, "Unknown selected color: {0}", prefColor);
		unknownSelectedColor = new Color(	Display.getDefault(),
											StringConverter.asRGB(prefColor));

		// selected color true
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_TRUE);
		LOG.log(Level.FINER, "True selected color: {0}", prefColor);
		trueSelectedColor = new Color(	Display.getDefault(),
										StringConverter.asRGB(prefColor));

		// selected color false
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_FALSE);
		LOG.log(Level.FINER, "False selected color: {0}", prefColor);
		falseSelectedColor = new Color(	Display.getDefault(),
										StringConverter.asRGB(prefColor));

		LOG.log(Level.FINE, "Colors updated");
	}

	@SuppressWarnings("unchecked")
	public TreeNode<Void, OCLValue> getOCLValueTransitionTreeNode() {
		return (TreeNode<Void, OCLValue>) getModel();
	}

}
