/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.ocl.gef.edit.parts;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.figures.BezierConnectionRouter;
import at.bitandart.zoubek.statespace.vis.figures.SideAnchor;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.BaseConnectionEditPart;
import at.bitandart.zoubek.statespace.vis.gef.edit.policies.ColoredSelectionFeedbackPolicy;
import at.bitandart.zoubek.statespace.vis.ocl.SSPVisOCLPlugin;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OCLValueEP extends BaseConnectionEditPart {

	private static final Logger				LOG						= Logger.getLogger(OCLValueEP.class.getCanonicalName());

	/**
	 * the lifeline that contains this object state
	 */
	private OCLLifeline						parentLifeline			= null;

	/**
	 * figure color for "true" values
	 */
	private Color							trueColor				= null;

	/**
	 * figure color for "false" values
	 */
	private Color							falseColor				= null;

	/**
	 * figure color for unknown values
	 */
	private Color							unknownColor			= null;

	/**
	 * figure color for selected "true" values
	 */
	private Color							trueSelectedColor		= null;

	/**
	 * figure color for selected "false" values
	 */
	private Color							falseSelectedColor		= null;

	/**
	 * figure color for selected unknown values
	 */
	private Color							unknownSelectedColor	= null;

	/**
	 * the preference listener
	 */
	private IPropertyChangeListener			preferenceListener		= null;

	private ColoredSelectionFeedbackPolicy	selectionPolicy;

	public OCLValueEP(OCLLifeline parentLifeline) {
		this.parentLifeline = parentLifeline;

		// add preference listener
		preferenceListener = new IPropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {
				LOG.log(Level.FINE, "Processing property update...");
				if (event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_TRUE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_FALSE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_UNKNOWN)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_TRUE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_FALSE)
					|| event.getProperty()
							.equals(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_UNKNOWN)) {
					updateColorsFromPreferences();
					refresh();
				} else {
					LOG.log(Level.FINE, "Nothing to update");
				}
				LOG.log(Level.FINE, "Property update processed");
			}
		};
		SSPVisOCLPlugin.getInstance().getPreferenceStore()
						.addPropertyChangeListener(preferenceListener);
		selectionPolicy = new ColoredSelectionFeedbackPolicy(	ColorConstants.black,
																ColorConstants.black,
																ColorConstants.gray,
																ColorConstants.gray); // default values, will be set to the correct values by updateColorsFormPreferences()
		// load colors
		updateColorsFromPreferences();
	}

	@Override
	protected IFigure createFigure() {
		LOG.log(Level.FINE, "Creating figure...");
		PolylineConnection figure = new PolylineConnection();
		figure.setBackgroundColor(unknownColor);
		figure.setForegroundColor(unknownColor);
		BezierConnectionRouter router = new BezierConnectionRouter();
		figure.setConnectionRouter(router);
		LOG.log(Level.FINE, "Figure created");
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		LOG.log(Level.FINE, "Refreshing visuals...");
		TreeTransition<Void, OCLValue> oclValueTreeTransition = getOCLValueTreeTransition();
		PolylineConnection figure = (PolylineConnection) getConnectionFigure();

		OCLValue oclValue = oclValueTreeTransition.getTransitionValue();
		// update figure colors
		Color figureColor = unknownColor;
		Color selectionColor = unknownSelectedColor;
		if (oclValue != null && oclValue.getValue() instanceof Boolean) {
			Boolean value = (Boolean) oclValue.getValue();
			if (value.booleanValue()) {
				figureColor = trueColor;
				selectionColor = trueSelectedColor;
			} else {
				figureColor = falseColor;
				selectionColor = falseSelectedColor;
			}
		}

		figure.setBackgroundColor(figureColor);
		figure.setForegroundColor(figureColor);
		if (selectionPolicy != null) {
			selectionPolicy.setDefaultForegroundColor(figureColor);
			selectionPolicy.setDefaultBackgroundColor(figureColor);
			selectionPolicy.setSelectedForegroundColor(selectionColor);
			selectionPolicy.setSelectedBackgroundColor(selectionColor);
		}

		figure.setLineWidthFloat((parentLifeline.getParentLifeline() == null)
				? 5.0f : 3.0f);

		super.refreshVisuals();
		LOG.log(Level.FINE, "Visuals refreshed");
	}

	@Override
	protected ConnectionAnchor getSourceConnectionAnchor() {
		if (getSource() != null) {
			return new SideAnchor(	((GraphicalEditPart) getSource()).getFigure(),
									SideAnchor.LEFT | SideAnchor.RIGHT);
		}
		return super.getTargetConnectionAnchor();
	}

	@Override
	protected ConnectionAnchor getTargetConnectionAnchor() {
		if (getTarget() != null) {
			return new SideAnchor(	((GraphicalEditPart) getTarget()).getFigure(),
									SideAnchor.LEFT | SideAnchor.RIGHT);
		}
		return super.getTargetConnectionAnchor();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, selectionPolicy);
	}

	@Override
	public void deactivate() {
		super.deactivate();
		disposeColors();
		SSPVisOCLPlugin.getInstance().getPreferenceStore()
						.removePropertyChangeListener(preferenceListener);
	}

	private void disposeColors() {
		LOG.log(Level.FINE, "Disposing old colors...");
		for (Color color : new Color[] { trueColor, falseColor, unknownColor,
				trueSelectedColor, falseSelectedColor, unknownSelectedColor }) {
			if (color != null) {
				color.dispose();
			}
		}
		LOG.log(Level.FINE, "colors disposed...");
	}

	/**
	 * updates the colors used for the figures based on the current preferences
	 */
	private void updateColorsFromPreferences() {
		LOG.log(Level.FINE, "Updating color from preferences...");

		// dispose previous colors
		disposeColors();

		// unknown color
		String prefColor = SSPVisOCLPlugin.getInstance()
											.getPreferenceStore()
											.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_UNKNOWN);
		LOG.log(Level.FINER, "Unknown color: {0}", prefColor);
		unknownColor = new Color(	Display.getDefault(),
									StringConverter.asRGB(prefColor));
		// true color
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_TRUE);
		LOG.log(Level.FINER, "True color: {0}", prefColor);
		trueColor = new Color(	Display.getDefault(),
								StringConverter.asRGB(prefColor));
		// false color
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_FALSE);
		LOG.log(Level.FINER, "False color: {0}", prefColor);
		falseColor = new Color(	Display.getDefault(),
								StringConverter.asRGB(prefColor));

		// selected color unkown
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_UNKNOWN);
		LOG.log(Level.FINER, "Unknown selected color: {0}", prefColor);
		unknownSelectedColor = new Color(	Display.getDefault(),
											StringConverter.asRGB(prefColor));

		// selected color true
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_TRUE);
		LOG.log(Level.FINER, "True selected color: {0}", prefColor);
		trueSelectedColor = new Color(	Display.getDefault(),
										StringConverter.asRGB(prefColor));

		// selected color false
		prefColor = SSPVisOCLPlugin.getInstance()
									.getPreferenceStore()
									.getString(SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_FALSE);
		LOG.log(Level.FINER, "False selected color: {0}", prefColor);
		falseSelectedColor = new Color(	Display.getDefault(),
										StringConverter.asRGB(prefColor));

		LOG.log(Level.FINE, "Colors updated");
	}

	@SuppressWarnings("unchecked")
	public TreeTransition<Void, OCLValue> getOCLValueTreeTransition() {
		return (TreeTransition<Void, OCLValue>) getModel();
	}
}
