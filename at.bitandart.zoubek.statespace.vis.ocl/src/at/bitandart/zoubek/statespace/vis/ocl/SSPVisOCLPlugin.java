package at.bitandart.zoubek.statespace.vis.ocl;

/**
 * 
 */

import org.eclipse.jface.resource.StringConverter;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisOCLPlugin extends AbstractUIPlugin {

	/**
	 * the plugin id
	 */
	public static final String		PLUGIN_ID									= "at.bitandart.zoubek.statespace.vis.ocl";

	/**
	 * plugin preference node path (defaults to the plugin id)
	 */
	public static final String		PREF_NODE_PLUGIN							= PLUGIN_ID;

	// preference keys

	/**
	 * color for ocl lifelines representing a unknown value (as String, use
	 * {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String		PREF_OCL_LIFELINE_COLOR_UNKNOWN				= "oclLifelineColorUnknown";
	/**
	 * color for ocl lifelines representing a true value (as String, use
	 * {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String		PREF_OCL_LIFELINE_COLOR_TRUE				= "oclLifelineColorTrue";
	/**
	 * color for ocl lifelines representing a false value (as String, use
	 * {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String		PREF_OCL_LIFELINE_COLOR_FALSE				= "oclLifelineColorFalse";
	/**
	 * color for selected ocl lifelines representing a unknown value (as String,
	 * use {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String		PREF_OCL_LIFELINE_SELECTION_COLOR_UNKNOWN	= "oclLifelineSelectionColorUnknown";
	/**
	 * color for selected ocl lifelines representing a true value (as String,
	 * use {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String		PREF_OCL_LIFELINE_SELECTION_COLOR_TRUE		= "oclLifelineSelectionColorTrue";
	/**
	 * color for selected ocl lifelines representing a false value (as String,
	 * use {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String		PREF_OCL_LIFELINE_SELECTION_COLOR_FALSE		= "oclLifelineSelectionColorFalse";

	/**
	 * singleton instance
	 */
	private static SSPVisOCLPlugin	instance;

	/**
	 * @return the default plugin instance
	 */
	public static SSPVisOCLPlugin getInstance() {
		return instance;
	}

	public SSPVisOCLPlugin() {
		super();
		instance = this;
	}

}
