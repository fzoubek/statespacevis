package at.bitandart.zoubek.statespace.vis.ocl;

/**
 * 
 */

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class DefaultPreferencesInitializer extends AbstractPreferenceInitializer {

	/**
	 * creates a new preference initializer that sets the default values for the
	 * SSPVis OCL plugin
	 */
	public DefaultPreferencesInitializer() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences pluginNode = DefaultScope.INSTANCE.getNode(SSPVisOCLPlugin.PREF_NODE_PLUGIN);

		// ocl lifeline color true
		pluginNode.put(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_TRUE, "145,191,219");
		// ocl lifeline color selected & true 
		pluginNode.put(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_TRUE,
						"169,222,255");

		// ocl lifeline color false
		pluginNode.put(SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_FALSE, "252,141,89");
		// ocl lifeline color selected & false 
		pluginNode.put(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_FALSE,
						"252,189,159");

		// ocl lifeline color unknown
		pluginNode.put(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_COLOR_UNKNOWN,
						"180,180,180");
		// ocl lifeline color selected & unknown 
		pluginNode.put(	SSPVisOCLPlugin.PREF_OCL_LIFELINE_SELECTION_COLOR_UNKNOWN,
						"225,225,225");
	}

}
