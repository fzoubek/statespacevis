/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLFactory
 * @model kind="package"
 * @generated
 */
public interface SSPVisOCLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String				eNAME														= "model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String				eNS_URI														= "http://zoubek.bitandart.at/sspvisocl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String				eNS_PREFIX													= "sspvisocl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SSPVisOCLPackage	eINSTANCE													= at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl.init();

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineBuilderImpl <em>OCL Lifeline Builder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineBuilderImpl
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLLifelineBuilder()
	 * @generated
	 */
	int					OCL_LIFELINE_BUILDER										= 0;

	/**
	 * The number of structural features of the '<em>OCL Lifeline Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE_BUILDER_FEATURE_COUNT							= StatespacevisPackage.LIFELINE_BUILDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Build Lifelines</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY	= StatespacevisPackage.LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY;

	/**
	 * The number of operations of the '<em>OCL Lifeline Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE_BUILDER_OPERATION_COUNT						= StatespacevisPackage.LIFELINE_BUILDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineImpl <em>OCL Lifeline</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineImpl
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLLifeline()
	 * @generated
	 */
	int					OCL_LIFELINE												= 1;

	/**
	 * The feature id for the '<em><b>Parent Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE__PARENT_LIFELINE								= StatespacevisPackage.LIFELINE__PARENT_LIFELINE;

	/**
	 * The feature id for the '<em><b>Child Lifelines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE__CHILD_LIFELINES								= StatespacevisPackage.LIFELINE__CHILD_LIFELINES;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE__VISIBLE										= StatespacevisPackage.LIFELINE__VISIBLE;

	/**
	 * The feature id for the '<em><b>Collapsed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE__COLLAPSED										= StatespacevisPackage.LIFELINE__COLLAPSED;

	/**
	 * The feature id for the '<em><b>Ocl Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE__OCL_VALUES									= StatespacevisPackage.LIFELINE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE__EXPRESSION									= StatespacevisPackage.LIFELINE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>OCL Lifeline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE_FEATURE_COUNT									= StatespacevisPackage.LIFELINE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>OCL Lifeline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE_OPERATION_COUNT								= StatespacevisPackage.LIFELINE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLValueImpl <em>OCL Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLValueImpl
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLValue()
	 * @generated
	 */
	int					OCL_VALUE													= 2;

	/**
	 * The feature id for the '<em><b>Clazz</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_VALUE__CLAZZ											= 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_VALUE__VALUE											= 1;

	/**
	 * The feature id for the '<em><b>Known</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_VALUE__KNOWN											= 2;

	/**
	 * The number of structural features of the '<em>OCL Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_VALUE_FEATURE_COUNT										= 3;

	/**
	 * The number of operations of the '<em>OCL Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_VALUE_OPERATION_COUNT									= 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineTransitionImpl <em>OCL Lifeline Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineTransitionImpl
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLLifelineTransition()
	 * @generated
	 */
	int					OCL_LIFELINE_TRANSITION										= 3;

	/**
	 * The number of structural features of the '<em>OCL Lifeline Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE_TRANSITION_FEATURE_COUNT						= 0;

	/**
	 * The number of operations of the '<em>OCL Lifeline Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int					OCL_LIFELINE_TRANSITION_OPERATION_COUNT						= 0;

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineBuilder <em>OCL Lifeline Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OCL Lifeline Builder</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineBuilder
	 * @generated
	 */
	EClass getOCLLifelineBuilder();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline <em>OCL Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OCL Lifeline</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline
	 * @generated
	 */
	EClass getOCLLifeline();

	/**
	 * Returns the meta object for the reference '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getOclValues <em>Ocl Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ocl Values</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getOclValues()
	 * @see #getOCLLifeline()
	 * @generated
	 */
	EReference getOCLLifeline_OclValues();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getExpression()
	 * @see #getOCLLifeline()
	 * @generated
	 */
	EAttribute getOCLLifeline_Expression();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue <em>OCL Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OCL Value</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue
	 * @generated
	 */
	EClass getOCLValue();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getClazz <em>Clazz</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clazz</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getClazz()
	 * @see #getOCLValue()
	 * @generated
	 */
	EAttribute getOCLValue_Clazz();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getValue()
	 * @see #getOCLValue()
	 * @generated
	 */
	EAttribute getOCLValue_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#isKnown <em>Known</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Known</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#isKnown()
	 * @see #getOCLValue()
	 * @generated
	 */
	EAttribute getOCLValue_Known();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineTransition <em>OCL Lifeline Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OCL Lifeline Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineTransition
	 * @generated
	 */
	EClass getOCLLifelineTransition();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SSPVisOCLFactory getSSPVisOCLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineBuilderImpl <em>OCL Lifeline Builder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineBuilderImpl
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLLifelineBuilder()
		 * @generated
		 */
		EClass		OCL_LIFELINE_BUILDER		= eINSTANCE.getOCLLifelineBuilder();
		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineImpl <em>OCL Lifeline</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineImpl
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLLifeline()
		 * @generated
		 */
		EClass		OCL_LIFELINE				= eINSTANCE.getOCLLifeline();
		/**
		 * The meta object literal for the '<em><b>Ocl Values</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference	OCL_LIFELINE__OCL_VALUES	= eINSTANCE.getOCLLifeline_OclValues();
		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute	OCL_LIFELINE__EXPRESSION	= eINSTANCE.getOCLLifeline_Expression();
		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLValueImpl <em>OCL Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLValueImpl
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLValue()
		 * @generated
		 */
		EClass		OCL_VALUE					= eINSTANCE.getOCLValue();
		/**
		 * The meta object literal for the '<em><b>Clazz</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute	OCL_VALUE__CLAZZ			= eINSTANCE.getOCLValue_Clazz();
		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute	OCL_VALUE__VALUE			= eINSTANCE.getOCLValue_Value();
		/**
		 * The meta object literal for the '<em><b>Known</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute	OCL_VALUE__KNOWN			= eINSTANCE.getOCLValue_Known();
		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineTransitionImpl <em>OCL Lifeline Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineTransitionImpl
		 * @see at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLPackageImpl#getOCLLifelineTransition()
		 * @generated
		 */
		EClass		OCL_LIFELINE_TRANSITION		= eINSTANCE.getOCLLifelineTransition();

	}

} //SSPVisOCLPackage
