/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.ocl.handlers;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressService;

import at.bitandart.zoubek.statespace.vis.ocl.dialogs.EvaluateNewExpressionDialog;

/**
 * Handler that opens a dialog to collect all necessary data need to evaluate a
 * new ocl expresion, evaluates it and views the result in a state space path
 * visualization
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class EvaluateExpressionHandler extends AbstractHandler {

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IProgressService progressService = window.getWorkbench()
													.getProgressService();

		EvaluateNewExpressionDialog dialog = new EvaluateNewExpressionDialog(window.getShell());

		if (dialog.open() == Window.OK) {

			String oclExpression = dialog.getOCLExpression();
			String henshinFilename = dialog.getHenshinFilename();
			String ecoreFilename = dialog.getEcoreFilename();
			String initFilename = dialog.getInitFilename();
			File modelDir = dialog.getBaseModelDirectory();
			try {
				progressService.busyCursorWhile(new EvaluateNewExpressionRunnable(	window,
																					oclExpression,
																					modelDir,
																					ecoreFilename,
																					henshinFilename,
																					initFilename));
			} catch (InvocationTargetException | InterruptedException e) {
				MessageDialog.openError(window.getShell(),
										"Couldn't open editor",
										"Couldn't open the state space path visualization editor, see message log for further details");
			}
		}

		return null;
	}

}
