/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model.impl;

import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineBuilder;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineTransition;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue;
import at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLFactory;
import at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SSPVisOCLPackageImpl extends EPackageImpl implements SSPVisOCLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass	oclLifelineBuilderEClass	= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass	oclLifelineEClass			= null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass	oclValueEClass				= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass	oclLifelineTransitionEClass	= null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SSPVisOCLPackageImpl() {
		super(eNS_URI, SSPVisOCLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean	isInited	= false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SSPVisOCLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SSPVisOCLPackage init() {
		if (isInited)
			return (SSPVisOCLPackage) EPackage.Registry.INSTANCE.getEPackage(SSPVisOCLPackage.eNS_URI);

		// Obtain or create and register package
		SSPVisOCLPackageImpl theSSPVisOCLPackage = (SSPVisOCLPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SSPVisOCLPackageImpl
				? EPackage.Registry.INSTANCE.get(eNS_URI)
				: new SSPVisOCLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StatespacevisPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSSPVisOCLPackage.createPackageContents();

		// Initialize created meta-data
		theSSPVisOCLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSSPVisOCLPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(	SSPVisOCLPackage.eNS_URI,
										theSSPVisOCLPackage);
		return theSSPVisOCLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOCLLifelineBuilder() {
		return oclLifelineBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOCLLifeline() {
		return oclLifelineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOCLLifeline_OclValues() {
		return (EReference) oclLifelineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOCLLifeline_Expression() {
		return (EAttribute) oclLifelineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOCLValue() {
		return oclValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOCLValue_Clazz() {
		return (EAttribute) oclValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOCLValue_Value() {
		return (EAttribute) oclValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOCLValue_Known() {
		return (EAttribute) oclValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOCLLifelineTransition() {
		return oclLifelineTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSPVisOCLFactory getSSPVisOCLFactory() {
		return (SSPVisOCLFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean	isCreated	= false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		oclLifelineBuilderEClass = createEClass(OCL_LIFELINE_BUILDER);

		oclLifelineEClass = createEClass(OCL_LIFELINE);
		createEReference(oclLifelineEClass, OCL_LIFELINE__OCL_VALUES);
		createEAttribute(oclLifelineEClass, OCL_LIFELINE__EXPRESSION);

		oclValueEClass = createEClass(OCL_VALUE);
		createEAttribute(oclValueEClass, OCL_VALUE__CLAZZ);
		createEAttribute(oclValueEClass, OCL_VALUE__VALUE);
		createEAttribute(oclValueEClass, OCL_VALUE__KNOWN);

		oclLifelineTransitionEClass = createEClass(OCL_LIFELINE_TRANSITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean	isInitialized	= false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StatespacevisPackage theStatespacevisPackage = (StatespacevisPackage) EPackage.Registry.INSTANCE.getEPackage(StatespacevisPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		oclLifelineBuilderEClass.getESuperTypes()
								.add(theStatespacevisPackage.getLifelineBuilder());
		oclLifelineEClass.getESuperTypes()
							.add(theStatespacevisPackage.getLifeline());

		// Initialize classes, features, and operations; add parameters
		initEClass(	oclLifelineBuilderEClass,
					OCLLifelineBuilder.class,
					"OCLLifelineBuilder",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		initEClass(	oclLifelineEClass,
					OCLLifeline.class,
					"OCLLifeline",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(theStatespacevisPackage.getTree());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getOCLValue());
		g1.getETypeArguments().add(g2);
		initEReference(	getOCLLifeline_OclValues(),
						g1,
						null,
						"oclValues",
						null,
						1,
						1,
						OCLLifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getOCLLifeline_Expression(),
						ecorePackage.getEString(),
						"expression",
						null,
						0,
						1,
						OCLLifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	oclValueEClass,
					OCLValue.class,
					"OCLValue",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(	getOCLValue_Clazz(),
						g1,
						"clazz",
						null,
						0,
						1,
						OCLValue.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getOCLValue_Value(),
						ecorePackage.getEJavaObject(),
						"value",
						null,
						0,
						1,
						OCLValue.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getOCLValue_Known(),
						ecorePackage.getEBoolean(),
						"known",
						null,
						0,
						1,
						OCLValue.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	oclLifelineTransitionEClass,
					OCLLifelineTransition.class,
					"OCLLifelineTransition",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //SSPVisOCLPackageImpl
