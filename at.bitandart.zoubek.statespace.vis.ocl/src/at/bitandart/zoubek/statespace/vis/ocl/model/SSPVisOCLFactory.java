/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage
 * @generated
 */
public interface SSPVisOCLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SSPVisOCLFactory	eINSTANCE	= at.bitandart.zoubek.statespace.vis.ocl.model.impl.SSPVisOCLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>OCL Lifeline Builder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OCL Lifeline Builder</em>'.
	 * @generated
	 */
	OCLLifelineBuilder createOCLLifelineBuilder();

	/**
	 * Returns a new object of class '<em>OCL Lifeline</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OCL Lifeline</em>'.
	 * @generated
	 */
	OCLLifeline createOCLLifeline();

	/**
	 * Returns a new object of class '<em>OCL Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OCL Value</em>'.
	 * @generated
	 */
	OCLValue createOCLValue();

	/**
	 * Returns a new object of class '<em>OCL Lifeline Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OCL Lifeline Transition</em>'.
	 * @generated
	 */
	OCLLifelineTransition createOCLLifelineTransition();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SSPVisOCLPackage getSSPVisOCLPackage();

} //SSPVisOCLFactory
