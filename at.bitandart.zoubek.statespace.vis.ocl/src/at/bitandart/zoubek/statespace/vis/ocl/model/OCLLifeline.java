/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model;



import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>OCL Lifeline</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getOclValues <em>Ocl Values</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLLifeline()
 * @model
 * @generated
 */
public interface OCLLifeline extends Lifeline {
	/**
	 * Returns the value of the '<em><b>Ocl Values</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ocl Values</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Ocl Values</em>' reference.
	 * @see #setOclValues(Tree)
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLLifeline_OclValues()
	 * @model required="true"
	 * @generated
	 */
	Tree<?, OCLValue> getOclValues();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getOclValues <em>Ocl Values</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Ocl Values</em>' reference.
	 * @see #getOclValues()
	 * @generated
	 */
	void setOclValues(Tree<?, OCLValue> value);

	/**
	 * Returns the value of the '<em><b>Expression</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Expression</em>' attribute.
	 * @see #setExpression(String)
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLLifeline_Expression()
	 * @model
	 * @generated
	 */
	String getExpression();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline#getExpression <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' attribute.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(String value);

} // OCLLifeline
