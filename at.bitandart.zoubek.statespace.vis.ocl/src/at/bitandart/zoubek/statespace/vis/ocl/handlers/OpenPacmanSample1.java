package at.bitandart.zoubek.statespace.vis.ocl.handlers;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressService;

/**
 * opens the SSPVis editor with the ocl pacman sample 1.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OpenPacmanSample1 extends AbstractHandler {

	@SuppressWarnings("unused")
	private static final Logger	LOG	= Logger.getLogger(OpenPacmanSample1.class.getCanonicalName());

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IProgressService progressService = window.getWorkbench()
													.getProgressService();
		try {
			progressService.busyCursorWhile(new EvaluateNewExpressionRunnable(	window,
																	"always globally not(pacman.on.treasure)",
																	"model"
																			+ File.separator
																			+ "pacman",
																	"pacman.ecore",
																	"pacman.henshin",
																	"Game.xmi"));
		} catch (InvocationTargetException | InterruptedException e) {
			MessageDialog.openError(window.getShell(),
									"Couldn't open editor",
									"Couldn't open the state space visualization editor, see message log for further details");
			e.printStackTrace();
		}
		return null;
	}
}
