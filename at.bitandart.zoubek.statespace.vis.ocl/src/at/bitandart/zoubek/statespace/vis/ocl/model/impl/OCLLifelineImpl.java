/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model.impl;

import at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline;
import at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue;
import at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OCL Lifeline</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineImpl#getOclValues <em>Ocl Values</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLLifelineImpl#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OCLLifelineImpl extends LifelineImpl implements OCLLifeline {
	/**
	 * The cached value of the '{@link #getOclValues() <em>Ocl Values</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclValues()
	 * @generated
	 * @ordered
	 */
	protected Tree<?, OCLValue>		oclValues;

	/**
	 * The default value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String	EXPRESSION_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected String				expression			= EXPRESSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OCLLifelineImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SSPVisOCLPackage.Literals.OCL_LIFELINE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public Tree<?, OCLValue> getOclValues() {
		if (oclValues != null && oclValues.eIsProxy()) {
			InternalEObject oldOclValues = (InternalEObject) oclValues;
			oclValues = (Tree<?, OCLValue>) eResolveProxy(oldOclValues);
			if (oclValues != oldOclValues) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													SSPVisOCLPackage.OCL_LIFELINE__OCL_VALUES,
													oldOclValues,
													oclValues));
			}
		}
		return oclValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tree<?, OCLValue> basicGetOclValues() {
		return oclValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOclValues(Tree<?, OCLValue> newOclValues) {
		Tree<?, OCLValue> oldOclValues = oclValues;
		oclValues = newOclValues;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											SSPVisOCLPackage.OCL_LIFELINE__OCL_VALUES,
											oldOclValues,
											oclValues));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(String newExpression) {
		String oldExpression = expression;
		expression = newExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											SSPVisOCLPackage.OCL_LIFELINE__EXPRESSION,
											oldExpression,
											expression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_LIFELINE__OCL_VALUES:
				if (resolve)
					return getOclValues();
				return basicGetOclValues();
			case SSPVisOCLPackage.OCL_LIFELINE__EXPRESSION:
				return getExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_LIFELINE__OCL_VALUES:
				setOclValues((Tree<?, OCLValue>) newValue);
				return;
			case SSPVisOCLPackage.OCL_LIFELINE__EXPRESSION:
				setExpression((String) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_LIFELINE__OCL_VALUES:
				setOclValues((Tree<?, OCLValue>) null);
				return;
			case SSPVisOCLPackage.OCL_LIFELINE__EXPRESSION:
				setExpression(EXPRESSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_LIFELINE__OCL_VALUES:
				return oclValues != null;
			case SSPVisOCLPackage.OCL_LIFELINE__EXPRESSION:
				return EXPRESSION_EDEFAULT == null
						? expression != null
						: !EXPRESSION_EDEFAULT.equals(expression);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (expression: ");
		result.append(expression);
		result.append(')');
		return result.toString();
	}

} //OCLLifelineImpl
