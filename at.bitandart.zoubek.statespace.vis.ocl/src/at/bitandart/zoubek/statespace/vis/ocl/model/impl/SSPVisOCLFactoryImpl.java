/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model.impl;

import at.bitandart.zoubek.statespace.vis.ocl.model.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SSPVisOCLFactoryImpl extends EFactoryImpl implements SSPVisOCLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SSPVisOCLFactory init() {
		try {
			SSPVisOCLFactory theSSPVisOCLFactory = (SSPVisOCLFactory) EPackage.Registry.INSTANCE.getEFactory(SSPVisOCLPackage.eNS_URI);
			if (theSSPVisOCLFactory != null) {
				return theSSPVisOCLFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SSPVisOCLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSPVisOCLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SSPVisOCLPackage.OCL_LIFELINE_BUILDER:
				return createOCLLifelineBuilder();
			case SSPVisOCLPackage.OCL_LIFELINE:
				return createOCLLifeline();
			case SSPVisOCLPackage.OCL_VALUE:
				return createOCLValue();
			case SSPVisOCLPackage.OCL_LIFELINE_TRANSITION:
				return createOCLLifelineTransition();
			default:
				throw new IllegalArgumentException("The class '"
													+ eClass.getName()
													+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLLifelineBuilder createOCLLifelineBuilder() {
		OCLLifelineBuilderImpl oclLifelineBuilder = new OCLLifelineBuilderImpl();
		return oclLifelineBuilder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLLifeline createOCLLifeline() {
		OCLLifelineImpl oclLifeline = new OCLLifelineImpl();
		return oclLifeline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLValue createOCLValue() {
		OCLValueImpl oclValue = new OCLValueImpl();
		return oclValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLLifelineTransition createOCLLifelineTransition() {
		OCLLifelineTransitionImpl oclLifelineTransition = new OCLLifelineTransitionImpl();
		return oclLifelineTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSPVisOCLPackage getSSPVisOCLPackage() {
		return (SSPVisOCLPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SSPVisOCLPackage getPackage() {
		return SSPVisOCLPackage.eINSTANCE;
	}

} //SSPVisOCLFactoryImpl
