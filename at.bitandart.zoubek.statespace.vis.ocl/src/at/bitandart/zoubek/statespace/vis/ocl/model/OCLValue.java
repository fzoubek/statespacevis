/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OCL Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getClazz <em>Clazz</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getValue <em>Value</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#isKnown <em>Known</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLValue()
 * @model
 * @generated
 */
public interface OCLValue extends EObject {
	/**
	 * Returns the value of the '<em><b>Clazz</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clazz</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clazz</em>' attribute.
	 * @see #setClazz(Class)
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLValue_Clazz()
	 * @model
	 * @generated
	 */
	Class<?> getClazz();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getClazz <em>Clazz</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clazz</em>' attribute.
	 * @see #getClazz()
	 * @generated
	 */
	void setClazz(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Object)
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLValue_Value()
	 * @model
	 * @generated
	 */
	Object getValue();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Object value);

	/**
	 * Returns the value of the '<em><b>Known</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Known</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Known</em>' attribute.
	 * @see #setKnown(boolean)
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLValue_Known()
	 * @model
	 * @generated
	 */
	boolean isKnown();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue#isKnown <em>Known</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Known</em>' attribute.
	 * @see #isKnown()
	 * @generated
	 */
	void setKnown(boolean value);

} // OCLValue
