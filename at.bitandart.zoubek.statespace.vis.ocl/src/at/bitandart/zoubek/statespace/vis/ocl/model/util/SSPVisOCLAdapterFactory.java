/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model.util;

import at.bitandart.zoubek.statespace.vis.ocl.model.*;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage
 * @generated
 */
public class SSPVisOCLAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SSPVisOCLPackage	modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSPVisOCLAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SSPVisOCLPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SSPVisOCLSwitch<Adapter>	modelSwitch	= new SSPVisOCLSwitch<Adapter>() {
														@Override
														public Adapter caseOCLLifelineBuilder(
																OCLLifelineBuilder object) {
															return createOCLLifelineBuilderAdapter();
														}

														@Override
														public Adapter caseOCLLifeline(
																OCLLifeline object) {
															return createOCLLifelineAdapter();
														}

														@Override
														public Adapter caseOCLValue(
																OCLValue object) {
															return createOCLValueAdapter();
														}

														@Override
														public Adapter caseOCLLifelineTransition(
																OCLLifelineTransition object) {
															return createOCLLifelineTransitionAdapter();
														}

														@Override
														public Adapter caseLifelineBuilder(
																LifelineBuilder object) {
															return createLifelineBuilderAdapter();
														}

														@Override
														public Adapter caseLifeline(
																Lifeline object) {
															return createLifelineAdapter();
														}

														@Override
														public Adapter defaultCase(
																EObject object) {
															return createEObjectAdapter();
														}
													};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineBuilder <em>OCL Lifeline Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineBuilder
	 * @generated
	 */
	public Adapter createOCLLifelineBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline <em>OCL Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifeline
	 * @generated
	 */
	public Adapter createOCLLifelineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue <em>OCL Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue
	 * @generated
	 */
	public Adapter createOCLValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineTransition <em>OCL Lifeline Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.ocl.model.OCLLifelineTransition
	 * @generated
	 */
	public Adapter createOCLLifelineTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder <em>Lifeline Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * @generated
	 */
	public Adapter createLifelineBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline <em>Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline
	 * @generated
	 */
	public Adapter createLifelineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SSPVisOCLAdapterFactory
