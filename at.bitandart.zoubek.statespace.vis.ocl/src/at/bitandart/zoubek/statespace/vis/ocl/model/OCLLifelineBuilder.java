/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model;

import java.util.List;

import org.modelevolution.mc.stateconvert.api.Expression;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>OCL Lifeline Builder</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage#getOCLLifelineBuilder()
 * @model
 * @generated
 */
public interface OCLLifelineBuilder extends LifelineBuilder {

	/**
	 * @return the expressions
	 */
	public List<Expression> getExpressions();

	/**
	 * @param expressions
	 *            the expressions to set
	 */
	public void setExpressions(List<Expression> expressions);
} // OCLLifelineBuilder
