/**
 */
package at.bitandart.zoubek.statespace.vis.ocl.model.impl;

import at.bitandart.zoubek.statespace.vis.ocl.model.OCLValue;
import at.bitandart.zoubek.statespace.vis.ocl.model.SSPVisOCLPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OCL Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLValueImpl#getClazz <em>Clazz</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLValueImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.ocl.model.impl.OCLValueImpl#isKnown <em>Known</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OCLValueImpl extends MinimalEObjectImpl.Container implements OCLValue {
	/**
	 * The cached value of the '{@link #getClazz() <em>Clazz</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClazz()
	 * @generated
	 * @ordered
	 */
	protected Class<?>				clazz;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final Object	VALUE_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Object				value			= VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isKnown() <em>Known</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKnown()
	 * @generated
	 * @ordered
	 */
	protected static final boolean	KNOWN_EDEFAULT	= false;

	/**
	 * The cached value of the '{@link #isKnown() <em>Known</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKnown()
	 * @generated
	 * @ordered
	 */
	protected boolean				known			= KNOWN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OCLValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SSPVisOCLPackage.Literals.OCL_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getClazz() {
		return clazz;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClazz(Class<?> newClazz) {
		Class<?> oldClazz = clazz;
		clazz = newClazz;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											SSPVisOCLPackage.OCL_VALUE__CLAZZ,
											oldClazz,
											clazz));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(Object newValue) {
		Object oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											SSPVisOCLPackage.OCL_VALUE__VALUE,
											oldValue,
											value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isKnown() {
		return known;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKnown(boolean newKnown) {
		boolean oldKnown = known;
		known = newKnown;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											SSPVisOCLPackage.OCL_VALUE__KNOWN,
											oldKnown,
											known));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_VALUE__CLAZZ:
				return getClazz();
			case SSPVisOCLPackage.OCL_VALUE__VALUE:
				return getValue();
			case SSPVisOCLPackage.OCL_VALUE__KNOWN:
				return isKnown();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_VALUE__CLAZZ:
				setClazz((Class<?>) newValue);
				return;
			case SSPVisOCLPackage.OCL_VALUE__VALUE:
				setValue(newValue);
				return;
			case SSPVisOCLPackage.OCL_VALUE__KNOWN:
				setKnown((Boolean) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_VALUE__CLAZZ:
				setClazz((Class<?>) null);
				return;
			case SSPVisOCLPackage.OCL_VALUE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case SSPVisOCLPackage.OCL_VALUE__KNOWN:
				setKnown(KNOWN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SSPVisOCLPackage.OCL_VALUE__CLAZZ:
				return clazz != null;
			case SSPVisOCLPackage.OCL_VALUE__VALUE:
				return VALUE_EDEFAULT == null
						? value != null : !VALUE_EDEFAULT.equals(value);
			case SSPVisOCLPackage.OCL_VALUE__KNOWN:
				return known != KNOWN_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (clazz: ");
		result.append(clazz);
		result.append(", value: ");
		result.append(value);
		result.append(", known: ");
		result.append(known);
		result.append(')');
		return result.toString();
	}

} //OCLValueImpl
