/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.provider;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class LifelineGroupItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LifelineGroupItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLifelinesPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Lifelines feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addLifelinesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_LifelineGroup_lifelines_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_LifelineGroup_lifelines_feature",
																				"_UI_LifelineGroup_type"),
																	StatespacevisPackage.Literals.LIFELINE_GROUP__LIFELINES,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Name feature. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_LifelineGroup_name_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_LifelineGroup_name_feature",
																				"_UI_LifelineGroup_type"),
																	StatespacevisPackage.Literals.LIFELINE_GROUP__NAME,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This returns LifelineGroup.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
							getResourceLocator().getImage("full/obj16/LifelineGroup"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LifelineGroup) object).getName();
		return label == null || label.length() == 0
				? getString("_UI_LifelineGroup_type")
				: getString("_UI_LifelineGroup_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LifelineGroup.class)) {
			case StatespacevisPackage.LIFELINE_GROUP__NAME:
				fireNotifyChanged(new ViewerNotification(	notification,
															notification.getNotifier(),
															false,
															true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModelEditPlugin.INSTANCE;
	}

}
