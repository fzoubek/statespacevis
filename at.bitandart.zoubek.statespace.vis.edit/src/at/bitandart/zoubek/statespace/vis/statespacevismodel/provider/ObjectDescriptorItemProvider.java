/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.provider;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class ObjectDescriptorItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectDescriptorItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addObjectIDPropertyDescriptor(object);
			addClassNamePropertyDescriptor(object);
			addInstanceNamePropertyDescriptor(object);
			addClassDescriptorPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Class Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addClassNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectDescriptor_ClassName_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectDescriptor_ClassName_feature",
																				"_UI_ObjectDescriptor_type"),
																	StatespacevisPackage.Literals.OBJECT_DESCRIPTOR__CLASS_NAME,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Instance Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addInstanceNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectDescriptor_InstanceName_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectDescriptor_InstanceName_feature",
																				"_UI_ObjectDescriptor_type"),
																	StatespacevisPackage.Literals.OBJECT_DESCRIPTOR__INSTANCE_NAME,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Class Descriptor feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addClassDescriptorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectDescriptor_classDescriptor_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectDescriptor_classDescriptor_feature",
																				"_UI_ObjectDescriptor_type"),
																	StatespacevisPackage.Literals.OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Object ID feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addObjectIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectDescriptor_ObjectID_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectDescriptor_ObjectID_feature",
																				"_UI_ObjectDescriptor_type"),
																	StatespacevisPackage.Literals.OBJECT_DESCRIPTOR__OBJECT_ID,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This returns ObjectDescriptor.gif. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
							getResourceLocator().getImage("full/obj16/ObjectDescriptor"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ObjectDescriptor) object).getClassName();
		return label == null || label.length() == 0
				? getString("_UI_ObjectDescriptor_type")
				: getString("_UI_ObjectDescriptor_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ObjectDescriptor.class)) {
			case StatespacevisPackage.OBJECT_DESCRIPTOR__OBJECT_ID:
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_NAME:
			case StatespacevisPackage.OBJECT_DESCRIPTOR__INSTANCE_NAME:
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR:
				fireNotifyChanged(new ViewerNotification(	notification,
															notification.getNotifier(),
															false,
															true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModelEditPlugin.INSTANCE;
	}

}
