/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.provider;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

/**
 * This is the item provider adapter for a
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class StateSpacePathVisItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpacePathVisItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLifelineGroupsPropertyDescriptor(object);
			addStatePathTreePropertyDescriptor(object);
			addStatespacePropertyDescriptor(object);
			addModelHistoryPropertyDescriptor(object);
			addModelHistoryBuilderPropertyDescriptor(object);
			addLifelineBuildersPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Lifeline Groups feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addLifelineGroupsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpacePathVis_lifelineGroups_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpacePathVis_lifelineGroups_feature",
																				"_UI_StateSpacePathVis_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_PATH_VIS__LIFELINE_GROUPS,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the State Path Tree feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addStatePathTreePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpacePathVis_statePathTree_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpacePathVis_statePathTree_feature",
																				"_UI_StateSpacePathVis_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_PATH_VIS__STATE_PATH_TREE,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Statespace feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addStatespacePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpacePathVis_statespace_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpacePathVis_statespace_feature",
																				"_UI_StateSpacePathVis_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_PATH_VIS__STATESPACE,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Model History feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addModelHistoryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpacePathVis_modelHistory_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpacePathVis_modelHistory_feature",
																				"_UI_StateSpacePathVis_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_PATH_VIS__MODEL_HISTORY,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Model History Builder feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addModelHistoryBuilderPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpacePathVis_modelHistoryBuilder_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpacePathVis_modelHistoryBuilder_feature",
																				"_UI_StateSpacePathVis_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Lifeline Builders feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addLifelineBuildersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpacePathVis_lifelineBuilders_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpacePathVis_lifelineBuilders_feature",
																				"_UI_StateSpacePathVis_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This returns StateSpacePathVis.gif. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
							getResourceLocator().getImage("full/obj16/StateSpacePathVis"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_StateSpacePathVis_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModelEditPlugin.INSTANCE;
	}

}
