/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.provider;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class ObjectHistoryDiffItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryDiffItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNewStatePropertyDescriptor(object);
			addNewObjectInstancePropertyDescriptor(object);
			addOldObjectInstancePropertyDescriptor(object);
			addRawDiffDataPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the New State feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNewStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectHistoryDiff_newState_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectHistoryDiff_newState_feature",
																				"_UI_ObjectHistoryDiff_type"),
																	StatespacevisPackage.Literals.OBJECT_HISTORY_DIFF__NEW_STATE,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the New Object Instance feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNewObjectInstancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectHistoryDiff_newObjectInstance_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectHistoryDiff_newObjectInstance_feature",
																				"_UI_ObjectHistoryDiff_type"),
																	StatespacevisPackage.Literals.OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Old Object Instance feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addOldObjectInstancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectHistoryDiff_oldObjectInstance_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectHistoryDiff_oldObjectInstance_feature",
																				"_UI_ObjectHistoryDiff_type"),
																	StatespacevisPackage.Literals.OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Raw Diff Data feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRawDiffDataPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_ObjectHistoryDiff_rawDiffData_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_ObjectHistoryDiff_rawDiffData_feature",
																				"_UI_ObjectHistoryDiff_type"),
																	StatespacevisPackage.Literals.OBJECT_HISTORY_DIFF__RAW_DIFF_DATA,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This returns ObjectHistoryDiff.gif. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
							getResourceLocator().getImage("full/obj16/ObjectHistoryDiff"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		ObjectStateType labelValue = ((ObjectHistoryDiff) object).getNewState();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0
				? getString("_UI_ObjectHistoryDiff_type")
				: getString("_UI_ObjectHistoryDiff_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ObjectHistoryDiff.class)) {
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_STATE:
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE:
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE:
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__RAW_DIFF_DATA:
				fireNotifyChanged(new ViewerNotification(	notification,
															notification.getNotifier(),
															false,
															true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModelEditPlugin.INSTANCE;
	}

}
