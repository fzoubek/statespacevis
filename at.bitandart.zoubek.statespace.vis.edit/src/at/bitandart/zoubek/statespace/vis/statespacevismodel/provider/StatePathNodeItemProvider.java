/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.provider;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

/**
 * This is the item provider adapter for a
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatePathNodeItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addStatePropertyDescriptor(object);
			addNextTransitionPropertyDescriptor(object);
			addIncomingTransitionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the State feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StatePathNode_state_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StatePathNode_state_feature",
																				"_UI_StatePathNode_type"),
																	StatespacevisPackage.Literals.STATE_PATH_NODE__STATE,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Next Transition feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNextTransitionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StatePathNode_nextTransition_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StatePathNode_nextTransition_feature",
																				"_UI_StatePathNode_type"),
																	StatespacevisPackage.Literals.STATE_PATH_NODE__NEXT_TRANSITION,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Incoming Transition feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addIncomingTransitionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StatePathNode_incomingTransition_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StatePathNode_incomingTransition_feature",
																				"_UI_StatePathNode_type"),
																	StatespacevisPackage.Literals.STATE_PATH_NODE__INCOMING_TRANSITION,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This returns StatePathNode.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
							getResourceLocator().getImage("full/obj16/StatePathNode"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_StatePathNode_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModelEditPlugin.INSTANCE;
	}

}
