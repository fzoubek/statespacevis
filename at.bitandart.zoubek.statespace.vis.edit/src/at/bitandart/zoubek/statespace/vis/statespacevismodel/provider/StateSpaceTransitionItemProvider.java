/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.provider;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class StateSpaceTransitionItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpaceTransitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNextStatePropertyDescriptor(object);
			addTransitionNamePropertyDescriptor(object);
			addPrevStatePropertyDescriptor(object);
			addRawTransitionDataPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Next State feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNextStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpaceTransition_nextState_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpaceTransition_nextState_feature",
																				"_UI_StateSpaceTransition_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_TRANSITION__NEXT_STATE,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Transition Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addTransitionNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpaceTransition_transitionName_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpaceTransition_transitionName_feature",
																				"_UI_StateSpaceTransition_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_TRANSITION__TRANSITION_NAME,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Prev State feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addPrevStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpaceTransition_prevState_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpaceTransition_prevState_feature",
																				"_UI_StateSpaceTransition_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_TRANSITION__PREV_STATE,
																	true,
																	false,
																	true,
																	null,
																	null,
																	null));
	}

	/**
	 * This adds a property descriptor for the Raw Transition Data feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRawTransitionDataPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(	((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
																	getResourceLocator(),
																	getString("_UI_StateSpaceTransition_rawTransitionData_feature"),
																	getString(	"_UI_PropertyDescriptor_description",
																				"_UI_StateSpaceTransition_rawTransitionData_feature",
																				"_UI_StateSpaceTransition_type"),
																	StatespacevisPackage.Literals.STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA,
																	true,
																	false,
																	false,
																	ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
																	null,
																	null));
	}

	/**
	 * This returns StateSpaceTransition.gif. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
							getResourceLocator().getImage("full/obj16/StateSpaceTransition"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((StateSpaceTransition) object).getTransitionName();
		return label == null || label.length() == 0
				? getString("_UI_StateSpaceTransition_type")
				: getString("_UI_StateSpaceTransition_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(StateSpaceTransition.class)) {
			case StatespacevisPackage.STATE_SPACE_TRANSITION__TRANSITION_NAME:
			case StatespacevisPackage.STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA:
				fireNotifyChanged(new ViewerNotification(	notification,
															notification.getNotifier(),
															false,
															true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModelEditPlugin.INSTANCE;
	}

}
