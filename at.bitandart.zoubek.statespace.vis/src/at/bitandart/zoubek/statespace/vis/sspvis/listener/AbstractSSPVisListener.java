/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.sspvis.listener;

import at.bitandart.zoubek.statespace.vis.editors.StateSpacePathVisEditor;

/**
 * Abstract implementation of SSPVisEditorListener that delegates all known
 * events (defined by the <code>ET_*</code> constants of
 * <code>SSPVisEvent</code>) to the corresponding methods. This class exists
 * mainly for convenience. Unknown events can be handled in
 * <code>unknownEventOccured()</code>.
 * 
 * @see SSPVisEvent
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public abstract class AbstractSSPVisListener implements SSPVisEditorListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.bitandart.zoubek.statespace.vis.sspvis.listener.SSPVisEditorListener
	 * #handleEvent
	 * (at.bitandart.zoubek.statespace.vis.sspvis.listener.SSPVisEvent)
	 */
	@Override
	public void handleEvent(SSPVisEvent event) {
		String eventType = event.getEventType();
		
		if (eventType.equals(SSPVisEvent.ET_STATE_SPACE_TREE_PATH_CHANGED)) {
			stateSpaceTreePathChanged(event.getEditorInstance());
		} else if (eventType.equals(SSPVisEvent.ET_FILTER_DEFINITION_CHANGED)) {
			filterDefinitionChanged(event.getEditorInstance());
		} else {
			unknownEventOccured(event);
		}
	}

	/**
	 * called if the state space path has been changed
	 * 
	 * @param editorInstance
	 *            the editor instance where the state space path has been
	 *            changed
	 */
	abstract public void stateSpaceTreePathChanged(StateSpacePathVisEditor editorInstance);

	/**
	 * called if a filter definition has been changed
	 * 
	 * @param editorInstance
	 *            the editor instance where the filter definition should be
	 *            applied
	 */
	abstract public void filterDefinitionChanged(
			StateSpacePathVisEditor editorInstance);

	/**
	 * called if an unknown event has occured
	 * 
	 * @param event
	 *            the unknown event
	 */
	abstract public void unknownEventOccured(SSPVisEvent event);

}
