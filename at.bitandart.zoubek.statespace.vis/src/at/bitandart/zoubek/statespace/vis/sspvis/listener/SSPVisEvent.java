/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.sspvis.listener;

import at.bitandart.zoubek.statespace.vis.editors.StateSpacePathVisEditor;

/**
 * This class represents a event that occurs mainly in StateSpacePathVis
 * editors.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisEvent {

	/**
	 * the event type to use if a state space path has been changed
	 */
	public static String ET_STATE_SPACE_TREE_PATH_CHANGED = "ET_STATE_SPACE_PATH_CHANGED";
	
	/**
	 * the event type to use if a filter definition has been changed
	 */
	public static String ET_FILTER_DEFINITION_CHANGED = "ET_FILTER_DEFINITION_CHANGED";

	/**
	 * the editor instance that fired the event
	 */
	private StateSpacePathVisEditor editorInstance;

	/**
	 * the event type
	 */
	private String eventType;

	/**
	 * @param editorInstance
	 *            the editor instance that fired the event
	 * @param eventType
	 *            the event type
	 */
	public SSPVisEvent(StateSpacePathVisEditor editorInstance, String eventType) {
		super();
		this.editorInstance = editorInstance;
		this.eventType = eventType;
	}

	/**
	 * @return the editor instance that fired the event
	 */
	public StateSpacePathVisEditor getEditorInstance() {
		return editorInstance;
	}

	/**
	 * @return the event type
	 */
	public String getEventType() {
		return eventType;
	}

}
