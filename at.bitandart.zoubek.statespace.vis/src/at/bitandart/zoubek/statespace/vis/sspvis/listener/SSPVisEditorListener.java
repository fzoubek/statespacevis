/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.sspvis.listener;

/**
 * Interface for listeners that listen to SSPVis-Events
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public interface SSPVisEditorListener {

	/**
	 * called once a SSPVis-Event occurs in any StateSpaceVis editor instance. Further
	 * context is given by the passed event.
	 * 
	 * @param event
	 *            the event that occurred
	 */
	void handleEvent(SSPVisEvent event);

}
