/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.handler;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeNodeEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeSubNodeEP;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class AddStateToStateTreeHandler extends AbstractHandler {

	private static Logger	LOG	= Logger.getLogger(AddStateToStateTreeHandler.class.getCanonicalName());

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		LOG.info("Request to add state to tree path received");
		ISelection selection = HandlerUtil.getCurrentSelectionChecked(event);
		if (selection instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selection).getFirstElement();

			// check if selections is a subnode
			if (firstElement instanceof StateTreeSubNodeEP) {
				StateTreeSubNodeEP nodeEP = (StateTreeSubNodeEP) firstElement;
				TreeNode<State, StateSpaceTransition> treeNode = nodeEP.getTreeNode();
				EditPart parentEP = nodeEP.getParent();

				// check parent edit part
				if (parentEP instanceof StateTreeNodeEP) {
					// check validity of sub node
					if (treeNode.getParentTransition() != null) {
						TreeNode<State, StateSpaceTransition> targetTreeNode = ((StateTreeNodeEP) parentEP).getStateTreeNode();
						StatespacevisFactory factory = StatespacevisFactory.eINSTANCE;

						LOG.log(Level.FINE,
								"Adding state to tree path. state: {0}, parent state: {1}",
								new Object[] { treeNode.getNodeValue(),
										targetTreeNode.getNodeValue() });

						// create new transition and node that should be attached to the target node 
						TreeNode<State, StateSpaceTransition> newNode = factory.createTreeNode();
						newNode.setNodeValue(treeNode.getNodeValue());
						TreeTransition<State, StateSpaceTransition> newTransition = factory.createTreeTransition();
						newTransition.setTransitionValue(treeNode.getParentTransition()
																	.getTransitionValue());
						newTransition.setNextNode(newNode);

						// attach new transition and node to target
						targetTreeNode.getChildTransitions().add(newTransition);
						LOG.info("Successfully added state to tree path");
						
					} else {
						LOG.log(Level.SEVERE,
								"Invalid sub node, a parent transition must exist (was {0})",
								new Object[] { treeNode.getParentTransition() });
					}
				} else {
					LOG.log(Level.SEVERE,
							"Invalid parent, parent must be an instance of {0}, but was {1}",
							new Object[] {
									StateTreeNodeEP.class.getCanonicalName(),
									parentEP.getClass().getCanonicalName() });
				}
			} else {
				LOG.log(Level.SEVERE,
						"Invalid selection, selection must be an instance of {0}, but was {1}",
						new Object[] {
								StateTreeSubNodeEP.class.getCanonicalName(),
								firstElement.getClass().getCanonicalName() });
			}
		} else {
			LOG.log(Level.SEVERE,
					"Invalid selection, selection must be an instance of {0}, but was {1}",
					new Object[] {
							IStructuredSelection.class.getCanonicalName(),
							selection.getClass().getCanonicalName() });
		}
		return null;
	}

}
