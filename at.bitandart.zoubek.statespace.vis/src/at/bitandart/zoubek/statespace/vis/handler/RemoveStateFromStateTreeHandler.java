/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.handler;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeNodeEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeSubNodeEP;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class RemoveStateFromStateTreeHandler extends AbstractHandler {

	private static Logger	LOG	= Logger.getLogger(RemoveStateFromStateTreeHandler.class.getCanonicalName());

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		LOG.info("Request to remove state from tree path received");
		ISelection selection = HandlerUtil.getCurrentSelectionChecked(event);
		if (selection instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selection).getFirstElement();

			// check if selections is a subnode
			if (firstElement instanceof StateTreeSubNodeEP) {
				StateTreeSubNodeEP nodeEP = (StateTreeSubNodeEP) firstElement;
				TreeNode<State, StateSpaceTransition> subTreeNode = nodeEP.getTreeNode();
				EditPart parentEP = nodeEP.getParent();

				// check parent edit part
				if (parentEP instanceof StateTreeNodeEP) {
					// check validity of sub node
					if (subTreeNode.getParentTransition() == null) {
						TreeNode<State, StateSpaceTransition> targetTreeNode = ((StateTreeNodeEP) parentEP).getStateTreeNode();

						LOG.log(Level.FINE,
								"Removing state to from path tree. state: {0}, parent state: {1}",
								new Object[] { subTreeNode.getNodeValue(),
										targetTreeNode.getNodeValue() });

						// detach parent transition from the current path tree
						TreeTransition<State, StateSpaceTransition> parentTransition = targetTreeNode.getParentTransition();
						parentTransition.setPreviousNode(null);
						parentTransition.setNextNode(null);

						LOG.info("Successfully removed state from path tree");

					} else {
						LOG.log(Level.SEVERE,
								"Invalid sub node, a parent transition must not exist (was {0})",
								new Object[] { subTreeNode.getParentTransition() });
					}
				} else {
					LOG.log(Level.SEVERE,
							"Invalid parent, parent must be an instance of {0}, but was {1}",
							new Object[] {
									StateTreeNodeEP.class.getCanonicalName(),
									parentEP.getClass().getCanonicalName() });
				}
			} else {
				LOG.log(Level.SEVERE,
						"Invalid selection, selection must be an instance of {0}, but was {1}",
						new Object[] {
								StateTreeSubNodeEP.class.getCanonicalName(),
								firstElement.getClass().getCanonicalName() });
			}
		} else {
			LOG.log(Level.SEVERE,
					"Invalid selection, selection must be an instance of {0}, but was {1}",
					new Object[] {
							IStructuredSelection.class.getCanonicalName(),
							selection.getClass().getCanonicalName() });
		}
		return null;
	}

}
