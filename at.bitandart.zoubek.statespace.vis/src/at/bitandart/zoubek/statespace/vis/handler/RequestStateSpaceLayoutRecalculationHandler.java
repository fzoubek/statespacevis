/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import at.bitandart.zoubek.statespace.vis.views.StateSpaceView;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class RequestStateSpaceLayoutRecalculationHandler extends AbstractHandler {

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IViewPart viewPart = window.getActivePage().findView(StateSpaceView.VIEW_ID);
		if(viewPart instanceof StateSpaceView){
			StateSpaceView stateSpaceView = (StateSpaceView) viewPart;
			stateSpaceView.requestLayoutRecalculation();
		}
		return null;
	}

}
