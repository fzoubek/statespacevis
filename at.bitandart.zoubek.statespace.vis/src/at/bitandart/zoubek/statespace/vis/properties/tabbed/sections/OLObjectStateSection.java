/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.properties.tabbed.sections;

import java.lang.reflect.Field;
import java.util.logging.Logger;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.OLObjectStateEP;
import at.bitandart.zoubek.statespace.vis.swt.providers.FieldNameLabelProvider;
import at.bitandart.zoubek.statespace.vis.swt.providers.FieldValueLabelProvider;
import at.bitandart.zoubek.statespace.vis.swt.providers.ReflectionAndEMFTreeContentProvider;
import at.bitandart.zoubek.statespace.vis.swt.providers.data.RowDescriptor;

/**
 * A {@link RowDescriptor} representing a row describing a single collection
 * item.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OLObjectStateSection extends AbstractPropertySection {

	@SuppressWarnings("unused")
	private static final Logger	LOG	= Logger.getLogger(OLObjectStateSection.class.getCanonicalName());

	private TreeViewer			valuesTableViewer;

	private OLObjectStateEP		transitionEP;

	/**
	 * 
	 */
	public OLObjectStateSection() {
	}

	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		FormData data;

		// create table
		Tree valuesTree = getWidgetFactory().createTree(composite,
														SWT.BORDER
																| SWT.H_SCROLL
																| SWT.V_SCROLL
																| SWT.VIRTUAL);
		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(100, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		valuesTree.setLayoutData(data);
		valuesTree.setHeaderVisible(true);

		valuesTree.setLayoutData(data);
		valuesTree.setHeaderVisible(true);
		valuesTree.setLinesVisible(true);
		valuesTableViewer = new TreeViewer(valuesTree);
		valuesTableViewer.setUseHashlookup(true); // Important!

		TreeColumn column1 = new TreeColumn(valuesTree, SWT.LEFT);
		TreeViewerColumn treeViewerColumn1 = new TreeViewerColumn(	valuesTableViewer,
																	column1);
		treeViewerColumn1.setLabelProvider(new FieldNameLabelProvider());
		column1.setAlignment(SWT.LEFT);
		column1.setText("Field");
		column1.setWidth(200);
		column1.setResizable(true);

		TreeColumn column2 = new TreeColumn(valuesTree, SWT.RIGHT);
		TreeViewerColumn treeViewerColumn2 = new TreeViewerColumn(	valuesTableViewer,
																	column2);
		treeViewerColumn2.setLabelProvider(new FieldValueLabelProvider());
		column2.setAlignment(SWT.LEFT);
		column2.setText("Value");
		column2.setWidth(200);
		column2.setResizable(true);

		valuesTableViewer.setContentProvider(new ReflectionAndEMFTreeContentProvider(valuesTableViewer));
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		if (selection instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) selection).getFirstElement();
			if (element instanceof OLObjectStateEP) {
				transitionEP = (OLObjectStateEP) element;
			}
		}
	}

	@Override
	public void refresh() {
		super.refresh();
		valuesTableViewer.setInput(transitionEP.getObjectState()
												.getTransitionValue()
												.getObjectInstance());
	}

	@Override
	public boolean shouldUseExtraSpace() {
		return true;
	}

}

interface OLStateTableEntry {
	String getValueString();

	String getFieldString();
}

/**
 * Represents a table entry describing a field in a {@link OLObjectStateSection}
 * .
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
class OLStateTableFieldEntry implements OLStateTableEntry {
	public Field	field;
	public Object	value;

	@Override
	public String getValueString() {
		if (value != null) {
			return value.toString();
		}
		return "<null>";
	}

	@Override
	public String getFieldString() {
		if (field != null) {
			return field.getName();
		}
		return "<null>";
	}
}

/**
 * Represents a table entry created using EMF in a {@link OLObjectStateSection}.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
class OLStateTableEStructuralFeatureEntry implements OLStateTableEntry {
	public EStructuralFeature	feature;
	public Object				value;

	@Override
	public String getValueString() {
		if (value != null) {
			return value.toString();
		}
		return "<null>";
	}

	@Override
	public String getFieldString() {
		if (feature != null) {
			return feature.getName();
		}
		return "<null>";
	}
}
