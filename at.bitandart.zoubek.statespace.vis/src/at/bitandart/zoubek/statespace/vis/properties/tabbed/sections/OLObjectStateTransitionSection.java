/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.properties.tabbed.sections;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.OLObjectStateTransitionEP;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * A property section that displays values of a object lifeline transition of an
 * {@link OLObjectStateTransitionEP}
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OLObjectStateTransitionSection extends AbstractPropertySection {

	private static final Logger			LOG	= Logger.getLogger(OLObjectStateTransitionSection.class.getCanonicalName());

	private TableViewer					valuesTableViewer;

	private OLObjectStateTransitionEP	transitionEP;

	/**
	 * 
	 */
	public OLObjectStateTransitionSection() {
	}

	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		FormData data;

		// create table
		Table valuesTable = getWidgetFactory().createTable(	composite,
															SWT.SINGLE
																	| SWT.H_SCROLL
																	| SWT.V_SCROLL
																	| SWT.BORDER);
		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(100, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		valuesTable.setLayoutData(data);
		valuesTable.setHeaderVisible(true);

		// attach table viewer
		valuesTableViewer = new TableViewer(valuesTable);
		valuesTableViewer.setContentProvider(new IStructuredContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput,
					Object newInput) {
				// no op
			}

			@Override
			public void dispose() {
				// no op
			}

			@Override
			public Object[] getElements(Object inputElement) {
				List<OLTransitionTableEntry> entries = new ArrayList<>();

				@SuppressWarnings("unchecked")
				TreeNode<StateSpaceTransition, ObjectLifelineState> input = (TreeNode<StateSpaceTransition, ObjectLifelineState>) inputElement;
				Class<?> clazz = Object.class;
				Object prevTransitionInstance = null;

				if (input.getParentTransition() != null) {
					prevTransitionInstance = input.getParentTransition()
													.getTransitionValue()
													.getObjectInstance();
					if (prevTransitionInstance != null) {
						clazz = prevTransitionInstance.getClass();
					} else {
						LOG.log(Level.FINEST, "Previous value is null");
					}
				}
				if (clazz == Object.class
					&& !input.getChildTransitions().isEmpty()) {
					Object nextValue = input.getChildTransitions().get(0)
											.getTransitionValue()
											.getObjectInstance();
					if (nextValue != null) {
						clazz = nextValue.getClass();
					} else {
						LOG.log(Level.FINEST, "Next value is null");
					}
				} else {
					LOG.log(Level.FINEST,
							"Node has no parent transition and no children");
				}
				LOG.log(Level.FINER, "Class of object: {0}", (clazz != null)
						? clazz.getCanonicalName() : "none");

				// create a entry for each field
				while (clazz != null) {
					for (Field field : clazz.getDeclaredFields()) {

						if (!((field.getModifiers() & Modifier.FINAL) == Modifier.FINAL)) {

							OLTransitionTableEntry entry = new OLTransitionTableEntry();
							entry.field = field;
							LOG.log(Level.FINER,
									"Creating entry for field {0}",
									field.getName());

							field.setAccessible(true);

							// retrieve values 
							try {
								if (prevTransitionInstance != null) {
									entry.values.add(entry.field.get(prevTransitionInstance));
								} else {
									entry.values.add(null);
								}

								for (TreeTransition<StateSpaceTransition, ObjectLifelineState> childTransition : input.getChildTransitions()) {
									Object nextValue = childTransition.getTransitionValue()
																		.getObjectInstance();
									if (nextValue != null) {
										entry.values.add(entry.field.get(nextValue));
									} else {
										entry.values.add(null);
									}
								}

								entries.add(entry);
							} catch (	IllegalArgumentException
										| IllegalAccessException e) {
								LOG.log(Level.SEVERE,
										"Could not acccess field "
												+ field.getName(),
										e);
							}
						}
					}
					clazz = clazz.getSuperclass();
				}

				return entries.toArray();
			}
		});

		// field column
		TableViewerColumn colFieldName = new TableViewerColumn(	valuesTableViewer,
																SWT.NONE);
		colFieldName.getColumn().setText("field");
		colFieldName.getColumn().setWidth(200);
		colFieldName.getColumn().setResizable(true);
		colFieldName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				OLTransitionTableEntry entry = (OLTransitionTableEntry) element;
				return entry.field.getName();
			}
		});

		// previous value column
		TableViewerColumn colFieldOldValue = new TableViewerColumn(	valuesTableViewer,
																	SWT.NONE);
		colFieldOldValue.getColumn().setText("previous value");
		colFieldOldValue.getColumn().setWidth(200);
		colFieldOldValue.getColumn().setResizable(true);
		colFieldOldValue.setLabelProvider(new EntryValueLabelProvider(0));
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		if (selection instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) selection).getFirstElement();
			if (element instanceof OLObjectStateTransitionEP) {
				transitionEP = (OLObjectStateTransitionEP) element;
			}
		}
	}

	@Override
	public void refresh() {
		super.refresh();
		valuesTableViewer.setInput(transitionEP.getObjectStateTransition());
		clearNextValueColumns();
		createColumns();
	}

	/**
	 * updates the column visibility
	 */
	private void clearNextValueColumns() {
		LOG.log(Level.FINER, "Clearing table columns...");
		int i = 0;
		int visibleColumns = 2;

		@SuppressWarnings("unchecked")
		TreeNode<StateSpaceTransition, ObjectLifelineState> input = (TreeNode<StateSpaceTransition, ObjectLifelineState>) valuesTableViewer.getInput();

		if (input != null) {
			visibleColumns += input.getChildTransitions().size();
		}

		for (TableColumn column : valuesTableViewer.getTable().getColumns()) {
			if (i < visibleColumns) {
				if (column.getWidth() <= 0) {
					column.setWidth(200);
				}
			} else {
				column.setWidth(0);
			}
			i++;
		}
		LOG.log(Level.FINER, "Table columns cleared");
	}

	/**
	 * create columns if necessary
	 */
	private void createColumns() {
		LOG.log(Level.FINER, "Creating table columns...");

		@SuppressWarnings("unchecked")
		TreeNode<StateSpaceTransition, ObjectLifelineState> input = (TreeNode<StateSpaceTransition, ObjectLifelineState>) valuesTableViewer.getInput();
		if (input != null) {

			for (int i = valuesTableViewer.getTable().getColumns().length - 2; i < input.getChildTransitions()
																						.size(); i++) {
				TableViewerColumn colValue = new TableViewerColumn(	valuesTableViewer,
																	SWT.NONE);
				colValue.getColumn().setText("Value (" + (i + 1) + ")");
				colValue.getColumn().setWidth(200);
				colValue.getColumn().setResizable(true);
				colValue.setLabelProvider(new EntryValueLabelProvider(i + 1));
			}
		}
		LOG.log(Level.FINER, "Table columns created");
	}

	@Override
	public boolean shouldUseExtraSpace() {
		return true;
	}

}

/**
 * Label provider for {@link OLTransitionTableEntry}
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
class EntryValueLabelProvider extends ColumnLabelProvider {

	private int	index	= 0;

	public EntryValueLabelProvider(int index) {
		this.index = index;
	}

	@Override
	public String getText(Object element) {
		OLTransitionTableEntry entry = (OLTransitionTableEntry) element;
		if (index >= entry.values.size()) {
			return "";
		}
		Object value = entry.values.get(index);
		if (value != null) {
			return value.toString();
		} else {
			return "<null>";
		}
	}
}

/**
 * Represents a table entry in a {@link OLObjectStateTransitionSection}.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
class OLTransitionTableEntry {
	public Field		field;
	public List<Object>	values	= new ArrayList<>();
}
