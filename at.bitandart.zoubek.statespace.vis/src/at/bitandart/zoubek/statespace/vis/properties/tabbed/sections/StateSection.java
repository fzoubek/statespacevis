/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.properties.tabbed.sections;

import java.util.logging.Logger;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.OLObjectStateEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeNodeEP;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.swt.providers.FieldNameLabelProvider;
import at.bitandart.zoubek.statespace.vis.swt.providers.FieldValueLabelProvider;
import at.bitandart.zoubek.statespace.vis.swt.providers.ReflectionAndEMFTreeContentProvider;

/**
 * A property section that displays values of a object lifeline transition of an
 * {@link OLObjectStateEP}
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSection extends AbstractPropertySection {

	@SuppressWarnings("unused")
	private static final Logger	LOG	= Logger.getLogger(StateSection.class.getCanonicalName());

	private TreeViewer			snapshotTreeViewer;

	private State				state;

	/**
	 * 
	 */
	public StateSection() {
	}

	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		FormData data;

		// create snapshot label
		
		CLabel snapshotLabel = getWidgetFactory().createCLabel(composite, "Model snapshot:");
		
		data = new FormData();
		data.top = new FormAttachment(0, 0);
		data.left = new FormAttachment(0, 0);
		snapshotLabel.setLayoutData(data);
		
		// create snapshot table
		Tree snapshotTree = getWidgetFactory().createTree(	composite,
															SWT.BORDER
																	| SWT.H_SCROLL
																	| SWT.V_SCROLL | SWT.VIRTUAL);
		data = new FormData();
		data.top = new FormAttachment(snapshotLabel);
		data.bottom = new FormAttachment(100, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);

		snapshotTree.setLayoutData(data);
		snapshotTree.setHeaderVisible(true);
		snapshotTree.setLinesVisible(true);
		snapshotTreeViewer = new TreeViewer(snapshotTree);
		snapshotTreeViewer.setUseHashlookup(true); // Important!

		TreeColumn column1 = new TreeColumn(snapshotTree, SWT.LEFT);
		TreeViewerColumn treeViewerColumn1 = new TreeViewerColumn(snapshotTreeViewer, column1);
		treeViewerColumn1.setLabelProvider(new FieldNameLabelProvider());
		column1.setAlignment(SWT.LEFT);
		column1.setText("Field");
		column1.setWidth(200);
		column1.setResizable(true);
		
		TreeColumn column2 = new TreeColumn(snapshotTree, SWT.RIGHT);
		TreeViewerColumn treeViewerColumn2 = new TreeViewerColumn(snapshotTreeViewer, column2);
		treeViewerColumn2.setLabelProvider(new FieldValueLabelProvider());
		column2.setAlignment(SWT.LEFT);
		column2.setText("Value");
		column2.setWidth(200);
		column2.setResizable(true);

		snapshotTreeViewer.setContentProvider(new ReflectionAndEMFTreeContentProvider(snapshotTreeViewer));
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		if (selection instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) selection).getFirstElement();
			if (element instanceof StateEP) {
				state = ((StateEP) element).getState();
			} else if (element instanceof StateTreeNodeEP) {
				state = ((StateTreeNodeEP) element).getStateTreeNode()
													.getNodeValue();
			}
		}
	}

	@Override
	public void refresh() {
		super.refresh();
		snapshotTreeViewer.setInput(state.getModelSnapshot());
	}

	@Override
	public boolean shouldUseExtraSpace() {
		return true;
	}

}
