/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.properties.tabbed.sections;

import org.eclipse.gef.EditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import at.bitandart.zoubek.statespace.vis.swt.providers.FieldNameLabelProvider;
import at.bitandart.zoubek.statespace.vis.swt.providers.FieldValueLabelProvider;
import at.bitandart.zoubek.statespace.vis.swt.providers.ReflectionAndEMFTreeContentProvider;

/**
 * Property section for development purposes
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class DevDefaultSection extends AbstractPropertySection {

	private CLabel		epClassLabel;

	private EditPart	selectedEditPart;

	private TreeViewer	modelTableViewer;

	/**
	 * 
	 */
	public DevDefaultSection() {
	}

	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		FormData data;

		epClassLabel = getWidgetFactory().createCLabel(composite, "");
		data = new FormData();
		data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
		data.right = new FormAttachment(100, 0);
		epClassLabel.setLayoutData(data);

		CLabel epLabel = getWidgetFactory().createCLabel(composite, "EditPart:");
		data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(epClassLabel,
										-ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(epClassLabel, 0, SWT.CENTER);
		epLabel.setLayoutData(data);

		// create table for edit part model
		
		CLabel modelLabel = getWidgetFactory().createCLabel(composite, "Model:");
		data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		data.top = new FormAttachment(epLabel, 0);
		modelLabel.setLayoutData(data);
		
		Tree valuesTree = getWidgetFactory().createTree(composite,
														SWT.BORDER
																| SWT.H_SCROLL
																| SWT.V_SCROLL
																| SWT.VIRTUAL);
		data = new FormData();
		data.top = new FormAttachment(modelLabel, 0, SWT.BOTTOM);
		data.bottom = new FormAttachment(100, 0);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		valuesTree.setLayoutData(data);
		valuesTree.setHeaderVisible(true);

		valuesTree.setLayoutData(data);
		valuesTree.setHeaderVisible(true);
		valuesTree.setLinesVisible(true);
		modelTableViewer = new TreeViewer(valuesTree);
		modelTableViewer.setUseHashlookup(true); // Important!

		TreeColumn column1 = new TreeColumn(valuesTree, SWT.LEFT);
		TreeViewerColumn treeViewerColumn1 = new TreeViewerColumn(	modelTableViewer,
																	column1);
		treeViewerColumn1.setLabelProvider(new FieldNameLabelProvider());
		column1.setAlignment(SWT.LEFT);
		column1.setText("Field");
		column1.setWidth(200);
		column1.setResizable(true);

		TreeColumn column2 = new TreeColumn(valuesTree, SWT.RIGHT);
		TreeViewerColumn treeViewerColumn2 = new TreeViewerColumn(	modelTableViewer,
																	column2);
		treeViewerColumn2.setLabelProvider(new FieldValueLabelProvider());
		column2.setAlignment(SWT.LEFT);
		column2.setText("Value");
		column2.setWidth(200);
		column2.setResizable(true);

		modelTableViewer.setContentProvider(new ReflectionAndEMFTreeContentProvider(modelTableViewer));
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		if (selection instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) selection).getFirstElement();
			if (element instanceof EditPart) {
				selectedEditPart = (EditPart) element;
			}
		}
	}

	@Override
	public void refresh() {
		super.refresh();
		epClassLabel.setText((selectedEditPart != null)
				? selectedEditPart.getClass().getCanonicalName() : "<none>");
		modelTableViewer.setInput(selectedEditPart.getModel());
	}
	
	@Override
	public boolean shouldUseExtraSpace() {
		return true;
	}

}
