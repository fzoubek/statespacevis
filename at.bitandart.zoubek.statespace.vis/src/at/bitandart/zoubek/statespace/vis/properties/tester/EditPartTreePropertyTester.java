/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.properties.tester;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.gef.EditPart;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;

/**
 * A property tester for eclipse core expressions which is able to perform the
 * following property tests:
 * 
 * <ul>
 * <li>isNonRootLeafEP - tests if a EditParts model TreeNode has no children and a parent</li>
 * <li>isRootEP - tests if a EditParts model TreeNode is a root node (has no parent transition)</li>
 * </ul>
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class EditPartTreePropertyTester extends PropertyTester {

	private static Logger	LOG	= Logger.getLogger(EditPartTreePropertyTester.class.getCanonicalName());

	/**
	 * 
	 */
	public EditPartTreePropertyTester() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.expressions.IPropertyTester#test(java.lang.Object, java.lang.String, java.lang.Object[], java.lang.Object)
	 */
	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		LOG.log(Level.FINE,
				"property test requested:  {0}, receiver: {1}, expected: {2}",
				new Object[] { property, receiver, expectedValue });
		if (property.equals("isNonRootLeafEP")) {
			boolean expected = true;
			if (expectedValue != null) {
				expected = ((Boolean) expectedValue).booleanValue();
			}
			Object model = ((EditPart) receiver).getModel();
			if(model instanceof TreeNode<?, ?>){
				return isNonRootLeaf((TreeNode<?, ?>) model, expected);
			}
		}else if (property.equals("isRootEP")) {
			boolean expected = true;
			if (expectedValue != null) {
				expected = ((Boolean) expectedValue).booleanValue();
			}
			Object model = ((EditPart) receiver).getModel();
			if(model instanceof TreeNode<?, ?>){
				return isRoot((TreeNode<?, ?>) model, expected);
			}
		}
		return false;
	}

	/**
	 * tests if the node is a leaf node and has a parent
	 * 
	 * @param node
	 *            the node to test
	 * @param expectedValue
	 *            the expected test value
	 * @return true if the test equals the expected value
	 */
	private boolean isNonRootLeaf(TreeNode<?, ?> node, boolean expectedValue) {
		boolean testValue = node.getParentTransition() != null
							&& node.getChildTransitions().isEmpty();
		LOG.log(Level.FINEST,
				"isNonRootLeaf test - test result: {0}, expected: {1}",
				new Object[] { testValue, expectedValue });
		return testValue == expectedValue;

	}
	
	/**
	 * tests if the node is a root node (has no parent transition)
	 * 
	 * @param node
	 *            the node to test
	 * @param expectedValue
	 *            the expected test value
	 * @return true if the test equals the expected value
	 */
	private boolean isRoot(TreeNode<?, ?> node, boolean expectedValue) {
		boolean testValue = node.getParentTransition() == null;
		LOG.log(Level.FINEST,
				"isRoot test - test result: {0}, expected: {1}",
				new Object[] { testValue, expectedValue });
		return testValue == expectedValue;

	}

}
