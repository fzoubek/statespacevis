package at.bitandart.zoubek.statespace.vis.outline;

import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;

/**
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisOutline extends ContentOutlinePage {

	/**
	 * the model to outline on this page
	 */
	private StateSpacePathVis sspvis;

	private CheckboxTreeViewer treeViewer;

	/**
	 * creates a new outline page for the given {@link StateSpacePathVis} model.
	 * 
	 * @param sspvis
	 *            the model to outline
	 */
	public SSPVisOutline(StateSpacePathVis sspvis) {
		this.sspvis = sspvis;
	}

	public void createControl(Composite parent) {
		treeViewer = new CheckboxTreeViewer(parent, getTreeStyle());
		treeViewer.addSelectionChangedListener(this);

		treeViewer.setContentProvider(new SSPVisOutlineContentProvider());
		treeViewer.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {

				// TODO implement label mechanism for unknown instances

				if (element instanceof LifelineGroupsContainer) {
					return "Lifeline groups";
				} else if (element instanceof LifelineGroup) {
					return ((LifelineGroup) element).getName();
				} else if (element instanceof ObjectLifeline) {
					ObjectDescriptor objectDescriptor = ((ObjectLifeline) element)
							.getObjectDescriptor();
					return objectDescriptor.getInstanceName() + " : "
							+ objectDescriptor.getClassName();
				}

				// fallback
				return element.toString();
			}

			@Override
			public Image getImage(Object element) {

				if (element instanceof ObjectLifeline) {
					if (((ObjectLifeline) element).getParentLifeline() != null) {
						return JavaUI
								.getSharedImages()
								.getImage(
										org.eclipse.jdt.ui.ISharedImages.IMG_FIELD_PRIVATE);
					} else {
						// TODO replace image
						return PlatformUI
								.getWorkbench()
								.getSharedImages()
								.getImage(
										org.eclipse.ui.ISharedImages.IMG_OBJ_ELEMENT);
					}
				} else if (element instanceof LifelineGroupsContainer) {
					// TODO replace image
					return PlatformUI
							.getWorkbench()
							.getSharedImages()
							.getImage(
									org.eclipse.ui.ISharedImages.IMG_OBJ_ELEMENT);
				} else if (element instanceof LifelineGroup) {
					// TODO replace image
					return PlatformUI
							.getWorkbench()
							.getSharedImages()
							.getImage(
									org.eclipse.ui.ISharedImages.IMG_OBJ_FOLDER);
				}

				return null;
			}
		});
		treeViewer.addCheckStateListener(new ICheckStateListener() {
			public void checkStateChanged(CheckStateChangedEvent event) {

				Object element = event.getElement();
				if (element instanceof Lifeline) {
					Lifeline lifeline = (Lifeline) element;
					if (lifeline.getParentLifeline() == null
							|| (lifeline.getParentLifeline() != null && lifeline
									.getParentLifeline().isVisible())) {
						// a lifeline is only visible if the parent lifeline is
						// also visible
						((Lifeline) element).setVisible(event.getChecked());
					}
				}else if (element instanceof LifelineGroup) {
					// set all direct child lifelines visible
					LifelineGroup group = (LifelineGroup) event.getElement();
					for (Lifeline lifeline : group.getLifelines()) {
						lifeline.setVisible(event.getChecked());
					}
				} else if (element instanceof LifelineGroupsContainer) {
					// set all direct children of all groups visible
					for (LifelineGroup group : (LifelineGroupsContainer) element) {
						for (Lifeline lifeline : group.getLifelines()) {
							lifeline.setVisible(event.getChecked());
						}
					}
				}

				treeViewer.refresh();
			}
		});
		treeViewer.setCheckStateProvider(new ICheckStateProvider() {

			@Override
			public boolean isGrayed(Object element) {
				return false;
			}

			@Override
			public boolean isChecked(Object element) {
				if (element instanceof Lifeline) {
					return ((Lifeline) element).isVisible();
				} else if (element instanceof LifelineGroup) {
					for (Lifeline lifeline : ((LifelineGroup) element)
							.getLifelines()) {
						if (!lifeline.isVisible()) {
							return false;
						}
					}
					return true;
				} else if (element instanceof LifelineGroupsContainer) {
					for (LifelineGroup group : (LifelineGroupsContainer) element) {
						if (!isChecked(group)) {
							return false;
						}
					}
					return true;
				}
				return false;
			}
		});
		treeViewer.setInput(sspvis);
		treeViewer.expandAll();
	}

	/*
	 * overridden to use the correct tree viewer
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.contentoutline.ContentOutlinePage#getSelection()
	 */
	@Override
	public ISelection getSelection() {
		if (treeViewer == null) {
			return StructuredSelection.EMPTY;
		}

		return treeViewer.getSelection();
	}

	/*
	 * overridden to use the correct tree viewer
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#getControl()
	 */
	@Override
	public Control getControl() {
		if (treeViewer == null) {
			return null;
		}
		return treeViewer.getControl();
	}

	@Override
	protected TreeViewer getTreeViewer() {
		return treeViewer;
	}

	/*
	 * overridden to use the correct tree viewer
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#setFocus()
	 */
	@Override
	public void setFocus() {
		treeViewer.getControl().setFocus();
	}

	/*
	 * overridden to use the correct tree viewer
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.views.contentoutline.ContentOutlinePage#setSelection(org
	 * .eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void setSelection(ISelection selection) {
		if (treeViewer != null) {
			treeViewer.setSelection(selection);
		}
	}
}
