package at.bitandart.zoubek.statespace.vis.outline;

import java.util.ArrayList;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;

/**
 * Container for a list of {@link LifelineGroup}s.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class LifelineGroupsContainer extends ArrayList<LifelineGroup> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6322105810197471245L;

}
