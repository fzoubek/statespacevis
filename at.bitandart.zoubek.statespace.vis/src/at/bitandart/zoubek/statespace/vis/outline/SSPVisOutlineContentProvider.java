package at.bitandart.zoubek.statespace.vis.outline;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;

/**
 * {@link ITreeContentProvider} for the {@link StateSpacePathVis} model.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisOutlineContentProvider implements ITreeContentProvider {

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof List) {
			return true;
		} else if (element instanceof LifelineGroup) {
			return !((LifelineGroup) element).getLifelines().isEmpty();
		} else if (element instanceof Lifeline) {
			return !((Lifeline) element).getChildLifelines().isEmpty();
		}
		return false;
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public Object[] getElements(Object inputElement) {

		if (inputElement instanceof StateSpacePathVis) {
			// currently only lifeline groups and their lifelines will be
			// outlined
			LifelineGroupsContainer lifelineGroups = new LifelineGroupsContainer();
			lifelineGroups.addAll(((StateSpacePathVis) inputElement)
					.getLifelineGroups());

			return new Object[] { lifelineGroups };
		}
		return new Object[0];
	}

	@Override
	public Object[] getChildren(Object parentElement) {

		if (parentElement instanceof LifelineGroupsContainer) {
			return ((LifelineGroupsContainer) parentElement).toArray();
		} else if (parentElement instanceof LifelineGroup) {
			return ((LifelineGroup) parentElement).getLifelines().toArray();
		} else if (parentElement instanceof Lifeline) {
			return ((Lifeline) parentElement).getChildLifelines().toArray();
		}

		// fallback, return empty array
		return new Object[0];
	}
}