/**
 * 
 */
package at.bitandart.zoubek.statespace.vis;

import org.eclipse.jface.resource.StringConverter;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import at.bitandart.zoubek.statespace.vis.sspvis.listener.SSPVisEditorListener;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpaceVisPlugin extends AbstractUIPlugin {

	/**
	 * id of the extension point for {@link SSPVisEditorListener}s
	 */
	public static final String			EXT_POINT_SSPVIS_LISTENER_ID		= "at.bitandart.zoubek.statespace.vis.ssp.sspvislistener";

	/**
	 * the plugin id
	 */
	public static final String			PLUGIN_ID							= "at.bitandart.zoubek.statespace.vis";

	/**
	 * plugin preference node path (defaults to the plugin id)
	 */
	public static final String			PREF_NODE_PLUGIN					= PLUGIN_ID;

	// preference keys

	/**
	 * primary color for object lifelines (as String, use
	 * {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String			PREF_OBJ_LIFELINE_COLOR_1			= "objectLifelineColor1";
	/**
	 * secondary color for object lifelines (as String, use
	 * {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String			PREF_OBJ_LIFELINE_COLOR_2			= "objectLifelineColor2";
	/**
	 * primary color for selected object lifelines (as String, use
	 * {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String			PREF_OBJ_LIFELINE_SELECTION_COLOR_1	= "objectLifelineSelectionColor1";
	/**
	 * secondary color for selected object lifelines (as String, use
	 * {@link StringConverter#asRGB(String)} to restore value)
	 */
	public static final String			PREF_OBJ_LIFELINE_SELECTION_COLOR_2	= "objectLifelineSelectionColor2";

	/**
	 * singleton instance
	 */
	private static StateSpaceVisPlugin	instance;

	/**
	 * @return the default plugin instance
	 */
	public static StateSpaceVisPlugin getInstance() {
		return instance;
	}

	public StateSpaceVisPlugin() {
		super();
		instance = this;
	}

}
