/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.layout;

import org.eclipse.draw2d.IFigure;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class TreeLayoutMarker {
	private boolean leaf = false;
	private IFigure prevFigure = null;

	/**
	 * @return the leaf
	 */
	public boolean isLeaf() {
		return leaf;
	}

	/**
	 * @param leaf
	 *            the leaf to set
	 */
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	/**
	 * @return the prevFigure
	 */
	public IFigure getPrevFigure() {
		return prevFigure;
	}

	/**
	 * @param prevFigure
	 *            the prevFigure to set
	 */
	public void setPrevFigure(IFigure prevFigure) {
		this.prevFigure = prevFigure;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TreeLayoutMarker [leaf=" + leaf + ", prevFigure=" + prevFigure
				+ "]";
	}
	
}
