/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.layout;

import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.draw2d.geometry.PrecisionRectangle;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class LifelineLayout extends AbstractLayout {

	private static Logger	LOG				= Logger.getLogger(LifelineLayout.class.getCanonicalName());

	private double			lifelineHeight	= 15.0;

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.LayoutManager#layout(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public void layout(IFigure container) {
		LOG.fine("Calculating figure layout..");
		List<?> children = container.getChildren();
		Rectangle clientArea = container.getClientArea();

		ListIterator<?> childIterator = children.listIterator();
		int childCounter = 0;

		while (childIterator.hasNext()) {
			IFigure child = (IFigure) childIterator.next();
			if (!child.isVisible()) {
				continue;
			}
			Dimension childPreferredSize = child.getPreferredSize();

			PrecisionRectangle newBounds = new PrecisionRectangle(	clientArea.preciseX(),
																	clientArea.preciseY()
																			+ childCounter
																			* lifelineHeight,
																	childPreferredSize.preciseWidth(),
																	childPreferredSize.preciseHeight());
			child.setBounds(newBounds);

			childCounter++;
		}

		LOG.fine("Figure layout completed");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.AbstractLayout#calculatePreferredSize(org.eclipse.draw2d.IFigure, int, int)
	 */
	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		LOG.fine("Calculating preferred size...");

		PrecisionDimension preferredSize = new PrecisionDimension(1000, 1000);
		List<?> children = container.getChildren();

		ListIterator<?> childIterator = children.listIterator();
		int childCounter = 0;

		while (childIterator.hasNext()) {

			IFigure child = (IFigure) childIterator.next();
			if (!child.isVisible()) {
				continue;
			}
			Dimension childPreferredSize = child.getPreferredSize();

			if (childCounter == 0) {
				preferredSize.setSize(childPreferredSize);
			} else {
				if (preferredSize.preciseWidth() < childPreferredSize.preciseWidth()) {
					preferredSize.setPreciseWidth(childPreferredSize.preciseWidth());
				}
				preferredSize.setPreciseHeight(preferredSize.preciseHeight()
												+ lifelineHeight);
			}

			childCounter++;
		}

		LOG.log(Level.FINE,
				"Preferred size calculated: {0}, {1}",
				new Object[] { preferredSize.width,
				               preferredSize.height });
		return preferredSize;
	}

	/**
	 * @return the lifelineHeight
	 */
	public double getLifelineHeight() {
		return lifelineHeight;
	}

	/**
	 * @param lifelineHeight
	 *            the lifelineHeight to set
	 */
	public void setLifelineHeight(double lifelineHeight) {
		this.lifelineHeight = lifelineHeight;
	}

}
