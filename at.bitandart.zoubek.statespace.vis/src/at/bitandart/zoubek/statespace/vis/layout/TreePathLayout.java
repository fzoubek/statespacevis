/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.layout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.draw2d.geometry.PrecisionRectangle;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Layouts figures as a tree from left to right, the children must be ordered in
 * a depth first manner. The tree layout is uniquely defined by the order of the
 * figures. For each figure a constraint of the type TreeLayoutMarker must be
 * set.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class TreePathLayout extends AbstractLayout {

	private static Logger					LOG						= Logger.getLogger(TreePathLayout.class.getCanonicalName());

	private static float					DEFAULT_HEIGHT_SPACING	= 30.0f;
	private static float					DEFAULT_NODE_X_SPACING	= 150.0f;

	private double							heightSpacing			= DEFAULT_HEIGHT_SPACING;
	private double							nodeXSpacing			= DEFAULT_NODE_X_SPACING;

	private double							xOffset					= 0.0;
	private double							yOffset					= 0.0;

	private Map<IFigure, TreeLayoutMarker>	constraints				= new HashMap<>();

	@Override
	public void setConstraint(IFigure child, Object constraint) {
		if (constraint != null && constraint instanceof TreeLayoutMarker) {
			constraints.put(child, (TreeLayoutMarker) constraint);
		}
		super.setConstraint(child, constraint);
	}

	@Override
	public Object getConstraint(IFigure child) {
		return constraints.get(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LayoutManager#layout(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public void layout(IFigure container) {

		LOG.fine("Calculating figure layout..");

		List<?> children = container.getChildren();
		Rectangle clientArea = container.getClientArea();

		ListIterator<?> childIterator = children.listIterator();
		int row = 0;
		int col = 0;
		Map<IFigure, Integer> figureColumns = new HashMap<>();
		Map<IFigure, List<IFigure>> successorMap = new HashMap<>();
		boolean reparent = false;

		// 1. run: compute exact x position, compute y estimation
		IFigure lastFigure = null;
		while (childIterator.hasNext()) {

			IFigure childFigure = (IFigure) childIterator.next();
			TreeLayoutMarker marker = constraints.get(childFigure);

			if (reparent) {
				// leaf has been reached in the last iteration -> restore parent
				lastFigure = marker.getPrevFigure();
				col = figureColumns.get(marker.getPrevFigure()) + 1;
				reparent = false;
			}

			// update the positions
			Dimension preferredChildSize = childFigure.getPreferredSize();
			childFigure.setBounds(new PrecisionRectangle(	xOffset
																	+ clientArea.preciseX()
																	+ col
																	* nodeXSpacing,
															yOffset
																	+ clientArea.preciseY()
																	+ row
																	* heightSpacing
																	+ row
																	* preferredChildSize.preciseHeight(),
															preferredChildSize.preciseWidth(),
															preferredChildSize.preciseHeight()));
			figureColumns.put(childFigure, col);
			col++;

			// update successor map
			if (lastFigure != null) {
				List<IFigure> successors = successorMap.get(lastFigure);
				if (successors == null) {
					successors = new ArrayList<IFigure>();
					successorMap.put(lastFigure, successors);
				}
				successors.add(childFigure);
			}

			if (marker.isLeaf()) {
				// leaf reached, the parent of the next figure must be restored
				// in the next iteration
				row++;
				reparent = true;
			}
			lastFigure = childFigure;
		}

		// 2. run: compute exact y position (center vertically)
		childIterator = children.listIterator(children.size());
		while (childIterator.hasPrevious()) {
			IFigure childFigure = (IFigure) childIterator.previous();
			List<IFigure> successors = successorMap.get(childFigure);

			/*
			 * while(successors != null && successors.size() == 1){ successors =
			 * successorMap.get(successors.get(0)); }
			 */

			if (successors != null && successors.size() > 1) {
				double y = 0;
				for (IFigure successor : successors) {
					y += successor.getBounds().preciseY();
				}
				y = y / successors.size();
				Rectangle bounds = childFigure.getBounds();
				childFigure.setBounds(new PrecisionRectangle(	bounds.preciseX(),
																y,
																bounds.preciseWidth(),
																bounds.preciseHeight()));
			} else if (successors != null && successors.size() == 1) {
				Rectangle bounds = childFigure.getBounds();
				childFigure.setBounds(new PrecisionRectangle(	bounds.preciseX(),
																successors.get(0)
																			.getBounds()
																			.preciseY(),
																bounds.preciseWidth(),
																bounds.preciseHeight()));
			}
		}

		LOG.fine("Figure layout completed");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.draw2d.AbstractLayout#calculatePreferredSize(org.eclipse.
	 * draw2d.IFigure, int, int)
	 */
	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		LOG.fine("Calculating preferred size...");

		double width = 0;
		double height = yOffset;

		List<?> children = container.getChildren();

		ListIterator<?> childIterator = children.listIterator();
		int row = 0;
		int col = 0;
		Map<IFigure, Integer> figureColumns = new HashMap<>();
		Map<IFigure, List<IFigure>> successorMap = new HashMap<>();
		boolean reparent = false;

		IFigure lastFigure = null;
		while (childIterator.hasNext()) {

			IFigure childFigure = (IFigure) childIterator.next();
			TreeLayoutMarker marker = constraints.get(childFigure);

			if (reparent) {
				// leaf has been reached in the last iteration -> restore parent
				lastFigure = marker.getPrevFigure();
				col = figureColumns.get(marker.getPrevFigure()) + 1;
				reparent = false;
			}
			// update the positions
			Dimension preferredChildSize = childFigure.getPreferredSize();
			double currentWidth = xOffset
									+ col
									* nodeXSpacing
									+ preferredChildSize.preciseWidth();
			if (currentWidth > width) {
				width = currentWidth;
			}
			double currentHeight = yOffset
									+ row
									* heightSpacing + row
									* preferredChildSize.preciseHeight()
									+ preferredChildSize.preciseHeight();
			if (currentHeight > height) {
				height = currentHeight;
			}

			figureColumns.put(childFigure, col);
			col++;

			// update successor map
			if (lastFigure != null) {
				List<IFigure> successors = successorMap.get(lastFigure);
				if (successors == null) {
					successors = new ArrayList<IFigure>();
					successorMap.put(lastFigure, successors);
				}
				successors.add(childFigure);
			}

			if (marker.isLeaf()) {
				// leaf reached, the parent of the next figure must be restored
				// in the next iteration
				row++;
				reparent = true;
			}
			lastFigure = childFigure;
		}

		if (wHint >= 0) {
			width = wHint;
		}
		if (hHint >= 0) {
			height = hHint;
		}

		LOG.log(Level.FINE,
				"Calculated preferred size: {0}, {1}",
				new Object[] { width, height });

		return new PrecisionDimension(width, height);
	}

	/**
	 * @return the offset in x direction
	 */
	public double getOffsetX() {
		return xOffset;
	}

	/**
	 * @param xOffset
	 *            the offset in x direction to set
	 */
	public void setOffsetX(double xOffset) {
		this.xOffset = xOffset;
	}

	/**
	 * @return the offset in y direction
	 */
	public double getOffsetY() {
		return yOffset;
	}

	/**
	 * @param yOffset
	 *            offset in y direction to set
	 */
	public void setOffsetY(double yOffset) {
		this.yOffset = yOffset;
	}

	/**
	 * @return the heightSpacing
	 */
	public double getHeightSpacing() {
		return heightSpacing;
	}

	/**
	 * @param d
	 *            the heightSpacing to set
	 */
	public void setHeightSpacing(double d) {
		this.heightSpacing = d;
	}

	/**
	 * @return the nodeXSpacing
	 */
	public double getNodeXSpacing() {
		return nodeXSpacing;
	}

	/**
	 * @param nodeXSpacing
	 *            the nodeXSpacing to set
	 */
	public void setNodeXSpacing(double nodeXSpacing) {
		this.nodeXSpacing = nodeXSpacing;
	}

}
