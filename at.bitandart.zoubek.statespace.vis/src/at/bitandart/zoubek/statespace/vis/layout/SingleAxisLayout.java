/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.layout;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.draw2d.geometry.PrecisionRectangle;
import org.eclipse.draw2d.geometry.Rectangle;

import at.bitandart.zoubek.statespace.vis.figures.AxisConstraint;
import at.bitandart.zoubek.statespace.vis.figures.AxisConstraint.PlacementMethod;

/**
 * </p> Layout that aligns child figures along a given axis (major axis) and
 * position them on a fixed y value or centers them on the other axis (minor
 * axis). Constraints must be instances of {@link AxisConstraint}</p>
 * <p>
 * <b>IMPORTANT NOTE:</b> By default the preferred size of the figure is
 * considered, however, if a constraint has been set, the preferred, minimal,
 * and maximal size provided by the figure is ignored.
 * </p>
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SingleAxisLayout extends AbstractLayout {

	private static Logger					LOG			= Logger.getLogger(SingleAxisLayout.class.getCanonicalName());

	private Map<IFigure, AxisConstraint>	constraints	= new HashMap<>();

	private boolean							horizontal	= true;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LayoutManager#layout(org.eclipse.draw2d.IFigure )
	 */
	@Override
	public void layout(IFigure container) {

		LOG.fine("Calculating figure layout..");

		List<?> children = container.getChildren();
		Rectangle clientArea = container.getClientArea();

		ListIterator<?> childIterator = children.listIterator();
		IFigure lastFigure = null;
		while (childIterator.hasNext()) {

			IFigure childFigure = (IFigure) childIterator.next();

			LOG.log(Level.FINER, "Layouting child: {0}", childFigure);

			if (!childFigure.isVisible()) {
				continue;
			}
			AxisConstraint constraint = (AxisConstraint) getConstraint(childFigure);
			Dimension preferredSize = childFigure.getPreferredSize();

			PrecisionRectangle lastBounds = new PrecisionRectangle(	clientArea.preciseX(),
																	clientArea.preciseY(),
																	0.0,
																	0.0);
			if (lastFigure != null) {
				lastBounds.setBounds(lastFigure.getBounds());
			}

			if (constraint == null) {
				constraint = new AxisConstraint();
			}

			LOG.log(Level.FINEST,
					"Using the following AxisConstraint: {0}",
					constraint);

			PrecisionRectangle newBounds = new PrecisionRectangle();
			Dimension dimensionConstraint = constraint.getChildDimension();

			if (dimensionConstraint.preciseWidth() >= 0) {
				newBounds.setPreciseWidth(dimensionConstraint.preciseWidth());
			} else {
				newBounds.setPreciseWidth(preferredSize.preciseWidth());
			}

			if (dimensionConstraint.preciseHeight() >= 0) {
				newBounds.setPreciseHeight(dimensionConstraint.preciseHeight());
			} else {
				newBounds.setPreciseHeight(preferredSize.preciseHeight());
			}

			if (horizontal) {
				newBounds.setPreciseX(lastBounds.preciseX()
										+ lastBounds.preciseWidth());
				if (constraint.getMinorAxisPlacementMethod() == PlacementMethod.CENTER) {
					// center child on the minor axis (vertical)
					newBounds.setPreciseY(clientArea.preciseY()
											+ clientArea.preciseHeight() / 2
											- preferredSize.preciseHeight()
											/ 2.0);
				} else if (constraint.getMinorAxisPlacementMethod() == PlacementMethod.FIXED) {
					// fixed placement on the minor axis (vertical)
					newBounds.setPreciseY(constraint.getMinorAxisFixedValue());
				}
			} else {
				if (constraint.getMinorAxisPlacementMethod() == PlacementMethod.CENTER) {
					// center child on the minor axis (horizontal)
					newBounds.setPreciseX(clientArea.preciseX()
											+ clientArea.preciseWidth() / 2
											- preferredSize.preciseWidth()
											/ 2.0);

				} else if (constraint.getMinorAxisPlacementMethod() == PlacementMethod.FIXED) {
					// fixed placement on the minor axis (horizontal)
					newBounds.setPreciseX(constraint.getMinorAxisFixedValue());
				}
				newBounds.setPreciseY(lastBounds.preciseY()
										+ lastBounds.preciseHeight());
			}

			childFigure.setBounds(newBounds);

			lastFigure = childFigure;
		}

		LOG.fine("Figure layout completed");
	}

	@Override
	public void setConstraint(IFigure child, Object constraint) {
		if (constraint instanceof AxisConstraint) {
			constraints.put(child, (AxisConstraint) constraint);
		}
	}

	@Override
	public Object getConstraint(IFigure child) {
		return constraints.get(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.AbstractLayout#calculatePreferredSize(org.eclipse.draw2d.IFigure, int, int)
	 */
	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {

		LOG.fine("Calculating preferredSize...");

		List<?> children = container.getChildren();
		PrecisionDimension preferredDimension = new PrecisionDimension();
		Rectangle clientArea = container.getClientArea();

		ListIterator<?> childIterator = children.listIterator();
		while (childIterator.hasNext()) {

			IFigure childFigure = (IFigure) childIterator.next();
			if (!childFigure.isVisible()) {
				continue;
			}
			AxisConstraint constraint = (AxisConstraint) getConstraint(childFigure);
			Dimension preferredChildSize = childFigure.getPreferredSize();

			if (constraint == null) {
				constraint = new AxisConstraint();
			}

			Dimension dimensionConstraint = constraint.getChildDimension();
			if (horizontal) {
				if (dimensionConstraint.preciseWidth() >= 0) {
					preferredDimension.setPreciseWidth(preferredDimension.preciseWidth()
														+ dimensionConstraint.preciseWidth());
				} else {
					preferredDimension.setPreciseWidth(preferredDimension.preciseWidth()
														+ preferredChildSize.preciseWidth());
				}

				if (dimensionConstraint.preciseHeight() >= 0) {
					// if the placement method is set to fixed, the needed height is the sum of the fixed value and the constraint height 
					double height = dimensionConstraint.preciseHeight()
									+ ((constraint.getMinorAxisPlacementMethod() == PlacementMethod.FIXED && constraint.getMinorAxisFixedValue() > 0)
											? constraint.getMinorAxisFixedValue()
												- clientArea.preciseY() : 0);
					if (preferredDimension.preciseHeight() < height) {
						preferredDimension.setPreciseHeight(height);
					}
				} else {
					// if the placement method is set to fixed, the needed height is the sum of the fixed value and the preferred height 
					LOG.log(Level.SEVERE,
							"{0}, {1}, {2}, {3}",
							new Object[] {
									preferredChildSize.preciseHeight(),
									constraint.getMinorAxisPlacementMethod() == PlacementMethod.FIXED,
									constraint.getMinorAxisFixedValue(),
									clientArea.preciseY() });
					double height = preferredChildSize.preciseHeight()
									+ ((constraint.getMinorAxisPlacementMethod() == PlacementMethod.FIXED && constraint.getMinorAxisFixedValue() > 0)
											? constraint.getMinorAxisFixedValue()
												- clientArea.preciseY() : 0);
					LOG.log(Level.SEVERE, "{0}", new Object[] { height });
					if (preferredDimension.preciseHeight() < height) {
						preferredDimension.setPreciseHeight(height);
					}
				}

			} else {
				if (dimensionConstraint.preciseHeight() >= 0) {
					preferredDimension.setPreciseHeight(preferredDimension.preciseHeight()
														+ dimensionConstraint.preciseHeight());
				} else {
					preferredDimension.setPreciseHeight(preferredDimension.preciseHeight()
														+ preferredChildSize.preciseHeight());
				}

				if (dimensionConstraint.preciseWidth() >= 0) {
					// if the placement method is set to fixed, the needed width is the sum of the fixed value and the constraint width 
					double width = dimensionConstraint.preciseWidth()
									+ ((constraint.getMinorAxisPlacementMethod() == PlacementMethod.FIXED && constraint.getMinorAxisFixedValue() > 0)
											? constraint.getMinorAxisFixedValue()
												- clientArea.preciseX() : 0);
					if (preferredDimension.preciseWidth() < width) {
						preferredDimension.setPreciseWidth(width);
					}
				} else {
					// if the placement method is set to fixed, the needed width is the sum of the fixed value and the preferred width 
					double width = preferredChildSize.preciseWidth()
									+ ((constraint.getMinorAxisPlacementMethod() == PlacementMethod.FIXED && constraint.getMinorAxisFixedValue() > 0)
											? constraint.getMinorAxisFixedValue()
												- clientArea.preciseX() : 0);
					if (preferredDimension.preciseWidth() < width) {
						preferredDimension.setPreciseWidth(width);
					}
				}
			}

		}

		LOG.log(Level.FINE,
				"Preferred size calculated: {0}, {1}",
				new Object[] { preferredDimension.width,
						preferredDimension.height });
		return preferredDimension;
	}

	/**
	 * @return the true if the horizontal axis is the major axis, false if the
	 *         vertical axis is the major axis
	 */
	public boolean isHorizontal() {
		return horizontal;
	}

	/**
	 * @param horizontal
	 *            true if the horizontal axis is the major axis, false if the
	 *            vertical axis is the major axis
	 */
	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}

}
