/**
 * 
 */
package at.bitandart.zoubek.statespace.vis;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class MainPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	/**
	 * Creates a new preference page with an empty title and no image.
	 */
	public MainPreferencePage() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(StateSpaceVisPlugin.getInstance()
												.getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {
		addField(new ColorFieldEditor(	StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_1,
										"Primary object lifeline color:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_2,
										"Secondary object lifeline color:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_1,
										"Primary selected object lifeline color:",
										getFieldEditorParent()));
		addField(new ColorFieldEditor(	StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_2,
										"Secondary selected object lifeline color:",
										getFieldEditorParent()));

	}

}
