/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PrecisionPoint;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 *
 */
public class CenteredAnchor extends AbstractConnectionAnchor {
	
	public CenteredAnchor(IFigure owner) {
		super(owner);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.ConnectionAnchor#getLocation(org.eclipse.draw2d.geometry.Point)
	 */
	@Override
	public Point getLocation(Point reference) {
		PrecisionPoint point = new PrecisionPoint(getOwner().getBounds().getCenter());
		getOwner().translateToAbsolute(point);
		return point;
	}
	
	

}
