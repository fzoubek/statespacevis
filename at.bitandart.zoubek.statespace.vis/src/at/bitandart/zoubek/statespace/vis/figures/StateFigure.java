/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateFigure extends Figure {
	
	public enum StateType {
		NORMAL, INITIAL, END
	}

	StateType		type		= StateType.NORMAL;

	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		graphics.pushState();
		Rectangle optimizedBounds = getOptimizedRect(	graphics.getLineWidthFloat(),
														getBounds());
		switch (type) {
			case NORMAL:
				graphics.fillOval(optimizedBounds);
				graphics.drawOval(optimizedBounds);
				break;
			case INITIAL:
				graphics.setBackgroundColor(graphics.getForegroundColor());
				graphics.fillOval(getBounds().getCopy());
				break;
			case END:
				graphics.drawOval(optimizedBounds);
				graphics.setBackgroundColor(graphics.getForegroundColor());
				graphics.fillOval(getBounds().getCopy().shrink(3, 3));
				break;
			default:
				Rectangle bounds = getBounds().getCopy();
				graphics.drawLine(bounds.getTopLeft(), bounds.getBottomRight());
				graphics.drawLine(bounds.getTopRight(), bounds.getBottomLeft());
				break;
		}
		graphics.popState();
	}

	/**
	 * returns the rectangle respecting the given line width
	 * 
	 * @param lineWidth
	 * @param rect
	 *            the rectangle to optimize
	 * @return rectangle with respect to the given line width
	 * 
	 * @see Ellipse#getOptimizedBounds
	 */
	private Rectangle getOptimizedRect(float lineWidth, Rectangle rect) {
		float lineInset = Math.max(1.0f, lineWidth) / 2.0f;
		int inset1 = (int) Math.floor(lineInset);
		int inset2 = (int) Math.ceil(lineInset);

		Rectangle r = new Rectangle(rect);
		r.x += inset1;
		r.y += inset1;
		r.width -= inset1 + inset2;
		r.height -= inset1 + inset2;
		return r;
	}

	/**
	 * @return the state type
	 */
	public StateType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the state type to set
	 */
	public void setType(StateType type) {
		this.type = type;
	}
}
