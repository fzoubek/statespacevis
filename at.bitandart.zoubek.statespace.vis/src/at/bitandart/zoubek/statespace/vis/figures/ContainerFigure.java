package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;

/**
 * represents a figure that holds any number of child figures. It has no
 * graphical representation and it delegates the
 * {@link #containsPoint(int, int)} to its children. The main prupose of this
 * Figure is to allow overlapping of {@link ContainerFigure}s and let child
 * figures receive mouse events - which is not possible with the original
 * implementation of {@link Figure}.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class ContainerFigure extends Figure {

	@Override
	public boolean containsPoint(int x, int y) {
		for (Object childObj : getChildren()) {
			if (childObj instanceof IFigure
				&& ((IFigure) childObj).containsPoint(x, y)) {
				return true;
			}
		}
		return false;
	}
}
