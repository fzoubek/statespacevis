/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

/**
 * A connection router that routes connections in a loop when target and end
 * figure are the same. An alternative router can be defined to route
 * connections where the target and the source figure are not the same. By the
 * default a {@link NullConnectionRouter} is used as alternative router.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class LoopConnectionRouter extends BezierConnectionRouter {

	private static Logger		LOG					= Logger.getLogger(LoopConnectionRouter.class.getCanonicalName());

	private ConnectionRouter	alternativeRouter	= ConnectionRouter.NULL;

	private Point				controlpoint1Offset	= new Point(80, 0);
	private Point				controlpoint2Offset	= new Point(40, 80);

	/**
	 * 
	 * @param controlpoint1Offset
	 *            the offset for the first controlpoint of the loop. The
	 *            absolute position of a control point will be cropped to 0
	 * @param controlpoint2Offset
	 *            the offset for the second controlpoint of the loop. The
	 *            absolute position of a control point will be cropped to 0
	 */
	public LoopConnectionRouter(Point controlpoint1Offset,
			Point controlpoint2Offset) {
		this.controlpoint1Offset = controlpoint1Offset;
		this.controlpoint2Offset = controlpoint2Offset;
	}

	@Override
	public void route(Connection connection) {
		LOG.log(Level.FINE, "Routing connection {0}", connection);
		if (connection.getTargetAnchor().getOwner() == connection.getSourceAnchor()
																	.getOwner()) {
			LOG.log(Level.FINE, "Routing loop for connection {0}", connection);
			super.route(connection);
		} else {
			alternativeRouter.route(connection);
		}
	}

	@Override
	protected PointList getControlPoints(Point startPoint, Point endPoint) {
		Point controlPoint1 = startPoint.getCopy();
		controlPoint1.x = Math.max(controlPoint1.x + controlpoint1Offset.x, 0);
		controlPoint1.y = Math.max(controlPoint1.y + controlpoint1Offset.y, 0);

		Point controlPoint2 = startPoint.getCopy();
		controlPoint2.x = Math.max(controlPoint2.x + controlpoint2Offset.x, 0);
		controlPoint2.y = Math.max(controlPoint2.y + controlpoint2Offset.y, 0);
		
		PointList controlPoints = new PointList();
		controlPoints.addPoint(startPoint);
		controlPoints.addPoint(controlPoint1);
		controlPoints.addPoint(controlPoint2);
		controlPoints.addPoint(endPoint);
		LOG.log(Level.FINE,
				"Calculated control points: {0}",
				Arrays.toString(controlPoints.toIntArray()));
		return controlPoints;
	}

	/**
	 * @return the alternative router which is used when the target and source
	 */
	public ConnectionRouter getAlternativeRouter() {
		return alternativeRouter;
	}

	/**
	 * @param alternativeRouter
	 *            the alternative router to set
	 */
	public void setAlternativeRouter(ConnectionRouter alternativeRouter) {
		this.alternativeRouter = alternativeRouter;
	}

}
