/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.AbstractRouter;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.PrecisionPoint;

/**
 * Routes connections using a Bezier curve with control points.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class BezierConnectionRouter extends AbstractRouter {

	private static Logger	LOG					= Logger.getLogger(BezierConnectionRouter.class.getCanonicalName());

	private int				approximationSteps	= 2;

	private boolean			dynamicSteps		= true;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.draw2d.ConnectionRouter#route(org.eclipse.draw2d.Connection)
	 */
	@Override
	public void route(Connection connection) {

		LOG.log(Level.FINE,
				"Calculating new bezier route for {0}...",
				connection);

		if (!connection.isVisible()) {
			return;
		}

		PointList points = connection.getPoints();
		points.removeAllPoints();

		Point startPoint = getStartPoint(connection);
		Point endPoint = getEndPoint(connection);
		connection.translateToRelative(startPoint);
		connection.translateToRelative(endPoint);

		PointList controlPoints = getControlPoints(startPoint, endPoint);

		// heuristic to determine the correct approximation steps
		//System.out.println(startPoint.getDistance(controlPoint1) + controlPoint1.getDistance(controlPoint2) + controlPoint2.getDistance(endPoint));
		int steps = approximationSteps;
		if (dynamicSteps) {
			steps = calculateDynamicSteps(controlPoints);
		}

		// create approximation of bezier curve
		for (int i = 0; i <= steps; i++) {
			Point point = calculateBezierPoint(	controlPoints,
												i * (1.0 / (float) steps));
			if ((i > 0 && point.getDistance(points.getLastPoint()) > 8)
				|| i == 0 || i == steps) {
				points.addPoint(point);
			}
		}
		connection.setPoints(points);

		LOG.fine("route calculated");
	}

	/**
	 * calculates the step size if the dynamic step calculation is enabled
	 * 
	 * @param points
	 * @return the number of steps to use
	 */
	private int calculateDynamicSteps(PointList points) {
		double steps = 0;
		for (int i = 1; i < points.size(); i++) {
			steps += points.getPoint(i - 1).getDistance(points.getPoint(i));
		}
		return (int) Math.ceil(steps);
	}

	/**
	 * 
	 * @param startPoint
	 * @param endPoint
	 * @return a point list containing the start point, the control points and
	 *         the end point
	 */
	protected PointList getControlPoints(Point startPoint, Point endPoint) {
		// TODO replace hardcoded control points with control points provided by
		// the constraint mechanism

		double x1 = Math.max(startPoint.preciseX(), endPoint.preciseX());
		double x2 = Math.min(startPoint.preciseX(), endPoint.preciseX());

		double controlPointTranslation = (x1 - x2) / 3;

		Point controlPoint1 = startPoint.getCopy()
										.translate(controlPointTranslation, 0);
		Point controlPoint2 = endPoint.getCopy()
										.translate(-controlPointTranslation, 0);
		PointList controlPoints = new PointList();
		controlPoints.addPoint(startPoint);
		controlPoints.addPoint(controlPoint1);
		controlPoints.addPoint(controlPoint2);
		controlPoints.addPoint(endPoint);
		return controlPoints;
	}

	/**
	 * calculates a point with parameter u on a Bezier curve with the given
	 * control points
	 * 
	 * @param controlPoints
	 *            a list of control points
	 * @param u
	 *            the paramter of the curve point
	 * @return the point with the parameter u
	 */
	private Point calculateBezierPoint(PointList controlPoints, double u) {
		PrecisionPoint point = new PrecisionPoint();
		int n = controlPoints.size() - 1;
		double x = 0.0;
		double y = 0.0;

		// calculate point
		for (int i = 0; i <= n; i++) {
			// compute n!
			double nFac = 1;
			for (int k = 2; k <= n; k++) {
				nFac *= k;
			}

			// compute i!
			double iFac = 1;
			for (int k = 2; k <= i; k++) {
				iFac *= k;
			}

			// compute n-1!
			double niFac = 1;
			for (int k = 2; k <= (n - i); k++) {
				niFac *= k;
			}

			// compute the Bernstein polynomial
			double bernsteinVal = nFac
									/ (iFac * niFac) * Math.pow(u, i)
									* Math.pow((1 - u), n - i);

			Point controlPoint = controlPoints.getPoint(i);
			double xCP = controlPoint.preciseX();
			double yCP = controlPoint.preciseY();

			// blend control point
			x += xCP * bernsteinVal;
			y += yCP * bernsteinVal;

		}
		point.setX((int) Math.round(x));
		point.setY((int) Math.round(y));
		return point;
	}

	/**
	 * @return the number of steps used to compute the approximated bezier curve
	 */
	public int getApproximationSteps() {
		return approximationSteps;
	}

	/**
	 * @param approximationSteps
	 *            the number of steps used to compute the approximated bezier
	 *            curve
	 */
	public void setApproximationSteps(int approximationSteps) {
		this.approximationSteps = approximationSteps;
	}

	/**
	 * @return true if the approximation steps should be calculated during
	 *         routing
	 */
	public boolean isUsingDynamicSteps() {
		return dynamicSteps;
	}

	/**
	 * @param dynamicSteps
	 *            a boolean indicating if the approximation steps should be
	 *            calculated during routing or not. If this is true, the value
	 *            of approximationSteps is ignored
	 */
	public void setDynamicStepsUse(boolean dynamicSteps) {
		this.dynamicSteps = dynamicSteps;
	}

}
