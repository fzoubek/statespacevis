/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * An Anchor that behaves in general like a {@link ChopboxAnchor}. But if the
 * reference point is at the center of the figures bounds, the reference point
 * is chosen as the right of the figures bounds. This usually happens when the
 * connection target and source are the same figure.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class ChopboxRightAnchor extends ChopboxAnchor {

	private Point	offset;

	public ChopboxRightAnchor(IFigure figure) {
		this(figure, new Point());
	}

	/**
	 * 
	 * @param figure
	 * @param offset
	 *            a relative offset, which is used to translate the reference
	 *            point when it is at the center of the owners figure
	 */
	public ChopboxRightAnchor(IFigure figure, Point offset) {
		super(figure);
		this.offset = offset;
	}

	@Override
	public Point getLocation(Point reference) {
		Rectangle r = Rectangle.SINGLETON;
		r.setBounds(getBox());
		r.translate(-1, -1);
		r.resize(1, 1);

		getOwner().translateToAbsolute(r);
		float centerX = r.x + 0.5f * r.width;
		float centerY = r.y + 0.5f * r.height;

		if (reference.x == (int) centerX && reference.y == (int) centerY) {
			reference = r.getRight().translate(offset);
		}
		return super.getLocation(reference);
	}
}
