/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.geometry.Dimension;

import at.bitandart.zoubek.statespace.vis.layout.SingleAxisLayout;

/**
 * Represents a constraint for the {@link SingleAxisLayout}.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class AxisConstraint {

	/**
	 * the dimension constraint for the child, values of -1 indicate
	 * unconstrained values
	 */
	private Dimension		childDimension;

	/**
	 * the placement method to use for the minor axis
	 */
	private PlacementMethod	minorAxisPlacementMethod;

	/**
	 * the fixed value to use if the minor axis placement method is the to
	 * {@link PlacementMethod#FIXED}
	 */
	private double			minorAxisFixedValue;

	/**
	 * Placement method identifiers
	 * 
	 * @author Florian Zoubek <zoubek@bitandart.at>
	 * 
	 */
	public enum PlacementMethod {
		/**
		 * Center child
		 */
		CENTER,
		/**
		 * Position child on a fixed position
		 */
		FIXED
	}

	/**
	 * creates constraint with no dimension constraint, minor axis placement set
	 * to {@link PlacementMethod#CENTER}, and fixed value set to -1
	 */
	public AxisConstraint() {
		this(new Dimension(-1, -1), PlacementMethod.CENTER, -1);
	}

	/**
	 * @param childDimension
	 *            the dimension constraint, values of -1 will be ignored
	 */
	public AxisConstraint(Dimension childDimension) {
		this(childDimension, PlacementMethod.CENTER, -1);
	}

	/**
	 * @param childDimension
	 *            the dimension constraint, values of -1 will be ignored
	 * @param minorAxisPlacementMethod
	 *            the placement method for the minor axis
	 * @param minorAxisFixedValue
	 *            the value to use if the placement method is set to
	 *            {@link PlacementMethod#FIXED}
	 */
	public AxisConstraint(Dimension childDimension,
			PlacementMethod minorAxisPlacementMethod, double minorAxisFixedValue) {
		super();
		this.childDimension = childDimension;
		this.minorAxisPlacementMethod = minorAxisPlacementMethod;
		this.minorAxisFixedValue = minorAxisFixedValue;
	}

	/**
	 * @return the child dimension constraints
	 */
	public Dimension getChildDimension() {
		return childDimension;
	}

	/**
	 * @param childDimension
	 *            the child dimension to use during layout, pass -1 as width
	 *            and/or height to indicate that the value is not set
	 */
	public void setChildDimension(Dimension childDimension) {
		this.childDimension = childDimension;
	}

	/**
	 * @return the placement method used for the minor axis
	 */
	public PlacementMethod getMinorAxisPlacementMethod() {
		return minorAxisPlacementMethod;
	}

	/**
	 * @param minorAxisPlacementMethod
	 *            the placement method used for the minor axis
	 */
	public void setMinorAxisPlacementMethod(
			PlacementMethod minorAxisPlacementMethod) {
		this.minorAxisPlacementMethod = minorAxisPlacementMethod;
	}

	/**
	 * @return the value to use if the placement method is set to
	 *         {@link PlacementMethod#FIXED}
	 */
	public double getMinorAxisFixedValue() {
		return minorAxisFixedValue;
	}

	/**
	 * @param minorAxisFixedValue
	 *            the value to use if the placement method is set to
	 *            {@link PlacementMethod#FIXED}
	 */
	public void setMinorAxisFixedValue(double minorAxisFixedValue) {
		this.minorAxisFixedValue = minorAxisFixedValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AxisConstraint [childDimension="
				+ childDimension + ", minorAxisPlacementMethod="
				+ minorAxisPlacementMethod + ", minorAxisFixedValue="
				+ minorAxisFixedValue + "]";
	}

}
