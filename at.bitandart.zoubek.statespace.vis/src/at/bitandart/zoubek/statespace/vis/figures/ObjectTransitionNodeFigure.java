/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.draw2d.geometry.PrecisionRectangle;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class ObjectTransitionNodeFigure extends Figure {

	public enum FigureState {
		NULL, NOTNULL, CHANGED
	}

	private FigureState	state					= FigureState.NULL;
	private float		nullWidth				= 1.0f;
	private float		notNullWidth			= 5.0f;
	private float		changedDiameter			= 14.0f;
	private boolean		connectedToLeftSide		= true;
	private boolean		connectedToRightSide	= true;

	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle bounds = getBounds().getCopy();

		graphics.setBackgroundColor(getBackgroundColor());
		graphics.setForegroundColor(getForegroundColor());

		// draw side connections
		graphics.pushState();
		switch (state) {
			case CHANGED:
			case NOTNULL:
				graphics.setLineWidth(Math.round(notNullWidth));
				break;
			case NULL:
				graphics.setLineWidth(Math.round(nullWidth));
				break;
		}
		if (connectedToLeftSide) {
			graphics.drawLine(bounds.getLeft(), bounds.getCenter());
		}
		if (connectedToRightSide) {
			graphics.drawLine(bounds.getCenter(), bounds.getRight());
		}
		graphics.popState();

		if (state == FigureState.CHANGED) {

			PrecisionRectangle rect = new PrecisionRectangle(bounds);
			PrecisionPoint center = new PrecisionPoint(	rect.preciseX()
																+ (rect.preciseWidth() / 2.0),
														rect.preciseY()
																+ (rect.preciseHeight() / 2.0));
			float radius = changedDiameter / 2.0f;

			PointList pathPoints = new PointList();
			pathPoints.addPoint(center.getTranslated(-radius, 0));
			pathPoints.addPoint(center.getTranslated(0, -radius));
			pathPoints.addPoint(center.getTranslated(radius, 0));
			pathPoints.addPoint(center.getTranslated(0, radius));
			graphics.fillPolygon(pathPoints);

		} else if (state == FigureState.NULL) {

			PrecisionRectangle rect = new PrecisionRectangle(bounds);
			rect.setPreciseLocation(new PrecisionPoint(	Math.floor(rect.preciseX()
																	+ rect.preciseWidth()
																	/ 2.0
																	- nullWidth
																	/ 2.0 + 1),
														Math.floor(rect.preciseY()
																	+ rect.preciseHeight()
																	/ 2.0
																	- nullWidth
																	/ 2.0 + 1)));
			rect.setPreciseWidth(nullWidth);
			rect.setPreciseHeight(nullWidth);

			graphics.fillOval(rect);

		} else if (state == FigureState.NOTNULL) {

			PrecisionRectangle rect = new PrecisionRectangle(bounds);
			rect.setPreciseLocation(new PrecisionPoint(	Math.floor(rect.preciseX()
																	+ rect.preciseWidth()
																	/ 2.0
																	- notNullWidth
																	/ 2.0 + 1),
														Math.floor(rect.preciseY()
																	+ rect.preciseHeight()
																	/ 2.0
																	- notNullWidth
																	/ 2.0 + 1)));
			rect.setPreciseWidth(notNullWidth);
			rect.setPreciseHeight(notNullWidth);
			graphics.fillOval(rect);

		}

	}

	/**
	 * @return the state which is rendered
	 */
	public FigureState getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to render
	 */
	public void setState(FigureState state) {
		this.state = state;
	}

	/**
	 * @return the width used for FigureState.NULL
	 */
	public float getNullWidth() {
		return nullWidth;
	}

	/**
	 * @param width
	 *            the width used for FigureState.NULL
	 */
	public void setNullWidth(float width) {
		this.nullWidth = width;
	}

	/**
	 * @return the width used for FigureState.NOTNULL
	 */
	public float getNotNullWidth() {
		return notNullWidth;
	}

	/**
	 * @param width
	 *            the width used for FigureState.NOTNULL
	 */
	public void setNotNullWidth(float width) {
		this.notNullWidth = width;
	}

	/**
	 * @return the diameter used for FigureState.CHANGED
	 */
	public float getChangedDiameter() {
		return changedDiameter;
	}

	/**
	 * @param changedDiameter
	 *            the diameter used for FigureState.CHANGED
	 */
	public void setChangedDiameter(float changedDiameter) {
		this.changedDiameter = changedDiameter;
	}

	/**
	 * @return true if the figure should be drawn with an line connecting the
	 *         center and the left side center of the figure, false otherwise.
	 */
	public boolean isConnectedToLeftSide() {
		return connectedToLeftSide;
	}

	/**
	 * @param drawIncomingConnection
	 *            true if the figure should be drawn with an line connecting the
	 *            center and the left side center of the figure, false
	 *            otherwise. The line width is equal to the width used for
	 *            {@link FigureState#NOTNULL} or {@link FigureState#NULL},
	 *            depending on the current figure state. If the state is
	 *            {@link FigureState#CHANGED} the line width is equal to the
	 *            width used for {@link FigureState#NOTNULL}
	 */
	public void setConnectedToLeftSide(boolean drawIncomingConnection) {
		this.connectedToLeftSide = drawIncomingConnection;
	}

	/**
	 * @return true if the figure should be drawn with an line connecting the
	 *         center and the right side center of the figure, false otherwise.
	 */
	public boolean isConnectedToRightSide() {
		return connectedToRightSide;
	}

	/**
	 * @param drawOutgoingConnection
	 *            true if the figure should be drawn with an line connecting the
	 *            center and the right side center of the figure, false. The
	 *            line width is equal to the width used for
	 *            {@link FigureState#NOTNULL} or {@link FigureState#NULL},
	 *            depending on the current figure state. If the state is
	 *            {@link FigureState#CHANGED} the line width is equal to the
	 *            width used for {@link FigureState#NOTNULL}
	 */
	public void setConnectedToRightSide(boolean drawOutgoingConnection) {
		this.connectedToRightSide = drawOutgoingConnection;
	}

}
