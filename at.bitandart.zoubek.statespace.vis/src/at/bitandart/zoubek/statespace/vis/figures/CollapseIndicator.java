/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Toggle;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class CollapseIndicator extends Toggle {

	public CollapseIndicator() {
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// save the current graphics state so we can modify it without side effects
		graphics.pushState();

		// current bounds
		Rectangle bounds = getBounds().getCopy();
		// length of the hypotenuse of the triangle
		int hypLength = Math.min(bounds.width, bounds.height);

		// move origin to the figures coordinates
		graphics.translate(bounds.x+1, bounds.y);
		// rotate triangle if selected
		if (getModel().isSelected()) {
			graphics.translate(hypLength / 2, hypLength / 2);
			graphics.rotate(90.0f);
			graphics.translate(-hypLength / 2, -hypLength / 2);
		}

		// create triangle
		PointList triangle = new PointList();
		triangle.addPoint(0, 0);
		triangle.addPoint(hypLength, hypLength / 2);
		triangle.addPoint(0, hypLength);

		// draw it
		graphics.setBackgroundColor(ColorConstants.black);
		graphics.fillPolygon(triangle);
		//graphics.fillRectangle(0, 0, hypLength, hypLength);

		// restore the state
		graphics.popState();
	}

	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		return new Dimension(10, 10);
	}
}
