/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;

/**
 * Background canvas for the state space path visualization. Responsible for
 * drawing separation lines that should support the user identifying states.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisCanvas extends FreeformLayer {

	// default values

	private static int	DEFAULT_NODE_SPACING				= 150;
	private static int	DEFAULT_SEPARATION_LINE_OFFSET		= 0;
	private static int	DEFAULT_NUM_SEPARATION_LINES		= 0;
	private static int	DEFAULT_SEPARATION_LINE_START_INSET	= 5;
	private static int	DEFAULT_SEPARATION_LINE_END_INSET	= 5;

	/**
	 * the spacing between the separation lines
	 */
	private int			separationLineSpacing				= DEFAULT_NODE_SPACING;

	/**
	 * the additional x offset of the separation lines (separation lines have a
	 * fixed offset of nodeSpacing/2)
	 */
	private int			separationLineOffset				= DEFAULT_SEPARATION_LINE_OFFSET;

	/**
	 * the padding to add to the start point of each separation line
	 */
	private int			separationLineStartInset			= DEFAULT_SEPARATION_LINE_START_INSET;

	/**
	 * the padding to subtract from the end point of each separation line
	 */
	private int			separationLineEndInset				= DEFAULT_SEPARATION_LINE_END_INSET;

	/**
	 * the number of separation lines to draw
	 */
	private int			numSeparationLines					= DEFAULT_NUM_SEPARATION_LINES;

	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		final Rectangle bounds = getBounds();

		graphics.pushState();
		graphics.setLineStyle(SWT.LINE_DASHDOT);
		graphics.setForegroundColor(ColorConstants.lightGray);
		int x = separationLineOffset;
		int y1 = separationLineStartInset;
		int y2 = bounds.height() - separationLineEndInset;
		for (int i = 0; i < numSeparationLines; i++) {
			graphics.drawLine(x, y1, x, y2);
			x += separationLineSpacing;
		}
		graphics.popState();
	}

	// getter/setter

	/**
	 * @return the spacing between the separation lines
	 */
	public int getSeparationLineSpacing() {
		return separationLineSpacing;
	}

	/**
	 * sets the spacing between the separation lines
	 * 
	 * @param separationLineSpacing
	 *            the spacing to set
	 */
	public void setSeparationLineSpacing(int separationLineSpacing) {
		this.separationLineSpacing = separationLineSpacing;
	}

	/**
	 * @return the x offset of the separation lines
	 */
	public int getSeparationLineOffset() {
		return separationLineOffset;
	}

	/**
	 * sets the x offset of the separation lines
	 * 
	 * @param separationLineOffset
	 *            the offset to set
	 */
	public void setSeparationLineOffset(int separationLineOffset) {
		this.separationLineOffset = separationLineOffset;
	}

	/**
	 * @return the number of separation lines to draw
	 */
	public int getNumSeparationLines() {
		return numSeparationLines;
	}

	/**
	 * sets the number of separation lines to draw
	 * 
	 * @param numSeparationLines
	 *            the number of separation lines to draw
	 */
	public void setNumSeparationLines(int numSeparationLines) {
		this.numSeparationLines = numSeparationLines;
	}

}
