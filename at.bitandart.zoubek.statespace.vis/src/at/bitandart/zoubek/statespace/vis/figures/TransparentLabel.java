/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.AbstractBackground;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;

/**
 * A Label that supports transparency settings for the background and the label figures
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class TransparentLabel extends Label {

	private int	backgroundTransparency	= 255;
	private int	transparency			= 255;

	@Override
	protected void paintFigure(Graphics graphics) {
		if (isOpaque()) {
			// set transparency (the figure is technically not opaque then, but it's currently the only way without rewriting the whole Label class)
			graphics.setAlpha(backgroundTransparency);
			graphics.fillRectangle(getBounds());
			if (getBorder() instanceof AbstractBackground)
				((AbstractBackground) getBorder()).paintBackground(	this,
																	graphics,
																	NO_INSETS);
			graphics.setAlpha(transparency);
		}
		Rectangle bounds = getBounds();
		graphics.translate(bounds.x, bounds.y);
		Image icon = getIcon();
		if (icon != null)
			graphics.drawImage(icon, getIconLocation());
		if (!isEnabled()) {
			graphics.translate(1, 1);
			graphics.setForegroundColor(ColorConstants.buttonLightest);
			graphics.drawText(getSubStringText(), getTextLocation());
			graphics.translate(-1, -1);
			graphics.setForegroundColor(ColorConstants.buttonDarker);
		}
		graphics.drawText(getSubStringText(), getTextLocation());
		graphics.translate(-bounds.x, -bounds.y);
		graphics.setAlpha(255);
	}

	/**
	 * @return the transparency value
	 */
	public int getBackgroundTransparency() {
		return backgroundTransparency;
	}

	/**
	 * @param alpha
	 *            the transparency value to set (0 = transparent, 255 = Opaque)
	 */
	public void setBackgroundTransparency(int alpha) {
		this.backgroundTransparency = alpha;
	}

	/**
	 * @return the transparency
	 */
	public int getTransparency() {
		return transparency;
	}

	/**
	 * @param transparency
	 *            the transparency to set
	 */
	public void setTransparency(int transparency) {
		this.transparency = transparency;
	}
}
