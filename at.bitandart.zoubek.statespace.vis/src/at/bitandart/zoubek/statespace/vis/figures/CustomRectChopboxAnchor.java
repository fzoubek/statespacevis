/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * A {@link ChopboxAnchor} that uses a given rectangle (position is treated as
 * relative to the owners bounds) instead of the owners bounds.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class CustomRectChopboxAnchor extends ChopboxAnchor {

	private Rectangle	rect;

	/**
	 * 
	 * @param owner
	 * @param rect
	 *            the rectangle to use instead of the owners bounds. The
	 *            position of the rectangle is treated as relative to the owners
	 *            bounds
	 * @see ChopboxAnchor#ChopboxAnchor(IFigure)
	 */
	public CustomRectChopboxAnchor(IFigure owner, Rectangle rect) {
		super(owner);
		this.rect = rect;
	}

	@Override
	protected Rectangle getBox() {
		Rectangle copy = rect.getCopy();
		Rectangle ownerBounds = getOwner().getBounds();
		copy.x += ownerBounds.x;
		copy.y += ownerBounds.y;
		return copy;
	}
}
