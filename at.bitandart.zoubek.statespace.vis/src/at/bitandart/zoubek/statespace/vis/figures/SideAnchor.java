/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.figures;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * An Anchor that is positioned at the center of the figures side opposite to
 * the reference point
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SideAnchor extends AbstractConnectionAnchor {

	private static final Logger	LOG						= Logger.getLogger(SideAnchor.class.getCanonicalName());

	/**
	 * bit pattern representing the left anchor position
	 */
	public static final int		LEFT					= 1;

	/**
	 * bit pattern representing the top anchor position
	 */
	public static final int		TOP						= LEFT << 1;

	/**
	 * bit pattern representing the right anchor position
	 */
	public static final int		RIGHT					= TOP << 1;

	/**
	 * bit pattern representing the bottom anchor position
	 */
	public static final int		BOTTOM					= RIGHT << 1;

	/**
	 * bit pattern representing all anchor positions
	 */
	public static final int		ALL						= LEFT
															| TOP | RIGHT
															| BOTTOM;

	/**
	 * bit pattern that defines which anchor positions are allowed.
	 */
	private int					allowedAnchorPositions	= ALL;

	/**
	 * creates a anchor for the given figure with no restrictions for the anchor
	 * positions
	 * 
	 * @param figure
	 */
	public SideAnchor(IFigure figure) {
		super(figure);
		allowedAnchorPositions = ALL;
	}

	/**
	 * creates a anchor for the given figure restricted to the given anchor
	 * positions
	 * 
	 * @param figure
	 * @param positions
	 *            bit pattern that defines which anchor positions to use. At
	 *            least one position must be specified, otherwise a
	 *            {@link IllegalArgumentException} is thrown.
	 * 
	 * @see SideAnchor#LEFT
	 * @see SideAnchor#TOP
	 * @see SideAnchor#RIGHT
	 * @see SideAnchor#BOTTOM
	 * @see SideAnchor#ALL
	 */
	public SideAnchor(IFigure figure, int positions) {
		super(figure);
		this.allowedAnchorPositions = positions;
		if ((this.allowedAnchorPositions & ALL) == 0) {
			throw new IllegalArgumentException("At least one anchor position must be specified");
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.ConnectionAnchor#getLocation(org.eclipse.draw2d.geometry.Point)
	 */
	@Override
	public Point getLocation(Point reference) {
		LOG.log(Level.FINE,
				"Calculating anchor location for reference point {0} ...",
				reference);
		Rectangle bounds = getOwner().getBounds();

		List<Point> points = new ArrayList<>(4);

		if ((allowedAnchorPositions & LEFT) == LEFT) {
			Point point = bounds.getLeft().translate(1, 0);
			getOwner().translateToAbsolute(point);
			points.add(point);
		}
		if ((allowedAnchorPositions & TOP) == TOP) {
			Point point = bounds.getTop().translate(0, 1);
			getOwner().translateToAbsolute(point);
			points.add(point);
		}
		if ((allowedAnchorPositions & RIGHT) == RIGHT) {
			Point point = bounds.getRight().translate(-1, 0);
			getOwner().translateToAbsolute(point);
			points.add(point);
		}
		if ((allowedAnchorPositions & BOTTOM) == BOTTOM) {
			Point point = bounds.getBottom().translate(0, -1);
			getOwner().translateToAbsolute(point);
			points.add(point);
		}

		// choose closest allowed point as anchor location
		Point anchor = null;
		double minDistance = -1;
		for (Point point : points) {
			double distance = point.getDistance(reference);
			if (minDistance < 0) {
				minDistance = distance;
				anchor = point;
			} else if (distance < minDistance) {
				minDistance = distance;
				anchor = point;
			}
		}

		LOG.log(Level.FINE, "Calculated anchor location {0}", anchor);
		return anchor;
	}
}
