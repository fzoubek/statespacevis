package at.bitandart.zoubek.statespace.vis.views;

import java.util.logging.Logger;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import at.bitandart.zoubek.statespace.vis.editors.StateSpacePathVisEditor;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateSpaceEP;
import at.bitandart.zoubek.statespace.vis.gef.factories.StateSpaceEditPartFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;

/**
 * A view that visualizes a computed state space
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpaceView extends ViewPart implements IPartListener2, ITabbedPropertySheetPageContributor {

	private static final Logger			LOG		= Logger.getLogger(StateSpaceView.class.getCanonicalName());

	public static final String			VIEW_ID	= "at.bitandart.zoubek.statespace.vis.views.StateSpace";

	private EditDomain					editDomain;

	/**
	 * the GEF Viewer component
	 */
	private GraphicalViewer				viewer;

	/**
	 * the GEF EditPart factory
	 */
	private StateSpaceEditPartFactory	editPartFactory;

	private StateSpacePathVisEditor		currentEditorInstance;

	public StateSpaceView() {
	}

	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
		IPartService service = PlatformUI.getWorkbench()
											.getActiveWorkbenchWindow()
											.getPartService();
		service.addPartListener(this);
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {

		// handle created GEF elements
		if (adapter == GraphicalViewer.class || adapter == EditPartViewer.class)
			return getGraphicalViewer();
		else if (adapter == CommandStack.class)
			return getEditDomain().getCommandStack();
		else if (adapter == EditDomain.class)
			return getEditDomain();
		else if (adapter == IPropertySheetPage.class)
			return new TabbedPropertySheetPage(this);

		return super.getAdapter(adapter);
	}

	@Override
	public void createPartControl(Composite parent) {

		viewer = createGraphicalViewer(parent);
	}

	/**
	 * creates and initializes a new GraphicalViewer and assigns it as a child
	 * of <code>parent</code>.
	 * 
	 * @param parent
	 *            the parent SWT component
	 * @return the viewer instance
	 */
	private GraphicalViewer createGraphicalViewer(Composite parent) {

		GraphicalViewer viewer = new ScrollingGraphicalViewer();
		viewer.createControl(parent);

		// configure the viewer
		viewer.getControl().setBackground(parent.getBackground());
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));
		
		// context menu
		MenuManager menuProvider = new MenuManager();
	    viewer.setContextMenu(menuProvider);
	    getSite().registerContextMenu(menuProvider, viewer);
			    
		// set viewer as selection Provider
		getSite().setSelectionProvider(viewer);

		// set the edit part factory
		viewer.setEditPartFactory(getEditPartFactory());

		// initialize the viewer with the the model
		viewer.setContents(null);

		return viewer;
	}

	/**
	 * 
	 * @return the GEF Viewer component
	 */
	private GraphicalViewer getGraphicalViewer() {
		return viewer;
	}

	/**
	 * 
	 * @return the <code>EditDomain</code> used by this editor
	 */
	private EditDomain getEditDomain() {
		if (editDomain == null) {
			editDomain = new EditDomain();
		}
		return editDomain;
	}

	/**
	 * 
	 * @return the <code>EditPart</code>-Factory used by this editor
	 */
	private EditPartFactory getEditPartFactory() {
		if (editPartFactory == null) {
			editPartFactory = new StateSpaceEditPartFactory();
		}
		return editPartFactory;
	}

	@Override
	public void setFocus() {
		if (viewer != null) {
			viewer.setFocus(viewer.getRootEditPart());
		}
	}

	@Override
	public String getContributorId() {
		return StateSpacePathVisEditor.EDITOR_ID; // use the same contributor id as the editor
	}

	/**
	 * requests a layout calculation of the state space node layout, <strong>
	 * does not request a recalculation of any other draw2d layout
	 * manager</strong>
	 */
	public void requestLayoutRecalculation() {
		LOG.info("State space layout recalculation requested");
		if (viewer != null && currentEditorInstance != null) {
			StateSpacePathVis model = currentEditorInstance.getModel();
			if (model != null) {
				Object editPart = viewer.getEditPartRegistry()
										.get(model.getStatespace());
				if (editPart instanceof StateSpaceEP) {
					((StateSpaceEP) editPart).requestLayoutRecalculation();
				}
			}
		}
	}

	/**
	 * requests the layout calculation thread of the state space node layout to
	 * stop.
	 */
	public void stopLayoutCalculation() {
		LOG.info("State space layout thread stop requested");
		if (viewer != null && currentEditorInstance != null) {
			StateSpacePathVis model = currentEditorInstance.getModel();
			if (model != null) {
				Object editPart = viewer.getEditPartRegistry()
										.get(model.getStatespace());
				if (editPart instanceof StateSpaceEP) {
					((StateSpaceEP) editPart).requestLayoutCalculationStop();
				}
			}
		}
	}

	// IPartListener2 methods

	@Override
	public void partActivated(IWorkbenchPartReference partRef) {
		final IWorkbenchPart part = partRef.getPart(false);
		if (part != null
			&& part instanceof StateSpacePathVisEditor && viewer != null) {

			LOG.fine("State space path editor activated");
			if (currentEditorInstance != part) {
				currentEditorInstance = (StateSpacePathVisEditor) part;
				StateSpacePathVis model = currentEditorInstance.getModel();

				if (model != null) {
					viewer.setContents(model);
					getEditDomain().addViewer(viewer);
					LOG.fine("State space view initialized");
				} else {
					LOG.severe("Model instance of state space path editor was null");
				}
			} else {
				LOG.fine("Already known editor instance has been activated");
			}

		}
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference partRef) {
		// No op
	}

	@Override
	public void partClosed(IWorkbenchPartReference partRef) {
		// No op
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference partRef) {
		// active part changed -> remove viewer from edit domain if necessary
		IWorkbenchPart part = partRef.getPart(false);
		if (part != null
			&& part instanceof StateSpacePathVisEditor && viewer != null) {
			LOG.fine("State space path editor deactivated");
			currentEditorInstance = (StateSpacePathVisEditor) part;
			((EditDomain) currentEditorInstance.getAdapter(EditDomain.class)).removeViewer(viewer);
		}
	}

	@Override
	public void partOpened(IWorkbenchPartReference partRef) {
		// No op
	}

	@Override
	public void partHidden(IWorkbenchPartReference partRef) {
		// No op
	}

	@Override
	public void partVisible(IWorkbenchPartReference partRef) {
		// No op
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference partRef) {
		IWorkbenchPart part = partRef.getPart(false);
		if (part != null && part == currentEditorInstance) {
			LOG.fine("input of the current state space path editor has changed");

			StateSpacePathVis model = currentEditorInstance.getModel();

			if (model != null) {
				viewer.setContents(model);
				getEditDomain().addViewer(viewer);
				LOG.fine("State space view correctly initialized");
			} else {
				LOG.severe("Model instance of state space path editor was null");
			}
		}
	}

}
