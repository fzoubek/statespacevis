/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.logging.Logger;

import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.figures.TransparentLabel;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Connection edit part for StateSpaceTransition instances
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateTreeSubTransitionEP extends BaseConnectionEditPart {

	private TransparentLabel					label;
	private PolylineConnection					connection;
	private PolylineDecoration					targetDecoration;

	private Tree<State, StateSpaceTransition>	selectedStatePathTree;
	private Color								highlightColor;
	private Color								normalColor;

	@SuppressWarnings("unused")
	private static final Logger					LOG	= Logger.getLogger(StateTreeSubTransitionEP.class.getCanonicalName());

	public StateTreeSubTransitionEP(
			Tree<State, StateSpaceTransition> selectedStatePathTree) {
		if (selectedStatePathTree == null) {
			this.selectedStatePathTree = StatespacevisFactory.eINSTANCE.createTree();
		} else {
			this.selectedStatePathTree = selectedStatePathTree;
		}
		highlightColor = new Color(Display.getDefault(), 0, 0, 0);
		normalColor = new Color(Display.getDefault(), 150, 150, 150);
	}

	@Override
	protected IFigure createFigure() {
		connection = new PolylineConnection();
		targetDecoration = new PolylineDecoration();
		connection.setTargetDecoration(targetDecoration);

		label = new TransparentLabel();
		label.setBorder(new MarginBorder(1, 3, 1, 3));
		label.setOpaque(true);
		label.setBackgroundTransparency(220);
		label.setVisible(false);

		connection.add(label, new ConnectionLocator(connection,
													ConnectionLocator.MIDDLE) {
			@Override
			public void relocate(IFigure target) {
				super.relocate(target);
				// never allow negative positions as they result in an infinite loop
				Rectangle bounds = target.getBounds();
				bounds.x = Math.max(bounds.x, 0);
				bounds.y = Math.max(bounds.y, 0);
				target.setBounds(bounds);
			}
		});

		connection.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent me) {
				// No op
			}

			@Override
			public void mouseHover(MouseEvent me) {
				// No op
			}

			@Override
			public void mouseExited(MouseEvent me) {
				label.setVisible(false);
				connection.setLineWidth(1);
				targetDecoration.setLineWidth(1);
			}

			@Override
			public void mouseEntered(MouseEvent me) {
				label.setVisible(true);
				label.setForegroundColor(highlightColor);
				connection.setLineWidth(3);
				targetDecoration.setLineWidth(3);
			}

			@Override
			public void mouseDragged(MouseEvent me) {
				// No op
			}
		});

		return connection;
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		StateSpaceTransition stateSpaceTransition = getStateSpaceTransition();
		if (connection != null && stateSpaceTransition != null) {

			label.setText(stateSpaceTransition.getTransitionName());

			// determine highlight state
			if (selectedStatePathTree.containsTransitionValue(stateSpaceTransition)) {
				connection.setForegroundColor(highlightColor);
			} else {
				figure.setForegroundColor(normalColor);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// No op
	}

	public StateSpaceTransition getStateSpaceTransition() {
		Object model = getModel();
		if (model instanceof TreeTransition<?, ?>) {
			Object transitionVal = ((TreeTransition<?, ?>) model).getTransitionValue();
			if (transitionVal instanceof StateSpaceTransition) {
				return (StateSpaceTransition) transitionVal;
			}
		}
		return null;
	}

}
