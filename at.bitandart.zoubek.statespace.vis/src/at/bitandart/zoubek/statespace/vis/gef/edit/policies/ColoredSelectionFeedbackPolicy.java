/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.policies;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.editpolicies.SelectionEditPolicy;
import org.eclipse.swt.graphics.Color;

/**
 * {@link SelectionEditPolicy} that shows selection feedback by changing the
 * colors of the hosts main figure.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class ColoredSelectionFeedbackPolicy extends SelectionEditPolicy {

	private static final Logger	LOG	= Logger.getLogger(ColoredSelectionFeedbackPolicy.class.getCanonicalName());

	/**
	 * the default background color of the figure
	 */
	private Color				defaultBackgroundColor;

	/**
	 * the default foreground color of the figure
	 */
	private Color				defaultForegroundColor;

	/**
	 * the selection foreground color of the figure
	 */
	private Color				selectedForegroundColor;

	/**
	 * the selection background color of the figure
	 */
	private Color				selectedBackgroundColor;
	
	/**
	 * @param defaultForegroundColor
	 *            default foreground color to use if the figure is not selected
	 * @param defaultBackgroundColor
	 *            default background color to use if the figure is not selected
	 * @param selectedForegroundColor
	 *            the foreground color to use if the figure is selected
	 * @param selectedBackgroundColor
	 *            the backgroundColor to use if the figure is selected
	 */
	public ColoredSelectionFeedbackPolicy(Color defaultForegroundColor,
			Color defaultBackgroundColor, Color selectedForegroundColor,
			Color selectedBackgroundColor) {
		super();
		this.defaultForegroundColor = defaultForegroundColor;
		this.defaultBackgroundColor = defaultBackgroundColor;
		this.selectedForegroundColor = selectedForegroundColor;
		this.selectedBackgroundColor = selectedBackgroundColor;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#hideSelection()
	 */
	@Override
	protected void hideSelection() {
		LOG.log(Level.FINE, "Hiding selection...");
		LOG.log(Level.FINER, "Using color: {0}", defaultForegroundColor);
		getHostFigure().setForegroundColor(defaultForegroundColor);
		getHostFigure().setBackgroundColor(defaultBackgroundColor);
		LOG.log(Level.FINE, "Selection hidden");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#showSelection()
	 */
	@Override
	protected void showSelection() {
		LOG.log(Level.FINE, "Showing selection ...");
		LOG.log(Level.FINER, "Using color: {0}", selectedForegroundColor);
		IFigure hostFigure = getHostFigure();
		hostFigure.setForegroundColor(selectedForegroundColor);
		hostFigure.setBackgroundColor(selectedBackgroundColor);
		LOG.log(Level.FINE, "Selection visible");
	}

	/**
	 * @return the default foreground color to use if the figure is not selected
	 */
	public Color getDefaultForegroundColor() {
		return defaultForegroundColor;
	}

	/**
	 * @param defaultForegroundColor
	 *            default foreground color to use if the figure is not selected
	 */
	public void setDefaultForegroundColor(Color defaultForegroundColor) {
		this.defaultForegroundColor = defaultForegroundColor;
	}

	/**
	 * @return the default background color to use if the figure is not selected
	 */
	public Color getDefaultBackgroundColor() {
		return defaultBackgroundColor;
	}

	/**
	 * @param defaultBackgroundColor
	 *            the default background color to use if the figure is not
	 *            selected
	 */
	public void setDefaultBackgroundColor(Color defaultBackgroundColor) {
		this.defaultBackgroundColor = defaultBackgroundColor;
	}

	/**
	 * @return the foreground color to use if the figure is selected
	 */
	public Color getSelectedForegroundColor() {
		return selectedForegroundColor;
	}

	/**
	 * @param selectedForegroundColor
	 *            the foreground color to use if the figure is selected
	 */
	public void setSelectedForegroundColor(Color selectedForegroundColor) {
		this.selectedForegroundColor = selectedForegroundColor;
	}

	/**
	 * @return the background color to use if the figure is selected
	 */
	public Color getSelectedBackgroundColor() {
		return selectedBackgroundColor;
	}

	/**
	 * @param selectedBackgroundColor
	 *            the background color to use if the figure is selected
	 */
	public void setSelectedBackgroundColor(Color selectedBackgroundColor) {
		this.selectedBackgroundColor = selectedBackgroundColor;
	}

}
