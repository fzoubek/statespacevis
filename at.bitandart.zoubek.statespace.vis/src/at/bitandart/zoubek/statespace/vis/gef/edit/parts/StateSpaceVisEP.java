/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ToolbarLayout;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;

/**
 * Edit part for a StateSpacePathVis instance but only for the visualization of
 * the state space, provides only a Freeform layer for all child elements
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpaceVisEP extends BaseEditPart {

	public StateSpaceVisEP() {
	}

	public StateSpaceVisEP(StateSpacePathVis sspvis) {
		setModel(sspvis);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		Figure figure = new FreeformLayer();
		figure.setLayoutManager(new ToolbarLayout());
		return figure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
	}

	@Override
	protected List<?> getModelChildren() {
		List<Object> children = new ArrayList<Object>();

		StateSpacePathVis model = (StateSpacePathVis) getModel();
		children.add(model.getStatespace());

		return children;
	}

}
