package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;

public abstract class BaseConnectionEditPart extends AbstractConnectionEditPart {

	protected EContentAdapter	modelUpdateListener;

	public BaseConnectionEditPart() {
		super();
	}

	/**
	 * adapter class that listens for model updates and refreshes the edit part
	 * accordingly
	 * 
	 * @author Florian Zoubek <zoubek@bitandart.at>
	 * 
	 */
	private class ModelUpdateListener extends EContentAdapter {
		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			notifyModelChanged();
		}
	}
	
	/**
	 * called when model change has been detected by the current model listener
	 */
	protected void notifyModelChanged() {
		refresh();
	}

	@Override
	public void setModel(Object model) {
		unregisterModelUpdateListener();
		super.setModel(model);
		registerModelUpdateListener();
	}

	/**
	 * adds the model update listener to the current model instance
	 */
	protected void registerModelUpdateListener() {
		Object model = getModel();
		if (model != null && model instanceof EObject) {
			((EObject) model).eAdapters().add(getModelUpdateListener());
		}
	}

	/**
	 * removes the model update listener from the current model instance
	 */
	protected void unregisterModelUpdateListener() {
		Object model = getModel();
		if (model != null && model instanceof EObject) {
			((EObject) model).eAdapters().remove(getModelUpdateListener());
		}
	}

	/**
	 * 
	 * @return the edit parts listener for model changes
	 */
	protected EContentAdapter getModelUpdateListener() {
		if (modelUpdateListener == null) {
			modelUpdateListener = createModelUpdateListener();
		}
		return modelUpdateListener;
	}

	/**
	 * creates the adapter that is used to track model changes for this edit
	 * part. Creates a listener which refreshes the edit part if the model
	 * changes. Subclasses may override.
	 * 
	 * @return
	 */
	protected EContentAdapter createModelUpdateListener() {
		return new ModelUpdateListener();
	}

	@Override
	public void deactivate() {
		unregisterModelUpdateListener();
	}

}