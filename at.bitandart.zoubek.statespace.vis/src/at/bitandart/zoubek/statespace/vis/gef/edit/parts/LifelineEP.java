/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LayoutListener;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;

import at.bitandart.zoubek.statespace.vis.figures.AxisConstraint;
import at.bitandart.zoubek.statespace.vis.figures.AxisConstraint.PlacementMethod;
import at.bitandart.zoubek.statespace.vis.figures.CollapseIndicator;
import at.bitandart.zoubek.statespace.vis.figures.ContainerFigure;
import at.bitandart.zoubek.statespace.vis.layout.SingleAxisLayout;
import at.bitandart.zoubek.statespace.vis.layout.TreePathLayout;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Abstract edit part that provides default functionality for edit parts that
 * act upon {@link Lifeline}s
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public abstract class LifelineEP extends BaseEditPart {

	private static Logger		LOG							= Logger.getLogger(LifelineEP.class.getCanonicalName());

	/**
	 * adapter used to listen to model changes
	 */
	protected Adapter			notifyAdapter;

	/**
	 * the content pane of this edit part's figure, holds figures of child edit
	 * parts
	 */
	private IFigure				contentPane;

	/**
	 * the collapse indicator figure
	 */
	private CollapseIndicator	collapseIndicator;

	/**
	 * the lifeline label
	 */
	private Label				lifelineLabel;

	private TreePathLayout		contentPaneLayoutManager	= new TreePathLayout();

	private SingleAxisLayout	labelLayout;

	protected abstract void updateLabel(Label label, Lifeline lifeline);

	protected abstract Tree<?, ?> getLifelineTree(Lifeline lifeline);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {

		LOG.log(Level.FINE, "Creating figure for {0}", getModel());
		Lifeline objectLifeline = getLifeline();

		IFigure figure = new ContainerFigure();
		figure.setBackgroundColor(ColorConstants.red);
		labelLayout = new SingleAxisLayout();
		labelLayout.setHorizontal(true);
		figure.setLayoutManager(labelLayout);

		if (!objectLifeline.getChildLifelines().isEmpty()) {
			collapseIndicator = new CollapseIndicator();
			collapseIndicator.getModel()
								.addActionListener(new ActionListener() {

									@Override
									public void actionPerformed(
											ActionEvent event) {
										getLifeline().setCollapsed(!getLifeline().isCollapsed());
									}
								});
			figure.add(collapseIndicator);
			figure.setConstraint(	collapseIndicator,
									new AxisConstraint(	new PrecisionDimension(	15.0,
																				-1.0),
														PlacementMethod.FIXED,
														0));
		}

		// create label
		lifelineLabel = new Label("Lifeline");
		lifelineLabel.setLabelAlignment(PositionConstants.RIGHT);
		figure.add(lifelineLabel);
		figure.setConstraint(	lifelineLabel,
								new AxisConstraint(	new PrecisionDimension(	150.0 + ((collapseIndicator == null)
																					? 15
																					: 0),
																			-1.0),
													PlacementMethod.FIXED,
													0)); // TODO read from model

		LOG.log(Level.FINE, "Completed figure creation for {0}", getModel());
		
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		Lifeline lifeline = getLifeline();
		LOG.log(Level.FINE, "Refreshing visuals for {0}", lifeline);
		super.refreshVisuals();

		if (collapseIndicator != null) {
			LOG.log(Level.FINE, "Updating collapse indicator...");
			collapseIndicator.setSelected(!lifeline.isCollapsed());
		}

		// update label
		updateLabel(lifelineLabel, lifeline);

		updateAxisConstraints();

		// update height spacing based on the calculated height of the parent edit part 
		EditPart parent = getParent();
		if (parent instanceof LifelineGroupEP) {
			LOG.log(Level.FINE, "Updating height spacing...");
			contentPaneLayoutManager.setHeightSpacing(((LifelineGroupEP) parent).getCalculatedLifelineHeight());
		}

		LOG.log(Level.FINE, "Updating main figure...");
		updateMainFigure(lifeline, getFigure());

		// update visibility of all child lifelines
		LOG.log(Level.FINE, "Refreshing child lifelines...");
		if (getViewer() != null) {
			@SuppressWarnings("unchecked")
			Map<?, EditPart> editPartRegistry = getViewer().getEditPartRegistry();
			for (Lifeline childlifeline : lifeline.getChildLifelines()) {
				EditPart editPart = editPartRegistry.get(childlifeline);
				if (editPart != null) {
					editPart.refresh();
				}
			}

			// update visibility of all child transitions
			LOG.log(Level.FINE, "Refreshing child transitions...");
			for (TreeTransition<?, ?> transition : getLifelineTree(lifeline).getAllTransitions()) {
				EditPart editPart = editPartRegistry.get(transition);
				if (editPart != null && editPart instanceof GraphicalEditPart) {
					((GraphicalEditPart) editPart).getFigure()
													.setVisible((lifeline.getParentLifeline() == null && lifeline.isVisible())
																|| (lifeline.getParentLifeline() != null
																	&& !isAnyParentLifelineCollapsed(lifeline) && lifeline.isVisible()));
					LOG.log(Level.FINE,
							"New Visibility for child transition {0}: {1}",
							new Object[] {
									transition,
									(lifeline.getParentLifeline() == null && lifeline.isVisible())
											|| (lifeline.getParentLifeline() != null
												&& !isAnyParentLifelineCollapsed(lifeline) && lifeline.isVisible()) });
				}
			}
		} else {
			LOG.log(Level.FINE,
					"No viewer reference, could not update visibility of child lifelines");
		}
		LOG.log(Level.FINE, "Visuals refreshed for {0}", lifeline);
	}

	private void updateAxisConstraints() {
		LOG.log(Level.FINE, "Updating axis constraints...");

		@SuppressWarnings("unchecked")
		List<IFigure> children = getContentPane().getChildren();
		double horizontalAxisY = 0.0;
		if (!children.isEmpty()) {
			Rectangle bounds = children.get(0).getBounds().getCopy();
			getContentPane().translateToAbsolute(bounds);
			lifelineLabel.getParent().translateToRelative(bounds);
			horizontalAxisY = bounds.preciseY();
		}
		LOG.log(Level.FINER,
				"Y-coordinate of the horizontal axis {0}",
				horizontalAxisY);

		// update layout constraint such that the collapse indicator is placed on the horizontal axis
		Object constraint = labelLayout.getConstraint(collapseIndicator);
		if (constraint != null && constraint instanceof AxisConstraint) {
			AxisConstraint axisConstraint = (AxisConstraint) constraint;
			axisConstraint.setMinorAxisFixedValue(horizontalAxisY);
			getFigure().setConstraint(collapseIndicator, axisConstraint);
			LOG.log(Level.FINER,
					"Updated axis constraint for collapse indicator");
		}

		// update layout constraint such that the label is placed on the horizontal axis
		constraint = labelLayout.getConstraint(lifelineLabel);
		if (constraint != null && constraint instanceof AxisConstraint) {
			AxisConstraint axisConstraint = (AxisConstraint) constraint;
			axisConstraint.setMinorAxisFixedValue(horizontalAxisY);
			getFigure().setConstraint(lifelineLabel, axisConstraint);
			LOG.log(Level.FINER,
					"Updated axis constraint for lifeline label");
		}
	}

	@Override
	public IFigure getContentPane() {
		if (contentPane == null) {
			contentPane = new ContainerFigure();
			contentPane.setLayoutManager(contentPaneLayoutManager);
			contentPane.addLayoutListener(new LayoutListener() {

				@Override
				public void setConstraint(IFigure child, Object constraint) {
				}

				@Override
				public void remove(IFigure child) {
				}

				@Override
				public void postLayout(IFigure container) {
					updateAxisConstraints();
				}

				@Override
				public boolean layout(IFigure container) {
					return false;
				}

				@Override
				public void invalidate(IFigure container) {
				}
			});
			figure.add(contentPane);
		}
		return contentPane;
	}

	/**
	 * 
	 * @return the lifeline instance or null if the model is not an instance of
	 *         {@link Lifeline}
	 */
	public Lifeline getLifeline() {
		Object model = getModel();
		if (model instanceof Lifeline) {
			return (Lifeline) model;
		}
		return null;
	}

	/**
	 * 
	 * @param lifeline
	 * @return true if any parent of the given lifeline is collapsed, false
	 *         otherwise
	 */
	protected boolean isAnyParentLifelineCollapsed(Lifeline lifeline) {
		Lifeline parent = lifeline.getParentLifeline();
		while (parent != null) {
			if (parent.isCollapsed()) {
				return true;
			}
			parent = parent.getParentLifeline();
		}
		return false;
	}

	/**
	 * treats the given figure as main figure and updates it accordingly
	 * 
	 * @param lifeline
	 *            the lifeline instance used to update the figure
	 * @param figure
	 *            the figure to update
	 */
	protected void updateMainFigure(Lifeline lifeline, IFigure figure) {
		// update visibility of the main figure
		Lifeline parentLifeline = lifeline.getParentLifeline();
		if (parentLifeline == null) {
			// no parent
			figure.setVisible(lifeline.isVisible());
		} else {
			// parent exists
			figure.setVisible(!isAnyParentLifelineCollapsed(lifeline)
								&& lifeline.isVisible());
		}
	}

}
