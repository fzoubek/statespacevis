/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ToolbarLayout;

import at.bitandart.zoubek.statespace.vis.figures.SSPVisCanvas;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;

/**
 * Edit part for a StateSpacePathVis instance, provides only a Freeform layer
 * for all child elements
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpacePathVisEP extends BaseEditPart {

	private SSPVisCanvas	canvas;

	public StateSpacePathVisEP(StateSpacePathVis sspvis) {
		setModel(sspvis);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		canvas = new SSPVisCanvas();
		ToolbarLayout layout = new ToolbarLayout();
		layout.setSpacing(10);
		canvas.setLayoutManager(layout);
		return canvas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		canvas.setSeparationLineOffset(165 + canvas.getSeparationLineSpacing() + 7);
		StateSpacePathVis model = getSSPVis();
		if (model != null) {
			canvas.setNumSeparationLines(model.getStatePathTree().getMaximumDepth()-1);
		}
	}

	@Override
	protected List<?> getModelChildren() {
		List<Object> children = new ArrayList<Object>();

		StateSpacePathVis model = getSSPVis();
		if (model != null) {
			children.add(model.getStatePathTree());
			children.addAll(model.getLifelineGroups());
		}

		return children;
	}

	/**
	 * 
	 * @return the StateSpacePathVis instance of the model, or null if the
	 *         instance could not be determined
	 */
	public StateSpacePathVis getSSPVis() {
		Object model = getModel();
		if (model != null && model instanceof StateSpacePathVis) {
			return (StateSpacePathVis) model;
		}
		return null;
	}

}
