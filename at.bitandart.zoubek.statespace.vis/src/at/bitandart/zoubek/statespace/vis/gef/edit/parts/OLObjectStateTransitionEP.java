/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.StateSpaceVisPlugin;
import at.bitandart.zoubek.statespace.vis.figures.ObjectTransitionNodeFigure;
import at.bitandart.zoubek.statespace.vis.gef.edit.policies.ColoredSelectionFeedbackPolicy;
import at.bitandart.zoubek.statespace.vis.layout.TreeLayoutMarker;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Edit part that represents a state space transition in an Object Lifeline
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OLObjectStateTransitionEP extends BaseEditPart {

	private static final Logger				LOG					= Logger.getLogger(OLObjectStateTransitionEP.class.getCanonicalName());

	private ObjectLifeline					parentLifeline		= null;

	/**
	 * the main color of the figure
	 */
	private Color							mainColor			= null;

	/**
	 * the preference listener
	 */
	private IPropertyChangeListener			preferenceListener	= null;

	private ColoredSelectionFeedbackPolicy	selectionPolicy;

	public OLObjectStateTransitionEP(ObjectLifeline parentLifeline) {
		this.parentLifeline = parentLifeline;

		// add preference listener
		preferenceListener = new IPropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {
				LOG.log(Level.FINE, "Processing property update...");
				if (event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_1)
					|| event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_2)
					|| event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_1)
					|| event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_2)) {
					updateColorsFromPreferences();
					refresh();
				} else {
					LOG.log(Level.FINE, "Nothing to update");
				}
				LOG.log(Level.FINE, "Property update processed");
			}
		};
		StateSpaceVisPlugin.getInstance().getPreferenceStore()
							.addPropertyChangeListener(preferenceListener);
		selectionPolicy = new ColoredSelectionFeedbackPolicy(	ColorConstants.black,
																ColorConstants.black,
																ColorConstants.gray,
																ColorConstants.gray); // default values, will be set to the correct values by updateColorsFormPreferences()
		// load colors
		updateColorsFromPreferences();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		LOG.log(Level.FINE, "Creating figure...");
		float dim = (parentLifeline.getParentLifeline() == null)
				? 16.0f : 12.0f;
		ObjectTransitionNodeFigure figure = new ObjectTransitionNodeFigure();
		figure.setPreferredSize(new PrecisionDimension(16, dim));
		figure.setChangedDiameter(dim - 2);
		figure.setBackgroundColor(mainColor);
		figure.setForegroundColor(mainColor);
		figure.setNullWidth(1.0f);
		figure.setNotNullWidth((parentLifeline.getParentLifeline() == null)
				? 5.0f : 3.0f);
		LOG.log(Level.FINE, "Figure created");
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		LOG.log(Level.FINE, "Refreshing visuals...");

		// create Layout constraint
		@SuppressWarnings("unchecked")
		TreeNode<StateSpaceTransition, ObjectLifelineState> node = (TreeNode<StateSpaceTransition, ObjectLifelineState>) getModel();
		Map<?, ?> ePRegistry = getRoot().getViewer().getEditPartRegistry();

		TreeLayoutMarker marker = new TreeLayoutMarker();
		marker.setLeaf(node.getChildTransitions().isEmpty());
		if (node.getParentTransition() != null) {
			TreeTransition<StateSpaceTransition, ObjectLifelineState> transition = node.getParentTransition();
			TreeNode<StateSpaceTransition, ObjectLifelineState> prevNode = transition.getPreviousNode();
			if (ePRegistry.containsKey(prevNode)) {
				GraphicalEditPart gEP = (GraphicalEditPart) ePRegistry.get(prevNode);
				marker.setPrevFigure(gEP.getFigure());
			}
		}

		((GraphicalEditPart) this.getParent()).setLayoutConstraint(	this,
																	getFigure(),
																	marker);

		// update figure

		ObjectTransitionNodeFigure figure = (ObjectTransitionNodeFigure) getFigure();
		figure.setBackgroundColor(mainColor);
		figure.setForegroundColor(mainColor);
		List<TreeTransition<StateSpaceTransition, ObjectLifelineState>> transitions = node.getChildTransitions();
		ObjectStateType nextState = null;
		ObjectStateType prevState = null;
		ObjectStateType stateToSet = ObjectStateType.NULL;
		if (!transitions.isEmpty()) {
			// all states of outgoing transitions must have the same value ->
			// retrieve it from the first
			nextState = transitions.get(0).getTransitionValue().getState();
		}
		if (node.getParentTransition() != null) {
			prevState = node.getParentTransition().getTransitionValue()
							.getState();
		}

		if (nextState != null && prevState != null) {
			// Node with incoming and outgoing transitions, determine state
			// based on the difference between those two

			if (prevState == ObjectStateType.NOTNULL
				&& nextState == ObjectStateType.NOTNULL) {
				stateToSet = ObjectStateType.NOTNULL;
				figure.setConnectedToLeftSide(true);
				figure.setConnectedToRightSide(true);

			} else if (prevState == ObjectStateType.NULL
						&& nextState == ObjectStateType.NULL) {
				stateToSet = ObjectStateType.NULL;
				figure.setConnectedToLeftSide(true);
				figure.setConnectedToRightSide(true);

			} else if (prevState == ObjectStateType.CHANGED
						&& nextState == ObjectStateType.CHANGED) {
				stateToSet = ObjectStateType.CHANGED;
				figure.setConnectedToLeftSide(false);
				figure.setConnectedToRightSide(true);

			} else if (prevState == ObjectStateType.NOTNULL
						&& nextState == ObjectStateType.CHANGED) {
				stateToSet = ObjectStateType.CHANGED;
				figure.setConnectedToLeftSide(false);
				figure.setConnectedToRightSide(true);

			} else if (prevState == ObjectStateType.NOTNULL
						&& nextState == ObjectStateType.NULL) {
				stateToSet = ObjectStateType.NOTNULL;
				figure.setConnectedToLeftSide(true);
				figure.setConnectedToRightSide(false);

			} else if (prevState == ObjectStateType.NULL
						&& nextState != ObjectStateType.NULL) {
				stateToSet = ObjectStateType.NOTNULL;
				figure.setConnectedToLeftSide(false);
				figure.setConnectedToRightSide(true);

			} else if (prevState == ObjectStateType.CHANGED
						&& nextState != ObjectStateType.CHANGED) {
				stateToSet = ObjectStateType.NOTNULL;

				if (nextState == ObjectStateType.NOTNULL) {
					figure.setConnectedToLeftSide(true);
					figure.setConnectedToRightSide(true);
				} else if (nextState == ObjectStateType.NULL) {
					figure.setConnectedToLeftSide(true);
					figure.setConnectedToRightSide(false);
				}
			}

		} else if (nextState != null) {
			// Node has only outgoing transitions
			if (nextState == ObjectStateType.CHANGED) {
				stateToSet = ObjectStateType.NOTNULL;
			} else {
				stateToSet = nextState;
			}
			figure.setConnectedToLeftSide(false);
			figure.setConnectedToRightSide(true);
		} else if (prevState != null) {
			if (prevState == ObjectStateType.CHANGED) {
				stateToSet = ObjectStateType.NOTNULL;
			} else {
				stateToSet = prevState;
			}
			figure.setConnectedToLeftSide(true);
			figure.setConnectedToRightSide(false);
		}

		switch (stateToSet) {
			case NULL:
				figure.setState(ObjectTransitionNodeFigure.FigureState.NULL);
				break;
			case NOTNULL:
				figure.setState(ObjectTransitionNodeFigure.FigureState.NOTNULL);
				break;
			case CHANGED:
				figure.setState(ObjectTransitionNodeFigure.FigureState.CHANGED);
				break;
		}

		super.refreshVisuals();
		LOG.log(Level.FINE, "Visuals refreshed");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, selectionPolicy);
	}

	@Override
	protected List<?> getModelSourceConnections() {
		@SuppressWarnings("unchecked")
		TreeNode<StateSpaceTransition, ObjectLifelineState> model = (TreeNode<StateSpaceTransition, ObjectLifelineState>) getModel();
		List<Object> sourceConnections = new ArrayList<>();
		sourceConnections.addAll(model.getChildTransitions());
		return sourceConnections;
	}

	@Override
	protected List<?> getModelTargetConnections() {
		@SuppressWarnings("unchecked")
		TreeNode<StateSpaceTransition, ObjectLifelineState> model = (TreeNode<StateSpaceTransition, ObjectLifelineState>) getModel();
		List<Object> targetConnections = new ArrayList<>();
		if (model.getParentTransition() != null) {
			targetConnections.add(model.getParentTransition());
		}
		return targetConnections;
	}


	@Override
	public void deactivate() {
		super.deactivate();
		if (mainColor != null) {
			LOG.log(Level.FINE, "Disposing color...");
			mainColor.dispose();
		}
		// dispose selection color (fore- and background color are the same)
		Color selectionColor = selectionPolicy.getSelectedForegroundColor();
		if (selectionColor != null) {
			selectionColor.dispose();
		}
		StateSpaceVisPlugin.getInstance().getPreferenceStore()
							.removePropertyChangeListener(preferenceListener);
	}

	/**
	 * updates the colors used for the figures based on the current preferences
	 */
	private void updateColorsFromPreferences() {
		LOG.log(Level.FINE, "Updating color from preferences...");

		// dispose previous colors
		if (mainColor != null) {
			LOG.log(Level.FINE, "Disposing old color...");
			mainColor.dispose();
		}
		Color selectionColor = selectionPolicy.getSelectedForegroundColor();
		if (selectionColor != null) {
			selectionColor.dispose();
		}

		// main color
		String prefColorId = (parentLifeline.getParentLifeline() == null)
				? StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_1
				: StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_2;
		LOG.log(Level.FINER, "Using color preference: {0}", prefColorId);
		String prefColor = StateSpaceVisPlugin.getInstance()
												.getPreferenceStore()
												.getString(prefColorId);
		LOG.log(Level.FINER, "Main color: {0}", prefColor);

		setMainColor(new Color(	Display.getDefault(),
								StringConverter.asRGB(prefColor)));

		// selection color
		prefColorId = (parentLifeline.getParentLifeline() == null)
				? StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_1
				: StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_2;
		LOG.log(Level.FINER,
				"Using selection color preference: {0}",
				prefColorId);
		prefColor = StateSpaceVisPlugin.getInstance().getPreferenceStore()
										.getString(prefColorId);
		LOG.log(Level.FINER, "Selection color: {0}", prefColor);
		selectionPolicy.setSelectedForegroundColor(new Color(	Display.getDefault(),
																StringConverter.asRGB(prefColor)));
		selectionPolicy.setSelectedBackgroundColor(new Color(	Display.getDefault(),
																StringConverter.asRGB(prefColor)));
		LOG.log(Level.FINE, "Colors updated");
	}

	/**
	 * 
	 * @param mainColor
	 *            the main color of the figure
	 */
	private void setMainColor(Color mainColor) {
		this.mainColor = mainColor;
		if (selectionPolicy != null) {
			selectionPolicy.setDefaultForegroundColor(mainColor);
			selectionPolicy.setDefaultBackgroundColor(mainColor);
		}
	}

	@SuppressWarnings("unchecked")
	public TreeNode<StateSpaceTransition, ObjectLifelineState> getObjectStateTransition() {
		return (TreeNode<StateSpaceTransition, ObjectLifelineState>) getModel();
	}

}
