/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.StateSpaceVisPlugin;
import at.bitandart.zoubek.statespace.vis.figures.BezierConnectionRouter;
import at.bitandart.zoubek.statespace.vis.figures.SideAnchor;
import at.bitandart.zoubek.statespace.vis.gef.edit.policies.ColoredSelectionFeedbackPolicy;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class OLObjectStateEP extends BaseConnectionEditPart {

	private static final Logger				LOG					= Logger.getLogger(OLObjectStateEP.class.getCanonicalName());

	/**
	 * the lifeline that contains this object state
	 */
	private ObjectLifeline					parentLifeline		= null;

	/**
	 * the main color of the figure
	 */
	private Color							mainColor			= null;

	/**
	 * the preference listener
	 */
	private IPropertyChangeListener			preferenceListener	= null;

	private ColoredSelectionFeedbackPolicy	selectionPolicy;

	public OLObjectStateEP(ObjectLifeline parentLifeline) {
		this.parentLifeline = parentLifeline;

		// add preference listener
		preferenceListener = new IPropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {
				LOG.log(Level.FINE, "Processing property update...");
				if (event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_1)
					|| event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_2)
					|| event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_1)
					|| event.getProperty()
							.equals(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_2)) {
					updateColorsFromPreferences();
					refresh();
				} else {
					LOG.log(Level.FINE, "Nothing to update");
				}
				LOG.log(Level.FINE, "Property update processed");
			}
		};
		StateSpaceVisPlugin.getInstance().getPreferenceStore()
							.addPropertyChangeListener(preferenceListener);
		selectionPolicy = new ColoredSelectionFeedbackPolicy(	ColorConstants.black,
																ColorConstants.black,
																ColorConstants.gray,
																ColorConstants.gray); // default values, will be set to the correct values by updateColorsFormPreferences()
		// load colors
		updateColorsFromPreferences();
	}

	@Override
	protected IFigure createFigure() {
		LOG.log(Level.FINE, "Creating figure...");
		PolylineConnection figure = new PolylineConnection();
		figure.setBackgroundColor(mainColor);
		figure.setForegroundColor(mainColor);
		BezierConnectionRouter router = new BezierConnectionRouter();
		figure.setConnectionRouter(router);
		LOG.log(Level.FINE, "Figure created");
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		LOG.log(Level.FINE, "Refreshing visuals...");
		@SuppressWarnings("unchecked")
		TreeTransition<StateSpaceTransition, ObjectLifelineState> transition = (TreeTransition<StateSpaceTransition, ObjectLifelineState>) getModel();
		PolylineConnection figure = (PolylineConnection) getConnectionFigure();
		figure.setBackgroundColor(mainColor);
		figure.setForegroundColor(mainColor);

		switch (transition.getTransitionValue().getState()) {
			case NOTNULL:
			case CHANGED:
				figure.setLineWidthFloat((parentLifeline.getParentLifeline() == null)
						? 5.0f : 3.0f);
				break;
			case NULL:
				figure.setLineWidthFloat(1.0f);
				break;
			default:
				figure.setLineWidthFloat(1.0f);
				break;
		}

		super.refreshVisuals();
		LOG.log(Level.FINE, "Visuals refreshed");
	}

	@Override
	protected ConnectionAnchor getSourceConnectionAnchor() {
		if (getSource() != null) {
			return new SideAnchor(	((GraphicalEditPart) getSource()).getFigure(),
									SideAnchor.LEFT | SideAnchor.RIGHT);
		}
		return super.getTargetConnectionAnchor();
	}

	@Override
	protected ConnectionAnchor getTargetConnectionAnchor() {
		if (getTarget() != null) {
			return new SideAnchor(	((GraphicalEditPart) getTarget()).getFigure(),
									SideAnchor.LEFT | SideAnchor.RIGHT);
		}
		return super.getTargetConnectionAnchor();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, selectionPolicy);
	}

	@Override
	public void deactivate() {
		super.deactivate();
		if (mainColor != null) {
			LOG.log(Level.FINE, "Disposing color...");
			mainColor.dispose();
		}
		// dispose selection color (fore- and background color are the same)
		Color selectionColor = selectionPolicy.getSelectedForegroundColor();
		if (selectionColor != null) {
			selectionColor.dispose();
		}
		StateSpaceVisPlugin.getInstance().getPreferenceStore()
							.removePropertyChangeListener(preferenceListener);
	}

	/**
	 * updates the colors used for the figures based on the current preferences
	 */
	private void updateColorsFromPreferences() {
		LOG.log(Level.FINE, "Updating color from preferences...");

		// dispose previous colors
		if (mainColor != null) {
			LOG.log(Level.FINE, "Disposing old color...");
			mainColor.dispose();
		}
		Color selectionColor = selectionPolicy.getSelectedForegroundColor();
		if (selectionColor != null) {
			selectionColor.dispose();
		}

		// main color
		String prefColorId = (parentLifeline.getParentLifeline() == null)
				? StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_1
				: StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_2;
		LOG.log(Level.FINER, "Using color preference: {0}", prefColorId);
		String prefColor = StateSpaceVisPlugin.getInstance()
												.getPreferenceStore()
												.getString(prefColorId);
		LOG.log(Level.FINER, "Main color: {0}", prefColor);

		setMainColor(new Color(	Display.getDefault(),
								StringConverter.asRGB(prefColor)));

		// selection color
		prefColorId = (parentLifeline.getParentLifeline() == null)
				? StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_1
				: StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_2;
		LOG.log(Level.FINER,
				"Using selection color preference: {0}",
				prefColorId);
		prefColor = StateSpaceVisPlugin.getInstance().getPreferenceStore()
										.getString(prefColorId);
		LOG.log(Level.FINER, "Selection color: {0}", prefColor);
		selectionPolicy.setSelectedForegroundColor(new Color(	Display.getDefault(),
																StringConverter.asRGB(prefColor)));
		selectionPolicy.setSelectedBackgroundColor(new Color(	Display.getDefault(),
																StringConverter.asRGB(prefColor)));

		LOG.log(Level.FINE, "Colors updated");
	}

	/**
	 * 
	 * @param mainColor
	 *            the main color of the figure
	 */
	private void setMainColor(Color mainColor) {
		this.mainColor = mainColor;
		if (selectionPolicy != null) {
			selectionPolicy.setDefaultForegroundColor(mainColor);
			selectionPolicy.setDefaultBackgroundColor(mainColor);
		}
	}

	@SuppressWarnings("unchecked")
	public TreeTransition<StateSpaceTransition, ObjectLifelineState> getObjectState() {
		return (TreeTransition<StateSpaceTransition, ObjectLifelineState>) getModel();
	}
}
