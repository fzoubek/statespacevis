/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.Label;
import org.eclipse.emf.ecore.util.EContentAdapter;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;

/**
 * Edit part for a lifeline of an object
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class ObjectLifelineEP extends LifelineEP {

	private static Logger		LOG							= Logger.getLogger(ObjectLifelineEP.class.getCanonicalName());

	private static String		OBJECT_NAME_LABEL_SEPARATOR	= " : ";

	@Override
	protected Tree<?, ?> getLifelineTree(Lifeline lifeline) {
		return ((ObjectLifeline)lifeline).getObjectStateTree();
	}
	
	@Override
	protected void updateLabel(Label label, Lifeline lifeline) {
		ObjectLifeline objLifeline = (ObjectLifeline)lifeline;
		LOG.log(Level.FINE, "Updating label...");
		String labelString = "";
		if (lifeline != null) {
			ObjectDescriptor descriptor = objLifeline.getObjectDescriptor();
			if (descriptor != null) {
				labelString = descriptor.getInstanceName()
								+ OBJECT_NAME_LABEL_SEPARATOR
								+ descriptor.getClassName();
			}
		}
		label.setText(labelString);
	}


	/**
	 * 
	 * @return the object lifeline instance or null if the model is not an
	 *         instance of <code>ObjectLifeline</code>
	 */
	public ObjectLifeline getObjectLifeline() {
		Object model = getModel();
		if (model instanceof ObjectLifeline) {
			return (ObjectLifeline) model;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
	}
	

	@Override
	protected void registerModelUpdateListener() {
		super.registerModelUpdateListener();
		// register also to tree node changes
		ObjectLifeline objectLifeline = getObjectLifeline();
		if (objectLifeline != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<?, ?> node : objectLifeline.getObjectStateTree().getAllNodes()) {
				node.eAdapters().add(modelUpdateListener);
			}
		}
	}
	
	@Override
	protected void notifyModelChanged() {
		super.notifyModelChanged();
		ObjectLifeline objectLifeline = getObjectLifeline();
		// register also to new tree node changes
		if (objectLifeline != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<?, ?> node : objectLifeline.getObjectStateTree().getAllNodes()) {
				if(!node.eAdapters().contains(modelUpdateListener)){
					node.eAdapters().add(modelUpdateListener);
				}
			}
		}
	}

	@Override
	protected void unregisterModelUpdateListener() {
		super.registerModelUpdateListener();
		// unregister also to tree node change listener
		ObjectLifeline objectLifeline = getObjectLifeline();
		if (objectLifeline != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<?, ?> node : objectLifeline.getObjectStateTree().getAllNodes()) {
				node.eAdapters().remove(modelUpdateListener);
			}
		}
	}

	@Override
	protected List<?> getModelChildren() {
		ObjectLifeline model = (ObjectLifeline) getModel();
		List<Object> children = new ArrayList<>();
		children.addAll(model.getObjectStateTree().getAllNodes());
		return children;
	}
}
