/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LayoutListener;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.emf.ecore.util.EContentAdapter;

import at.bitandart.zoubek.statespace.vis.figures.AxisConstraint;
import at.bitandart.zoubek.statespace.vis.figures.AxisConstraint.PlacementMethod;
import at.bitandart.zoubek.statespace.vis.layout.SingleAxisLayout;
import at.bitandart.zoubek.statespace.vis.layout.TreePathLayout;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;

/**
 * Edit part for instances of StatePathTree, provides a Panel for all child
 * elements
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateTreeEP extends BaseEditPart {

	private static final Logger	LOG	= Logger.getLogger(StateTreeEP.class.getCanonicalName());

	private IFigure				contentPane;

	private Label				statePathLabel;

	private SingleAxisLayout	labelLayout;

	public StateTreeEP() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		IFigure figure = new Figure();
		labelLayout = new SingleAxisLayout();
		labelLayout.setHorizontal(true);
		figure.setLayoutManager(labelLayout);
		statePathLabel = new Label("State space path");
		statePathLabel.setLabelAlignment(PositionConstants.RIGHT);
		figure.add(statePathLabel);
		figure.setConstraint(	statePathLabel,
								new AxisConstraint(	new PrecisionDimension(	150.0,
																			-1),
													PlacementMethod.FIXED,
													0)); // TODO read from model
		return figure;
	}

	@Override
	public IFigure getContentPane() {
		if (contentPane == null) {
			contentPane = new Figure();
			//FlowLayout layoutManager = new FlowLayout();
			//layoutManager.setMinorSpacing(100);
			TreePathLayout layoutManager = new TreePathLayout();
			layoutManager.setOffsetX(165.0 / 2);
			contentPane.setLayoutManager(layoutManager);
			contentPane.addLayoutListener(new LayoutListener() {

				@Override
				public void setConstraint(IFigure child, Object constraint) {
				}

				@Override
				public void remove(IFigure child) {
				}

				@Override
				public void postLayout(IFigure container) {
					updateAxisConstraints();
				}

				@Override
				public boolean layout(IFigure container) {
					return false;
				}

				@Override
				public void invalidate(IFigure container) {
				}
			});
			figure.add(contentPane);
		}
		return contentPane;
	}

	private void updateAxisConstraints() {
		LOG.log(Level.FINE, "Updating axis constraints...");

		@SuppressWarnings("unchecked")
		List<IFigure> children = getContentPane().getChildren();
		double horizontalAxisY = 0.0;
		if (!children.isEmpty()) {

			horizontalAxisY = children.get(0).getBounds().getBottom().preciseY() - 11;
		}
		LOG.log(Level.FINER,
				"Y-coordinate of the horizontal axis {0}",
				horizontalAxisY);

		// update layout constraint such that the collapse indicator is placed on the horizontal axis
		Object constraint = labelLayout.getConstraint(statePathLabel);
		if (constraint != null && constraint instanceof AxisConstraint) {
			AxisConstraint axisConstraint = (AxisConstraint) constraint;
			axisConstraint.setMinorAxisFixedValue(horizontalAxisY);
			getFigure().setConstraint(statePathLabel, axisConstraint);
			LOG.log(Level.FINER,
					"Updated axis constraint for label");
		}

	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		updateAxisConstraints();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}

	@Override
	protected List<?> getModelChildren() {

		Tree<State, StateSpaceTransition> spTree = getTree();
		List<Object> children = new ArrayList<>();

		children.addAll(spTree.getAllNodes());

		return children;
	}

	@Override
	protected void registerModelUpdateListener() {
		super.registerModelUpdateListener();
		Tree<State, StateSpaceTransition> tree = getTree();
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				node.eAdapters().add(modelUpdateListener);
			}
		}
	}

	@Override
	protected void notifyModelChanged() {
		super.notifyModelChanged();
		Tree<State, StateSpaceTransition> tree = getTree();
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				if (!node.eAdapters().contains(modelUpdateListener)) {
					node.eAdapters().add(modelUpdateListener);
				}
			}
		}
	}

	@Override
	protected void unregisterModelUpdateListener() {
		super.registerModelUpdateListener();
		Tree<State, StateSpaceTransition> tree = getTree();
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				node.eAdapters().remove(modelUpdateListener);
			}
		}
	}

	/**
	 * 
	 * @return the tree model instance or null if the model is not a
	 *         {@link Tree}
	 */
	@SuppressWarnings("unchecked")
	private Tree<State, StateSpaceTransition> getTree() {
		Object model = getModel();
		if (model instanceof Tree<?, ?>) {
			return (Tree<State, StateSpaceTransition>) model;
		}
		return null;
	}

}
