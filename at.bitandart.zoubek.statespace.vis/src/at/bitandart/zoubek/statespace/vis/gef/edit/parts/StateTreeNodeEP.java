/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;

import at.bitandart.zoubek.statespace.vis.layout.TreeLayoutMarker;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Edit part for a StatePathTreeNode instance
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateTreeNodeEP extends BaseEditPart {

	@SuppressWarnings("unused")
	private static final Logger	LOG	= Logger.getLogger(StateTreeNodeEP.class.getCanonicalName());

	public StateTreeNodeEP() {
	}

	@Override
	protected IFigure createFigure() {
		IFigure figure = new Figure();
		figure.setLayoutManager(new XYLayout());
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		@SuppressWarnings("unchecked")
		TreeNode<State, StateSpaceTransition> node = (TreeNode<State, StateSpaceTransition>) getModel();
		Map<?, ?> ePRegistry = getRoot().getViewer().getEditPartRegistry();

		TreeLayoutMarker marker = new TreeLayoutMarker();
		marker.setLeaf(node.getChildTransitions().isEmpty());
		if (node.getParentTransition() != null) {
			TreeTransition<State, StateSpaceTransition> transition = node.getParentTransition();
			TreeNode<State, StateSpaceTransition> prevNode = transition.getPreviousNode();
			if (ePRegistry.containsKey(prevNode)) {
				GraphicalEditPart gEP = (GraphicalEditPart) ePRegistry.get(prevNode);
				marker.setPrevFigure(gEP.getFigure());
			}
		}

		((GraphicalEditPart) this.getParent()).setLayoutConstraint(	this,
																	getFigure(),
																	marker);
		super.refreshVisuals();
	}

	@Override
	protected List<?> getModelSourceConnections() {
		@SuppressWarnings("unchecked")
		TreeNode<State, StateSpaceTransition> node = (TreeNode<State, StateSpaceTransition>) getModel();
		List<Object> sourceConnections = new ArrayList<>();
		sourceConnections.addAll(node.getChildTransitions());

		return sourceConnections;
	}

	@Override
	protected List<?> getModelTargetConnections() {
		@SuppressWarnings("unchecked")
		TreeNode<State, StateSpaceTransition> node = (TreeNode<State, StateSpaceTransition>) getModel();
		List<Object> targetConnections = new ArrayList<>();
		TreeTransition<State, StateSpaceTransition> transition = node.getParentTransition();
		if (transition != null) {
			targetConnections.add(transition);
		}
		return targetConnections;
	}

	@Override
	protected List<?> getModelChildren() {
		TreeNode<State, StateSpaceTransition> node = getStateTreeNode();
		State currentState = node.getNodeValue();
		// build a tree of the current state and is successors, the tree object itself is not needed so just create the nodes
		TreeNode<State, StateSpaceTransition> rootSubNode = StatespacevisFactory.eINSTANCE.createTreeNode();
		rootSubNode.setNodeValue(currentState);

		EList<StateSpaceTransition> outgoingTransitions = currentState.getOutgoingTransitions();
		List<Object> children = new ArrayList<>(outgoingTransitions.size() + 1);

		children.add(rootSubNode);
		for (StateSpaceTransition transition : outgoingTransitions) {

			if (!childTransitionsContainsValue(node, transition)) {
				TreeNode<State, StateSpaceTransition> childSubNode = StatespacevisFactory.eINSTANCE.createTreeNode();
				childSubNode.setNodeValue(transition.getNextState());
				TreeTransition<State, StateSpaceTransition> childSubTransition = StatespacevisFactory.eINSTANCE.createTreeTransition();
				childSubTransition.setTransitionValue(transition);
				childSubTransition.setPreviousNode(rootSubNode);
				childSubTransition.setNextNode(childSubNode);

				children.add(childSubNode);
			}
		}
		return children;
	}

	/**
	 * determines if a stateSpaceTransition value is already contained in the
	 * child transitions of the given node
	 * 
	 * @param node
	 * @param stateSpaceTransition
	 * @return true if the stateSpaceTransition is contained as value in a child
	 *         transition, false otherwise
	 */
	private boolean childTransitionsContainsValue(
			TreeNode<State, StateSpaceTransition> node,
			StateSpaceTransition stateSpaceTransition) {
		for (TreeTransition<State, StateSpaceTransition> childTransition : node.getChildTransitions()) {
			if (childTransition.getTransitionValue() == stateSpaceTransition) {
				return true;
			}
		}

		return false;
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		super.addChildVisual(childEditPart, index);
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		IFigure contentPane = getContentPane();
		LayoutManager layoutManager = contentPane.getLayoutManager();
		if (index > 0 && childEditPart instanceof GraphicalEditPart) {
			if (layoutManager instanceof XYLayout) {
				layoutManager.setConstraint(child,
											new Rectangle(	20 * (index - 1),
															0,
															10,
															10));
			}
		} else if (index < 1 && childEditPart instanceof GraphicalEditPart) {
			if (layoutManager instanceof XYLayout) {
				layoutManager.setConstraint(child, new Rectangle(0, 50, 10, 10));
			}
		}
	}

	@Override
	protected void createEditPolicies() {
	}

	/**
	 * 
	 * @return the state tree node , or null if the model is not a state
	 */
	@SuppressWarnings("unchecked")
	public TreeNode<State, StateSpaceTransition> getStateTreeNode() {
		Object model = getModel();
		if (model instanceof TreeNode) {
			return (TreeNode<State, StateSpaceTransition>) model;
		}
		return null;
	}
}
