package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

public abstract class BaseEditPart extends AbstractGraphicalEditPart {

	private static final Logger	LOG	= Logger.getLogger(BaseEditPart.class.getCanonicalName());
	private EContentAdapter		modelUpdateListener;

	public BaseEditPart() {
		super();
	}

	/**
	 * adapter class that listens for model changes and calls
	 * {@link BaseEditPart#notifyModelChanged()}
	 * 
	 * @author Florian Zoubek <zoubek@bitandart.at>
	 * 
	 */
	private class ModelUpdateListener extends EContentAdapter {
		@Override
		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			LOG.log(Level.FINE,
					"model change notification received for editpart {0}, new value: {1}",
					new Object[] { BaseEditPart.this,
							notification.getNewValue() });
			notifyModelChanged();
		}
	}

	/**
	 * called when model change has been detected by the current model listener
	 */
	protected void notifyModelChanged() {
		refresh();
	}

	@Override
	public void setModel(Object model) {
		unregisterModelUpdateListener();
		super.setModel(model);
		registerModelUpdateListener();
	}

	/**
	 * adds the model update listener to the current model instance
	 */
	protected void registerModelUpdateListener() {
		Object model = getModel();
		if (model != null && model instanceof EObject) {
			((EObject) model).eAdapters().add(getModelUpdateListener());
		}
	}

	/**
	 * removes the model update listener from the current model instance
	 */
	protected void unregisterModelUpdateListener() {
		Object model = getModel();
		if (model != null && model instanceof EObject) {
			((EObject) model).eAdapters().remove(getModelUpdateListener());
		}
	}

	/**
	 * 
	 * @return the edit parts listener for model changes
	 */
	protected EContentAdapter getModelUpdateListener() {
		if (modelUpdateListener == null) {
			modelUpdateListener = createModelUpdateListener();
		}
		return modelUpdateListener;
	}

	/**
	 * creates the adapter that is used to track model changes for this edit
	 * part. Creates a listener which which calls {@link #notifyModelChanged()}
	 * if the model changes. Subclasses may override, but the listener provided
	 * by this method should call {@link #notifyModelChanged()} if the model has
	 * changed and the edit part needs to be notified of model changes.
	 * 
	 * @return
	 */
	protected EContentAdapter createModelUpdateListener() {
		return new ModelUpdateListener();
	}

	@Override
	public void deactivate() {
		unregisterModelUpdateListener();
	}

}