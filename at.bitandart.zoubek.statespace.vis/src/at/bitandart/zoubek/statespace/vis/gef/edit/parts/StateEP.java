/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.FanRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.PrecisionDimension;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.figures.StateFigure;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Edit part for a State instance
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateEP extends BaseEditPart {

	private ConnectionRouter					connectionRouter	= new FanRouter();
	private Tree<State, StateSpaceTransition>	selectedStatePathTree;
	private Color								highlightColor;
	private Color								normalColor;

	@SuppressWarnings("unused")
	private static final Logger					LOG					= Logger.getLogger(StateEP.class.getCanonicalName());

	public StateEP(Tree<State, StateSpaceTransition> selectedStatePathTree) {
		this(selectedStatePathTree, true, true);
	}

	public StateEP(Tree<State, StateSpaceTransition> selectedStatePathTree,
			boolean showTargetConnections, boolean showSourceConnections) {
		if (selectedStatePathTree == null) {
			this.selectedStatePathTree = StatespacevisFactory.eINSTANCE.createTree();
		} else {
			this.selectedStatePathTree = selectedStatePathTree;
		}
		highlightColor = new Color(Display.getDefault(), 0, 0, 0);
		normalColor = new Color(Display.getDefault(), 150, 150, 150);
	}

	@Override
	protected IFigure createFigure() {
		IFigure figure = new StateFigure();
		figure.setPreferredSize(new PrecisionDimension(15, 15));
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		State state = getState();
		if (figure != null && state != null) {
			StateFigure figure = (StateFigure) this.figure;
			TreeNode<State, StateSpaceTransition> rootNode = selectedStatePathTree.getRootNode();

			// determine highlight state
			if (selectedStatePathTree.containsNodeValue(state)) {
				figure.setForegroundColor(highlightColor);
			} else {
				figure.setForegroundColor(normalColor);
			}

			// determine state type
			StateFigure.StateType type = StateFigure.StateType.NORMAL;
			if (state.getOutgoingTransitions().isEmpty()) {
				type = StateFigure.StateType.END;
			} else if (rootNode != null
						&& state.equals(rootNode.getNodeValue())) {
				type = StateFigure.StateType.INITIAL;
			}
			figure.setType(type);
		}
	}

	@Override
	protected List<?> getModelSourceConnections() {
		State state = (State) getModel();
		List<Object> sourceConnections = new ArrayList<>();
		sourceConnections.addAll(state.getOutgoingTransitions());
		return sourceConnections;
	}

	@Override
	protected List<?> getModelTargetConnections() {
		State state = (State) getModel();
		List<Object> targetConnections = new ArrayList<>();
		targetConnections.addAll(state.getIncomingTransitions());
		return targetConnections;
	}

	@Override
	protected void createEditPolicies() {
	}

	@Override
	protected void registerModelUpdateListener() {
		super.registerModelUpdateListener();
		// register also to state path changes
		Tree<State, StateSpaceTransition> tree = selectedStatePathTree;
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				node.eAdapters().add(modelUpdateListener);
			}
			for (TreeTransition<State, StateSpaceTransition> transition : tree.getAllTransitions()) {
				transition.eAdapters().add(modelUpdateListener);
			}
		}
	}

	@Override
	protected void notifyModelChanged() {
		super.notifyModelChanged();
		// add listener also to new state path elements
		Tree<State, StateSpaceTransition> tree = selectedStatePathTree;
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				if (!node.eAdapters().contains(modelUpdateListener)) {
					node.eAdapters().add(modelUpdateListener);
				}
			}
			for (TreeTransition<State, StateSpaceTransition> transition : tree.getAllTransitions()) {
				if (!transition.eAdapters().contains(modelUpdateListener)) {
					transition.eAdapters().add(modelUpdateListener);
				}
			}
		}
	}

	@Override
	protected void unregisterModelUpdateListener() {
		super.registerModelUpdateListener();
		// unregister also all state path listeners 
		Tree<State, StateSpaceTransition> tree = selectedStatePathTree;
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				node.eAdapters().remove(modelUpdateListener);
			}
			for (TreeTransition<State, StateSpaceTransition> transition : tree.getAllTransitions()) {
				transition.eAdapters().remove(modelUpdateListener);
			}
		}
	}

	@Override
	public void deactivate() {
		super.deactivate();
		highlightColor.dispose();
		normalColor.dispose();
	}

	/**
	 * 
	 * @return the state, or null if the model is not a state
	 */
	public State getState() {
		Object model = getModel();
		if (model instanceof State) {
			return (State) model;
		}
		return null;
	}

	/**
	 * @return the connectionRouter
	 */
	public ConnectionRouter getConnectionRouter() {
		return connectionRouter;
	}

}
