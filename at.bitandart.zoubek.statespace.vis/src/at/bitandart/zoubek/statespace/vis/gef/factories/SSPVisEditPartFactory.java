/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.factories;

import java.util.logging.Logger;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.LifelineGroupEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.OLObjectStateEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.OLObjectStateTransitionEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.ObjectLifelineEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateSpacePathVisEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeNodeEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeSubNodeEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeSubTransitionEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateTreeTransitionEP;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * The edit part factory for the state space path visualization. The default
 * Behavior of this class can be overridden by usage of the epfactories
 * extension point.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisEditPartFactory implements EditPartFactory {

	private static final Logger	LOG										= Logger.getLogger(SSPVisEditPartFactory.class.getCanonicalName());

	/**
	 * the extension point id used to find edit part factories for the SSP
	 * visualization
	 */
	private static final String	EXT_POINT_SSPVIS_EDIT_PART_FACTORIES_ID	= "at.bitandart.zoubek.statespace.vis.ssp.epfactories";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public EditPart createEditPart(EditPart contextEditPart, Object object) {
		EditPart newEP = createEditPartFromContributingFactories(	contextEditPart,
																	object);
		if (newEP != null) {
			return newEP;
		}
		// TODO refactor to use setModel() instead of the constructor
		if (object instanceof StateSpacePathVis) {
			newEP = new StateSpacePathVisEP((StateSpacePathVis) object);
		}
		if (object instanceof LifelineGroup) {
			newEP = new LifelineGroupEP();
			newEP.setModel(object);
		}
		if (object instanceof ObjectLifeline) {
			newEP = new ObjectLifelineEP();
			newEP.setModel(object);
		}
		if (object instanceof Tree) {
			if (contextEditPart.getModel() instanceof StateSpacePathVis) {
				newEP = new StateTreeEP();
				newEP.setModel(object);
			}
		}
		if (object instanceof TreeNode) {
			if (contextEditPart.getModel() instanceof ObjectLifeline) {
				newEP = new OLObjectStateTransitionEP((ObjectLifeline) contextEditPart.getModel());
				newEP.setModel(object);
			}
			if (contextEditPart.getParent().getModel() instanceof StateSpacePathVis) {
				newEP = new StateTreeNodeEP();
				newEP.setModel(object);
			}
			if (contextEditPart.getModel() instanceof TreeNode) {
				newEP = new StateTreeSubNodeEP((Tree<State, StateSpaceTransition>) contextEditPart.getParent().getModel());
				newEP.setModel(object);
			}
		}
		if (object instanceof TreeTransition) {
			if (contextEditPart.getParent().getModel() instanceof ObjectLifeline) {
				newEP = new OLObjectStateEP((ObjectLifeline) contextEditPart.getParent()
																			.getModel());
				newEP.setModel(object);
			}
			if (contextEditPart.getParent().getParent().getModel() instanceof StateSpacePathVis) {
				newEP = new StateTreeTransitionEP();
				newEP.setModel(object);
			}
			if (contextEditPart.getParent().getModel() instanceof TreeNode) {
				newEP = new StateTreeSubTransitionEP((Tree<State, StateSpaceTransition>) contextEditPart.getParent().getParent().getModel());
				newEP.setModel(object);
			}

		}
		return newEP;
	}

	/**
	 * checks if any of the contributing factories already provides an EditPart
	 * implementation for the given model and returns the EditPart
	 * 
	 * @param editPart
	 *            the context
	 * @param object
	 *            the object
	 * @return null if no factory provides an edit part for the given object
	 */
	private EditPart createEditPartFromContributingFactories(
			final EditPart editPart, final Object object) {

		IConfigurationElement[] config = Platform.getExtensionRegistry()
													.getConfigurationElementsFor(EXT_POINT_SSPVIS_EDIT_PART_FACTORIES_ID);
		try {
			for (IConfigurationElement e : config) {

				final Object o = e.createExecutableExtension("class");
				if (o instanceof EditPartFactory) {

					// make sure exceptions thrown in contributed factories
					// won't mess up the whole visualization
					SafeEPFactoryEvaluator runnable = new SafeEPFactoryEvaluator(	editPart,
																					object,
																					(EditPartFactory) o);
					SafeRunner.run(runnable);
					if (runnable.getEditPart() != null) {
						return runnable.getEditPart();
					}
				}
			}
		} catch (CoreException ex) {
			LOG.severe(ex.getMessage());
		}
		return null;
	}
}
