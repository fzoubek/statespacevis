/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.logging.Logger;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;

import at.bitandart.zoubek.statespace.vis.figures.CustomRectChopboxAnchor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Connection edit part for StatePathTreeConnection instances
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateTreeTransitionEP extends BaseConnectionEditPart {

	@SuppressWarnings("unused")
	private static final Logger	LOG	= Logger.getLogger(StateTreeTransitionEP.class.getCanonicalName());

	private Label				label;
	private PolylineConnection	connection;
	private PolylineDecoration	targetDecoration;

	@Override
	protected IFigure createFigure() {
		connection = new PolylineConnection();
		targetDecoration = new PolylineDecoration();
		connection.setTargetDecoration(targetDecoration);

		label = new Label();
		label.setBorder(new MarginBorder(1, 3, 1, 3));
		label.setOpaque(true);

		connection.add(label, new ConnectionLocator(connection,
													ConnectionLocator.MIDDLE) {
			@Override
			public void relocate(IFigure target) {
				super.relocate(target);
				// never allow negative positions as they result in an infinite loop
				Rectangle bounds = target.getBounds();
				bounds.x = Math.max(bounds.x, 0);
				bounds.y = Math.max(bounds.y, 0);
				target.setBounds(bounds);
			}
		});

		return connection;
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		StateSpaceTransition stateSpaceTransition = getStateSpaceTransition();
		if (stateSpaceTransition != null) {
			label.setText(stateSpaceTransition.getTransitionName());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// No op
	}
	
	@Override
	protected ConnectionAnchor getSourceConnectionAnchor() {
		if (getSource() != null) {
			return new CustomRectChopboxAnchor(((GraphicalEditPart) getSource()).getFigure(), new Rectangle(0, 50, 10, 10));
		}
		return super.getSourceConnectionAnchor();
	}

	@Override
	protected ConnectionAnchor getTargetConnectionAnchor() {
		if (getTarget() != null) {
			return new CustomRectChopboxAnchor(((GraphicalEditPart) getTarget()).getFigure(), new Rectangle(0, 50, 10, 10));
		}
		return super.getTargetConnectionAnchor();
	}

	@SuppressWarnings("unchecked")
	public StateSpaceTransition getStateSpaceTransition() {
		return ((TreeTransition<State, StateSpaceTransition>) getModel()).getTransitionValue();
	}

}
