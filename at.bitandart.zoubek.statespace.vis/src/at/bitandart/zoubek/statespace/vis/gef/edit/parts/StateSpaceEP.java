/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PrecisionRectangle;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.utils.Vector2D;

/**
 * Edit part for a StateSpace instance, provides only a Freeform layer for all
 * child elements
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpaceEP extends BaseEditPart {

	private static final Logger	LOG	= Logger.getLogger(StateSpaceEP.class.getCanonicalName());

	/**
	 * Runnable the updates the child figures of the edit parts main figure
	 * based using a spring layout algorithm.
	 * 
	 * @author Florian Zoubek <zoubek@bitandart.at>
	 * 
	 */
	private class SpringLayoutConstraintUpdater implements Runnable {

		/**
		 * indicates if the algorithm should be stopped, regardless of if it is
		 * finished or not
		 */
		private boolean	shouldStop				= false;

		/**
		 * the virtual spring length used for attraction
		 */
		private double	springLength			= 60;

		/**
		 * the factor used to scale the spring length for important nodes
		 */
		private double	springFacImportant		= 1;

		/**
		 * the strength used for repulsion
		 */
		private double	repStrength				= 2;

		/**
		 * the factor used to scale the repulsion strength for important nodes
		 */
		private double	repStrenghtFacImportant	= 1;

		/**
		 * the threshold of the translation used to determine when the algorithm
		 * should stop. This value must be >= 1.0 if the constraints are
		 * rectangles with single precision.
		 */
		private double	stopThreshold			= 0.025;

		/**
		 * determines the minimum time to wait in milliseconds before a redraw
		 * is requested
		 */
		private long	updateThreshold			= 200;

		/**
		 * indicates if a redraw request has been set and not yet finished
		 */
		private boolean	updateInProgress		= false;

		@Override
		public void run() {
			LOG.fine("Started layout updater");
			long lastUpdate = 0;
			Runnable redrawRequest = new Runnable() {

				@Override
				public void run() {
					LOG.log(Level.FINER, "Requesting redraw");
					getFigure().revalidate();
					updateInProgress = false;
				}
			};
			Vector2D minCoords = null;
			while (!shouldStop) {

				final IFigure mainFigure = getFigure();
				minCoords = null;

				// calculate forces and new positions
				List<State> nodes = ((StateSpace) getModel()).getAllStateSpaceNodes();
				ArrayList<Vector2D> positions = new ArrayList<>();
				ArrayList<IFigure> figures = new ArrayList<>();
				double maxDelta = 0;
				for (State node : nodes) {
					Vector2D force = new Vector2D(0, 0);
					Vector2D cAttForce = new Vector2D(0, 0);
					Vector2D cRepForce = new Vector2D(0, 0);
					IFigure nodeFigure = getNodeFigure(node);
					if (nodeFigure != null) {
						figures.add(nodeFigure);
						Vector2D nodePos = new Vector2D(getFigureConstraint(nodeFigure).getTopLeft());
						LOG.log(Level.FINEST, "node position: {0}", nodePos);

						for (State sibling : nodes) {
							if (!sibling.equals(node)) {
								Rectangle siblingBounds = getNodeBounds(sibling);
								if (siblingBounds != null) {
									Vector2D siblingPosition = new Vector2D(siblingBounds.getTopLeft());
									LOG.log(Level.FINEST,
											"sibling position: {0}",
											siblingPosition);

									Vector2D repForce = new Vector2D(siblingPosition);
									repForce.subtract(nodePos);
									LOG.log(Level.FINEST,
											"distance vector: {0}",
											repForce);

									repForce.normalize();
									LOG.log(Level.FINEST,
											"normalized: {0}",
											repForce);
									double repFactor = 1;
									double sprFactor = 1;
									if (selectedStatePathTree.containsNodeValue(sibling)) {
										// increase factor for important nodes
										repFactor = repStrenghtFacImportant;
										sprFactor = springFacImportant;
									}
									repForce.multiply((Math.pow(springLength
																* sprFactor, 2) * (repStrength * repFactor))
														/ Math.pow(	Vector2D.distance(	nodePos,
																						siblingPosition),
																	2));
									repForce.multiply(-1);
									cRepForce.add(repForce);
								}
							}
						}

						List<StateSpaceTransition> transitions = new ArrayList<>();
						transitions.addAll(node.getIncomingTransitions());
						transitions.addAll(node.getOutgoingTransitions());
						for (StateSpaceTransition edge : transitions) {
							State sibling = edge.getPrevState();
							if (sibling.equals(node)) {
								sibling = edge.getNextState();
							}
							if (!sibling.equals(node)) {
								Rectangle siblingBounds = getNodeBounds(sibling);
								if (siblingBounds != null) {
									Vector2D siblingPosition = new Vector2D(siblingBounds.getTopLeft());
									Vector2D attForce = new Vector2D(siblingPosition).subtract(nodePos);
									attForce.normalize();
									double sprFactor = 1;
									if (selectedStatePathTree.containsNodeValue(sibling)) {
										// increase factor for important nodes
										sprFactor = springFacImportant;
									}
									attForce.multiply(Vector2D.distance(nodePos,
																		siblingPosition)
														/ (springLength * sprFactor));
									cAttForce.add(attForce);
								}
							}
						}
						LOG.log(Level.FINEST,
								"Calculated attraction force: {0}",
								cAttForce);
						LOG.log(Level.FINEST,
								"Calculated repulsion force: {0}",
								cAttForce);
						force.add(cAttForce);
						force.add(cRepForce);

						double length = force.getLength();
						nodePos.add(force);

						// make sure we don't have negative coordinates 
						if (nodePos.getX() < 0) {
							nodePos.setX(0);
						}
						if (nodePos.getY() < 0) {
							nodePos.setY(0);
						}

						if (length > maxDelta) {
							maxDelta = length;
						}

						positions.add(nodePos);
					}
				}

				// apply position constraints
				if (mainFigure != null) {
					LayoutManager layoutManager = mainFigure.getLayoutManager();
					for (int i = 0; i < positions.size(); i++) {
						IFigure child = figures.get(i);
						Vector2D position = positions.get(i);
						Object constraint = layoutManager.getConstraint(child);
						if (constraint instanceof Rectangle) {

							Rectangle rect = (Rectangle) constraint;
							if (constraint instanceof PrecisionRectangle) {
								// use precise value if possible
								PrecisionRectangle pRect = (PrecisionRectangle) rect;
								pRect.setPreciseX(position.getX());
								pRect.setPreciseY(position.getY());
							} else {
								// fallback for single precision, stopThreshold must be >= 1.0 or this thread will never stop
								rect.x = (int) Math.round(position.getX());
								rect.y = (int) Math.round(position.getY());
							}

							// update minimum X and Y coordinates, these will be used to reposition all nodes after determination of the final relative positions
							if (minCoords == null) {
								minCoords = new Vector2D(position);
							} else {
								if (position.getX() < minCoords.getX()) {
									minCoords.setX(position.getX());
								}
								if (position.getY() < minCoords.getY()) {
									minCoords.setY(position.getY());
								}
							}

							LOG.log(Level.FINEST,
									"Setting new constraint for {0} : {1}",
									new Object[] { child, rect });
							layoutManager.setConstraint(child, rect);
						}

					}
				}
				LOG.log(Level.FINER, "Constraints updated");

				if (System.currentTimeMillis() - lastUpdate > updateThreshold
					&& !updateInProgress) {
					updateInProgress = true;
					lastUpdate = System.currentTimeMillis();
					Display.getDefault().asyncExec(redrawRequest);
				}

				LOG.log(Level.FINER, "Current delta: {0}", maxDelta);
				if (maxDelta <= stopThreshold) {
					shouldStop = true;
				}
			}

			// move the nodes to the top left
			LOG.log(Level.FINEST, "Moving node figures to top left");
			final IFigure mainFigure = getFigure();
			if (mainFigure != null && minCoords != null) {
				LayoutManager layoutManager = mainFigure.getLayoutManager();
				List<State> nodes = ((StateSpace) getModel()).getAllStateSpaceNodes();
				for (State node : nodes) {
					IFigure child = getNodeFigure(node);
					if (child != null) {
						Object constraint = layoutManager.getConstraint(child);
						if (constraint instanceof Rectangle) {

							Rectangle rect = (Rectangle) constraint;
							if (constraint instanceof PrecisionRectangle) {
								// use precise value if possible
								PrecisionRectangle pRect = (PrecisionRectangle) rect;
								pRect.setPreciseX(pRect.preciseX()
													- minCoords.getX());
								pRect.setPreciseY(pRect.preciseY()
													- minCoords.getY());
							} else {
								// fallback for single precision
								rect.x -= (int) Math.round(minCoords.getX());
								rect.y -= (int) Math.round(minCoords.getY());
							}

							LOG.log(Level.FINEST,
									"Setting new constraint for {0} : {1}",
									new Object[] { child, rect });
							layoutManager.setConstraint(child, rect);
						}
					}
				}
			}

			Display.getDefault().asyncExec(redrawRequest);

			LOG.fine("Stopped layout updater");
		}

		// some shortcut methods

		/**
		 * 
		 * @param node
		 * @return null if the edit part for the node does not exist (yet)
		 */
		private IFigure getNodeFigure(State node) {
			EditPart editPart = (EditPart) getViewer().getEditPartRegistry()
														.get(node);
			if (editPart != null) {
				return ((GraphicalEditPart) editPart).getFigure();
			}
			return null;
		}

		/**
		 * 
		 * @param node
		 * @return null if the edit part for the node does not exist (yet)
		 */
		private Rectangle getNodeBounds(State node) {
			return getFigureConstraint(getNodeFigure(node));
		}

		/**
		 * 
		 * @param figure
		 * @return null if the given figure was null
		 */
		private Rectangle getFigureConstraint(IFigure figure) {
			if (figure != null) {
				return (Rectangle) figure.getParent().getLayoutManager()
											.getConstraint(figure);
			} else {
				return null;
			}
		}

		public void stop() {
			shouldStop = true;
		}

		public void initialize() {
			shouldStop = false;
		}
	}

	private Thread								layoutUpdateThread;
	private SpringLayoutConstraintUpdater		springLayoutConstraintUpdater;
	private Tree<State, StateSpaceTransition>	selectedStatePathTree;

	public StateSpaceEP(Tree<State, StateSpaceTransition> selectedStatePathTree) {
		springLayoutConstraintUpdater = new SpringLayoutConstraintUpdater();
		this.selectedStatePathTree = selectedStatePathTree;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		Figure figure = new Figure();
		figure.setLayoutManager(new XYLayout());
		springLayoutConstraintUpdater.initialize();
		layoutUpdateThread = new Thread(springLayoutConstraintUpdater);
		layoutUpdateThread.start();
		return figure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
	}

	@Override
	protected List<?> getModelChildren() {
		List<Object> children = new ArrayList<Object>();

		StateSpace model = (StateSpace) getModel();
		children.addAll(model.getAllStateSpaceNodes());

		return children;
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		super.addChildVisual(childEditPart, index);
		if (childEditPart.getModel() instanceof State) {

			// assign random position for state figures
			LayoutManager layoutManager = figure.getLayoutManager();
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();

			if (layoutManager instanceof XYLayout) {
				randomizeChildPosition(child, (XYLayout) layoutManager);
			}

		}

		// start thread if it does not exist yet
		if (!layoutUpdateThread.isAlive()) {
			springLayoutConstraintUpdater.initialize();
			layoutUpdateThread = new Thread(springLayoutConstraintUpdater);
			layoutUpdateThread.start();
		}
	}

	@Override
	public void deactivate() {

		// stop runnables
		springLayoutConstraintUpdater.stop();
		super.deactivate();
	}

	/**
	 * sets the given child figure's XYLayout constraint to a random position
	 * 
	 * @param child
	 *            the child which is positioned
	 * @param layoutManager
	 *            the XYLayout Manager of the parent figure of the given child
	 */
	private void randomizeChildPosition(IFigure child, XYLayout layoutManager) {
		StateSpace model = (StateSpace) getModel();
		Dimension preferredSize = child.getPreferredSize();
		PrecisionRectangle constraint = new PrecisionRectangle();

		int numNodes = model.getAllStateSpaceNodes().size();
		constraint.setPreciseX(Math.round(preferredSize.width()
											* numNodes * Math.random()));
		constraint.setPreciseY(Math.round(preferredSize.height()
											* numNodes * Math.random()));

		constraint.setSize(preferredSize);
		layoutManager.setConstraint(child, constraint);
	}

	/**
	 * recalculates the layout if the layout thread is currently not running
	 */
	public void requestLayoutRecalculation() {
		if (!layoutUpdateThread.isAlive()) {
			springLayoutConstraintUpdater.initialize();
			
			LayoutManager layoutManager = figure.getLayoutManager();
			
			if(layoutManager instanceof XYLayout){
				XYLayout xyLayout = (XYLayout) layoutManager;
				for(Object child : figure.getChildren()){
					if(child instanceof IFigure){
						randomizeChildPosition((IFigure) child, xyLayout);
					}
				}
			}
			layoutUpdateThread = new Thread(springLayoutConstraintUpdater);
			layoutUpdateThread.start();
		}
	}

	/**
	 * requests the layout thread to stop if the layout thread is currently
	 * running
	 */
	public void requestLayoutCalculationStop() {
		if (!layoutUpdateThread.isAlive()) {
			springLayoutConstraintUpdater.stop();
		}
	}

}
