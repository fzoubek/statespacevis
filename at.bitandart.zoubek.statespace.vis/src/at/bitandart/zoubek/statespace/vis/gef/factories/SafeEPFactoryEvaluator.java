package at.bitandart.zoubek.statespace.vis.gef.factories;

import java.util.logging.Logger;

import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

/**
 * Utility class that executes the <code>createEditPart()</code> method of a
 * EditPartFactory safely. Thrown exceptions will be logged with the default
 * logger.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
class SafeEPFactoryEvaluator implements ISafeRunnable {

	private static final Logger	LOG	= Logger.getLogger(SafeEPFactoryEvaluator.class.getCanonicalName());

	private EditPart			context;
	private Object				object;
	private EditPart			editPart;
	private EditPartFactory		editPartFactory;

	/**
	 * 
	 * @param context
	 *            the context used for <code>createEditPart()</code>
	 * @param object
	 *            the object used for <code>createEditPart()</code>
	 * @param editPartFactory
	 *            the <code>EditPartFactory</code> to use
	 */
	public SafeEPFactoryEvaluator(EditPart context, Object object,
			EditPartFactory editPartFactory) {
		super();
		this.context = context;
		this.object = object;
		this.editPartFactory = editPartFactory;
	}

	@Override
	public void handleException(Throwable e) {

		LOG.severe("Exception thrown in EditPartFactory "
					+ object.getClass().getCanonicalName()
					+ "\n"
					+ ((e.getLocalizedMessage() != null)
							? e.getLocalizedMessage() : ""));
	}

	@Override
	public void run() throws Exception {
		editPart = editPartFactory.createEditPart(context, object);
	}

	/**
	 * 
	 * @return the edit part or null if an exception occurred or no edit part
	 *         has been created
	 */
	public EditPart getEditPart() {
		return editPart;
	}

}