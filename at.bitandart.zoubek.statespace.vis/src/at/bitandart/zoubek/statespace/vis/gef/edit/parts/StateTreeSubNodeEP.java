/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.figures.StateFigure;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Edit part for a State instance
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateTreeSubNodeEP extends BaseEditPart {

	private Tree<State, StateSpaceTransition>	selectedStatePathTree;
	private Color								highlightColor;
	private Color								normalColor;

	@SuppressWarnings("unused")
	private static final Logger					LOG	= Logger.getLogger(StateTreeSubNodeEP.class.getCanonicalName());

	public StateTreeSubNodeEP(
			Tree<State, StateSpaceTransition> selectedStatePathTree) {
		this(selectedStatePathTree, true, true);
	}

	public StateTreeSubNodeEP(
			Tree<State, StateSpaceTransition> selectedStatePathTree,
			boolean showTargetConnections, boolean showSourceConnections) {
		if (selectedStatePathTree == null) {
			this.selectedStatePathTree = StatespacevisFactory.eINSTANCE.createTree();
		} else {
			this.selectedStatePathTree = selectedStatePathTree;
		}
		highlightColor = new Color(Display.getDefault(), 0, 0, 0);
		normalColor = new Color(Display.getDefault(), 150, 150, 150);
	}

	@Override
	protected IFigure createFigure() {
		IFigure figure = new StateFigure();
		return figure;
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		State state = getState();
		if (figure != null && state != null) {
			StateFigure figure = (StateFigure) this.figure;
			TreeNode<State, StateSpaceTransition> rootNode = selectedStatePathTree.getRootNode();

			// determine highlight state
			if (selectedStatePathTree.containsNodeValue(state)) {
				figure.setForegroundColor(highlightColor);
			} else {
				figure.setForegroundColor(normalColor);
			}

			// determine state type
			StateFigure.StateType type = StateFigure.StateType.NORMAL;
			if (state.getOutgoingTransitions().isEmpty()) {
				type = StateFigure.StateType.END;
			} else if (rootNode != null
						&& state.equals(rootNode.getNodeValue())) {
				type = StateFigure.StateType.INITIAL;
			}
			figure.setType(type);
		}
	}

	@Override
	protected List<?> getModelSourceConnections() {
		TreeNode<State,StateSpaceTransition> node = getTreeNode();
		List<Object> sourceConnections = new ArrayList<>();
		sourceConnections.addAll(node.getChildTransitions());
		return sourceConnections;
	}

	@Override
	protected List<?> getModelTargetConnections() {
		TreeNode<State,StateSpaceTransition> node = getTreeNode();
		List<Object> targetConnections = new ArrayList<>();
		TreeTransition<State,StateSpaceTransition> parentTransition = node.getParentTransition();
		if(parentTransition != null){
			targetConnections.add(parentTransition);
		}
		return targetConnections;
	}

	@Override
	protected void createEditPolicies() {
	}

	@Override
	public void deactivate() {
		super.deactivate();
		highlightColor.dispose();
		normalColor.dispose();
	}

	/**
	 * 
	 * @return the state, or null if the model contains no state
	 */
	public State getState() {
		Object model = getModel();
		if (model instanceof TreeNode) {
			Object nodeVal = ((TreeNode<?, ?>) model).getNodeValue();
			if (nodeVal instanceof State) {
				return (State) nodeVal;
			}
		}
		return null;
	}

	/**
	 * 
	 * @return the tree node, or null if the model contains no tree node
	 */
	@SuppressWarnings("unchecked")
	public TreeNode<State, StateSpaceTransition> getTreeNode() {
		Object model = getModel();
		if (model instanceof TreeNode) {
			Object nodeVal = ((TreeNode<?, ?>) model).getNodeValue();
			if (nodeVal instanceof State) {
				return (TreeNode<State, StateSpaceTransition>) model;
			}
		}
		return null;
	}

}
