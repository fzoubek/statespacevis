/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;

import at.bitandart.zoubek.statespace.vis.layout.LifelineLayout;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;

/**
 * EditPart for a group of one or more lifelines
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class LifelineGroupEP extends BaseEditPart {

	/**
	 * the content pane of this edit part's figure, holds figures of child edit
	 * parts
	 */
	private IFigure	contentPane;

	private double	calculatedLifelineHeight	= 0.0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		IFigure figure = new Figure();
		FlowLayout layoutManager = new FlowLayout();
		figure.setLayoutManager(layoutManager);
		return figure;
	}

	@Override
	public IFigure getContentPane() {
		if (contentPane == null) {
			contentPane = new Figure();
			LifelineLayout layoutManager = new LifelineLayout();
			contentPane.setLayoutManager(layoutManager);
			figure.add(contentPane);
		}
		return contentPane;
	}

	@Override
	protected void refreshChildren() {
		super.refreshChildren();

		recalculateLifelineHeight();

		List<?> children = getChildren();
		ListIterator<?> childIterator = children.listIterator();

		while (childIterator.hasNext()) {
			EditPart child = ((EditPart) childIterator.next());
			child.refresh();
		}

		getFigure().invalidateTree();
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		super.addChildVisual(childEditPart, index);

		recalculateLifelineHeight();

		List<?> children = getChildren();
		ListIterator<?> childIterator = children.listIterator();

		while (childIterator.hasNext()) {
			((EditPart) childIterator.next()).refresh();
		}
	}

	private void recalculateLifelineHeight() {
		if (contentPane != null) {
			List<?> children = contentPane.getChildren();
			ListIterator<?> childIterator = children.listIterator();
			calculatedLifelineHeight = 0.0f;

			while (childIterator.hasNext()) {
				IFigure child = (IFigure) childIterator.next();
				if (child.isVisible()) {
					calculatedLifelineHeight += 20.0f;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}

	@Override
	protected List<?> getModelChildren() {
		LifelineGroup model = (LifelineGroup) getModel();
		List<Object> children = new ArrayList<Object>();

		for (Lifeline lifeline : model.getLifelines()) {
			addLifelineWithChildren(children, lifeline);
		}

		return children;
	}

	/**
	 * adds the lifeline and all children to the given collection.
	 * 
	 * @param collection
	 * @param lifeline
	 */
	private void addLifelineWithChildren(List<Object> collection,
			Lifeline lifeline) {
		collection.add(lifeline);
		for (Lifeline childLifeline : lifeline.getChildLifelines()) {
			addLifelineWithChildren(collection, childLifeline);
		}
	}

	@Override
	protected void registerModelUpdateListener() {
		super.registerModelUpdateListener();
		// register to child lifeline changes
		LifelineGroup lifelineGroup = getLifelineGroup();
		if (lifelineGroup != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (Lifeline lifeline : lifelineGroup.getLifelines()) {
				lifeline.eAdapters().add(modelUpdateListener);
			}
		}
	}

	@Override
	protected void notifyModelChanged() {
		super.notifyModelChanged();
		// register to child lifeline changes
		LifelineGroup lifelineGroup = getLifelineGroup();
		if (lifelineGroup != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (Lifeline lifeline : lifelineGroup.getLifelines()) {
				if (!lifeline.eAdapters().contains(modelUpdateListener)) {
					lifeline.eAdapters().add(modelUpdateListener);
				}
			}
		}
	}

	@Override
	protected void unregisterModelUpdateListener() {
		super.registerModelUpdateListener();
		// unregister to child lifeline changes
		LifelineGroup lifelineGroup = getLifelineGroup();
		if (lifelineGroup != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (Lifeline lifeline : lifelineGroup.getLifelines()) {
				lifeline.eAdapters().remove(modelUpdateListener);
			}
		}
	}

	/**
	 * 
	 * @return the lifeline group or null if the current model is not a instance
	 *         of {@link LifelineGroup}
	 */
	private LifelineGroup getLifelineGroup() {
		Object model = getModel();
		if (model instanceof LifelineGroup) {
			return (LifelineGroup) model;
		}
		return null;
	}

	/**
	 * @return the calculated Lifeline height
	 */
	public double getCalculatedLifelineHeight() {
		return calculatedLifelineHeight;
	}
}
