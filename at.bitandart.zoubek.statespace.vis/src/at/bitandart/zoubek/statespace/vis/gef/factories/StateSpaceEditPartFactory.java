/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.factories;

import java.util.logging.Logger;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateSpaceEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateSpaceTransitionEP;
import at.bitandart.zoubek.statespace.vis.gef.edit.parts.StateSpaceVisEP;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;

/**
 * The edit part factory for the state space visualization. The default Behavior
 * of this class can be overridden by usage of the
 * {@link StateSpaceEditPartFactory#EXT_POINT_SSPVIS_EDIT_PART_FACTORIES_ID}
 * extension point.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpaceEditPartFactory implements EditPartFactory {

	private static final Logger	LOG										= Logger.getLogger(StateSpaceEditPartFactory.class.getCanonicalName());

	/**
	 * the extension point id used to find edit part factories for the state
	 * space visualization
	 */
	private static final String	EXT_POINT_SSPVIS_EDIT_PART_FACTORIES_ID	= "at.bitandart.zoubek.statespace.vis.statespace.epfactories";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	@Override
	public EditPart createEditPart(EditPart contextEditPart, Object object) {
		EditPart newEP = createEditPartFromContributingFactories(	contextEditPart,
																	object);
		if (newEP != null) {
			return newEP;
		}
		if (object instanceof StateSpacePathVis) {
			newEP = new StateSpaceVisEP();
			newEP.setModel(object);
		}
		if (object instanceof StateSpace) {
			newEP = new StateSpaceEP(((StateSpacePathVis) contextEditPart.getModel()).getStatePathTree());
			newEP.setModel(object);
		}
		if (object instanceof State) {
			newEP = new StateEP(((StateSpacePathVis) contextEditPart.getParent().getModel()).getStatePathTree());
			newEP.setModel(object);
		}
		if (object instanceof StateSpaceTransition) {
			newEP = new StateSpaceTransitionEP(((StateSpacePathVis) contextEditPart.getParent().getParent().getModel()).getStatePathTree());
			newEP.setModel(object);
		}
		return newEP;
	}

	/**
	 * checks if any of the contributing factories already provides an EditPart
	 * implementation for the given model and returns the EditPart
	 * 
	 * @param editPart
	 *            the context
	 * @param object
	 *            the object
	 * @return null if no factory provides an edit part for the given object
	 */
	private EditPart createEditPartFromContributingFactories(
			final EditPart editPart, final Object object) {

		IConfigurationElement[] config = Platform.getExtensionRegistry()
													.getConfigurationElementsFor(EXT_POINT_SSPVIS_EDIT_PART_FACTORIES_ID);
		try {
			for (IConfigurationElement e : config) {

				final Object o = e.createExecutableExtension("class");
				if (o instanceof EditPartFactory) {

					// make sure exceptions thrown in contributed factories
					// won't mess up the whole visualization
					SafeEPFactoryEvaluator runnable = new SafeEPFactoryEvaluator(	editPart,
																					object,
																					(EditPartFactory) o);
					SafeRunner.run(runnable);
					if (runnable.getEditPart() != null) {
						return runnable.getEditPart();
					}
				}
			}
		} catch (CoreException ex) {
			LOG.severe(ex.getMessage());
		}
		return null;
	}
}
