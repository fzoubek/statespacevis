/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.gef.edit.parts;

import java.util.logging.Logger;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import at.bitandart.zoubek.statespace.vis.figures.ChopboxRightAnchor;
import at.bitandart.zoubek.statespace.vis.figures.LoopConnectionRouter;
import at.bitandart.zoubek.statespace.vis.figures.TransparentLabel;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Connection edit part for StateSpaceTransition instances
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpaceTransitionEP extends BaseConnectionEditPart {

	private TransparentLabel					label;
	private PolylineConnection					connection;
	private PolylineDecoration					targetDecoration;

	private Tree<State, StateSpaceTransition>	selectedStatePathTree;
	private Color								highlightColor;
	private Color								normalColor;

	@SuppressWarnings("unused")
	private static final Logger					LOG	= Logger.getLogger(StateSpaceTransitionEP.class.getCanonicalName());

	public StateSpaceTransitionEP(
			Tree<State, StateSpaceTransition> selectedStatePathTree) {
		if (selectedStatePathTree == null) {
			this.selectedStatePathTree = StatespacevisFactory.eINSTANCE.createTree();
		} else {
			this.selectedStatePathTree = selectedStatePathTree;
		}
		highlightColor = new Color(Display.getDefault(), 0, 0, 0);
		normalColor = new Color(Display.getDefault(), 150, 150, 150);
	}

	@Override
	protected IFigure createFigure() {
		connection = new PolylineConnection();
		connection.setConnectionRouter(new LoopConnectionRouter(new Point(	60,
																			-60),
																new Point(	60,
																			60)));
		targetDecoration = new PolylineDecoration();
		connection.setTargetDecoration(targetDecoration);

		label = new TransparentLabel();
		label.setBorder(new MarginBorder(1, 3, 1, 3));
		label.setOpaque(true);
		label.setBackgroundTransparency(220);
		label.setVisible(false);

		connection.add(label, new ConnectionLocator(connection,
													ConnectionLocator.MIDDLE) {
			@Override
			public void relocate(IFigure target) {
				super.relocate(target);
				// never allow negative positions as they result in an infinite loop
				Rectangle bounds = target.getBounds();
				bounds.x = Math.max(bounds.x, 0);
				bounds.y = Math.max(bounds.y, 0);
				target.setBounds(bounds);
			}
		});

		connection.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent me) {
				// No op
			}

			@Override
			public void mouseHover(MouseEvent me) {
				// No op
			}

			@Override
			public void mouseExited(MouseEvent me) {
				StateSpaceTransition stateSpaceTransition = getStateSpaceTransition();
				// check if this transition is not highlighted
				if (!selectedStatePathTree.containsTransitionValue(stateSpaceTransition)) {
					label.setVisible(false);
					connection.setLineWidth(1);
					targetDecoration.setLineWidth(1);
				}
			}

			@Override
			public void mouseEntered(MouseEvent me) {
				StateSpaceTransition stateSpaceTransition = getStateSpaceTransition();
				// check if this transition is not already highlighted
				if (!selectedStatePathTree.containsTransitionValue(stateSpaceTransition)) {
					label.setVisible(true);
					connection.setLineWidth(3);
					targetDecoration.setLineWidth(3);
				}
			}

			@Override
			public void mouseDragged(MouseEvent me) {
				// No op
			}
		});

		return connection;
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		StateSpaceTransition stateSpaceTransition = getStateSpaceTransition();
		if (connection != null && stateSpaceTransition != null) {

			label.setText(stateSpaceTransition.getTransitionName());

			if (getSource() != null) {
				EditPart parent = getSource();
				if (parent instanceof StateEP) {
					((LoopConnectionRouter) connection.getConnectionRouter()).setAlternativeRouter(((StateEP) parent).getConnectionRouter());
				}
			}

			// determine highlight state
			if (selectedStatePathTree.containsTransitionValue(stateSpaceTransition)) {
				connection.setForegroundColor(highlightColor);
				label.setVisible(true);
				connection.setLineWidth(3);
				targetDecoration.setLineWidth(3);
			} else {
				figure.setForegroundColor(normalColor);
				label.setVisible(false);
				connection.setLineWidth(1);
				targetDecoration.setLineWidth(1);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// No op
	}
	

	@Override
	protected void registerModelUpdateListener() {
		super.registerModelUpdateListener();
		// register also to state path changes
		Tree<State, StateSpaceTransition> tree = selectedStatePathTree;
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				node.eAdapters().add(modelUpdateListener);
			}
			for (TreeTransition<State, StateSpaceTransition> transition : tree.getAllTransitions()) {
				transition.eAdapters().add(modelUpdateListener);
			}
		}
	}

	@Override
	protected void notifyModelChanged() {
		super.notifyModelChanged();
		// add listener also to new state path elements
		Tree<State, StateSpaceTransition> tree = selectedStatePathTree;
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				if (!node.eAdapters().contains(modelUpdateListener)) {
					node.eAdapters().add(modelUpdateListener);
				}
			}
			for (TreeTransition<State, StateSpaceTransition> transition : tree.getAllTransitions()) {
				if (!transition.eAdapters().contains(modelUpdateListener)) {
					transition.eAdapters().add(modelUpdateListener);
				}
			}
		}
	}

	@Override
	protected void unregisterModelUpdateListener() {
		super.registerModelUpdateListener();
		// unregister also all state path listeners 
		Tree<State, StateSpaceTransition> tree = selectedStatePathTree;
		if (tree != null) {
			EContentAdapter modelUpdateListener = getModelUpdateListener();
			for (TreeNode<State, StateSpaceTransition> node : tree.getAllNodes()) {
				node.eAdapters().remove(modelUpdateListener);
			}
			for (TreeTransition<State, StateSpaceTransition> transition : tree.getAllTransitions()) {
				transition.eAdapters().remove(modelUpdateListener);
			}
		}
	}

	@Override
	protected ConnectionAnchor getSourceConnectionAnchor() {
		if (getSource() != null) {
			return new ChopboxRightAnchor(((GraphicalEditPart) getSource()).getFigure(), new Point(0, -2));
		}
		return super.getSourceConnectionAnchor();
	}

	@Override
	protected ConnectionAnchor getTargetConnectionAnchor() {
		if (getTarget() != null) {
			return new ChopboxRightAnchor(((GraphicalEditPart) getTarget()).getFigure(), new Point(0, 2));
		}
		return super.getTargetConnectionAnchor();
	}

	public StateSpaceTransition getStateSpaceTransition() {
		return (StateSpaceTransition) getModel();
	}

}
