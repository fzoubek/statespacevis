/**
 * 
 */
package at.bitandart.zoubek.statespace.vis;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class DefaultPreferencesInitializer extends AbstractPreferenceInitializer {

	/**
	 * creates a new preference initializer that sets the default values for the
	 * SSPVis plugin
	 */
	public DefaultPreferencesInitializer() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences pluginNode = DefaultScope.INSTANCE.getNode(StateSpaceVisPlugin.PREF_NODE_PLUGIN);
		
		// object lifeline color 1
		pluginNode.put(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_1, "90,180,172");
		// object lifeline selection color 1
		pluginNode.put(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_1, "117,235,224");

		// object lifeline color 2
		pluginNode.put(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_COLOR_2, "90,129,180");
		// object lifeline selection color 2
		pluginNode.put(StateSpaceVisPlugin.PREF_OBJ_LIFELINE_SELECTION_COLOR_2, "127,183,225");
	}

}
