/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Tree Transition</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getNextNode
 * <em>Next Node</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getPreviousNode
 * <em>Previous Node</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getTransitionValue
 * <em>Transition Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeTransition()
 * @model
 * @generated
 */
public interface TreeTransition<N, T> extends EObject {
	/**
	 * Returns the value of the '<em><b>Next Node</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next Node</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Next Node</em>' reference.
	 * @see #setNextNode(TreeNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeTransition_NextNode()
	 * @model required="true"
	 * @generated
	 */
	TreeNode<N, T> getNextNode();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getNextNode
	 * <em>Next Node</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Next Node</em>' reference.
	 * @see #getNextNode()
	 * @generated
	 */
	void setNextNode(TreeNode<N, T> value);

	/**
	 * Returns the value of the '<em><b>Previous Node</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Previous Node</em>' reference.
	 * @see #setPreviousNode(TreeNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeTransition_PreviousNode()
	 * @model required="true"
	 * @generated
	 */
	TreeNode<N, T> getPreviousNode();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getPreviousNode
	 * <em>Previous Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Previous Node</em>' reference.
	 * @see #getPreviousNode()
	 * @generated
	 */
	void setPreviousNode(TreeNode<N, T> value);

	/**
	 * Returns the value of the '<em><b>Transition Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Transition Value</em>' attribute.
	 * @see #setTransitionValue(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeTransition_TransitionValue()
	 * @model
	 * @generated
	 */
	T getTransitionValue();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getTransitionValue
	 * <em>Transition Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Transition Value</em>' attribute.
	 * @see #getTransitionValue()
	 * @generated
	 */
	void setTransitionValue(T value);

} // TreeTransition
