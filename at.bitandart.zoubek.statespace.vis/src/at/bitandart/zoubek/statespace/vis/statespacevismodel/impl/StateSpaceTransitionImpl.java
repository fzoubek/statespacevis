/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.lang.Object;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Space Transition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl#getNextState
 * <em>Next State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl#getTransitionName
 * <em>Transition Name</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl#getPrevState
 * <em>Prev State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl#getRawTransitionData
 * <em>Raw Transition Data</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StateSpaceTransitionImpl extends MinimalEObjectImpl.Container implements StateSpaceTransition {
	/**
	 * The cached value of the '{@link #getNextState() <em>Next State</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNextState()
	 * @generated
	 * @ordered
	 */
	protected State					nextState;

	/**
	 * The default value of the '{@link #getTransitionName()
	 * <em>Transition Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTransitionName()
	 * @generated
	 * @ordered
	 */
	protected static final String	TRANSITION_NAME_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getTransitionName()
	 * <em>Transition Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTransitionName()
	 * @generated
	 * @ordered
	 */
	protected String				transitionName					= TRANSITION_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPrevState() <em>Prev State</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPrevState()
	 * @generated
	 * @ordered
	 */
	protected State					prevState;

	/**
	 * The default value of the '{@link #getRawTransitionData()
	 * <em>Raw Transition Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRawTransitionData()
	 * @generated
	 * @ordered
	 */
	protected static final Object	RAW_TRANSITION_DATA_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getRawTransitionData()
	 * <em>Raw Transition Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRawTransitionData()
	 * @generated
	 * @ordered
	 */
	protected Object				rawTransitionData				= RAW_TRANSITION_DATA_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StateSpaceTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_SPACE_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State getNextState() {
		if (nextState != null && nextState.eIsProxy()) {
			InternalEObject oldNextState = (InternalEObject) nextState;
			nextState = (State) eResolveProxy(oldNextState);
			if (nextState != oldNextState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE,
													oldNextState,
													nextState));
			}
		}
		return nextState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State basicGetNextState() {
		return nextState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetNextState(State newNextState,
			NotificationChain msgs) {
		State oldNextState = nextState;
		nextState = newNextState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(	this,
																	Notification.SET,
																	StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE,
																	oldNextState,
																	newNextState);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNextState(State newNextState) {
		if (newNextState != nextState) {
			NotificationChain msgs = null;
			if (nextState != null)
				msgs = ((InternalEObject) nextState).eInverseRemove(this,
																	StatespacevisPackage.STATE__INCOMING_TRANSITIONS,
																	State.class,
																	msgs);
			if (newNextState != null)
				msgs = ((InternalEObject) newNextState).eInverseAdd(this,
																	StatespacevisPackage.STATE__INCOMING_TRANSITIONS,
																	State.class,
																	msgs);
			msgs = basicSetNextState(newNextState, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE,
											newNextState,
											newNextState));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getTransitionName() {
		return transitionName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTransitionName(String newTransitionName) {
		String oldTransitionName = transitionName;
		transitionName = newTransitionName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_TRANSITION__TRANSITION_NAME,
											oldTransitionName,
											transitionName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State getPrevState() {
		if (prevState != null && prevState.eIsProxy()) {
			InternalEObject oldPrevState = (InternalEObject) prevState;
			prevState = (State) eResolveProxy(oldPrevState);
			if (prevState != oldPrevState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE,
													oldPrevState,
													prevState));
			}
		}
		return prevState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State basicGetPrevState() {
		return prevState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetPrevState(State newPrevState,
			NotificationChain msgs) {
		State oldPrevState = prevState;
		prevState = newPrevState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(	this,
																	Notification.SET,
																	StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE,
																	oldPrevState,
																	newPrevState);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPrevState(State newPrevState) {
		if (newPrevState != prevState) {
			NotificationChain msgs = null;
			if (prevState != null)
				msgs = ((InternalEObject) prevState).eInverseRemove(this,
																	StatespacevisPackage.STATE__OUTGOING_TRANSITIONS,
																	State.class,
																	msgs);
			if (newPrevState != null)
				msgs = ((InternalEObject) newPrevState).eInverseAdd(this,
																	StatespacevisPackage.STATE__OUTGOING_TRANSITIONS,
																	State.class,
																	msgs);
			msgs = basicSetPrevState(newPrevState, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE,
											newPrevState,
											newPrevState));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getRawTransitionData() {
		return rawTransitionData;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRawTransitionData(Object newRawTransitionData) {
		Object oldRawTransitionData = rawTransitionData;
		rawTransitionData = newRawTransitionData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA,
											oldRawTransitionData,
											rawTransitionData));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE:
				if (nextState != null)
					msgs = ((InternalEObject) nextState).eInverseRemove(this,
																		StatespacevisPackage.STATE__INCOMING_TRANSITIONS,
																		State.class,
																		msgs);
				return basicSetNextState((State) otherEnd, msgs);
			case StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE:
				if (prevState != null)
					msgs = ((InternalEObject) prevState).eInverseRemove(this,
																		StatespacevisPackage.STATE__OUTGOING_TRANSITIONS,
																		State.class,
																		msgs);
				return basicSetPrevState((State) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE:
				return basicSetNextState(null, msgs);
			case StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE:
				return basicSetPrevState(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE:
				if (resolve)
					return getNextState();
				return basicGetNextState();
			case StatespacevisPackage.STATE_SPACE_TRANSITION__TRANSITION_NAME:
				return getTransitionName();
			case StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE:
				if (resolve)
					return getPrevState();
				return basicGetPrevState();
			case StatespacevisPackage.STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA:
				return getRawTransitionData();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE:
				setNextState((State) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__TRANSITION_NAME:
				setTransitionName((String) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE:
				setPrevState((State) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA:
				setRawTransitionData(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE:
				setNextState((State) null);
				return;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__TRANSITION_NAME:
				setTransitionName(TRANSITION_NAME_EDEFAULT);
				return;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE:
				setPrevState((State) null);
				return;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA:
				setRawTransitionData(RAW_TRANSITION_DATA_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE:
				return nextState != null;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__TRANSITION_NAME:
				return TRANSITION_NAME_EDEFAULT == null
						? transitionName != null
						: !TRANSITION_NAME_EDEFAULT.equals(transitionName);
			case StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE:
				return prevState != null;
			case StatespacevisPackage.STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA:
				return RAW_TRANSITION_DATA_EDEFAULT == null
						? rawTransitionData != null
						: !RAW_TRANSITION_DATA_EDEFAULT.equals(rawTransitionData);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (transitionName: ");
		result.append(transitionName);
		result.append(", rawTransitionData: ");
		result.append(rawTransitionData);
		result.append(')');
		return result.toString();
	}

} // StateSpaceTransitionImpl
