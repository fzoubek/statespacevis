/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.lang.Object;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl#getModelSnapshot
 * <em>Model Snapshot</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl#getRawStateData
 * <em>Raw State Data</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl#getIncomingTransitions
 * <em>Incoming Transitions</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl#getOutgoingTransitions
 * <em>Outgoing Transitions</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StateImpl extends MinimalEObjectImpl.Container implements State {
	/**
	 * The default value of the '{@link #getModelSnapshot()
	 * <em>Model Snapshot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getModelSnapshot()
	 * @generated
	 * @ordered
	 */
	protected static final Object			MODEL_SNAPSHOT_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getModelSnapshot()
	 * <em>Model Snapshot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getModelSnapshot()
	 * @generated
	 * @ordered
	 */
	protected Object						modelSnapshot			= MODEL_SNAPSHOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRawStateData()
	 * <em>Raw State Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRawStateData()
	 * @generated
	 * @ordered
	 */
	protected static final Object			RAW_STATE_DATA_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getRawStateData()
	 * <em>Raw State Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRawStateData()
	 * @generated
	 * @ordered
	 */
	protected Object						rawStateData			= RAW_STATE_DATA_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIncomingTransitions()
	 * <em>Incoming Transitions</em>}' reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getIncomingTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<StateSpaceTransition>	incomingTransitions;
	/**
	 * The cached value of the '{@link #getOutgoingTransitions()
	 * <em>Outgoing Transitions</em>}' reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getOutgoingTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<StateSpaceTransition>	outgoingTransitions;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getModelSnapshot() {
		return modelSnapshot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setModelSnapshot(Object newModelSnapshot) {
		Object oldModelSnapshot = modelSnapshot;
		modelSnapshot = newModelSnapshot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE__MODEL_SNAPSHOT,
											oldModelSnapshot,
											modelSnapshot));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getRawStateData() {
		return rawStateData;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRawStateData(Object newRawStateData) {
		Object oldRawStateData = rawStateData;
		rawStateData = newRawStateData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE__RAW_STATE_DATA,
											oldRawStateData,
											rawStateData));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<StateSpaceTransition> getIncomingTransitions() {
		if (incomingTransitions == null) {
			incomingTransitions = new EObjectWithInverseResolvingEList<StateSpaceTransition>(	StateSpaceTransition.class,
																								this,
																								StatespacevisPackage.STATE__INCOMING_TRANSITIONS,
																								StatespacevisPackage.STATE_SPACE_TRANSITION__NEXT_STATE);
		}
		return incomingTransitions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<StateSpaceTransition> getOutgoingTransitions() {
		if (outgoingTransitions == null) {
			outgoingTransitions = new EObjectWithInverseResolvingEList<StateSpaceTransition>(	StateSpaceTransition.class,
																								this,
																								StatespacevisPackage.STATE__OUTGOING_TRANSITIONS,
																								StatespacevisPackage.STATE_SPACE_TRANSITION__PREV_STATE);
		}
		return outgoingTransitions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.STATE__INCOMING_TRANSITIONS:
				return ((InternalEList<InternalEObject>) (InternalEList<?>) getIncomingTransitions()).basicAdd(	otherEnd,
																												msgs);
			case StatespacevisPackage.STATE__OUTGOING_TRANSITIONS:
				return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutgoingTransitions()).basicAdd(	otherEnd,
																												msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.STATE__INCOMING_TRANSITIONS:
				return ((InternalEList<?>) getIncomingTransitions()).basicRemove(	otherEnd,
																					msgs);
			case StatespacevisPackage.STATE__OUTGOING_TRANSITIONS:
				return ((InternalEList<?>) getOutgoingTransitions()).basicRemove(	otherEnd,
																					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE__MODEL_SNAPSHOT:
				return getModelSnapshot();
			case StatespacevisPackage.STATE__RAW_STATE_DATA:
				return getRawStateData();
			case StatespacevisPackage.STATE__INCOMING_TRANSITIONS:
				return getIncomingTransitions();
			case StatespacevisPackage.STATE__OUTGOING_TRANSITIONS:
				return getOutgoingTransitions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE__MODEL_SNAPSHOT:
				setModelSnapshot(newValue);
				return;
			case StatespacevisPackage.STATE__RAW_STATE_DATA:
				setRawStateData(newValue);
				return;
			case StatespacevisPackage.STATE__INCOMING_TRANSITIONS:
				getIncomingTransitions().clear();
				getIncomingTransitions().addAll((Collection<? extends StateSpaceTransition>) newValue);
				return;
			case StatespacevisPackage.STATE__OUTGOING_TRANSITIONS:
				getOutgoingTransitions().clear();
				getOutgoingTransitions().addAll((Collection<? extends StateSpaceTransition>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE__MODEL_SNAPSHOT:
				setModelSnapshot(MODEL_SNAPSHOT_EDEFAULT);
				return;
			case StatespacevisPackage.STATE__RAW_STATE_DATA:
				setRawStateData(RAW_STATE_DATA_EDEFAULT);
				return;
			case StatespacevisPackage.STATE__INCOMING_TRANSITIONS:
				getIncomingTransitions().clear();
				return;
			case StatespacevisPackage.STATE__OUTGOING_TRANSITIONS:
				getOutgoingTransitions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE__MODEL_SNAPSHOT:
				return MODEL_SNAPSHOT_EDEFAULT == null
						? modelSnapshot != null
						: !MODEL_SNAPSHOT_EDEFAULT.equals(modelSnapshot);
			case StatespacevisPackage.STATE__RAW_STATE_DATA:
				return RAW_STATE_DATA_EDEFAULT == null
						? rawStateData != null
						: !RAW_STATE_DATA_EDEFAULT.equals(rawStateData);
			case StatespacevisPackage.STATE__INCOMING_TRANSITIONS:
				return incomingTransitions != null
						&& !incomingTransitions.isEmpty();
			case StatespacevisPackage.STATE__OUTGOING_TRANSITIONS:
				return outgoingTransitions != null
						&& !outgoingTransitions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (modelSnapshot: ");
		result.append(modelSnapshot);
		result.append(", rawStateData: ");
		result.append(rawStateData);
		result.append(')');
		return result.toString();
	}

} // StateImpl
