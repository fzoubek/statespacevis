/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Lifeline</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineImpl#getParentLifeline
 * <em>Parent Lifeline</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineImpl#getChildLifelines
 * <em>Child Lifelines</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineImpl#isVisible
 * <em>Visible</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineImpl#isCollapsed
 * <em>Collapsed</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class LifelineImpl extends MinimalEObjectImpl.Container implements Lifeline {
	/**
	 * The cached value of the '{@link #getParentLifeline()
	 * <em>Parent Lifeline</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getParentLifeline()
	 * @generated
	 * @ordered
	 */
	protected Lifeline				parentLifeline;

	/**
	 * The cached value of the '{@link #getChildLifelines()
	 * <em>Child Lifelines</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getChildLifelines()
	 * @generated
	 * @ordered
	 */
	protected EList<Lifeline>		childLifelines;

	/**
	 * The default value of the '{@link #isVisible() <em>Visible</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isVisible()
	 * @generated
	 * @ordered
	 */
	protected static final boolean	VISIBLE_EDEFAULT	= true;

	/**
	 * The cached value of the '{@link #isVisible() <em>Visible</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isVisible()
	 * @generated
	 * @ordered
	 */
	protected boolean				visible				= VISIBLE_EDEFAULT;

	/**
	 * The default value of the '{@link #isCollapsed() <em>Collapsed</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isCollapsed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean	COLLAPSED_EDEFAULT	= true;

	/**
	 * The cached value of the '{@link #isCollapsed() <em>Collapsed</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isCollapsed()
	 * @generated
	 * @ordered
	 */
	protected boolean				collapsed			= COLLAPSED_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected LifelineImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.LIFELINE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Lifeline getParentLifeline() {
		if (parentLifeline != null && parentLifeline.eIsProxy()) {
			InternalEObject oldParentLifeline = (InternalEObject) parentLifeline;
			parentLifeline = (Lifeline) eResolveProxy(oldParentLifeline);
			if (parentLifeline != oldParentLifeline) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.LIFELINE__PARENT_LIFELINE,
													oldParentLifeline,
													parentLifeline));
			}
		}
		return parentLifeline;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Lifeline basicGetParentLifeline() {
		return parentLifeline;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetParentLifeline(Lifeline newParentLifeline,
			NotificationChain msgs) {
		Lifeline oldParentLifeline = parentLifeline;
		parentLifeline = newParentLifeline;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(	this,
																	Notification.SET,
																	StatespacevisPackage.LIFELINE__PARENT_LIFELINE,
																	oldParentLifeline,
																	newParentLifeline);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentLifeline(Lifeline newParentLifeline) {
		if (newParentLifeline != parentLifeline) {
			NotificationChain msgs = null;
			if (parentLifeline != null)
				msgs = ((InternalEObject) parentLifeline).eInverseRemove(	this,
																			StatespacevisPackage.LIFELINE__CHILD_LIFELINES,
																			Lifeline.class,
																			msgs);
			if (newParentLifeline != null)
				msgs = ((InternalEObject) newParentLifeline).eInverseAdd(	this,
																			StatespacevisPackage.LIFELINE__CHILD_LIFELINES,
																			Lifeline.class,
																			msgs);
			msgs = basicSetParentLifeline(newParentLifeline, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.LIFELINE__PARENT_LIFELINE,
											newParentLifeline,
											newParentLifeline));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Lifeline> getChildLifelines() {
		if (childLifelines == null) {
			childLifelines = new EObjectWithInverseResolvingEList<Lifeline>(Lifeline.class,
																			this,
																			StatespacevisPackage.LIFELINE__CHILD_LIFELINES,
																			StatespacevisPackage.LIFELINE__PARENT_LIFELINE);
		}
		return childLifelines;
	}

	/**
	 * <!-- begin-user-doc -->@return true if the lifeline is visible and all
	 * its parents are visible<!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isVisible() {
		if (parentLifeline == null
			|| (parentLifeline != null && parentLifeline.isVisible())) {
			return visible;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setVisible(boolean newVisible) {
		boolean oldVisible = visible;
		visible = newVisible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.LIFELINE__VISIBLE,
											oldVisible,
											visible));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean isCollapsed() {
		return collapsed;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCollapsed(boolean newCollapsed) {
		boolean oldCollapsed = collapsed;
		collapsed = newCollapsed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.LIFELINE__COLLAPSED,
											oldCollapsed,
											collapsed));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.LIFELINE__PARENT_LIFELINE:
				if (parentLifeline != null)
					msgs = ((InternalEObject) parentLifeline).eInverseRemove(	this,
																				StatespacevisPackage.LIFELINE__CHILD_LIFELINES,
																				Lifeline.class,
																				msgs);
				return basicSetParentLifeline((Lifeline) otherEnd, msgs);
			case StatespacevisPackage.LIFELINE__CHILD_LIFELINES:
				return ((InternalEList<InternalEObject>) (InternalEList<?>) getChildLifelines()).basicAdd(	otherEnd,
																											msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.LIFELINE__PARENT_LIFELINE:
				return basicSetParentLifeline(null, msgs);
			case StatespacevisPackage.LIFELINE__CHILD_LIFELINES:
				return ((InternalEList<?>) getChildLifelines()).basicRemove(otherEnd,
																			msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.LIFELINE__PARENT_LIFELINE:
				if (resolve)
					return getParentLifeline();
				return basicGetParentLifeline();
			case StatespacevisPackage.LIFELINE__CHILD_LIFELINES:
				return getChildLifelines();
			case StatespacevisPackage.LIFELINE__VISIBLE:
				return isVisible();
			case StatespacevisPackage.LIFELINE__COLLAPSED:
				return isCollapsed();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.LIFELINE__PARENT_LIFELINE:
				setParentLifeline((Lifeline) newValue);
				return;
			case StatespacevisPackage.LIFELINE__CHILD_LIFELINES:
				getChildLifelines().clear();
				getChildLifelines().addAll((Collection<? extends Lifeline>) newValue);
				return;
			case StatespacevisPackage.LIFELINE__VISIBLE:
				setVisible((Boolean) newValue);
				return;
			case StatespacevisPackage.LIFELINE__COLLAPSED:
				setCollapsed((Boolean) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.LIFELINE__PARENT_LIFELINE:
				setParentLifeline((Lifeline) null);
				return;
			case StatespacevisPackage.LIFELINE__CHILD_LIFELINES:
				getChildLifelines().clear();
				return;
			case StatespacevisPackage.LIFELINE__VISIBLE:
				setVisible(VISIBLE_EDEFAULT);
				return;
			case StatespacevisPackage.LIFELINE__COLLAPSED:
				setCollapsed(COLLAPSED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.LIFELINE__PARENT_LIFELINE:
				return parentLifeline != null;
			case StatespacevisPackage.LIFELINE__CHILD_LIFELINES:
				return childLifelines != null && !childLifelines.isEmpty();
			case StatespacevisPackage.LIFELINE__VISIBLE:
				return visible != VISIBLE_EDEFAULT;
			case StatespacevisPackage.LIFELINE__COLLAPSED:
				return collapsed != COLLAPSED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (visible: ");
		result.append(visible);
		result.append(", collapsed: ");
		result.append(collapsed);
		result.append(')');
		return result.toString();
	}

} //LifelineImpl
