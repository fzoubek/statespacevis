/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Object History Transition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryTransitionImpl#getHistoryData
 * <em>History Data</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryTransitionImpl#getStateTransition
 * <em>State Transition</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ObjectHistoryTransitionImpl extends MinimalEObjectImpl.Container implements ObjectHistoryTransition {
	/**
	 * The cached value of the '{@link #getHistoryData() <em>History Data</em>}'
	 * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHistoryData()
	 * @generated
	 * @ordered
	 */
	protected EList<ObjectHistoryData>	historyData;

	/**
	 * The cached value of the '{@link #getStateTransition()
	 * <em>State Transition</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getStateTransition()
	 * @generated
	 * @ordered
	 */
	protected StateSpaceTransition		stateTransition;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectHistoryTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.OBJECT_HISTORY_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ObjectHistoryData> getHistoryData() {
		if (historyData == null) {
			historyData = new EObjectResolvingEList<ObjectHistoryData>(	ObjectHistoryData.class,
																		this,
																		StatespacevisPackage.OBJECT_HISTORY_TRANSITION__HISTORY_DATA);
		}
		return historyData;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpaceTransition getStateTransition() {
		if (stateTransition != null && stateTransition.eIsProxy()) {
			InternalEObject oldStateTransition = (InternalEObject) stateTransition;
			stateTransition = (StateSpaceTransition) eResolveProxy(oldStateTransition);
			if (stateTransition != oldStateTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.OBJECT_HISTORY_TRANSITION__STATE_TRANSITION,
													oldStateTransition,
													stateTransition));
			}
		}
		return stateTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpaceTransition basicGetStateTransition() {
		return stateTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setStateTransition(StateSpaceTransition newStateTransition) {
		StateSpaceTransition oldStateTransition = stateTransition;
		stateTransition = newStateTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_TRANSITION__STATE_TRANSITION,
											oldStateTransition,
											stateTransition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__HISTORY_DATA:
				return getHistoryData();
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__STATE_TRANSITION:
				if (resolve)
					return getStateTransition();
				return basicGetStateTransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__HISTORY_DATA:
				getHistoryData().clear();
				getHistoryData().addAll((Collection<? extends ObjectHistoryData>) newValue);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__STATE_TRANSITION:
				setStateTransition((StateSpaceTransition) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__HISTORY_DATA:
				getHistoryData().clear();
				return;
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__STATE_TRANSITION:
				setStateTransition((StateSpaceTransition) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__HISTORY_DATA:
				return historyData != null && !historyData.isEmpty();
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION__STATE_TRANSITION:
				return stateTransition != null;
		}
		return super.eIsSet(featureID);
	}

} // ObjectHistoryTransitionImpl
