/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Space Transition</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState
 * <em>Next State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getTransitionName
 * <em>Transition Name</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState
 * <em>Prev State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getRawTransitionData
 * <em>Raw Transition Data</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceTransition()
 * @model
 * @generated
 */
public interface StateSpaceTransition extends EObject {
	/**
	 * Returns the value of the '<em><b>Next State</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next State</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Next State</em>' reference.
	 * @see #setNextState(StateSpaceNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceTransition_NextState()
	 * @model required="true"
	 * @generated
	 */
	State getNextState();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState
	 * <em>Next State</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Next State</em>' reference.
	 * @see #getNextState()
	 * @generated
	 */
	void setNextState(State value);

	/**
	 * Returns the value of the '<em><b>Transition Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Transition Name</em>' attribute.
	 * @see #setTransitionName(String)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceTransition_TransitionName()
	 * @model
	 * @generated
	 */
	String getTransitionName();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getTransitionName
	 * <em>Transition Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Transition Name</em>' attribute.
	 * @see #getTransitionName()
	 * @generated
	 */
	void setTransitionName(String value);

	/**
	 * Returns the value of the '<em><b>Prev State</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prev State</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Prev State</em>' reference.
	 * @see #setPrevState(StateSpaceNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceTransition_PrevState()
	 * @model required="true"
	 * @generated
	 */
	State getPrevState();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState
	 * <em>Prev State</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Prev State</em>' reference.
	 * @see #getPrevState()
	 * @generated
	 */
	void setPrevState(State value);

	/**
	 * Returns the value of the '<em><b>Raw Transition Data</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * Holds raw data of the original state space transition. This attribute may
	 * be null and is not used by the SSPVis plugin itself, but may help other
	 * plugins which rely on the original transition data. <!-- end-model-doc
	 * -->
	 * 
	 * @return the value of the '<em>Raw Transition Data</em>' attribute.
	 * @see #setRawTransitionData(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceTransition_RawTransitionData()
	 * @model
	 * @generated
	 */
	Object getRawTransitionData();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getRawTransitionData
	 * <em>Raw Transition Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Raw Transition Data</em>' attribute.
	 * @see #getRawTransitionData()
	 * @generated
	 */
	void setRawTransitionData(Object value);

} // StateSpaceTransition
