/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Path Tree Transition Reference</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference#getStatePathTreeTransition
 * <em>State Path Tree Transition</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTreeTransitionReference()
 * @model
 * @generated
 */
public interface StatePathTreeTransitionReference extends StatePathReference {
	/**
	 * Returns the value of the '<em><b>State Path Tree Transition</b></em>'
	 * reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Path Tree Transition</em>' reference
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>State Path Tree Transition</em>' reference.
	 * @see #setStatePathTreeTransition(TreeTransition)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTreeTransitionReference_StatePathTreeTransition()
	 * @model required="true"
	 * @generated
	 */
	TreeTransition<State, StateSpaceTransition> getStatePathTreeTransition();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference#getStatePathTreeTransition
	 * <em>State Path Tree Transition</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>State Path Tree Transition</em>'
	 *            reference.
	 * @see #getStatePathTreeTransition()
	 * @generated
	 */
	void setStatePathTreeTransition(
			TreeTransition<State, StateSpaceTransition> value);

} // StatePathTreeTransitionReference
