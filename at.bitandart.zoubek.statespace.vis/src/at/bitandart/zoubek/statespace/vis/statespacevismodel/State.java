/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getModelSnapshot
 * <em>Model Snapshot</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getRawStateData
 * <em>Raw State Data</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getIncomingTransitions
 * <em>Incoming Transitions</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getOutgoingTransitions
 * <em>Outgoing Transitions</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getState()
 * @model
 * @generated
 */
public interface State extends EObject {

	/**
	 * Returns the value of the '<em><b>Model Snapshot</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Snapshot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Model Snapshot</em>' attribute.
	 * @see #setModelSnapshot(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getState_ModelSnapshot()
	 * @model
	 * @generated
	 */
	Object getModelSnapshot();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getModelSnapshot
	 * <em>Model Snapshot</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Model Snapshot</em>' attribute.
	 * @see #getModelSnapshot()
	 * @generated
	 */
	void setModelSnapshot(Object value);

	/**
	 * Returns the value of the '<em><b>Raw State Data</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc --> Holds
	 * raw data of the original state . This attribute may be null and is not
	 * used by the SSPVis plugin itself, but may help other plugins which rely
	 * on the original state data. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Raw State Data</em>' attribute.
	 * @see #setRawStateData(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getState_RawStateData()
	 * @model
	 * @generated
	 */
	Object getRawStateData();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getRawStateData
	 * <em>Raw State Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Raw State Data</em>' attribute.
	 * @see #getRawStateData()
	 * @generated
	 */
	void setRawStateData(Object value);

	/**
	 * Returns the value of the '<em><b>Incoming Transitions</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition}
	 * . It is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState
	 * <em>Next State</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transitions</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Incoming Transitions</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getState_IncomingTransitions()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState
	 * @model opposite="nextState"
	 * @generated
	 */
	EList<StateSpaceTransition> getIncomingTransitions();

	/**
	 * Returns the value of the '<em><b>Outgoing Transitions</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition}
	 * . It is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState
	 * <em>Prev State</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transitions</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Outgoing Transitions</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getState_OutgoingTransitions()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState
	 * @model opposite="prevState"
	 * @generated
	 */
	EList<StateSpaceTransition> getOutgoingTransitions();
} // State
