/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Stack;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Space</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceImpl#getInitialState
 * <em>Initial State</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StateSpaceImpl extends MinimalEObjectImpl.Container implements StateSpace {
	/**
	 * The cached value of the '{@link #getInitialState()
	 * <em>Initial State</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInitialState()
	 * @generated
	 * @ordered
	 */
	protected State	initialState;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StateSpaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_SPACE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State getInitialState() {
		if (initialState != null && initialState.eIsProxy()) {
			InternalEObject oldInitialState = (InternalEObject) initialState;
			initialState = (State) eResolveProxy(oldInitialState);
			if (initialState != oldInitialState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_SPACE__INITIAL_STATE,
													oldInitialState,
													initialState));
			}
		}
		return initialState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State basicGetInitialState() {
		return initialState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setInitialState(State newInitialState) {
		State oldInitialState = initialState;
		initialState = newInitialState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE__INITIAL_STATE,
											oldInitialState,
											initialState));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<State> getAllStateSpaceNodes() {
		EList<State> allNodes = new BasicEList<>();

		Stack<State> nodesToCheck = new Stack<>();
		nodesToCheck.push(initialState);

		while (!nodesToCheck.isEmpty()) {
			State node = nodesToCheck.pop();
			allNodes.add(node);
			for (StateSpaceTransition nextTransition : node.getOutgoingTransitions()) {
				State nextNode = nextTransition.getNextState();
				if (!allNodes.contains(nextNode)
					&& !nodesToCheck.contains(nextNode)) {
					nodesToCheck.push(nextNode);
				}
			}
		}

		return allNodes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<StateSpaceTransition> getAllStateSpaceTransitions() {
		EList<StateSpaceTransition> allTransitions = new BasicEList<>();

		Stack<StateSpaceTransition> transitionsToCheck = new Stack<>();
		transitionsToCheck.addAll(initialState.getOutgoingTransitions());

		while (!transitionsToCheck.isEmpty()) {
			StateSpaceTransition transition = transitionsToCheck.pop();
			allTransitions.add(transition);
			State node = transition.getNextState();
			for (StateSpaceTransition nextTransition : node.getOutgoingTransitions()) {
				if (!allTransitions.contains(nextTransition)
					&& !transitionsToCheck.contains(nextTransition)) {
					transitionsToCheck.push(nextTransition);
				}
			}
		}

		return allTransitions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE__INITIAL_STATE:
				if (resolve)
					return getInitialState();
				return basicGetInitialState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE__INITIAL_STATE:
				setInitialState((State) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE__INITIAL_STATE:
				setInitialState((State) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE__INITIAL_STATE:
				return initialState != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatespacevisPackage.STATE_SPACE___GET_ALL_STATE_SPACE_NODES:
				return getAllStateSpaceNodes();
			case StatespacevisPackage.STATE_SPACE___GET_ALL_STATE_SPACE_TRANSITIONS:
				return getAllStateSpaceTransitions();
		}
		return super.eInvoke(operationID, arguments);
	}

} // StateSpaceImpl
