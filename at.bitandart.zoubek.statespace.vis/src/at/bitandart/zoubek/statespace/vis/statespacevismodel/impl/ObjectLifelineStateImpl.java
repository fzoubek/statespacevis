/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.lang.Object;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Object Lifeline State</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineStateImpl#getState
 * <em>State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineStateImpl#getObjectInstance
 * <em>Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineStateImpl#getBaseHistoryNode
 * <em>Base History Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ObjectLifelineStateImpl extends MinimalEObjectImpl.Container implements ObjectLifelineState {
	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final ObjectStateType	STATE_EDEFAULT				= ObjectStateType.NULL;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected ObjectStateType				state						= STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getObjectInstance()
	 * <em>Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected static final Object			OBJECT_INSTANCE_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getObjectInstance()
	 * <em>Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected Object						objectInstance				= OBJECT_INSTANCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBaseHistoryNode()
	 * <em>Base History Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getBaseHistoryNode()
	 * @generated
	 * @ordered
	 */
	protected ObjectHistoryNode				baseHistoryNode;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectLifelineStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.OBJECT_LIFELINE_STATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectStateType getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setState(ObjectStateType newState) {
		ObjectStateType oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_LIFELINE_STATE__STATE,
											oldState,
											state));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getObjectInstance() {
		return objectInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setObjectInstance(Object newObjectInstance) {
		Object oldObjectInstance = objectInstance;
		objectInstance = newObjectInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_LIFELINE_STATE__OBJECT_INSTANCE,
											oldObjectInstance,
											objectInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryNode getBaseHistoryNode() {
		if (baseHistoryNode != null && baseHistoryNode.eIsProxy()) {
			InternalEObject oldBaseHistoryNode = (InternalEObject) baseHistoryNode;
			baseHistoryNode = (ObjectHistoryNode) eResolveProxy(oldBaseHistoryNode);
			if (baseHistoryNode != oldBaseHistoryNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE,
													oldBaseHistoryNode,
													baseHistoryNode));
			}
		}
		return baseHistoryNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryNode basicGetBaseHistoryNode() {
		return baseHistoryNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setBaseHistoryNode(ObjectHistoryNode newBaseHistoryNode) {
		ObjectHistoryNode oldBaseHistoryNode = baseHistoryNode;
		baseHistoryNode = newBaseHistoryNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE,
											oldBaseHistoryNode,
											baseHistoryNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__STATE:
				return getState();
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__OBJECT_INSTANCE:
				return getObjectInstance();
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE:
				if (resolve)
					return getBaseHistoryNode();
				return basicGetBaseHistoryNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__STATE:
				setState((ObjectStateType) newValue);
				return;
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__OBJECT_INSTANCE:
				setObjectInstance(newValue);
				return;
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE:
				setBaseHistoryNode((ObjectHistoryNode) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__STATE:
				setState(STATE_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__OBJECT_INSTANCE:
				setObjectInstance(OBJECT_INSTANCE_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE:
				setBaseHistoryNode((ObjectHistoryNode) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__STATE:
				return state != STATE_EDEFAULT;
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__OBJECT_INSTANCE:
				return OBJECT_INSTANCE_EDEFAULT == null
						? objectInstance != null
						: !OBJECT_INSTANCE_EDEFAULT.equals(objectInstance);
			case StatespacevisPackage.OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE:
				return baseHistoryNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (state: ");
		result.append(state);
		result.append(", objectInstance: ");
		result.append(objectInstance);
		result.append(')');
		return result.toString();
	}

} // ObjectLifelineStateImpl
