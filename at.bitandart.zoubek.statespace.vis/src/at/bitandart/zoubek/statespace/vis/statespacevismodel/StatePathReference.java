/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Path Reference</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathReference()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface StatePathReference extends ObjectHistoryData {
} // StatePathReference
