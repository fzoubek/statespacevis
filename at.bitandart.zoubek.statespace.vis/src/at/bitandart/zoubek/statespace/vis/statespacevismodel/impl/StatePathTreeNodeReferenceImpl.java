/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Path Tree Node Reference</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeNodeReferenceImpl#getStatePathTreeNode
 * <em>State Path Tree Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StatePathTreeNodeReferenceImpl extends MinimalEObjectImpl.Container implements StatePathTreeNodeReference {
	/**
	 * The cached value of the '{@link #getStatePathTreeNode()
	 * <em>State Path Tree Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getStatePathTreeNode()
	 * @generated
	 * @ordered
	 */
	protected TreeNode<State, StateSpaceTransition>	statePathTreeNode;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathTreeNodeReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_PATH_TREE_NODE_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public TreeNode<State, StateSpaceTransition> getStatePathTreeNode() {
		if (statePathTreeNode != null && statePathTreeNode.eIsProxy()) {
			InternalEObject oldStatePathTreeNode = (InternalEObject) statePathTreeNode;
			statePathTreeNode = (TreeNode<State, StateSpaceTransition>) eResolveProxy(oldStatePathTreeNode);
			if (statePathTreeNode != oldStatePathTreeNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE,
													oldStatePathTreeNode,
													statePathTreeNode));
			}
		}
		return statePathTreeNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeNode<State, StateSpaceTransition> basicGetStatePathTreeNode() {
		return statePathTreeNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setStatePathTreeNode(
			TreeNode<State, StateSpaceTransition> newStatePathTreeNode) {
		TreeNode<State, StateSpaceTransition> oldStatePathTreeNode = statePathTreeNode;
		statePathTreeNode = newStatePathTreeNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE,
											oldStatePathTreeNode,
											statePathTreeNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE:
				if (resolve)
					return getStatePathTreeNode();
				return basicGetStatePathTreeNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE:
				setStatePathTreeNode((TreeNode<State, StateSpaceTransition>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE:
				setStatePathTreeNode((TreeNode<State, StateSpaceTransition>) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE:
				return statePathTreeNode != null;
		}
		return super.eIsSet(featureID);
	}

} // StatePathTreeNodeReferenceImpl
