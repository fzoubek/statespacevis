/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.lang.Object;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Object History Diff</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl#getNewState
 * <em>New State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl#getNewObjectInstance
 * <em>New Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl#getOldObjectInstance
 * <em>Old Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl#getRawDiffData
 * <em>Raw Diff Data</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ObjectHistoryDiffImpl extends MinimalEObjectImpl.Container implements ObjectHistoryDiff {
	/**
	 * The default value of the '{@link #getNewState() <em>New State</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNewState()
	 * @generated
	 * @ordered
	 */
	protected static final ObjectStateType	NEW_STATE_EDEFAULT				= ObjectStateType.NULL;

	/**
	 * The cached value of the '{@link #getNewState() <em>New State</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNewState()
	 * @generated
	 * @ordered
	 */
	protected ObjectStateType				newState						= NEW_STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNewObjectInstance()
	 * <em>New Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getNewObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected static final Object			NEW_OBJECT_INSTANCE_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getNewObjectInstance()
	 * <em>New Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getNewObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected Object						newObjectInstance				= NEW_OBJECT_INSTANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOldObjectInstance()
	 * <em>Old Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getOldObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected static final Object			OLD_OBJECT_INSTANCE_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getOldObjectInstance()
	 * <em>Old Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getOldObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected Object						oldObjectInstance				= OLD_OBJECT_INSTANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRawDiffData()
	 * <em>Raw Diff Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getRawDiffData()
	 * @generated
	 * @ordered
	 */
	protected static final Object			RAW_DIFF_DATA_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getRawDiffData() <em>Raw Diff Data</em>}
	 * ' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRawDiffData()
	 * @generated
	 * @ordered
	 */
	protected Object						rawDiffData						= RAW_DIFF_DATA_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectHistoryDiffImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.OBJECT_HISTORY_DIFF;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectStateType getNewState() {
		return newState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNewState(ObjectStateType newNewState) {
		ObjectStateType oldNewState = newState;
		newState = newNewState == null ? NEW_STATE_EDEFAULT : newNewState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_STATE,
											oldNewState,
											newState));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getNewObjectInstance() {
		return newObjectInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNewObjectInstance(Object newNewObjectInstance) {
		Object oldNewObjectInstance = newObjectInstance;
		newObjectInstance = newNewObjectInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE,
											oldNewObjectInstance,
											newObjectInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getOldObjectInstance() {
		return oldObjectInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setOldObjectInstance(Object newOldObjectInstance) {
		Object oldOldObjectInstance = oldObjectInstance;
		oldObjectInstance = newOldObjectInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE,
											oldOldObjectInstance,
											oldObjectInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getRawDiffData() {
		return rawDiffData;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRawDiffData(Object newRawDiffData) {
		Object oldRawDiffData = rawDiffData;
		rawDiffData = newRawDiffData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_DIFF__RAW_DIFF_DATA,
											oldRawDiffData,
											rawDiffData));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_STATE:
				return getNewState();
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE:
				return getNewObjectInstance();
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE:
				return getOldObjectInstance();
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__RAW_DIFF_DATA:
				return getRawDiffData();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_STATE:
				setNewState((ObjectStateType) newValue);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE:
				setNewObjectInstance(newValue);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE:
				setOldObjectInstance(newValue);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__RAW_DIFF_DATA:
				setRawDiffData(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_STATE:
				setNewState(NEW_STATE_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE:
				setNewObjectInstance(NEW_OBJECT_INSTANCE_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE:
				setOldObjectInstance(OLD_OBJECT_INSTANCE_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__RAW_DIFF_DATA:
				setRawDiffData(RAW_DIFF_DATA_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_STATE:
				return newState != NEW_STATE_EDEFAULT;
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE:
				return NEW_OBJECT_INSTANCE_EDEFAULT == null
						? newObjectInstance != null
						: !NEW_OBJECT_INSTANCE_EDEFAULT.equals(newObjectInstance);
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE:
				return OLD_OBJECT_INSTANCE_EDEFAULT == null
						? oldObjectInstance != null
						: !OLD_OBJECT_INSTANCE_EDEFAULT.equals(oldObjectInstance);
			case StatespacevisPackage.OBJECT_HISTORY_DIFF__RAW_DIFF_DATA:
				return RAW_DIFF_DATA_EDEFAULT == null
						? rawDiffData != null
						: !RAW_DIFF_DATA_EDEFAULT.equals(rawDiffData);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (newState: ");
		result.append(newState);
		result.append(", newObjectInstance: ");
		result.append(newObjectInstance);
		result.append(", oldObjectInstance: ");
		result.append(oldObjectInstance);
		result.append(", rawDiffData: ");
		result.append(rawDiffData);
		result.append(')');
		return result.toString();
	}

} // ObjectHistoryDiffImpl
