/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.lang.Object;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Object History Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl#getHistoryData
 * <em>History Data</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl#getObjectDescriptor
 * <em>Object Descriptor</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl#getObjectInstance
 * <em>Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl#getState
 * <em>State</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ObjectHistoryNodeImpl extends MinimalEObjectImpl.Container implements ObjectHistoryNode {
	/**
	 * The cached value of the '{@link #getHistoryData() <em>History Data</em>}'
	 * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHistoryData()
	 * @generated
	 * @ordered
	 */
	protected EList<ObjectHistoryData>	historyData;

	/**
	 * The cached value of the '{@link #getObjectDescriptor()
	 * <em>Object Descriptor</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectDescriptor()
	 * @generated
	 * @ordered
	 */
	protected ObjectDescriptor			objectDescriptor;

	/**
	 * The default value of the '{@link #getObjectInstance()
	 * <em>Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected static final Object		OBJECT_INSTANCE_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getObjectInstance()
	 * <em>Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectInstance()
	 * @generated
	 * @ordered
	 */
	protected Object					objectInstance				= OBJECT_INSTANCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State						state;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectHistoryNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.OBJECT_HISTORY_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<ObjectHistoryData> getHistoryData() {
		if (historyData == null) {
			historyData = new EObjectResolvingEList<ObjectHistoryData>(	ObjectHistoryData.class,
																		this,
																		StatespacevisPackage.OBJECT_HISTORY_NODE__HISTORY_DATA);
		}
		return historyData;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectDescriptor getObjectDescriptor() {
		if (objectDescriptor != null && objectDescriptor.eIsProxy()) {
			InternalEObject oldObjectDescriptor = (InternalEObject) objectDescriptor;
			objectDescriptor = (ObjectDescriptor) eResolveProxy(oldObjectDescriptor);
			if (objectDescriptor != oldObjectDescriptor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR,
													oldObjectDescriptor,
													objectDescriptor));
			}
		}
		return objectDescriptor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectDescriptor basicGetObjectDescriptor() {
		return objectDescriptor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setObjectDescriptor(ObjectDescriptor newObjectDescriptor) {
		ObjectDescriptor oldObjectDescriptor = objectDescriptor;
		objectDescriptor = newObjectDescriptor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR,
											oldObjectDescriptor,
											objectDescriptor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getObjectInstance() {
		return objectInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setObjectInstance(Object newObjectInstance) {
		Object oldObjectInstance = objectInstance;
		objectInstance = newObjectInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_INSTANCE,
											oldObjectInstance,
											objectInstance));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State getState() {
		if (state != null && state.eIsProxy()) {
			InternalEObject oldState = (InternalEObject) state;
			state = (State) eResolveProxy(oldState);
			if (state != oldState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.OBJECT_HISTORY_NODE__STATE,
													oldState,
													state));
			}
		}
		return state;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State basicGetState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_HISTORY_NODE__STATE,
											oldState,
											state));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_NODE__HISTORY_DATA:
				return getHistoryData();
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR:
				if (resolve)
					return getObjectDescriptor();
				return basicGetObjectDescriptor();
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_INSTANCE:
				return getObjectInstance();
			case StatespacevisPackage.OBJECT_HISTORY_NODE__STATE:
				if (resolve)
					return getState();
				return basicGetState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_NODE__HISTORY_DATA:
				getHistoryData().clear();
				getHistoryData().addAll((Collection<? extends ObjectHistoryData>) newValue);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR:
				setObjectDescriptor((ObjectDescriptor) newValue);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_INSTANCE:
				setObjectInstance(newValue);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_NODE__STATE:
				setState((State) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_NODE__HISTORY_DATA:
				getHistoryData().clear();
				return;
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR:
				setObjectDescriptor((ObjectDescriptor) null);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_INSTANCE:
				setObjectInstance(OBJECT_INSTANCE_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_HISTORY_NODE__STATE:
				setState((State) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_HISTORY_NODE__HISTORY_DATA:
				return historyData != null && !historyData.isEmpty();
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR:
				return objectDescriptor != null;
			case StatespacevisPackage.OBJECT_HISTORY_NODE__OBJECT_INSTANCE:
				return OBJECT_INSTANCE_EDEFAULT == null
						? objectInstance != null
						: !OBJECT_INSTANCE_EDEFAULT.equals(objectInstance);
			case StatespacevisPackage.OBJECT_HISTORY_NODE__STATE:
				return state != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (objectInstance: ");
		result.append(objectInstance);
		result.append(')');
		return result.toString();
	}

} // ObjectHistoryNodeImpl
