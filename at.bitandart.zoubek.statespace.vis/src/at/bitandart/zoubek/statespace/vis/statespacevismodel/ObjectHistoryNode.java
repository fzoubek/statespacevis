/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Object History Node</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getHistoryData
 * <em>History Data</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectDescriptor
 * <em>Object Descriptor</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectInstance
 * <em>Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getState
 * <em>State</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryNode()
 * @model
 * @generated
 */
public interface ObjectHistoryNode extends EObject {
	/**
	 * Returns the value of the '<em><b>History Data</b></em>' reference list.
	 * The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData}
	 * . <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>History Data</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>History Data</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryNode_HistoryData()
	 * @model
	 * @generated
	 */
	EList<ObjectHistoryData> getHistoryData();

	/**
	 * Returns the value of the '<em><b>Object Descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Descriptor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Object Descriptor</em>' reference.
	 * @see #setObjectDescriptor(ObjectDescriptor)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryNode_ObjectDescriptor()
	 * @model required="true"
	 * @generated
	 */
	ObjectDescriptor getObjectDescriptor();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectDescriptor
	 * <em>Object Descriptor</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Object Descriptor</em>' reference.
	 * @see #getObjectDescriptor()
	 * @generated
	 */
	void setObjectDescriptor(ObjectDescriptor value);

	/**
	 * Returns the value of the '<em><b>Object Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Instance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Object Instance</em>' attribute.
	 * @see #setObjectInstance(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryNode_ObjectInstance()
	 * @model
	 * @generated
	 */
	Object getObjectInstance();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectInstance
	 * <em>Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Object Instance</em>' attribute.
	 * @see #getObjectInstance()
	 * @generated
	 */
	void setObjectInstance(Object value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(State)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryNode_State()
	 * @model required="true"
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getState
	 * <em>State</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

} // ObjectHistoryNode
