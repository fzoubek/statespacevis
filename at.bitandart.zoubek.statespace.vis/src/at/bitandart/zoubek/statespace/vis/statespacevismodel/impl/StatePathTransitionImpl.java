/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Path Transition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTransitionImpl#getNextStatePathNode
 * <em>Next State Path Node</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTransitionImpl#getTransitionName
 * <em>Transition Name</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTransitionImpl#getPreviousStatePathNode
 * <em>Previous State Path Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StatePathTransitionImpl extends MinimalEObjectImpl.Container implements StatePathTransition {
	/**
	 * The cached value of the '{@link #getNextStatePathNode()
	 * <em>Next State Path Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getNextStatePathNode()
	 * @generated
	 * @ordered
	 */
	protected StatePathNode			nextStatePathNode;

	/**
	 * The default value of the '{@link #getTransitionName()
	 * <em>Transition Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTransitionName()
	 * @generated
	 * @ordered
	 */
	protected static final String	TRANSITION_NAME_EDEFAULT	= null;
	/**
	 * The cached value of the '{@link #getTransitionName()
	 * <em>Transition Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTransitionName()
	 * @generated
	 * @ordered
	 */
	protected String				transitionName				= TRANSITION_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPreviousStatePathNode()
	 * <em>Previous State Path Node</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getPreviousStatePathNode()
	 * @generated
	 * @ordered
	 */
	protected StatePathNode			previousStatePathNode;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_PATH_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNode getNextStatePathNode() {
		if (nextStatePathNode != null && nextStatePathNode.eIsProxy()) {
			InternalEObject oldNextStatePathNode = (InternalEObject) nextStatePathNode;
			nextStatePathNode = (StatePathNode) eResolveProxy(oldNextStatePathNode);
			if (nextStatePathNode != oldNextStatePathNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE,
													oldNextStatePathNode,
													nextStatePathNode));
			}
		}
		return nextStatePathNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNode basicGetNextStatePathNode() {
		return nextStatePathNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNextStatePathNode(StatePathNode newNextStatePathNode) {
		StatePathNode oldNextStatePathNode = nextStatePathNode;
		nextStatePathNode = newNextStatePathNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE,
											oldNextStatePathNode,
											nextStatePathNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getTransitionName() {
		return transitionName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTransitionName(String newTransitionName) {
		String oldTransitionName = transitionName;
		transitionName = newTransitionName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_TRANSITION__TRANSITION_NAME,
											oldTransitionName,
											transitionName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNode getPreviousStatePathNode() {
		if (previousStatePathNode != null && previousStatePathNode.eIsProxy()) {
			InternalEObject oldPreviousStatePathNode = (InternalEObject) previousStatePathNode;
			previousStatePathNode = (StatePathNode) eResolveProxy(oldPreviousStatePathNode);
			if (previousStatePathNode != oldPreviousStatePathNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE,
													oldPreviousStatePathNode,
													previousStatePathNode));
			}
		}
		return previousStatePathNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNode basicGetPreviousStatePathNode() {
		return previousStatePathNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPreviousStatePathNode(StatePathNode newPreviousStatePathNode) {
		StatePathNode oldPreviousStatePathNode = previousStatePathNode;
		previousStatePathNode = newPreviousStatePathNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE,
											oldPreviousStatePathNode,
											previousStatePathNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE:
				if (resolve)
					return getNextStatePathNode();
				return basicGetNextStatePathNode();
			case StatespacevisPackage.STATE_PATH_TRANSITION__TRANSITION_NAME:
				return getTransitionName();
			case StatespacevisPackage.STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE:
				if (resolve)
					return getPreviousStatePathNode();
				return basicGetPreviousStatePathNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE:
				setNextStatePathNode((StatePathNode) newValue);
				return;
			case StatespacevisPackage.STATE_PATH_TRANSITION__TRANSITION_NAME:
				setTransitionName((String) newValue);
				return;
			case StatespacevisPackage.STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE:
				setPreviousStatePathNode((StatePathNode) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE:
				setNextStatePathNode((StatePathNode) null);
				return;
			case StatespacevisPackage.STATE_PATH_TRANSITION__TRANSITION_NAME:
				setTransitionName(TRANSITION_NAME_EDEFAULT);
				return;
			case StatespacevisPackage.STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE:
				setPreviousStatePathNode((StatePathNode) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE:
				return nextStatePathNode != null;
			case StatespacevisPackage.STATE_PATH_TRANSITION__TRANSITION_NAME:
				return TRANSITION_NAME_EDEFAULT == null
						? transitionName != null
						: !TRANSITION_NAME_EDEFAULT.equals(transitionName);
			case StatespacevisPackage.STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE:
				return previousStatePathNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (transitionName: ");
		result.append(transitionName);
		result.append(')');
		return result.toString();
	}

} // StatePathTransitionImpl
