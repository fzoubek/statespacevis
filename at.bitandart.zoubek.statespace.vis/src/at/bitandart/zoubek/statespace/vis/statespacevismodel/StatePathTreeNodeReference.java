/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Path Tree Node Reference</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference#getStatePathTreeNode
 * <em>State Path Tree Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTreeNodeReference()
 * @model
 * @generated
 */
public interface StatePathTreeNodeReference extends StatePathReference {
	/**
	 * Returns the value of the '<em><b>State Path Tree Node</b></em>'
	 * reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Path Tree Node</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>State Path Tree Node</em>' reference.
	 * @see #setStatePathTreeNode(TreeNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTreeNodeReference_StatePathTreeNode()
	 * @model required="true"
	 * @generated
	 */
	TreeNode<State, StateSpaceTransition> getStatePathTreeNode();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference#getStatePathTreeNode
	 * <em>State Path Tree Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>State Path Tree Node</em>'
	 *            reference.
	 * @see #getStatePathTreeNode()
	 * @generated
	 */
	void setStatePathTreeNode(TreeNode<State, StateSpaceTransition> value);

} // StatePathTreeNodeReference
