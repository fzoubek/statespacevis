/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import java.lang.Object;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Object Descriptor</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl#getObjectID
 * <em>Object ID</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl#getClassName
 * <em>Class Name</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl#getInstanceName
 * <em>Instance Name</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl#getClassDescriptor
 * <em>Class Descriptor</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ObjectDescriptorImpl extends MinimalEObjectImpl.Container implements ObjectDescriptor {
	/**
	 * The default value of the '{@link #getObjectID() <em>Object ID</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getObjectID()
	 * @generated
	 * @ordered
	 */
	protected static final int		OBJECT_ID_EDEFAULT			= 0;

	/**
	 * The cached value of the '{@link #getObjectID() <em>Object ID</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getObjectID()
	 * @generated
	 * @ordered
	 */
	protected int					objectID					= OBJECT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getClassName() <em>Class Name</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getClassName()
	 * @generated
	 * @ordered
	 */
	protected static final String	CLASS_NAME_EDEFAULT			= null;

	/**
	 * The cached value of the '{@link #getClassName() <em>Class Name</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getClassName()
	 * @generated
	 * @ordered
	 */
	protected String				className					= CLASS_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getInstanceName()
	 * <em>Instance Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInstanceName()
	 * @generated
	 * @ordered
	 */
	protected static final String	INSTANCE_NAME_EDEFAULT		= null;

	/**
	 * The cached value of the '{@link #getInstanceName()
	 * <em>Instance Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInstanceName()
	 * @generated
	 * @ordered
	 */
	protected String				instanceName				= INSTANCE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getClassDescriptor()
	 * <em>Class Descriptor</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getClassDescriptor()
	 * @generated
	 * @ordered
	 */
	protected static final Object	CLASS_DESCRIPTOR_EDEFAULT	= null;

	/**
	 * The cached value of the '{@link #getClassDescriptor()
	 * <em>Class Descriptor</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getClassDescriptor()
	 * @generated
	 * @ordered
	 */
	protected Object				classDescriptor				= CLASS_DESCRIPTOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectDescriptorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.OBJECT_DESCRIPTOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setClassName(String newClassName) {
		String oldClassName = className;
		className = newClassName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_NAME,
											oldClassName,
											className));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getInstanceName() {
		return instanceName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setInstanceName(String newInstanceName) {
		String oldInstanceName = instanceName;
		instanceName = newInstanceName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_DESCRIPTOR__INSTANCE_NAME,
											oldInstanceName,
											instanceName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Object getClassDescriptor() {
		return classDescriptor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setClassDescriptor(Object newClassDescriptor) {
		Object oldClassDescriptor = classDescriptor;
		classDescriptor = newClassDescriptor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR,
											oldClassDescriptor,
											classDescriptor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getObjectID() {
		return objectID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setObjectID(int newObjectID) {
		int oldObjectID = objectID;
		objectID = newObjectID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_DESCRIPTOR__OBJECT_ID,
											oldObjectID,
											objectID));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_DESCRIPTOR__OBJECT_ID:
				return getObjectID();
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_NAME:
				return getClassName();
			case StatespacevisPackage.OBJECT_DESCRIPTOR__INSTANCE_NAME:
				return getInstanceName();
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR:
				return getClassDescriptor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_DESCRIPTOR__OBJECT_ID:
				setObjectID((Integer) newValue);
				return;
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_NAME:
				setClassName((String) newValue);
				return;
			case StatespacevisPackage.OBJECT_DESCRIPTOR__INSTANCE_NAME:
				setInstanceName((String) newValue);
				return;
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR:
				setClassDescriptor(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_DESCRIPTOR__OBJECT_ID:
				setObjectID(OBJECT_ID_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_NAME:
				setClassName(CLASS_NAME_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_DESCRIPTOR__INSTANCE_NAME:
				setInstanceName(INSTANCE_NAME_EDEFAULT);
				return;
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR:
				setClassDescriptor(CLASS_DESCRIPTOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_DESCRIPTOR__OBJECT_ID:
				return objectID != OBJECT_ID_EDEFAULT;
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_NAME:
				return CLASS_NAME_EDEFAULT == null
						? className != null
						: !CLASS_NAME_EDEFAULT.equals(className);
			case StatespacevisPackage.OBJECT_DESCRIPTOR__INSTANCE_NAME:
				return INSTANCE_NAME_EDEFAULT == null
						? instanceName != null
						: !INSTANCE_NAME_EDEFAULT.equals(instanceName);
			case StatespacevisPackage.OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR:
				return CLASS_DESCRIPTOR_EDEFAULT == null
						? classDescriptor != null
						: !CLASS_DESCRIPTOR_EDEFAULT.equals(classDescriptor);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ObjectID: ");
		result.append(objectID);
		result.append(", ClassName: ");
		result.append(className);
		result.append(", InstanceName: ");
		result.append(instanceName);
		result.append(", classDescriptor: ");
		result.append(classDescriptor);
		result.append(')');
		return result.toString();
	}

} // ObjectDescriptorImpl
