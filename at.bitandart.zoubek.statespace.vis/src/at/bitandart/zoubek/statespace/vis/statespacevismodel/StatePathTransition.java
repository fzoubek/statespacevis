/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Path Transition</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getNextStatePathNode
 * <em>Next State Path Node</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getTransitionName
 * <em>Transition Name</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getPreviousStatePathNode
 * <em>Previous State Path Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTransition()
 * @model
 * @generated
 */
public interface StatePathTransition extends EObject {
	/**
	 * Returns the value of the '<em><b>Next State Path Node</b></em>'
	 * reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next State Path Node</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Next State Path Node</em>' reference.
	 * @see #setNextStatePathNode(StatePathNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTransition_NextStatePathNode()
	 * @model
	 * @generated
	 */
	StatePathNode getNextStatePathNode();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getNextStatePathNode
	 * <em>Next State Path Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Next State Path Node</em>'
	 *            reference.
	 * @see #getNextStatePathNode()
	 * @generated
	 */
	void setNextStatePathNode(StatePathNode value);

	/**
	 * Returns the value of the '<em><b>Transition Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Transition Name</em>' attribute.
	 * @see #setTransitionName(String)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTransition_TransitionName()
	 * @model
	 * @generated
	 */
	String getTransitionName();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getTransitionName
	 * <em>Transition Name</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Transition Name</em>' attribute.
	 * @see #getTransitionName()
	 * @generated
	 */
	void setTransitionName(String value);

	/**
	 * Returns the value of the '<em><b>Previous State Path Node</b></em>'
	 * reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous State Path Node</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Previous State Path Node</em>' reference.
	 * @see #setPreviousStatePathNode(StatePathNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePathTransition_PreviousStatePathNode()
	 * @model
	 * @generated
	 */
	StatePathNode getPreviousStatePathNode();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getPreviousStatePathNode
	 * <em>Previous State Path Node</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Previous State Path Node</em>'
	 *            reference.
	 * @see #getPreviousStatePathNode()
	 * @generated
	 */
	void setPreviousStatePathNode(StatePathNode value);

} // StatePathTransition
