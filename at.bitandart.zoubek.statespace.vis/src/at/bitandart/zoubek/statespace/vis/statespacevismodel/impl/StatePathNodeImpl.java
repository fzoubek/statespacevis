/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Path Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathNodeImpl#getState
 * <em>State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathNodeImpl#getNextTransition
 * <em>Next Transition</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathNodeImpl#getIncomingTransition
 * <em>Incoming Transition</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StatePathNodeImpl extends MinimalEObjectImpl.Container implements StatePathNode {
	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State					state;

	/**
	 * The cached value of the '{@link #getNextTransition()
	 * <em>Next Transition</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getNextTransition()
	 * @generated
	 * @ordered
	 */
	protected StatePathTransition	nextTransition;

	/**
	 * The cached value of the '{@link #getIncomingTransition()
	 * <em>Incoming Transition</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getIncomingTransition()
	 * @generated
	 * @ordered
	 */
	protected StatePathTransition	incomingTransition;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_PATH_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State getState() {
		if (state != null && state.eIsProxy()) {
			InternalEObject oldState = (InternalEObject) state;
			state = (State) eResolveProxy(oldState);
			if (state != oldState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH_NODE__STATE,
													oldState,
													state));
			}
		}
		return state;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State basicGetState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_NODE__STATE,
											oldState,
											state));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTransition getNextTransition() {
		if (nextTransition != null && nextTransition.eIsProxy()) {
			InternalEObject oldNextTransition = (InternalEObject) nextTransition;
			nextTransition = (StatePathTransition) eResolveProxy(oldNextTransition);
			if (nextTransition != oldNextTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH_NODE__NEXT_TRANSITION,
													oldNextTransition,
													nextTransition));
			}
		}
		return nextTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTransition basicGetNextTransition() {
		return nextTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNextTransition(StatePathTransition newNextTransition) {
		StatePathTransition oldNextTransition = nextTransition;
		nextTransition = newNextTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_NODE__NEXT_TRANSITION,
											oldNextTransition,
											nextTransition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTransition getIncomingTransition() {
		if (incomingTransition != null && incomingTransition.eIsProxy()) {
			InternalEObject oldIncomingTransition = (InternalEObject) incomingTransition;
			incomingTransition = (StatePathTransition) eResolveProxy(oldIncomingTransition);
			if (incomingTransition != oldIncomingTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH_NODE__INCOMING_TRANSITION,
													oldIncomingTransition,
													incomingTransition));
			}
		}
		return incomingTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTransition basicGetIncomingTransition() {
		return incomingTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setIncomingTransition(StatePathTransition newIncomingTransition) {
		StatePathTransition oldIncomingTransition = incomingTransition;
		incomingTransition = newIncomingTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_NODE__INCOMING_TRANSITION,
											oldIncomingTransition,
											incomingTransition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_NODE__STATE:
				if (resolve)
					return getState();
				return basicGetState();
			case StatespacevisPackage.STATE_PATH_NODE__NEXT_TRANSITION:
				if (resolve)
					return getNextTransition();
				return basicGetNextTransition();
			case StatespacevisPackage.STATE_PATH_NODE__INCOMING_TRANSITION:
				if (resolve)
					return getIncomingTransition();
				return basicGetIncomingTransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_NODE__STATE:
				setState((State) newValue);
				return;
			case StatespacevisPackage.STATE_PATH_NODE__NEXT_TRANSITION:
				setNextTransition((StatePathTransition) newValue);
				return;
			case StatespacevisPackage.STATE_PATH_NODE__INCOMING_TRANSITION:
				setIncomingTransition((StatePathTransition) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_NODE__STATE:
				setState((State) null);
				return;
			case StatespacevisPackage.STATE_PATH_NODE__NEXT_TRANSITION:
				setNextTransition((StatePathTransition) null);
				return;
			case StatespacevisPackage.STATE_PATH_NODE__INCOMING_TRANSITION:
				setIncomingTransition((StatePathTransition) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_NODE__STATE:
				return state != null;
			case StatespacevisPackage.STATE_PATH_NODE__NEXT_TRANSITION:
				return nextTransition != null;
			case StatespacevisPackage.STATE_PATH_NODE__INCOMING_TRANSITION:
				return incomingTransition != null;
		}
		return super.eIsSet(featureID);
	}

} // StatePathNodeImpl
