/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Lifeline</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getParentLifeline
 * <em>Parent Lifeline</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getChildLifelines
 * <em>Child Lifelines</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isVisible
 * <em>Visible</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isCollapsed
 * <em>Collapsed</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getLifeline()
 * @model abstract="true"
 * @generated
 */
public interface Lifeline extends EObject {

	/**
	 * Returns the value of the '<em><b>Parent Lifeline</b></em>' reference. It
	 * is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getChildLifelines
	 * <em>Child Lifelines</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Lifeline</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Lifeline</em>' reference.
	 * @see #setParentLifeline(Lifeline)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getLifeline_ParentLifeline()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getChildLifelines
	 * @model opposite="childLifelines"
	 * @generated
	 */
	Lifeline getParentLifeline();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getParentLifeline
	 * <em>Parent Lifeline</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Parent Lifeline</em>' reference.
	 * @see #getParentLifeline()
	 * @generated
	 */
	void setParentLifeline(Lifeline value);

	/**
	 * Returns the value of the '<em><b>Child Lifelines</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline}.
	 * It is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getParentLifeline
	 * <em>Parent Lifeline</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Lifelines</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Child Lifelines</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getLifeline_ChildLifelines()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getParentLifeline
	 * @model opposite="parentLifeline"
	 * @generated
	 */
	EList<Lifeline> getChildLifelines();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isVisible
	 * <em>Visible</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Visible</em>' attribute.
	 * @see #isVisible()
	 * @generated
	 */
	void setVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Collapsed</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collapsed</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Collapsed</em>' attribute.
	 * @see #setCollapsed(boolean)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getLifeline_Collapsed()
	 * @model
	 * @generated
	 */
	boolean isCollapsed();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isCollapsed
	 * <em>Collapsed</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Collapsed</em>' attribute.
	 * @see #isCollapsed()
	 * @generated
	 */
	void setCollapsed(boolean value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * 
	 * @return true if the lifeline is visible and the parent (if set) is also
	 *         visible, false otherwise. <!-- end-model-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isVisible();
} // Lifeline
