/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.util;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage
 * @generated
 */
public class StatespacevisSwitch<T1> extends Switch<T1> {
	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static StatespacevisPackage	modelPackage;

	/**
	 * Creates an instance of the switch. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public StatespacevisSwitch() {
		if (modelPackage == null) {
			modelPackage = StatespacevisPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns
	 * a non null result; it yields that result. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code>
	 *         call.
	 * @generated
	 */
	@Override
	protected T1 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case StatespacevisPackage.LIFELINE_GROUP: {
				LifelineGroup lifelineGroup = (LifelineGroup) theEObject;
				T1 result = caseLifelineGroup(lifelineGroup);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.LIFELINE: {
				Lifeline lifeline = (Lifeline) theEObject;
				T1 result = caseLifeline(lifeline);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.OBJECT_LIFELINE: {
				ObjectLifeline objectLifeline = (ObjectLifeline) theEObject;
				T1 result = caseObjectLifeline(objectLifeline);
				if (result == null)
					result = caseLifeline(objectLifeline);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.OBJECT_DESCRIPTOR: {
				ObjectDescriptor objectDescriptor = (ObjectDescriptor) theEObject;
				T1 result = caseObjectDescriptor(objectDescriptor);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.OBJECT_LIFELINE_STATE: {
				ObjectLifelineState objectLifelineState = (ObjectLifelineState) theEObject;
				T1 result = caseObjectLifelineState(objectLifelineState);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.TREE: {
				Tree<?, ?> tree = (Tree<?, ?>) theEObject;
				T1 result = caseTree(tree);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.TREE_NODE: {
				TreeNode<?, ?> treeNode = (TreeNode<?, ?>) theEObject;
				T1 result = caseTreeNode(treeNode);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.TREE_TRANSITION: {
				TreeTransition<?, ?> treeTransition = (TreeTransition<?, ?>) theEObject;
				T1 result = caseTreeTransition(treeTransition);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE: {
				State state = (State) theEObject;
				T1 result = caseState(state);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_PATH: {
				StatePath statePath = (StatePath) theEObject;
				T1 result = caseStatePath(statePath);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_PATH_NODE: {
				StatePathNode statePathNode = (StatePathNode) theEObject;
				T1 result = caseStatePathNode(statePathNode);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_PATH_TRANSITION: {
				StatePathTransition statePathTransition = (StatePathTransition) theEObject;
				T1 result = caseStatePathTransition(statePathTransition);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_SPACE: {
				StateSpace stateSpace = (StateSpace) theEObject;
				T1 result = caseStateSpace(stateSpace);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_SPACE_PATH_VIS: {
				StateSpacePathVis stateSpacePathVis = (StateSpacePathVis) theEObject;
				T1 result = caseStateSpacePathVis(stateSpacePathVis);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_SPACE_TRANSITION: {
				StateSpaceTransition stateSpaceTransition = (StateSpaceTransition) theEObject;
				T1 result = caseStateSpaceTransition(stateSpaceTransition);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.MODEL_HISTORY: {
				ModelHistory modelHistory = (ModelHistory) theEObject;
				T1 result = caseModelHistory(modelHistory);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.MODEL_HISTORY_BUILDER: {
				ModelHistoryBuilder modelHistoryBuilder = (ModelHistoryBuilder) theEObject;
				T1 result = caseModelHistoryBuilder(modelHistoryBuilder);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.LIFELINE_BUILDER: {
				LifelineBuilder lifelineBuilder = (LifelineBuilder) theEObject;
				T1 result = caseLifelineBuilder(lifelineBuilder);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION: {
				ObjectHistoryTransition objectHistoryTransition = (ObjectHistoryTransition) theEObject;
				T1 result = caseObjectHistoryTransition(objectHistoryTransition);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.OBJECT_HISTORY_DATA: {
				ObjectHistoryData objectHistoryData = (ObjectHistoryData) theEObject;
				T1 result = caseObjectHistoryData(objectHistoryData);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.OBJECT_HISTORY_DIFF: {
				ObjectHistoryDiff objectHistoryDiff = (ObjectHistoryDiff) theEObject;
				T1 result = caseObjectHistoryDiff(objectHistoryDiff);
				if (result == null)
					result = caseObjectHistoryData(objectHistoryDiff);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.OBJECT_HISTORY_NODE: {
				ObjectHistoryNode objectHistoryNode = (ObjectHistoryNode) theEObject;
				T1 result = caseObjectHistoryNode(objectHistoryNode);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE: {
				StatePathTreeNodeReference statePathTreeNodeReference = (StatePathTreeNodeReference) theEObject;
				T1 result = caseStatePathTreeNodeReference(statePathTreeNodeReference);
				if (result == null)
					result = caseStatePathReference(statePathTreeNodeReference);
				if (result == null)
					result = caseObjectHistoryData(statePathTreeNodeReference);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_PATH_REFERENCE: {
				StatePathReference statePathReference = (StatePathReference) theEObject;
				T1 result = caseStatePathReference(statePathReference);
				if (result == null)
					result = caseObjectHistoryData(statePathReference);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			case StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE: {
				StatePathTreeTransitionReference statePathTreeTransitionReference = (StatePathTreeTransitionReference) theEObject;
				T1 result = caseStatePathTreeTransitionReference(statePathTreeTransitionReference);
				if (result == null)
					result = caseStatePathReference(statePathTreeTransitionReference);
				if (result == null)
					result = caseObjectHistoryData(statePathTreeTransitionReference);
				if (result == null)
					result = defaultCase(theEObject);
				return result;
			}
			default:
				return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Lifeline Group</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Lifeline Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseLifelineGroup(LifelineGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Lifeline</em>'. <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Lifeline</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseLifeline(Lifeline object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Space Path Vis</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Space Path Vis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStateSpacePathVis(StateSpacePathVis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Object Lifeline</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Object Lifeline</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseObjectLifeline(ObjectLifeline object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Path</em>'. <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStatePath(StatePath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State</em>'. <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Path Node</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Path Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStatePathNode(StatePathNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Path Transition</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Path Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStatePathTransition(StatePathTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Space</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Space</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStateSpace(StateSpace object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Space Transition</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Space Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStateSpaceTransition(StateSpaceTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Model History</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Model History</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseModelHistory(ModelHistory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Model History Builder</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Model History Builder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseModelHistoryBuilder(ModelHistoryBuilder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Lifeline Builder</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Lifeline Builder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseLifelineBuilder(LifelineBuilder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Object History Transition</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Object History Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseObjectHistoryTransition(ObjectHistoryTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Object History Data</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Object History Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseObjectHistoryData(ObjectHistoryData object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Object History Diff</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Object History Diff</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseObjectHistoryDiff(ObjectHistoryDiff object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Object History Node</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Object History Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseObjectHistoryNode(ObjectHistoryNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Path Tree Node Reference</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Path Tree Node Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStatePathTreeNodeReference(StatePathTreeNodeReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Path Reference</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Path Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStatePathReference(StatePathReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>State Path Tree Transition Reference</em>'. <!-- begin-user-doc -->
	 * This implementation returns null; returning a non-null result will
	 * terminate the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>State Path Tree Transition Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStatePathTreeTransitionReference(
			StatePathTreeTransitionReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Object Descriptor</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Object Descriptor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseObjectDescriptor(ObjectDescriptor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Object Lifeline State</em>'. <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Object Lifeline State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseObjectLifelineState(ObjectLifelineState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Tree</em>'. <!-- begin-user-doc --> This implementation returns null;
	 * returning a non-null result will terminate the switch. <!-- end-user-doc
	 * -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Tree</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <N, T> T1 caseTree(Tree<N, T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Tree Node</em>'. <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Tree Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <N, T> T1 caseTreeNode(TreeNode<N, T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>Tree Transition</em>'. <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>Tree Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <N, T> T1 caseTreeTransition(TreeTransition<N, T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '
	 * <em>EObject</em>'. <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch, but this is
	 * the last case anyway. <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '
	 *         <em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T1 defaultCase(EObject object) {
		return null;
	}

} // StatespacevisSwitch
