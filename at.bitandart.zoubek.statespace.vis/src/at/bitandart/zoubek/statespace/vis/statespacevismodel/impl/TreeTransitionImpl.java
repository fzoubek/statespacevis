/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Tree Transition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeTransitionImpl#getNextNode
 * <em>Next Node</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeTransitionImpl#getPreviousNode
 * <em>Previous Node</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeTransitionImpl#getTransitionValue
 * <em>Transition Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TreeTransitionImpl<N, T> extends MinimalEObjectImpl.Container implements TreeTransition<N, T> {
	/**
	 * The cached value of the '{@link #getNextNode() <em>Next Node</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNextNode()
	 * @generated
	 * @ordered
	 */
	protected TreeNode<N, T>	nextNode;

	/**
	 * The cached value of the '{@link #getPreviousNode()
	 * <em>Previous Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getPreviousNode()
	 * @generated
	 * @ordered
	 */
	protected TreeNode<N, T>	previousNode;

	/**
	 * The cached value of the '{@link #getTransitionValue()
	 * <em>Transition Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getTransitionValue()
	 * @generated
	 * @ordered
	 */
	protected T					transitionValue;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected TreeTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.TREE_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public TreeNode<N, T> getNextNode() {
		if (nextNode != null && nextNode.eIsProxy()) {
			InternalEObject oldNextNode = (InternalEObject) nextNode;
			nextNode = (TreeNode<N, T>) eResolveProxy(oldNextNode);
			if (nextNode != oldNextNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.TREE_TRANSITION__NEXT_NODE,
													oldNextNode,
													nextNode));
			}
		}
		return nextNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeNode<N, T> basicGetNextNode() {
		return nextNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetNextNode(TreeNode<N, T> newNextNode,
			NotificationChain msgs) {
		TreeNode<N, T> oldNextNode = nextNode;
		nextNode = newNextNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(	this,
																	Notification.SET,
																	StatespacevisPackage.TREE_TRANSITION__NEXT_NODE,
																	oldNextNode,
																	newNextNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNextNode(TreeNode<N, T> newNextNode) {
		if (newNextNode != nextNode) {
			NotificationChain msgs = null;
			if (nextNode != null)
				msgs = ((InternalEObject) nextNode).eInverseRemove(	this,
																	StatespacevisPackage.TREE_NODE__PARENT_TRANSITION,
																	TreeNode.class,
																	msgs);
			if (newNextNode != null)
				msgs = ((InternalEObject) newNextNode).eInverseAdd(	this,
																	StatespacevisPackage.TREE_NODE__PARENT_TRANSITION,
																	TreeNode.class,
																	msgs);
			msgs = basicSetNextNode(newNextNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.TREE_TRANSITION__NEXT_NODE,
											newNextNode,
											newNextNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public TreeNode<N, T> getPreviousNode() {
		if (previousNode != null && previousNode.eIsProxy()) {
			InternalEObject oldPreviousNode = (InternalEObject) previousNode;
			previousNode = (TreeNode<N, T>) eResolveProxy(oldPreviousNode);
			if (previousNode != oldPreviousNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE,
													oldPreviousNode,
													previousNode));
			}
		}
		return previousNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeNode<N, T> basicGetPreviousNode() {
		return previousNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetPreviousNode(
			TreeNode<N, T> newPreviousNode, NotificationChain msgs) {
		TreeNode<N, T> oldPreviousNode = previousNode;
		previousNode = newPreviousNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(	this,
																	Notification.SET,
																	StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE,
																	oldPreviousNode,
																	newPreviousNode);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setPreviousNode(TreeNode<N, T> newPreviousNode) {
		if (newPreviousNode != previousNode) {
			NotificationChain msgs = null;
			if (previousNode != null)
				msgs = ((InternalEObject) previousNode).eInverseRemove(	this,
																		StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS,
																		TreeNode.class,
																		msgs);
			if (newPreviousNode != null)
				msgs = ((InternalEObject) newPreviousNode).eInverseAdd(	this,
																		StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS,
																		TreeNode.class,
																		msgs);
			msgs = basicSetPreviousNode(newPreviousNode, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE,
											newPreviousNode,
											newPreviousNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public T getTransitionValue() {
		return transitionValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setTransitionValue(T newTransitionValue) {
		T oldTransitionValue = transitionValue;
		transitionValue = newTransitionValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.TREE_TRANSITION__TRANSITION_VALUE,
											oldTransitionValue,
											transitionValue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.TREE_TRANSITION__NEXT_NODE:
				if (nextNode != null)
					msgs = ((InternalEObject) nextNode).eInverseRemove(	this,
																		StatespacevisPackage.TREE_NODE__PARENT_TRANSITION,
																		TreeNode.class,
																		msgs);
				return basicSetNextNode((TreeNode<N, T>) otherEnd, msgs);
			case StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE:
				if (previousNode != null)
					msgs = ((InternalEObject) previousNode).eInverseRemove(	this,
																			StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS,
																			TreeNode.class,
																			msgs);
				return basicSetPreviousNode((TreeNode<N, T>) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.TREE_TRANSITION__NEXT_NODE:
				return basicSetNextNode(null, msgs);
			case StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE:
				return basicSetPreviousNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.TREE_TRANSITION__NEXT_NODE:
				if (resolve)
					return getNextNode();
				return basicGetNextNode();
			case StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE:
				if (resolve)
					return getPreviousNode();
				return basicGetPreviousNode();
			case StatespacevisPackage.TREE_TRANSITION__TRANSITION_VALUE:
				return getTransitionValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.TREE_TRANSITION__NEXT_NODE:
				setNextNode((TreeNode<N, T>) newValue);
				return;
			case StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE:
				setPreviousNode((TreeNode<N, T>) newValue);
				return;
			case StatespacevisPackage.TREE_TRANSITION__TRANSITION_VALUE:
				setTransitionValue((T) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.TREE_TRANSITION__NEXT_NODE:
				setNextNode((TreeNode<N, T>) null);
				return;
			case StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE:
				setPreviousNode((TreeNode<N, T>) null);
				return;
			case StatespacevisPackage.TREE_TRANSITION__TRANSITION_VALUE:
				setTransitionValue((T) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.TREE_TRANSITION__NEXT_NODE:
				return nextNode != null;
			case StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE:
				return previousNode != null;
			case StatespacevisPackage.TREE_TRANSITION__TRANSITION_VALUE:
				return transitionValue != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (transitionValue: ");
		result.append(transitionValue);
		result.append(')');
		return result.toString();
	}

} // TreeTransitionImpl
