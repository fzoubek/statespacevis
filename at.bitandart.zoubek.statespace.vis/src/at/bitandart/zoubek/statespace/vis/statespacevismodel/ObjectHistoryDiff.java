/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Object History Diff</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewState
 * <em>New State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewObjectInstance
 * <em>New Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getOldObjectInstance
 * <em>Old Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getRawDiffData
 * <em>Raw Diff Data</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryDiff()
 * @model
 * @generated
 */
public interface ObjectHistoryDiff extends ObjectHistoryData {
	/**
	 * Returns the value of the '<em><b>New State</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType}
	 * . <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>New State</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>New State</em>' attribute.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * @see #setNewState(ObjectStateType)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryDiff_NewState()
	 * @model
	 * @generated
	 */
	ObjectStateType getNewState();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewState
	 * <em>New State</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>New State</em>' attribute.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * @see #getNewState()
	 * @generated
	 */
	void setNewState(ObjectStateType value);

	/**
	 * Returns the value of the '<em><b>New Object Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>New Object Instance</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>New Object Instance</em>' attribute.
	 * @see #setNewObjectInstance(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryDiff_NewObjectInstance()
	 * @model
	 * @generated
	 */
	Object getNewObjectInstance();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewObjectInstance
	 * <em>New Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>New Object Instance</em>' attribute.
	 * @see #getNewObjectInstance()
	 * @generated
	 */
	void setNewObjectInstance(Object value);

	/**
	 * Returns the value of the '<em><b>Old Object Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Old Object Instance</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Old Object Instance</em>' attribute.
	 * @see #setOldObjectInstance(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryDiff_OldObjectInstance()
	 * @model
	 * @generated
	 */
	Object getOldObjectInstance();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getOldObjectInstance
	 * <em>Old Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Old Object Instance</em>' attribute.
	 * @see #getOldObjectInstance()
	 * @generated
	 */
	void setOldObjectInstance(Object value);

	/**
	 * Returns the value of the '<em><b>Raw Diff Data</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Diff Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Raw Diff Data</em>' attribute.
	 * @see #setRawDiffData(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryDiff_RawDiffData()
	 * @model
	 * @generated
	 */
	Object getRawDiffData();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getRawDiffData
	 * <em>Raw Diff Data</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Raw Diff Data</em>' attribute.
	 * @see #getRawDiffData()
	 * @generated
	 */
	void setRawDiffData(Object value);

} // ObjectHistoryDiff
