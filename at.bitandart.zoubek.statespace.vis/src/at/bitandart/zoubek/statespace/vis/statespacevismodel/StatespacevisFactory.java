/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage
 * @generated
 */
public interface StatespacevisFactory extends EFactory {
	/**
	 * The singleton instance of the factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	StatespacevisFactory	eINSTANCE	= at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Lifeline Group</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Lifeline Group</em>'.
	 * @generated
	 */
	LifelineGroup createLifelineGroup();

	/**
	 * Returns a new object of class '<em>State Space Path Vis</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State Space Path Vis</em>'.
	 * @generated
	 */
	StateSpacePathVis createStateSpacePathVis();

	/**
	 * Returns a new object of class '<em>Object Lifeline</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Object Lifeline</em>'.
	 * @generated
	 */
	ObjectLifeline createObjectLifeline();

	/**
	 * Returns a new object of class '<em>State Path</em>'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State Path</em>'.
	 * @generated
	 */
	StatePath createStatePath();

	/**
	 * Returns a new object of class '<em>State</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State</em>'.
	 * @generated
	 */
	State createState();

	/**
	 * Returns a new object of class '<em>State Path Node</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State Path Node</em>'.
	 * @generated
	 */
	StatePathNode createStatePathNode();

	/**
	 * Returns a new object of class '<em>State Path Transition</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State Path Transition</em>'.
	 * @generated
	 */
	StatePathTransition createStatePathTransition();

	/**
	 * Returns a new object of class '<em>State Space</em>'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State Space</em>'.
	 * @generated
	 */
	StateSpace createStateSpace();

	/**
	 * Returns a new object of class '<em>State Space Transition</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State Space Transition</em>'.
	 * @generated
	 */
	StateSpaceTransition createStateSpaceTransition();

	/**
	 * Returns a new object of class '<em>Model History</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Model History</em>'.
	 * @generated
	 */
	ModelHistory createModelHistory();

	/**
	 * Returns a new object of class '<em>Object History Transition</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Object History Transition</em>'.
	 * @generated
	 */
	ObjectHistoryTransition createObjectHistoryTransition();

	/**
	 * Returns a new object of class '<em>Object History Diff</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Object History Diff</em>'.
	 * @generated
	 */
	ObjectHistoryDiff createObjectHistoryDiff();

	/**
	 * Returns a new object of class '<em>Object History Node</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Object History Node</em>'.
	 * @generated
	 */
	ObjectHistoryNode createObjectHistoryNode();

	/**
	 * Returns a new object of class '<em>State Path Tree Node Reference</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>State Path Tree Node Reference</em>'.
	 * @generated
	 */
	StatePathTreeNodeReference createStatePathTreeNodeReference();

	/**
	 * Returns a new object of class '
	 * <em>State Path Tree Transition Reference</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '
	 *         <em>State Path Tree Transition Reference</em>'.
	 * @generated
	 */
	StatePathTreeTransitionReference createStatePathTreeTransitionReference();

	/**
	 * Returns a new object of class '<em>Object Descriptor</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Object Descriptor</em>'.
	 * @generated
	 */
	ObjectDescriptor createObjectDescriptor();

	/**
	 * Returns a new object of class '<em>Object Lifeline State</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Object Lifeline State</em>'.
	 * @generated
	 */
	ObjectLifelineState createObjectLifelineState();

	/**
	 * Returns a new object of class '<em>Tree</em>'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Tree</em>'.
	 * @generated
	 */
	<N, T> Tree<N, T> createTree();

	/**
	 * Returns a new object of class '<em>Tree Node</em>'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Tree Node</em>'.
	 * @generated
	 */
	<N, T> TreeNode<N, T> createTreeNode();

	/**
	 * Returns a new object of class '<em>Tree Transition</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>Tree Transition</em>'.
	 * @generated
	 */
	<N, T> TreeTransition<N, T> createTreeTransition();

	/**
	 * Returns the package supported by this factory. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * @generated
	 */
	StatespacevisPackage getStatespacevisPackage();

} // StatespacevisFactory
