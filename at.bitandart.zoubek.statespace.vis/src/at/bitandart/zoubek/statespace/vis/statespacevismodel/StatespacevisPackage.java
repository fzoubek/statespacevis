/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory
 * @model kind="package"
 * @generated
 */
public interface StatespacevisPackage extends EPackage {
	/**
	 * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String					eNAME																	= "statespacevismodel";

	/**
	 * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String					eNS_URI																	= "http://zoubek.bitandart.at/sspvis";

	/**
	 * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String					eNS_PREFIX																= "sspvis";

	/**
	 * The singleton instance of the package. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	StatespacevisPackage	eINSTANCE																= at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl.init();

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineGroupImpl
	 * <em>Lifeline Group</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineGroupImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getLifelineGroup()
	 * @generated
	 */
	int						LIFELINE_GROUP															= 0;

	/**
	 * The feature id for the '<em><b>Lifelines</b></em>' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_GROUP__LIFELINES												= 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_GROUP__NAME													= 1;

	/**
	 * The number of structural features of the '<em>Lifeline Group</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_GROUP_FEATURE_COUNT											= 2;

	/**
	 * The number of operations of the '<em>Lifeline Group</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_GROUP_OPERATION_COUNT											= 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineImpl
	 * <em>Lifeline</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getLifeline()
	 * @generated
	 */
	int						LIFELINE																= 1;

	/**
	 * The feature id for the '<em><b>Parent Lifeline</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE__PARENT_LIFELINE												= 0;

	/**
	 * The feature id for the '<em><b>Child Lifelines</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE__CHILD_LIFELINES												= 1;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE__VISIBLE														= 2;

	/**
	 * The feature id for the '<em><b>Collapsed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE__COLLAPSED														= 3;

	/**
	 * The number of structural features of the '<em>Lifeline</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_FEATURE_COUNT													= 4;

	/**
	 * The number of operations of the '<em>Lifeline</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_OPERATION_COUNT												= 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl
	 * <em>State Space Path Vis</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStateSpacePathVis()
	 * @generated
	 */
	int						STATE_SPACE_PATH_VIS													= 13;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineImpl
	 * <em>Object Lifeline</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectLifeline()
	 * @generated
	 */
	int						OBJECT_LIFELINE															= 2;

	/**
	 * The feature id for the '<em><b>Parent Lifeline</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE__PARENT_LIFELINE										= LIFELINE__PARENT_LIFELINE;

	/**
	 * The feature id for the '<em><b>Child Lifelines</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE__CHILD_LIFELINES										= LIFELINE__CHILD_LIFELINES;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE__VISIBLE												= LIFELINE__VISIBLE;

	/**
	 * The feature id for the '<em><b>Collapsed</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE__COLLAPSED												= LIFELINE__COLLAPSED;

	/**
	 * The feature id for the '<em><b>Object Descriptor</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE__OBJECT_DESCRIPTOR										= LIFELINE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object State Tree</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE__OBJECT_STATE_TREE										= LIFELINE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Object Lifeline</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE_FEATURE_COUNT											= LIFELINE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Object Lifeline</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE_OPERATION_COUNT											= LIFELINE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathImpl
	 * <em>State Path</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePath()
	 * @generated
	 */
	int						STATE_PATH																= 9;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl
	 * <em>State</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getState()
	 * @generated
	 */
	int						STATE																	= 8;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathNodeImpl
	 * <em>State Path Node</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathNodeImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathNode()
	 * @generated
	 */
	int						STATE_PATH_NODE															= 10;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTransitionImpl
	 * <em>State Path Transition</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTransitionImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathTransition()
	 * @generated
	 */
	int						STATE_PATH_TRANSITION													= 11;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceImpl
	 * <em>State Space</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStateSpace()
	 * @generated
	 */
	int						STATE_SPACE																= 12;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl
	 * <em>State Space Transition</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStateSpaceTransition()
	 * @generated
	 */
	int						STATE_SPACE_TRANSITION													= 14;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl
	 * <em>Object Descriptor</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectDescriptor()
	 * @generated
	 */
	int						OBJECT_DESCRIPTOR														= 3;

	/**
	 * The feature id for the '<em><b>Object ID</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_DESCRIPTOR__OBJECT_ID											= 0;

	/**
	 * The feature id for the '<em><b>Class Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_DESCRIPTOR__CLASS_NAME											= 1;

	/**
	 * The feature id for the '<em><b>Instance Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_DESCRIPTOR__INSTANCE_NAME										= 2;

	/**
	 * The feature id for the '<em><b>Class Descriptor</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR										= 3;

	/**
	 * The number of structural features of the '<em>Object Descriptor</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_DESCRIPTOR_FEATURE_COUNT											= 4;

	/**
	 * The number of operations of the '<em>Object Descriptor</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_DESCRIPTOR_OPERATION_COUNT										= 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeImpl
	 * <em>Tree</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getTree()
	 * @generated
	 */
	int						TREE																	= 5;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeNodeImpl
	 * <em>Tree Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeNodeImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getTreeNode()
	 * @generated
	 */
	int						TREE_NODE																= 6;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeTransitionImpl
	 * <em>Tree Transition</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeTransitionImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getTreeTransition()
	 * @generated
	 */
	int						TREE_TRANSITION															= 7;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ModelHistoryImpl
	 * <em>Model History</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ModelHistoryImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getModelHistory()
	 * @generated
	 */
	int						MODEL_HISTORY															= 15;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * <em>Model History Builder</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getModelHistoryBuilder()
	 * @generated
	 */
	int						MODEL_HISTORY_BUILDER													= 16;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * <em>Lifeline Builder</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getLifelineBuilder()
	 * @generated
	 */
	int						LIFELINE_BUILDER														= 17;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryTransitionImpl
	 * <em>Object History Transition</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryTransitionImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryTransition()
	 * @generated
	 */
	int						OBJECT_HISTORY_TRANSITION												= 18;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
	 * <em>Object History Data</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryData()
	 * @generated
	 */
	int						OBJECT_HISTORY_DATA														= 19;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl
	 * <em>Object History Diff</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryDiff()
	 * @generated
	 */
	int						OBJECT_HISTORY_DIFF														= 20;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * <em>Object State Type</em>}' enum. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectStateType()
	 * @generated
	 */
	int						OBJECT_STATE_TYPE														= 25;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineStateImpl
	 * <em>Object Lifeline State</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineStateImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectLifelineState()
	 * @generated
	 */
	int						OBJECT_LIFELINE_STATE													= 4;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE_STATE__STATE											= 0;

	/**
	 * The feature id for the '<em><b>Object Instance</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE_STATE__OBJECT_INSTANCE									= 1;

	/**
	 * The feature id for the '<em><b>Base History Node</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE								= 2;

	/**
	 * The number of structural features of the '<em>Object Lifeline State</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE_STATE_FEATURE_COUNT										= 3;

	/**
	 * The number of operations of the '<em>Object Lifeline State</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_LIFELINE_STATE_OPERATION_COUNT									= 0;

	/**
	 * The feature id for the '<em><b>Root Node</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE__ROOT_NODE															= 0;

	/**
	 * The number of structural features of the '<em>Tree</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_FEATURE_COUNT														= 1;

	/**
	 * The operation id for the '<em>Get All Nodes</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE___GET_ALL_NODES													= 0;

	/**
	 * The operation id for the '<em>Get All Transitions</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE___GET_ALL_TRANSITIONS												= 1;

	/**
	 * The operation id for the '<em>Contains Node Value</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE___CONTAINS_NODE_VALUE__OBJECT										= 2;

	/**
	 * The operation id for the '<em>Contains Transition Value</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE___CONTAINS_TRANSITION_VALUE__OBJECT								= 3;

	/**
	 * The operation id for the '<em>Get Maximum Depth</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE___GET_MAXIMUM_DEPTH												= 4;

	/**
	 * The number of operations of the '<em>Tree</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_OPERATION_COUNT													= 5;

	/**
	 * The feature id for the '<em><b>Child Transitions</b></em>' reference
	 * list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_NODE__CHILD_TRANSITIONS											= 0;

	/**
	 * The feature id for the '<em><b>Parent Transition</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_NODE__PARENT_TRANSITION											= 1;

	/**
	 * The feature id for the '<em><b>Node Value</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_NODE__NODE_VALUE													= 2;

	/**
	 * The number of structural features of the '<em>Tree Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_NODE_FEATURE_COUNT													= 3;

	/**
	 * The operation id for the '<em>Add Child Node</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_NODE___ADD_CHILD_NODE__TREENODE_TREETRANSITION						= 0;

	/**
	 * The number of operations of the '<em>Tree Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_NODE_OPERATION_COUNT												= 1;

	/**
	 * The feature id for the '<em><b>Next Node</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_TRANSITION__NEXT_NODE												= 0;

	/**
	 * The feature id for the '<em><b>Previous Node</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_TRANSITION__PREVIOUS_NODE											= 1;

	/**
	 * The feature id for the '<em><b>Transition Value</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_TRANSITION__TRANSITION_VALUE										= 2;

	/**
	 * The number of structural features of the '<em>Tree Transition</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_TRANSITION_FEATURE_COUNT											= 3;

	/**
	 * The number of operations of the '<em>Tree Transition</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						TREE_TRANSITION_OPERATION_COUNT											= 0;

	/**
	 * The feature id for the '<em><b>Model Snapshot</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE__MODEL_SNAPSHOT													= 0;

	/**
	 * The feature id for the '<em><b>Raw State Data</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE__RAW_STATE_DATA													= 1;

	/**
	 * The feature id for the '<em><b>Incoming Transitions</b></em>' reference
	 * list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE__INCOMING_TRANSITIONS												= 2;

	/**
	 * The feature id for the '<em><b>Outgoing Transitions</b></em>' reference
	 * list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE__OUTGOING_TRANSITIONS												= 3;

	/**
	 * The number of structural features of the '<em>State</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_FEATURE_COUNT														= 4;

	/**
	 * The number of operations of the '<em>State</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_OPERATION_COUNT													= 0;

	/**
	 * The feature id for the '<em><b>Initial State</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH__INITIAL_STATE												= 0;

	/**
	 * The number of structural features of the '<em>State Path</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_FEATURE_COUNT												= 1;

	/**
	 * The number of operations of the '<em>State Path</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_OPERATION_COUNT												= 0;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_NODE__STATE													= 0;

	/**
	 * The feature id for the '<em><b>Next Transition</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_NODE__NEXT_TRANSITION										= 1;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_NODE__INCOMING_TRANSITION									= 2;

	/**
	 * The number of structural features of the '<em>State Path Node</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_NODE_FEATURE_COUNT											= 3;

	/**
	 * The number of operations of the '<em>State Path Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_NODE_OPERATION_COUNT											= 0;

	/**
	 * The feature id for the '<em><b>Next State Path Node</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE								= 0;

	/**
	 * The feature id for the '<em><b>Transition Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TRANSITION__TRANSITION_NAME									= 1;

	/**
	 * The feature id for the '<em><b>Previous State Path Node</b></em>'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE							= 2;

	/**
	 * The number of structural features of the '<em>State Path Transition</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TRANSITION_FEATURE_COUNT										= 3;

	/**
	 * The number of operations of the '<em>State Path Transition</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TRANSITION_OPERATION_COUNT									= 0;

	/**
	 * The feature id for the '<em><b>Initial State</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE__INITIAL_STATE												= 0;

	/**
	 * The number of structural features of the '<em>State Space</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_FEATURE_COUNT												= 1;

	/**
	 * The operation id for the '<em>Get All State Space Nodes</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE___GET_ALL_STATE_SPACE_NODES									= 0;

	/**
	 * The operation id for the '<em>Get All State Space Transitions</em>'
	 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE___GET_ALL_STATE_SPACE_TRANSITIONS							= 1;

	/**
	 * The number of operations of the '<em>State Space</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_OPERATION_COUNT												= 2;

	/**
	 * The feature id for the '<em><b>Lifeline Groups</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS__LIFELINE_GROUPS									= 0;

	/**
	 * The feature id for the '<em><b>State Path Tree</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS__STATE_PATH_TREE									= 1;

	/**
	 * The feature id for the '<em><b>Statespace</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS__STATESPACE										= 2;

	/**
	 * The feature id for the '<em><b>Model History</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS__MODEL_HISTORY										= 3;

	/**
	 * The feature id for the '<em><b>Model History Builder</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER								= 4;

	/**
	 * The feature id for the '<em><b>Lifeline Builders</b></em>' reference
	 * list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS									= 5;

	/**
	 * The number of structural features of the '<em>State Space Path Vis</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS_FEATURE_COUNT										= 6;

	/**
	 * The operation id for the '<em>Update Model History</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS___UPDATE_MODEL_HISTORY								= 0;

	/**
	 * The operation id for the '<em>Update Lifelines</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS___UPDATE_LIFELINES									= 1;

	/**
	 * The number of operations of the '<em>State Space Path Vis</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_PATH_VIS_OPERATION_COUNT									= 2;

	/**
	 * The feature id for the '<em><b>Next State</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_TRANSITION__NEXT_STATE										= 0;

	/**
	 * The feature id for the '<em><b>Transition Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_TRANSITION__TRANSITION_NAME									= 1;

	/**
	 * The feature id for the '<em><b>Prev State</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_TRANSITION__PREV_STATE										= 2;

	/**
	 * The feature id for the '<em><b>Raw Transition Data</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA								= 3;

	/**
	 * The number of structural features of the '<em>State Space Transition</em>
	 * ' class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_TRANSITION_FEATURE_COUNT									= 4;

	/**
	 * The number of operations of the '<em>State Space Transition</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_SPACE_TRANSITION_OPERATION_COUNT									= 0;

	/**
	 * The feature id for the '<em><b>Object History Trees</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY__OBJECT_HISTORY_TREES										= 0;

	/**
	 * The number of structural features of the '<em>Model History</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY_FEATURE_COUNT												= 1;

	/**
	 * The operation id for the '<em>Update</em>' operation. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY___UPDATE__MODELHISTORYBUILDER_TREE						= 0;

	/**
	 * The operation id for the '<em>Get Object Descriptor</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY___GET_OBJECT_DESCRIPTOR__OBJECT							= 1;

	/**
	 * The operation id for the '<em>Create Object Descriptor</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY___CREATE_OBJECT_DESCRIPTOR__OBJECT						= 2;

	/**
	 * The operation id for the '<em>Get Object History</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY___GET_OBJECT_HISTORY__OBJECTDESCRIPTOR					= 3;

	/**
	 * The operation id for the '<em>Clear</em>' operation. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY___CLEAR													= 4;

	/**
	 * The operation id for the '<em>Register Object To Descriptor</em>'
	 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY___REGISTER_OBJECT_TO_DESCRIPTOR__OBJECTDESCRIPTOR_OBJECT	= 5;

	/**
	 * The number of operations of the '<em>Model History</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY_OPERATION_COUNT											= 6;

	/**
	 * The number of structural features of the '<em>Model History Builder</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY_BUILDER_FEATURE_COUNT										= 0;

	/**
	 * The operation id for the '<em>Update Object History</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY_BUILDER___UPDATE_OBJECT_HISTORY__TREE_MODELHISTORY		= 0;

	/**
	 * The number of operations of the '<em>Model History Builder</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						MODEL_HISTORY_BUILDER_OPERATION_COUNT									= 1;

	/**
	 * The number of structural features of the '<em>Lifeline Builder</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_BUILDER_FEATURE_COUNT											= 0;

	/**
	 * The operation id for the '<em>Build Lifelines</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY					= 0;

	/**
	 * The number of operations of the '<em>Lifeline Builder</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						LIFELINE_BUILDER_OPERATION_COUNT										= 1;

	/**
	 * The feature id for the '<em><b>History Data</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_TRANSITION__HISTORY_DATA									= 0;

	/**
	 * The feature id for the '<em><b>State Transition</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_TRANSITION__STATE_TRANSITION								= 1;

	/**
	 * The number of structural features of the '
	 * <em>Object History Transition</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_TRANSITION_FEATURE_COUNT									= 2;

	/**
	 * The number of operations of the '<em>Object History Transition</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_TRANSITION_OPERATION_COUNT								= 0;

	/**
	 * The number of structural features of the '<em>Object History Data</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DATA_FEATURE_COUNT										= 0;

	/**
	 * The number of operations of the '<em>Object History Data</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DATA_OPERATION_COUNT										= 0;

	/**
	 * The feature id for the '<em><b>New State</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DIFF__NEW_STATE											= OBJECT_HISTORY_DATA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>New Object Instance</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE								= OBJECT_HISTORY_DATA_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Old Object Instance</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE								= OBJECT_HISTORY_DATA_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Raw Diff Data</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DIFF__RAW_DIFF_DATA										= OBJECT_HISTORY_DATA_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Object History Diff</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DIFF_FEATURE_COUNT										= OBJECT_HISTORY_DATA_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Object History Diff</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_DIFF_OPERATION_COUNT										= OBJECT_HISTORY_DATA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl
	 * <em>Object History Node</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryNode()
	 * @generated
	 */
	int						OBJECT_HISTORY_NODE														= 21;

	/**
	 * The feature id for the '<em><b>History Data</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_NODE__HISTORY_DATA										= 0;

	/**
	 * The feature id for the '<em><b>Object Descriptor</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR									= 1;

	/**
	 * The feature id for the '<em><b>Object Instance</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_NODE__OBJECT_INSTANCE									= 2;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_NODE__STATE												= 3;

	/**
	 * The number of structural features of the '<em>Object History Node</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_NODE_FEATURE_COUNT										= 4;

	/**
	 * The number of operations of the '<em>Object History Node</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						OBJECT_HISTORY_NODE_OPERATION_COUNT										= 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
	 * <em>State Path Reference</em>}' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathReference()
	 * @generated
	 */
	int						STATE_PATH_REFERENCE													= 23;

	/**
	 * The number of structural features of the '<em>State Path Reference</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_REFERENCE_FEATURE_COUNT										= OBJECT_HISTORY_DATA_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>State Path Reference</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_REFERENCE_OPERATION_COUNT									= OBJECT_HISTORY_DATA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeNodeReferenceImpl
	 * <em>State Path Tree Node Reference</em>}' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeNodeReferenceImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathTreeNodeReference()
	 * @generated
	 */
	int						STATE_PATH_TREE_NODE_REFERENCE											= 22;

	/**
	 * The feature id for the '<em><b>State Path Tree Node</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE					= STATE_PATH_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '
	 * <em>State Path Tree Node Reference</em>' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TREE_NODE_REFERENCE_FEATURE_COUNT							= STATE_PATH_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>State Path Tree Node Reference</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TREE_NODE_REFERENCE_OPERATION_COUNT							= STATE_PATH_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeTransitionReferenceImpl
	 * <em>State Path Tree Transition Reference</em>}' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeTransitionReferenceImpl
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathTreeTransitionReference()
	 * @generated
	 */
	int						STATE_PATH_TREE_TRANSITION_REFERENCE									= 24;

	/**
	 * The feature id for the '<em><b>State Path Tree Transition</b></em>'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION		= STATE_PATH_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '
	 * <em>State Path Tree Transition Reference</em>' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TREE_TRANSITION_REFERENCE_FEATURE_COUNT						= STATE_PATH_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '
	 * <em>State Path Tree Transition Reference</em>' class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int						STATE_PATH_TREE_TRANSITION_REFERENCE_OPERATION_COUNT					= STATE_PATH_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup
	 * <em>Lifeline Group</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Lifeline Group</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup
	 * @generated
	 */
	EClass getLifelineGroup();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup#getLifelines
	 * <em>Lifelines</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Lifelines</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup#getLifelines()
	 * @see #getLifelineGroup()
	 * @generated
	 */
	EReference getLifelineGroup_Lifelines();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup#getName
	 * <em>Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup#getName()
	 * @see #getLifelineGroup()
	 * @generated
	 */
	EAttribute getLifelineGroup_Name();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline
	 * <em>Lifeline</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Lifeline</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline
	 * @generated
	 */
	EClass getLifeline();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getParentLifeline
	 * <em>Parent Lifeline</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Parent Lifeline</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getParentLifeline()
	 * @see #getLifeline()
	 * @generated
	 */
	EReference getLifeline_ParentLifeline();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getChildLifelines
	 * <em>Child Lifelines</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Child Lifelines</em>
	 *         '.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#getChildLifelines()
	 * @see #getLifeline()
	 * @generated
	 */
	EReference getLifeline_ChildLifelines();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isVisible
	 * <em>Visible</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Visible</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isVisible()
	 * @see #getLifeline()
	 * @generated
	 */
	EAttribute getLifeline_Visible();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isCollapsed
	 * <em>Collapsed</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Collapsed</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline#isCollapsed()
	 * @see #getLifeline()
	 * @generated
	 */
	EAttribute getLifeline_Collapsed();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis
	 * <em>State Space Path Vis</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State Space Path Vis</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis
	 * @generated
	 */
	EClass getStateSpacePathVis();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getLifelineGroups
	 * <em>Lifeline Groups</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>Lifeline Groups</em>
	 *         '.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getLifelineGroups()
	 * @see #getStateSpacePathVis()
	 * @generated
	 */
	EReference getStateSpacePathVis_LifelineGroups();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatePathTree
	 * <em>State Path Tree</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>State Path Tree</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatePathTree()
	 * @see #getStateSpacePathVis()
	 * @generated
	 */
	EReference getStateSpacePathVis_StatePathTree();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatespace
	 * <em>Statespace</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Statespace</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatespace()
	 * @see #getStateSpacePathVis()
	 * @generated
	 */
	EReference getStateSpacePathVis_Statespace();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistory
	 * <em>Model History</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Model History</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistory()
	 * @see #getStateSpacePathVis()
	 * @generated
	 */
	EReference getStateSpacePathVis_ModelHistory();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistoryBuilder
	 * <em>Model History Builder</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Model History Builder</em>
	 *         '.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistoryBuilder()
	 * @see #getStateSpacePathVis()
	 * @generated
	 */
	EReference getStateSpacePathVis_ModelHistoryBuilder();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getLifelineBuilders
	 * <em>Lifeline Builders</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference list '
	 *         <em>Lifeline Builders</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getLifelineBuilders()
	 * @see #getStateSpacePathVis()
	 * @generated
	 */
	EReference getStateSpacePathVis_LifelineBuilders();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#updateModelHistory()
	 * <em>Update Model History</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Update Model History</em>'
	 *         operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#updateModelHistory()
	 * @generated
	 */
	EOperation getStateSpacePathVis__UpdateModelHistory();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#updateLifelines()
	 * <em>Update Lifelines</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Update Lifelines</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#updateLifelines()
	 * @generated
	 */
	EOperation getStateSpacePathVis__UpdateLifelines();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline
	 * <em>Object Lifeline</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Object Lifeline</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline
	 * @generated
	 */
	EClass getObjectLifeline();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectDescriptor
	 * <em>Object Descriptor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Object Descriptor</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectDescriptor()
	 * @see #getObjectLifeline()
	 * @generated
	 */
	EReference getObjectLifeline_ObjectDescriptor();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectStateTree
	 * <em>Object State Tree</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Object State Tree</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectStateTree()
	 * @see #getObjectLifeline()
	 * @generated
	 */
	EReference getObjectLifeline_ObjectStateTree();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath
	 * <em>State Path</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State Path</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath
	 * @generated
	 */
	EClass getStatePath();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath#getInitialState
	 * <em>Initial State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Initial State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath#getInitialState()
	 * @see #getStatePath()
	 * @generated
	 */
	EReference getStatePath_InitialState();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State
	 * <em>State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getModelSnapshot
	 * <em>Model Snapshot</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Model Snapshot</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getModelSnapshot()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_ModelSnapshot();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getRawStateData
	 * <em>Raw State Data</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Raw State Data</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getRawStateData()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_RawStateData();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getIncomingTransitions
	 * <em>Incoming Transitions</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the reference list '
	 *         <em>Incoming Transitions</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getIncomingTransitions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_IncomingTransitions();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getOutgoingTransitions
	 * <em>Outgoing Transitions</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the reference list '
	 *         <em>Outgoing Transitions</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.State#getOutgoingTransitions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_OutgoingTransitions();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode
	 * <em>State Path Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State Path Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode
	 * @generated
	 */
	EClass getStatePathNode();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode#getState
	 * <em>State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode#getState()
	 * @see #getStatePathNode()
	 * @generated
	 */
	EReference getStatePathNode_State();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode#getNextTransition
	 * <em>Next Transition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Next Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode#getNextTransition()
	 * @see #getStatePathNode()
	 * @generated
	 */
	EReference getStatePathNode_NextTransition();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode#getIncomingTransition
	 * <em>Incoming Transition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Incoming Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode#getIncomingTransition()
	 * @see #getStatePathNode()
	 * @generated
	 */
	EReference getStatePathNode_IncomingTransition();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition
	 * <em>State Path Transition</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State Path Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition
	 * @generated
	 */
	EClass getStatePathTransition();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getNextStatePathNode
	 * <em>Next State Path Node</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Next State Path Node</em>
	 *         '.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getNextStatePathNode()
	 * @see #getStatePathTransition()
	 * @generated
	 */
	EReference getStatePathTransition_NextStatePathNode();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getTransitionName
	 * <em>Transition Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Transition Name</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getTransitionName()
	 * @see #getStatePathTransition()
	 * @generated
	 */
	EAttribute getStatePathTransition_TransitionName();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getPreviousStatePathNode
	 * <em>Previous State Path Node</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the reference '
	 *         <em>Previous State Path Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition#getPreviousStatePathNode()
	 * @see #getStatePathTransition()
	 * @generated
	 */
	EReference getStatePathTransition_PreviousStatePathNode();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace
	 * <em>State Space</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State Space</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace
	 * @generated
	 */
	EClass getStateSpace();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getInitialState
	 * <em>Initial State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Initial State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getInitialState()
	 * @see #getStateSpace()
	 * @generated
	 */
	EReference getStateSpace_InitialState();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceNodes()
	 * <em>Get All State Space Nodes</em>}' operation. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get All State Space Nodes</em>'
	 *         operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceNodes()
	 * @generated
	 */
	EOperation getStateSpace__GetAllStateSpaceNodes();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceTransitions()
	 * <em>Get All State Space Transitions</em>}' operation. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get All State Space Transitions</em>
	 *         ' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceTransitions()
	 * @generated
	 */
	EOperation getStateSpace__GetAllStateSpaceTransitions();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition
	 * <em>State Space Transition</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State Space Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition
	 * @generated
	 */
	EClass getStateSpaceTransition();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState
	 * <em>Next State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Next State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState()
	 * @see #getStateSpaceTransition()
	 * @generated
	 */
	EReference getStateSpaceTransition_NextState();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getTransitionName
	 * <em>Transition Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Transition Name</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getTransitionName()
	 * @see #getStateSpaceTransition()
	 * @generated
	 */
	EAttribute getStateSpaceTransition_TransitionName();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState
	 * <em>Prev State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Prev State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState()
	 * @see #getStateSpaceTransition()
	 * @generated
	 */
	EReference getStateSpaceTransition_PrevState();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getRawTransitionData
	 * <em>Raw Transition Data</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Raw Transition Data</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getRawTransitionData()
	 * @see #getStateSpaceTransition()
	 * @generated
	 */
	EAttribute getStateSpaceTransition_RawTransitionData();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory
	 * <em>Model History</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Model History</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory
	 * @generated
	 */
	EClass getModelHistory();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistoryTrees
	 * <em>Object History Trees</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Object History Trees</em>
	 *         '.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistoryTrees()
	 * @see #getModelHistory()
	 * @generated
	 */
	EAttribute getModelHistory_ObjectHistoryTrees();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#update(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder, at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree)
	 * <em>Update</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#update(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree)
	 * @generated
	 */
	EOperation getModelHistory__Update__ModelHistoryBuilder_Tree();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectDescriptor(java.lang.Object)
	 * <em>Get Object Descriptor</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get Object Descriptor</em>'
	 *         operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectDescriptor(java.lang.Object)
	 * @generated
	 */
	EOperation getModelHistory__GetObjectDescriptor__Object();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#createObjectDescriptor(java.lang.Object)
	 * <em>Create Object Descriptor</em>}' operation. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Create Object Descriptor</em>'
	 *         operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#createObjectDescriptor(java.lang.Object)
	 * @generated
	 */
	EOperation getModelHistory__CreateObjectDescriptor__Object();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor)
	 * <em>Get Object History</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get Object History</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor)
	 * @generated
	 */
	EOperation getModelHistory__GetObjectHistory__ObjectDescriptor();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#clear()
	 * <em>Clear</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Clear</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#clear()
	 * @generated
	 */
	EOperation getModelHistory__Clear();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#registerObjectToDescriptor(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor, java.lang.Object)
	 * <em>Register Object To Descriptor</em>}' operation. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Register Object To Descriptor</em>'
	 *         operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#registerObjectToDescriptor(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor,
	 *      java.lang.Object)
	 * @generated
	 */
	EOperation getModelHistory__RegisterObjectToDescriptor__ObjectDescriptor_Object();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * <em>Model History Builder</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Model History Builder</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * @generated
	 */
	EClass getModelHistoryBuilder();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * <em>Update Object History</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Update Object History</em>'
	 *         operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * @generated
	 */
	EOperation getModelHistoryBuilder__UpdateObjectHistory__Tree_ModelHistory();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * <em>Lifeline Builder</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Lifeline Builder</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * @generated
	 */
	EClass getLifelineBuilder();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * <em>Build Lifelines</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Build Lifelines</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * @generated
	 */
	EOperation getLifelineBuilder__BuildLifelines__Tree_ModelHistory();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition
	 * <em>Object History Transition</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Object History Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition
	 * @generated
	 */
	EClass getObjectHistoryTransition();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition#getHistoryData
	 * <em>History Data</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>History Data</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition#getHistoryData()
	 * @see #getObjectHistoryTransition()
	 * @generated
	 */
	EReference getObjectHistoryTransition_HistoryData();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition#getStateTransition
	 * <em>State Transition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>State Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition#getStateTransition()
	 * @see #getObjectHistoryTransition()
	 * @generated
	 */
	EReference getObjectHistoryTransition_StateTransition();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
	 * <em>Object History Data</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Object History Data</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
	 * @generated
	 */
	EClass getObjectHistoryData();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff
	 * <em>Object History Diff</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Object History Diff</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff
	 * @generated
	 */
	EClass getObjectHistoryDiff();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewState
	 * <em>New State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>New State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewState()
	 * @see #getObjectHistoryDiff()
	 * @generated
	 */
	EAttribute getObjectHistoryDiff_NewState();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewObjectInstance
	 * <em>New Object Instance</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>New Object Instance</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getNewObjectInstance()
	 * @see #getObjectHistoryDiff()
	 * @generated
	 */
	EAttribute getObjectHistoryDiff_NewObjectInstance();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getOldObjectInstance
	 * <em>Old Object Instance</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Old Object Instance</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getOldObjectInstance()
	 * @see #getObjectHistoryDiff()
	 * @generated
	 */
	EAttribute getObjectHistoryDiff_OldObjectInstance();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getRawDiffData
	 * <em>Raw Diff Data</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Raw Diff Data</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff#getRawDiffData()
	 * @see #getObjectHistoryDiff()
	 * @generated
	 */
	EAttribute getObjectHistoryDiff_RawDiffData();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode
	 * <em>Object History Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Object History Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode
	 * @generated
	 */
	EClass getObjectHistoryNode();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getHistoryData
	 * <em>History Data</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference list '<em>History Data</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getHistoryData()
	 * @see #getObjectHistoryNode()
	 * @generated
	 */
	EReference getObjectHistoryNode_HistoryData();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectDescriptor
	 * <em>Object Descriptor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Object Descriptor</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectDescriptor()
	 * @see #getObjectHistoryNode()
	 * @generated
	 */
	EReference getObjectHistoryNode_ObjectDescriptor();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectInstance
	 * <em>Object Instance</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Object Instance</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getObjectInstance()
	 * @see #getObjectHistoryNode()
	 * @generated
	 */
	EAttribute getObjectHistoryNode_ObjectInstance();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getState
	 * <em>State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode#getState()
	 * @see #getObjectHistoryNode()
	 * @generated
	 */
	EReference getObjectHistoryNode_State();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference
	 * <em>State Path Tree Node Reference</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '
	 *         <em>State Path Tree Node Reference</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference
	 * @generated
	 */
	EClass getStatePathTreeNodeReference();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference#getStatePathTreeNode
	 * <em>State Path Tree Node</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>State Path Tree Node</em>
	 *         '.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference#getStatePathTreeNode()
	 * @see #getStatePathTreeNodeReference()
	 * @generated
	 */
	EReference getStatePathTreeNodeReference_StatePathTreeNode();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
	 * <em>State Path Reference</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>State Path Reference</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
	 * @generated
	 */
	EClass getStatePathReference();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference
	 * <em>State Path Tree Transition Reference</em>}'. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '
	 *         <em>State Path Tree Transition Reference</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference
	 * @generated
	 */
	EClass getStatePathTreeTransitionReference();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference#getStatePathTreeTransition
	 * <em>State Path Tree Transition</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the reference '
	 *         <em>State Path Tree Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference#getStatePathTreeTransition()
	 * @see #getStatePathTreeTransitionReference()
	 * @generated
	 */
	EReference getStatePathTreeTransitionReference_StatePathTreeTransition();

	/**
	 * Returns the meta object for enum '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * <em>Object State Type</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for enum '<em>Object State Type</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * @generated
	 */
	EEnum getObjectStateType();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor
	 * <em>Object Descriptor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>Object Descriptor</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor
	 * @generated
	 */
	EClass getObjectDescriptor();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getClassName
	 * <em>Class Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Class Name</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getClassName()
	 * @see #getObjectDescriptor()
	 * @generated
	 */
	EAttribute getObjectDescriptor_ClassName();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getInstanceName
	 * <em>Instance Name</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Instance Name</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getInstanceName()
	 * @see #getObjectDescriptor()
	 * @generated
	 */
	EAttribute getObjectDescriptor_InstanceName();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getClassDescriptor
	 * <em>Class Descriptor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Class Descriptor</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getClassDescriptor()
	 * @see #getObjectDescriptor()
	 * @generated
	 */
	EAttribute getObjectDescriptor_ClassDescriptor();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree
	 * <em>Tree</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Tree</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree
	 * @generated
	 */
	EClass getTree();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getRootNode
	 * <em>Root Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Root Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getRootNode()
	 * @see #getTree()
	 * @generated
	 */
	EReference getTree_RootNode();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllNodes()
	 * <em>Get All Nodes</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get All Nodes</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllNodes()
	 * @generated
	 */
	EOperation getTree__GetAllNodes();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllTransitions()
	 * <em>Get All Transitions</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get All Transitions</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllTransitions()
	 * @generated
	 */
	EOperation getTree__GetAllTransitions();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsNodeValue(java.lang.Object)
	 * <em>Contains Node Value</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Contains Node Value</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsNodeValue(java.lang.Object)
	 * @generated
	 */
	EOperation getTree__ContainsNodeValue__Object();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsTransitionValue(java.lang.Object)
	 * <em>Contains Transition Value</em>}' operation. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Contains Transition Value</em>'
	 *         operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsTransitionValue(java.lang.Object)
	 * @generated
	 */
	EOperation getTree__ContainsTransitionValue__Object();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getMaximumDepth()
	 * <em>Get Maximum Depth</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Get Maximum Depth</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getMaximumDepth()
	 * @generated
	 */
	EOperation getTree__GetMaximumDepth();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode
	 * <em>Tree Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Tree Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode
	 * @generated
	 */
	EClass getTreeNode();

	/**
	 * Returns the meta object for the reference list '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getChildTransitions
	 * <em>Child Transitions</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference list '
	 *         <em>Child Transitions</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getChildTransitions()
	 * @see #getTreeNode()
	 * @generated
	 */
	EReference getTreeNode_ChildTransitions();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getParentTransition
	 * <em>Parent Transition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Parent Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getParentTransition()
	 * @see #getTreeNode()
	 * @generated
	 */
	EReference getTreeNode_ParentTransition();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getNodeValue
	 * <em>Node Value</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Node Value</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getNodeValue()
	 * @see #getTreeNode()
	 * @generated
	 */
	EAttribute getTreeNode_NodeValue();

	/**
	 * Returns the meta object for the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#addChildNode(at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode, at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition)
	 * <em>Add Child Node</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for the '<em>Add Child Node</em>' operation.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#addChildNode(at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition)
	 * @generated
	 */
	EOperation getTreeNode__AddChildNode__TreeNode_TreeTransition();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition
	 * <em>Tree Transition</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Tree Transition</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition
	 * @generated
	 */
	EClass getTreeTransition();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getNextNode
	 * <em>Next Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Next Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getNextNode()
	 * @see #getTreeTransition()
	 * @generated
	 */
	EReference getTreeTransition_NextNode();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getPreviousNode
	 * <em>Previous Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the reference '<em>Previous Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getPreviousNode()
	 * @see #getTreeTransition()
	 * @generated
	 */
	EReference getTreeTransition_PreviousNode();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getTransitionValue
	 * <em>Transition Value</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Transition Value</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getTransitionValue()
	 * @see #getTreeTransition()
	 * @generated
	 */
	EAttribute getTreeTransition_TransitionValue();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState
	 * <em>Object Lifeline State</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>Object Lifeline State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState
	 * @generated
	 */
	EClass getObjectLifelineState();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getState
	 * <em>State</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getState()
	 * @see #getObjectLifelineState()
	 * @generated
	 */
	EAttribute getObjectLifelineState_State();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getObjectInstance
	 * <em>Object Instance</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Object Instance</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getObjectInstance()
	 * @see #getObjectLifelineState()
	 * @generated
	 */
	EAttribute getObjectLifelineState_ObjectInstance();

	/**
	 * Returns the meta object for the reference '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getBaseHistoryNode
	 * <em>Base History Node</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Base History Node</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getBaseHistoryNode()
	 * @see #getObjectLifelineState()
	 * @generated
	 */
	EReference getObjectLifelineState_BaseHistoryNode();

	/**
	 * Returns the meta object for the attribute '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getObjectID
	 * <em>Object ID</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for the attribute '<em>Object ID</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor#getObjectID()
	 * @see #getObjectDescriptor()
	 * @generated
	 */
	EAttribute getObjectDescriptor_ObjectID();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatespacevisFactory getStatespacevisFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each operation of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineGroupImpl
		 * <em>Lifeline Group</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.LifelineGroupImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getLifelineGroup()
		 * @generated
		 */
		EClass		LIFELINE_GROUP															= eINSTANCE.getLifelineGroup();

		/**
		 * The meta object literal for the '<em><b>Lifelines</b></em>' reference
		 * list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	LIFELINE_GROUP__LIFELINES												= eINSTANCE.getLifelineGroup_Lifelines();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	LIFELINE_GROUP__NAME													= eINSTANCE.getLifelineGroup_Name();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline
		 * <em>Lifeline</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getLifeline()
		 * @generated
		 */
		EClass		LIFELINE																= eINSTANCE.getLifeline();

		/**
		 * The meta object literal for the '<em><b>Parent Lifeline</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	LIFELINE__PARENT_LIFELINE												= eINSTANCE.getLifeline_ParentLifeline();

		/**
		 * The meta object literal for the '<em><b>Child Lifelines</b></em>'
		 * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	LIFELINE__CHILD_LIFELINES												= eINSTANCE.getLifeline_ChildLifelines();

		/**
		 * The meta object literal for the '<em><b>Visible</b></em>' attribute
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	LIFELINE__VISIBLE														= eINSTANCE.getLifeline_Visible();

		/**
		 * The meta object literal for the '<em><b>Collapsed</b></em>' attribute
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	LIFELINE__COLLAPSED														= eINSTANCE.getLifeline_Collapsed();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl
		 * <em>State Space Path Vis</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStateSpacePathVis()
		 * @generated
		 */
		EClass		STATE_SPACE_PATH_VIS													= eINSTANCE.getStateSpacePathVis();

		/**
		 * The meta object literal for the '<em><b>Lifeline Groups</b></em>'
		 * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_PATH_VIS__LIFELINE_GROUPS									= eINSTANCE.getStateSpacePathVis_LifelineGroups();

		/**
		 * The meta object literal for the '<em><b>State Path Tree</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_PATH_VIS__STATE_PATH_TREE									= eINSTANCE.getStateSpacePathVis_StatePathTree();

		/**
		 * The meta object literal for the '<em><b>Statespace</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_PATH_VIS__STATESPACE										= eINSTANCE.getStateSpacePathVis_Statespace();

		/**
		 * The meta object literal for the '<em><b>Model History</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_PATH_VIS__MODEL_HISTORY										= eINSTANCE.getStateSpacePathVis_ModelHistory();

		/**
		 * The meta object literal for the '
		 * <em><b>Model History Builder</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER								= eINSTANCE.getStateSpacePathVis_ModelHistoryBuilder();

		/**
		 * The meta object literal for the '<em><b>Lifeline Builders</b></em>'
		 * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS									= eINSTANCE.getStateSpacePathVis_LifelineBuilders();

		/**
		 * The meta object literal for the '<em><b>Update Model History</b></em>
		 * ' operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	STATE_SPACE_PATH_VIS___UPDATE_MODEL_HISTORY								= eINSTANCE.getStateSpacePathVis__UpdateModelHistory();

		/**
		 * The meta object literal for the '<em><b>Update Lifelines</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	STATE_SPACE_PATH_VIS___UPDATE_LIFELINES									= eINSTANCE.getStateSpacePathVis__UpdateLifelines();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineImpl
		 * <em>Object Lifeline</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectLifeline()
		 * @generated
		 */
		EClass		OBJECT_LIFELINE															= eINSTANCE.getObjectLifeline();

		/**
		 * The meta object literal for the '<em><b>Object Descriptor</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_LIFELINE__OBJECT_DESCRIPTOR										= eINSTANCE.getObjectLifeline_ObjectDescriptor();

		/**
		 * The meta object literal for the '<em><b>Object State Tree</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_LIFELINE__OBJECT_STATE_TREE										= eINSTANCE.getObjectLifeline_ObjectStateTree();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathImpl
		 * <em>State Path</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePath()
		 * @generated
		 */
		EClass		STATE_PATH																= eINSTANCE.getStatePath();

		/**
		 * The meta object literal for the '<em><b>Initial State</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH__INITIAL_STATE												= eINSTANCE.getStatePath_InitialState();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl
		 * <em>State</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getState()
		 * @generated
		 */
		EClass		STATE																	= eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Model Snapshot</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATE__MODEL_SNAPSHOT													= eINSTANCE.getState_ModelSnapshot();

		/**
		 * The meta object literal for the '<em><b>Raw State Data</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATE__RAW_STATE_DATA													= eINSTANCE.getState_RawStateData();

		/**
		 * The meta object literal for the '<em><b>Incoming Transitions</b></em>
		 * ' reference list feature. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @generated
		 */
		EReference	STATE__INCOMING_TRANSITIONS												= eINSTANCE.getState_IncomingTransitions();

		/**
		 * The meta object literal for the '<em><b>Outgoing Transitions</b></em>
		 * ' reference list feature. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @generated
		 */
		EReference	STATE__OUTGOING_TRANSITIONS												= eINSTANCE.getState_OutgoingTransitions();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathNodeImpl
		 * <em>State Path Node</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathNodeImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathNode()
		 * @generated
		 */
		EClass		STATE_PATH_NODE															= eINSTANCE.getStatePathNode();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH_NODE__STATE													= eINSTANCE.getStatePathNode_State();

		/**
		 * The meta object literal for the '<em><b>Next Transition</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH_NODE__NEXT_TRANSITION										= eINSTANCE.getStatePathNode_NextTransition();

		/**
		 * The meta object literal for the '<em><b>Incoming Transition</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH_NODE__INCOMING_TRANSITION									= eINSTANCE.getStatePathNode_IncomingTransition();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTransitionImpl
		 * <em>State Path Transition</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTransitionImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathTransition()
		 * @generated
		 */
		EClass		STATE_PATH_TRANSITION													= eINSTANCE.getStatePathTransition();

		/**
		 * The meta object literal for the '<em><b>Next State Path Node</b></em>
		 * ' reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE								= eINSTANCE.getStatePathTransition_NextStatePathNode();

		/**
		 * The meta object literal for the '<em><b>Transition Name</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATE_PATH_TRANSITION__TRANSITION_NAME									= eINSTANCE.getStatePathTransition_TransitionName();

		/**
		 * The meta object literal for the '
		 * <em><b>Previous State Path Node</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE							= eINSTANCE.getStatePathTransition_PreviousStatePathNode();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceImpl
		 * <em>State Space</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStateSpace()
		 * @generated
		 */
		EClass		STATE_SPACE																= eINSTANCE.getStateSpace();

		/**
		 * The meta object literal for the '<em><b>Initial State</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE__INITIAL_STATE												= eINSTANCE.getStateSpace_InitialState();

		/**
		 * The meta object literal for the '
		 * <em><b>Get All State Space Nodes</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	STATE_SPACE___GET_ALL_STATE_SPACE_NODES									= eINSTANCE.getStateSpace__GetAllStateSpaceNodes();

		/**
		 * The meta object literal for the '
		 * <em><b>Get All State Space Transitions</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	STATE_SPACE___GET_ALL_STATE_SPACE_TRANSITIONS							= eINSTANCE.getStateSpace__GetAllStateSpaceTransitions();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl
		 * <em>State Space Transition</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpaceTransitionImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStateSpaceTransition()
		 * @generated
		 */
		EClass		STATE_SPACE_TRANSITION													= eINSTANCE.getStateSpaceTransition();

		/**
		 * The meta object literal for the '<em><b>Next State</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_TRANSITION__NEXT_STATE										= eINSTANCE.getStateSpaceTransition_NextState();

		/**
		 * The meta object literal for the '<em><b>Transition Name</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATE_SPACE_TRANSITION__TRANSITION_NAME									= eINSTANCE.getStateSpaceTransition_TransitionName();

		/**
		 * The meta object literal for the '<em><b>Prev State</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_SPACE_TRANSITION__PREV_STATE										= eINSTANCE.getStateSpaceTransition_PrevState();

		/**
		 * The meta object literal for the '<em><b>Raw Transition Data</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA								= eINSTANCE.getStateSpaceTransition_RawTransitionData();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ModelHistoryImpl
		 * <em>Model History</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ModelHistoryImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getModelHistory()
		 * @generated
		 */
		EClass		MODEL_HISTORY															= eINSTANCE.getModelHistory();

		/**
		 * The meta object literal for the '<em><b>Object History Trees</b></em>
		 * ' attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	MODEL_HISTORY__OBJECT_HISTORY_TREES										= eINSTANCE.getModelHistory_ObjectHistoryTrees();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	MODEL_HISTORY___UPDATE__MODELHISTORYBUILDER_TREE						= eINSTANCE.getModelHistory__Update__ModelHistoryBuilder_Tree();

		/**
		 * The meta object literal for the '
		 * <em><b>Get Object Descriptor</b></em>' operation. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	MODEL_HISTORY___GET_OBJECT_DESCRIPTOR__OBJECT							= eINSTANCE.getModelHistory__GetObjectDescriptor__Object();

		/**
		 * The meta object literal for the '
		 * <em><b>Create Object Descriptor</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	MODEL_HISTORY___CREATE_OBJECT_DESCRIPTOR__OBJECT						= eINSTANCE.getModelHistory__CreateObjectDescriptor__Object();

		/**
		 * The meta object literal for the '<em><b>Get Object History</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	MODEL_HISTORY___GET_OBJECT_HISTORY__OBJECTDESCRIPTOR					= eINSTANCE.getModelHistory__GetObjectHistory__ObjectDescriptor();

		/**
		 * The meta object literal for the '<em><b>Clear</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	MODEL_HISTORY___CLEAR													= eINSTANCE.getModelHistory__Clear();

		/**
		 * The meta object literal for the '
		 * <em><b>Register Object To Descriptor</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	MODEL_HISTORY___REGISTER_OBJECT_TO_DESCRIPTOR__OBJECTDESCRIPTOR_OBJECT	= eINSTANCE.getModelHistory__RegisterObjectToDescriptor__ObjectDescriptor_Object();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
		 * <em>Model History Builder</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getModelHistoryBuilder()
		 * @generated
		 */
		EClass		MODEL_HISTORY_BUILDER													= eINSTANCE.getModelHistoryBuilder();

		/**
		 * The meta object literal for the '
		 * <em><b>Update Object History</b></em>' operation. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	MODEL_HISTORY_BUILDER___UPDATE_OBJECT_HISTORY__TREE_MODELHISTORY		= eINSTANCE.getModelHistoryBuilder__UpdateObjectHistory__Tree_ModelHistory();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
		 * <em>Lifeline Builder</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getLifelineBuilder()
		 * @generated
		 */
		EClass		LIFELINE_BUILDER														= eINSTANCE.getLifelineBuilder();

		/**
		 * The meta object literal for the '<em><b>Build Lifelines</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY					= eINSTANCE.getLifelineBuilder__BuildLifelines__Tree_ModelHistory();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryTransitionImpl
		 * <em>Object History Transition</em>}' class. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryTransitionImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryTransition()
		 * @generated
		 */
		EClass		OBJECT_HISTORY_TRANSITION												= eINSTANCE.getObjectHistoryTransition();

		/**
		 * The meta object literal for the '<em><b>History Data</b></em>'
		 * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_HISTORY_TRANSITION__HISTORY_DATA									= eINSTANCE.getObjectHistoryTransition_HistoryData();

		/**
		 * The meta object literal for the '<em><b>State Transition</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_HISTORY_TRANSITION__STATE_TRANSITION								= eINSTANCE.getObjectHistoryTransition_StateTransition();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
		 * <em>Object History Data</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryData()
		 * @generated
		 */
		EClass		OBJECT_HISTORY_DATA														= eINSTANCE.getObjectHistoryData();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl
		 * <em>Object History Diff</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryDiffImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryDiff()
		 * @generated
		 */
		EClass		OBJECT_HISTORY_DIFF														= eINSTANCE.getObjectHistoryDiff();

		/**
		 * The meta object literal for the '<em><b>New State</b></em>' attribute
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_HISTORY_DIFF__NEW_STATE											= eINSTANCE.getObjectHistoryDiff_NewState();

		/**
		 * The meta object literal for the '<em><b>New Object Instance</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE								= eINSTANCE.getObjectHistoryDiff_NewObjectInstance();

		/**
		 * The meta object literal for the '<em><b>Old Object Instance</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE								= eINSTANCE.getObjectHistoryDiff_OldObjectInstance();

		/**
		 * The meta object literal for the '<em><b>Raw Diff Data</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_HISTORY_DIFF__RAW_DIFF_DATA										= eINSTANCE.getObjectHistoryDiff_RawDiffData();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl
		 * <em>Object History Node</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectHistoryNodeImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectHistoryNode()
		 * @generated
		 */
		EClass		OBJECT_HISTORY_NODE														= eINSTANCE.getObjectHistoryNode();

		/**
		 * The meta object literal for the '<em><b>History Data</b></em>'
		 * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_HISTORY_NODE__HISTORY_DATA										= eINSTANCE.getObjectHistoryNode_HistoryData();

		/**
		 * The meta object literal for the '<em><b>Object Descriptor</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR									= eINSTANCE.getObjectHistoryNode_ObjectDescriptor();

		/**
		 * The meta object literal for the '<em><b>Object Instance</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_HISTORY_NODE__OBJECT_INSTANCE									= eINSTANCE.getObjectHistoryNode_ObjectInstance();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_HISTORY_NODE__STATE												= eINSTANCE.getObjectHistoryNode_State();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeNodeReferenceImpl
		 * <em>State Path Tree Node Reference</em>}' class. <!-- begin-user-doc
		 * --> <!-- end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeNodeReferenceImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathTreeNodeReference()
		 * @generated
		 */
		EClass		STATE_PATH_TREE_NODE_REFERENCE											= eINSTANCE.getStatePathTreeNodeReference();

		/**
		 * The meta object literal for the '<em><b>State Path Tree Node</b></em>
		 * ' reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE					= eINSTANCE.getStatePathTreeNodeReference_StatePathTreeNode();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
		 * <em>State Path Reference</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathReference()
		 * @generated
		 */
		EClass		STATE_PATH_REFERENCE													= eINSTANCE.getStatePathReference();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeTransitionReferenceImpl
		 * <em>State Path Tree Transition Reference</em>}' class. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeTransitionReferenceImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getStatePathTreeTransitionReference()
		 * @generated
		 */
		EClass		STATE_PATH_TREE_TRANSITION_REFERENCE									= eINSTANCE.getStatePathTreeTransitionReference();

		/**
		 * The meta object literal for the '
		 * <em><b>State Path Tree Transition</b></em>' reference feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION		= eINSTANCE.getStatePathTreeTransitionReference_StatePathTreeTransition();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
		 * <em>Object State Type</em>}' enum. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectStateType()
		 * @generated
		 */
		EEnum		OBJECT_STATE_TYPE														= eINSTANCE.getObjectStateType();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl
		 * <em>Object Descriptor</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectDescriptorImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectDescriptor()
		 * @generated
		 */
		EClass		OBJECT_DESCRIPTOR														= eINSTANCE.getObjectDescriptor();

		/**
		 * The meta object literal for the '<em><b>Class Name</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_DESCRIPTOR__CLASS_NAME											= eINSTANCE.getObjectDescriptor_ClassName();

		/**
		 * The meta object literal for the '<em><b>Instance Name</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_DESCRIPTOR__INSTANCE_NAME										= eINSTANCE.getObjectDescriptor_InstanceName();

		/**
		 * The meta object literal for the '<em><b>Class Descriptor</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR										= eINSTANCE.getObjectDescriptor_ClassDescriptor();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeImpl
		 * <em>Tree</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getTree()
		 * @generated
		 */
		EClass		TREE																	= eINSTANCE.getTree();

		/**
		 * The meta object literal for the '<em><b>Root Node</b></em>' reference
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	TREE__ROOT_NODE															= eINSTANCE.getTree_RootNode();

		/**
		 * The meta object literal for the '<em><b>Get All Nodes</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	TREE___GET_ALL_NODES													= eINSTANCE.getTree__GetAllNodes();

		/**
		 * The meta object literal for the '<em><b>Get All Transitions</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	TREE___GET_ALL_TRANSITIONS												= eINSTANCE.getTree__GetAllTransitions();

		/**
		 * The meta object literal for the '<em><b>Contains Node Value</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	TREE___CONTAINS_NODE_VALUE__OBJECT										= eINSTANCE.getTree__ContainsNodeValue__Object();

		/**
		 * The meta object literal for the '
		 * <em><b>Contains Transition Value</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	TREE___CONTAINS_TRANSITION_VALUE__OBJECT								= eINSTANCE.getTree__ContainsTransitionValue__Object();

		/**
		 * The meta object literal for the '<em><b>Get Maximum Depth</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	TREE___GET_MAXIMUM_DEPTH												= eINSTANCE.getTree__GetMaximumDepth();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeNodeImpl
		 * <em>Tree Node</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
		 * -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeNodeImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getTreeNode()
		 * @generated
		 */
		EClass		TREE_NODE																= eINSTANCE.getTreeNode();

		/**
		 * The meta object literal for the '<em><b>Child Transitions</b></em>'
		 * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	TREE_NODE__CHILD_TRANSITIONS											= eINSTANCE.getTreeNode_ChildTransitions();

		/**
		 * The meta object literal for the '<em><b>Parent Transition</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	TREE_NODE__PARENT_TRANSITION											= eINSTANCE.getTreeNode_ParentTransition();

		/**
		 * The meta object literal for the '<em><b>Node Value</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	TREE_NODE__NODE_VALUE													= eINSTANCE.getTreeNode_NodeValue();

		/**
		 * The meta object literal for the '<em><b>Add Child Node</b></em>'
		 * operation. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation	TREE_NODE___ADD_CHILD_NODE__TREENODE_TREETRANSITION						= eINSTANCE.getTreeNode__AddChildNode__TreeNode_TreeTransition();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeTransitionImpl
		 * <em>Tree Transition</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeTransitionImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getTreeTransition()
		 * @generated
		 */
		EClass		TREE_TRANSITION															= eINSTANCE.getTreeTransition();

		/**
		 * The meta object literal for the '<em><b>Next Node</b></em>' reference
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	TREE_TRANSITION__NEXT_NODE												= eINSTANCE.getTreeTransition_NextNode();

		/**
		 * The meta object literal for the '<em><b>Previous Node</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	TREE_TRANSITION__PREVIOUS_NODE											= eINSTANCE.getTreeTransition_PreviousNode();

		/**
		 * The meta object literal for the '<em><b>Transition Value</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	TREE_TRANSITION__TRANSITION_VALUE										= eINSTANCE.getTreeTransition_TransitionValue();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineStateImpl
		 * <em>Object Lifeline State</em>}' class. <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineStateImpl
		 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl#getObjectLifelineState()
		 * @generated
		 */
		EClass		OBJECT_LIFELINE_STATE													= eINSTANCE.getObjectLifelineState();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_LIFELINE_STATE__STATE											= eINSTANCE.getObjectLifelineState_State();

		/**
		 * The meta object literal for the '<em><b>Object Instance</b></em>'
		 * attribute feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_LIFELINE_STATE__OBJECT_INSTANCE									= eINSTANCE.getObjectLifelineState_ObjectInstance();

		/**
		 * The meta object literal for the '<em><b>Base History Node</b></em>'
		 * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference	OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE								= eINSTANCE.getObjectLifelineState_BaseHistoryNode();

		/**
		 * The meta object literal for the '<em><b>Object ID</b></em>' attribute
		 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute	OBJECT_DESCRIPTOR__OBJECT_ID											= eINSTANCE.getObjectDescriptor_ObjectID();

	}

} // StatespacevisPackage
