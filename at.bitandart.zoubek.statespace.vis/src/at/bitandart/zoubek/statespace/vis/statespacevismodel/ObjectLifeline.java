/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Object Lifeline</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectDescriptor
 * <em>Object Descriptor</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectStateTree
 * <em>Object State Tree</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectLifeline()
 * @model
 * @generated
 */
public interface ObjectLifeline extends Lifeline {
	/**
	 * Returns the value of the '<em><b>Object Descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Descriptor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Object Descriptor</em>' reference.
	 * @see #setObjectDescriptor(ObjectDescriptor)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectLifeline_ObjectDescriptor()
	 * @model required="true"
	 * @generated
	 */
	ObjectDescriptor getObjectDescriptor();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectDescriptor
	 * <em>Object Descriptor</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Object Descriptor</em>' reference.
	 * @see #getObjectDescriptor()
	 * @generated
	 */
	void setObjectDescriptor(ObjectDescriptor value);

	/**
	 * Returns the value of the '<em><b>Object State Tree</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object State Tree</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Object State Tree</em>' reference.
	 * @see #setObjectStateTree(Tree)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectLifeline_ObjectStateTree()
	 * @model required="true"
	 * @generated
	 */
	Tree<StateSpaceTransition, ObjectLifelineState> getObjectStateTree();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline#getObjectStateTree
	 * <em>Object State Tree</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Object State Tree</em>' reference.
	 * @see #getObjectStateTree()
	 * @generated
	 */
	void setObjectStateTree(
			Tree<StateSpaceTransition, ObjectLifelineState> value);

} // ObjectLifeline
