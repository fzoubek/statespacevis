/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Path Tree Transition Reference</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathTreeTransitionReferenceImpl#getStatePathTreeTransition
 * <em>State Path Tree Transition</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StatePathTreeTransitionReferenceImpl extends MinimalEObjectImpl.Container implements StatePathTreeTransitionReference {
	/**
	 * The cached value of the '{@link #getStatePathTreeTransition()
	 * <em>State Path Tree Transition</em>}' reference. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getStatePathTreeTransition()
	 * @generated
	 * @ordered
	 */
	protected TreeTransition<State, StateSpaceTransition>	statePathTreeTransition;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathTreeTransitionReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_PATH_TREE_TRANSITION_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public TreeTransition<State, StateSpaceTransition> getStatePathTreeTransition() {
		if (statePathTreeTransition != null
			&& statePathTreeTransition.eIsProxy()) {
			InternalEObject oldStatePathTreeTransition = (InternalEObject) statePathTreeTransition;
			statePathTreeTransition = (TreeTransition<State, StateSpaceTransition>) eResolveProxy(oldStatePathTreeTransition);
			if (statePathTreeTransition != oldStatePathTreeTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION,
													oldStatePathTreeTransition,
													statePathTreeTransition));
			}
		}
		return statePathTreeTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeTransition<State, StateSpaceTransition> basicGetStatePathTreeTransition() {
		return statePathTreeTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setStatePathTreeTransition(
			TreeTransition<State, StateSpaceTransition> newStatePathTreeTransition) {
		TreeTransition<State, StateSpaceTransition> oldStatePathTreeTransition = statePathTreeTransition;
		statePathTreeTransition = newStatePathTreeTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION,
											oldStatePathTreeTransition,
											statePathTreeTransition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION:
				if (resolve)
					return getStatePathTreeTransition();
				return basicGetStatePathTreeTransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION:
				setStatePathTreeTransition((TreeTransition<State, StateSpaceTransition>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION:
				setStatePathTreeTransition((TreeTransition<State, StateSpaceTransition>) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION:
				return statePathTreeTransition != null;
		}
		return super.eIsSet(featureID);
	}

} // StatePathTreeTransitionReferenceImpl
