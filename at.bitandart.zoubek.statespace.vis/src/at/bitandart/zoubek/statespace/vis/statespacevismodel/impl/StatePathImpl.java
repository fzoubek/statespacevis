/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Path</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatePathImpl#getInitialState
 * <em>Initial State</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StatePathImpl extends MinimalEObjectImpl.Container implements StatePath {
	/**
	 * The cached value of the '{@link #getInitialState()
	 * <em>Initial State</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getInitialState()
	 * @generated
	 * @ordered
	 */
	protected StatePathNode	initialState;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_PATH;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNode getInitialState() {
		if (initialState != null && initialState.eIsProxy()) {
			InternalEObject oldInitialState = (InternalEObject) initialState;
			initialState = (StatePathNode) eResolveProxy(oldInitialState);
			if (initialState != oldInitialState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_PATH__INITIAL_STATE,
													oldInitialState,
													initialState));
			}
		}
		return initialState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNode basicGetInitialState() {
		return initialState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setInitialState(StatePathNode newInitialState) {
		StatePathNode oldInitialState = initialState;
		initialState = newInitialState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_PATH__INITIAL_STATE,
											oldInitialState,
											initialState));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH__INITIAL_STATE:
				if (resolve)
					return getInitialState();
				return basicGetInitialState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH__INITIAL_STATE:
				setInitialState((StatePathNode) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH__INITIAL_STATE:
				setInitialState((StatePathNode) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_PATH__INITIAL_STATE:
				return initialState != null;
		}
		return super.eIsSet(featureID);
	}

} // StatePathImpl
