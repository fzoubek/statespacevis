/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Lifeline Builder</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc --> Subclasses of this interface create lifelines
 * (usually for a <code>StateSpacePathVis</code> instance) based on a
 * <code>ModelHistory</code>.
 * 
 * @see ModelHistory
 * @see StateSpacePathVis <!-- end-model-doc -->
 * 
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getLifelineBuilder()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface LifelineBuilder extends EObject {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * builds and returns all lifelines based on the model history.
	 * 
	 * @param modelHistory
	 *            the model history used to create the lifelines
	 * @return a list of lifelines <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	EList<Lifeline> buildLifelines(Tree<State, StateSpaceTransition> stateTree,
			ModelHistory modelHistory);

} // LifelineBuilder
