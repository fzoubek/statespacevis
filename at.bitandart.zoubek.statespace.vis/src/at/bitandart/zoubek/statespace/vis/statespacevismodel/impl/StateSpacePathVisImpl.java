/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>State Space Path Vis</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl#getLifelineGroups
 * <em>Lifeline Groups</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl#getStatePathTree
 * <em>State Path Tree</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl#getStatespace
 * <em>Statespace</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl#getModelHistory
 * <em>Model History</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl#getModelHistoryBuilder
 * <em>Model History Builder</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StateSpacePathVisImpl#getLifelineBuilders
 * <em>Lifeline Builders</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StateSpacePathVisImpl extends MinimalEObjectImpl.Container implements StateSpacePathVis {
	/**
	 * The cached value of the '{@link #getLifelineGroups()
	 * <em>Lifeline Groups</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLifelineGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<LifelineGroup>				lifelineGroups;

	/**
	 * The cached value of the '{@link #getStatePathTree()
	 * <em>State Path Tree</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getStatePathTree()
	 * @generated
	 * @ordered
	 */
	protected Tree<State, StateSpaceTransition>	statePathTree;

	/**
	 * The cached value of the '{@link #getStatespace() <em>Statespace</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getStatespace()
	 * @generated
	 * @ordered
	 */
	protected StateSpace						statespace;

	/**
	 * The cached value of the '{@link #getModelHistory()
	 * <em>Model History</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getModelHistory()
	 * @generated
	 * @ordered
	 */
	protected ModelHistory						modelHistory;

	/**
	 * The cached value of the '{@link #getModelHistoryBuilder()
	 * <em>Model History Builder</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getModelHistoryBuilder()
	 * @generated
	 * @ordered
	 */
	protected ModelHistoryBuilder				modelHistoryBuilder;

	/**
	 * The cached value of the '{@link #getLifelineBuilders()
	 * <em>Lifeline Builders</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getLifelineBuilders()
	 * @generated
	 * @ordered
	 */
	protected EList<LifelineBuilder>			lifelineBuilders;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StateSpacePathVisImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.STATE_SPACE_PATH_VIS;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<LifelineGroup> getLifelineGroups() {
		if (lifelineGroups == null) {
			lifelineGroups = new EObjectResolvingEList<LifelineGroup>(	LifelineGroup.class,
																		this,
																		StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_GROUPS);
		}
		return lifelineGroups;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public Tree<State, StateSpaceTransition> getStatePathTree() {
		if (statePathTree != null && statePathTree.eIsProxy()) {
			InternalEObject oldStatePathTree = (InternalEObject) statePathTree;
			statePathTree = (Tree<State, StateSpaceTransition>) eResolveProxy(oldStatePathTree);
			if (statePathTree != oldStatePathTree) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_SPACE_PATH_VIS__STATE_PATH_TREE,
													oldStatePathTree,
													statePathTree));
			}
		}
		return statePathTree;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Tree<State, StateSpaceTransition> basicGetStatePathTree() {
		return statePathTree;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setStatePathTree(
			Tree<State, StateSpaceTransition> newStatePathTree) {
		Tree<State, StateSpaceTransition> oldStatePathTree = statePathTree;
		statePathTree = newStatePathTree;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_PATH_VIS__STATE_PATH_TREE,
											oldStatePathTree,
											statePathTree));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpace getStatespace() {
		if (statespace != null && statespace.eIsProxy()) {
			InternalEObject oldStatespace = (InternalEObject) statespace;
			statespace = (StateSpace) eResolveProxy(oldStatespace);
			if (statespace != oldStatespace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_SPACE_PATH_VIS__STATESPACE,
													oldStatespace,
													statespace));
			}
		}
		return statespace;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpace basicGetStatespace() {
		return statespace;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setStatespace(StateSpace newStatespace) {
		StateSpace oldStatespace = statespace;
		statespace = newStatespace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_PATH_VIS__STATESPACE,
											oldStatespace,
											statespace));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModelHistory getModelHistory() {
		if (modelHistory != null && modelHistory.eIsProxy()) {
			InternalEObject oldModelHistory = (InternalEObject) modelHistory;
			modelHistory = (ModelHistory) eResolveProxy(oldModelHistory);
			if (modelHistory != oldModelHistory) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY,
													oldModelHistory,
													modelHistory));
			}
		}
		return modelHistory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModelHistory basicGetModelHistory() {
		return modelHistory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setModelHistory(ModelHistory newModelHistory) {
		ModelHistory oldModelHistory = modelHistory;
		modelHistory = newModelHistory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY,
											oldModelHistory,
											modelHistory));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModelHistoryBuilder getModelHistoryBuilder() {
		if (modelHistoryBuilder != null && modelHistoryBuilder.eIsProxy()) {
			InternalEObject oldModelHistoryBuilder = (InternalEObject) modelHistoryBuilder;
			modelHistoryBuilder = (ModelHistoryBuilder) eResolveProxy(oldModelHistoryBuilder);
			if (modelHistoryBuilder != oldModelHistoryBuilder) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER,
													oldModelHistoryBuilder,
													modelHistoryBuilder));
			}
		}
		return modelHistoryBuilder;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModelHistoryBuilder basicGetModelHistoryBuilder() {
		return modelHistoryBuilder;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setModelHistoryBuilder(
			ModelHistoryBuilder newModelHistoryBuilder) {
		ModelHistoryBuilder oldModelHistoryBuilder = modelHistoryBuilder;
		modelHistoryBuilder = newModelHistoryBuilder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER,
											oldModelHistoryBuilder,
											modelHistoryBuilder));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<LifelineBuilder> getLifelineBuilders() {
		if (lifelineBuilders == null) {
			lifelineBuilders = new EObjectResolvingEList<LifelineBuilder>(	LifelineBuilder.class,
																			this,
																			StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS);
		}
		return lifelineBuilders;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void updateModelHistory() {
		ModelHistory modelHistory = getModelHistory();
		ModelHistoryBuilder modelHistoryBuilder = getModelHistoryBuilder();
		if (modelHistory == null) {
			modelHistory = StatespacevisFactoryImpl.eINSTANCE.createModelHistory();
			setModelHistory(modelHistory);
		}
		if (getModelHistoryBuilder() != null) {
			modelHistory.update(modelHistoryBuilder, getStatePathTree());
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void updateLifelines() {
		ModelHistory modelHistory = getModelHistory();
		EList<LifelineBuilder> lifelineBuilders = getLifelineBuilders();
		EList<LifelineGroup> lifelineGroups = getLifelineGroups();
		lifelineGroups.clear();

		for (LifelineBuilder builder : lifelineBuilders) {
			// each builder has it's own group
			LifelineGroup group = StatespacevisFactoryImpl.eINSTANCE.createLifelineGroup();
			group.getLifelines()
					.addAll(builder.buildLifelines(	getStatePathTree(),
													modelHistory));
			lifelineGroups.add(group);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_GROUPS:
				return getLifelineGroups();
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATE_PATH_TREE:
				if (resolve)
					return getStatePathTree();
				return basicGetStatePathTree();
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATESPACE:
				if (resolve)
					return getStatespace();
				return basicGetStatespace();
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY:
				if (resolve)
					return getModelHistory();
				return basicGetModelHistory();
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER:
				if (resolve)
					return getModelHistoryBuilder();
				return basicGetModelHistoryBuilder();
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS:
				return getLifelineBuilders();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_GROUPS:
				getLifelineGroups().clear();
				getLifelineGroups().addAll((Collection<? extends LifelineGroup>) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATE_PATH_TREE:
				setStatePathTree((Tree<State, StateSpaceTransition>) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATESPACE:
				setStatespace((StateSpace) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY:
				setModelHistory((ModelHistory) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER:
				setModelHistoryBuilder((ModelHistoryBuilder) newValue);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS:
				getLifelineBuilders().clear();
				getLifelineBuilders().addAll((Collection<? extends LifelineBuilder>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_GROUPS:
				getLifelineGroups().clear();
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATE_PATH_TREE:
				setStatePathTree((Tree<State, StateSpaceTransition>) null);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATESPACE:
				setStatespace((StateSpace) null);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY:
				setModelHistory((ModelHistory) null);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER:
				setModelHistoryBuilder((ModelHistoryBuilder) null);
				return;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS:
				getLifelineBuilders().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_GROUPS:
				return lifelineGroups != null && !lifelineGroups.isEmpty();
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATE_PATH_TREE:
				return statePathTree != null;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__STATESPACE:
				return statespace != null;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY:
				return modelHistory != null;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER:
				return modelHistoryBuilder != null;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS:
				return lifelineBuilders != null && !lifelineBuilders.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatespacevisPackage.STATE_SPACE_PATH_VIS___UPDATE_MODEL_HISTORY:
				updateModelHistory();
				return null;
			case StatespacevisPackage.STATE_SPACE_PATH_VIS___UPDATE_LIFELINES:
				updateLifelines();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} // StateSpacePathVisImpl
