/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Space Path Vis</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getLifelineGroups
 * <em>Lifeline Groups</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatePathTree
 * <em>State Path Tree</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatespace
 * <em>Statespace</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistory
 * <em>Model History</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistoryBuilder
 * <em>Model History Builder</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getLifelineBuilders
 * <em>Lifeline Builders</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpacePathVis()
 * @model
 * @generated
 */
public interface StateSpacePathVis extends EObject {
	/**
	 * Returns the value of the '<em><b>Lifeline Groups</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup}
	 * . <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifeline Groups</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Lifeline Groups</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpacePathVis_LifelineGroups()
	 * @model
	 * @generated
	 */
	EList<LifelineGroup> getLifelineGroups();

	/**
	 * Returns the value of the '<em><b>State Path Tree</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Path Tree</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>State Path Tree</em>' reference.
	 * @see #setStatePathTree(Tree)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpacePathVis_StatePathTree()
	 * @model
	 * @generated
	 */
	Tree<State, StateSpaceTransition> getStatePathTree();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatePathTree
	 * <em>State Path Tree</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>State Path Tree</em>' reference.
	 * @see #getStatePathTree()
	 * @generated
	 */
	void setStatePathTree(Tree<State, StateSpaceTransition> value);

	/**
	 * Returns the value of the '<em><b>Statespace</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statespace</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Statespace</em>' reference.
	 * @see #setStatespace(StateSpace)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpacePathVis_Statespace()
	 * @model
	 * @generated
	 */
	StateSpace getStatespace();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getStatespace
	 * <em>Statespace</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Statespace</em>' reference.
	 * @see #getStatespace()
	 * @generated
	 */
	void setStatespace(StateSpace value);

	/**
	 * Returns the value of the '<em><b>Model History</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model History</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Model History</em>' reference.
	 * @see #setModelHistory(ModelHistory)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpacePathVis_ModelHistory()
	 * @model
	 * @generated
	 */
	ModelHistory getModelHistory();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistory
	 * <em>Model History</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Model History</em>' reference.
	 * @see #getModelHistory()
	 * @generated
	 */
	void setModelHistory(ModelHistory value);

	/**
	 * Returns the value of the '<em><b>Model History Builder</b></em>'
	 * reference. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model History Builder</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Model History Builder</em>' reference.
	 * @see #setModelHistoryBuilder(ModelHistoryBuilder)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpacePathVis_ModelHistoryBuilder()
	 * @model
	 * @generated
	 */
	ModelHistoryBuilder getModelHistoryBuilder();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis#getModelHistoryBuilder
	 * <em>Model History Builder</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Model History Builder</em>'
	 *            reference.
	 * @see #getModelHistoryBuilder()
	 * @generated
	 */
	void setModelHistoryBuilder(ModelHistoryBuilder value);

	/**
	 * Returns the value of the '<em><b>Lifeline Builders</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder}
	 * . <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lifeline Builders</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Lifeline Builders</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpacePathVis_LifelineBuilders()
	 * @model
	 * @generated
	 */
	EList<LifelineBuilder> getLifelineBuilders();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * updates the model history with the current
	 * <code>ModelHistoryBuilder</code> based on the current state path tree. If
	 * no <code>ModelHistoryBuilder</code> is set the ModelHistory will be
	 * empty. <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void updateModelHistory();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * updates all lifelines based on the current <code>ModelHistory</code>. The
	 * specified <code>LifelineBuilders</code> are responsible for the lifeline
	 * creation, so if no
	 * <code>LifelineBuilder<code> is defined, no lifellines will be created. 
	 * <!-- end-model-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void updateLifelines();

} // StateSpacePathVis
