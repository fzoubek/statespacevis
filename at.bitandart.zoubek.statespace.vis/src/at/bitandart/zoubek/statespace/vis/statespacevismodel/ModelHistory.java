/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model History</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistoryTrees
 * <em>Object History Trees</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getModelHistory()
 * @model
 * @generated
 */
public interface ModelHistory extends EObject {
	/**
	 * Returns the value of the '<em><b>Object History Trees</b></em>'
	 * attribute. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object History Trees</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Object History Trees</em>' attribute.
	 * @see #setObjectHistoryTrees(Map)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getModelHistory_ObjectHistoryTrees()
	 * @model transient="true"
	 * @generated
	 */
	Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>> getObjectHistoryTrees();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistoryTrees
	 * <em>Object History Trees</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Object History Trees</em>'
	 *            attribute.
	 * @see #getObjectHistoryTrees()
	 * @generated
	 */
	void setObjectHistoryTrees(
			Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>> value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * updates this model history based on the given state path tree and
	 * <code>ModelHistoryBuilder</code>.
	 * 
	 * @param historyBuilder
	 *            the history builder used to update the current history
	 *            instance
	 * @param stateTree
	 *            the state path tree used to update the current history
	 *            instance <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void update(ModelHistoryBuilder historyBuilder,
			Tree<State, StateSpaceTransition> stateTree);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * returns the <code>ObjectDescriptor</code> for the given object. If the
	 * given object is not known yet, use <code>createObjectDescriptor()</code>
	 * to create a ObjectDescriptor for it.
	 * 
	 * @param object
	 *            the object
	 * @return the object descriptor or null if the object is unknown <!--
	 *         end-model-doc -->
	 * @model
	 * @generated
	 */
	ObjectDescriptor getObjectDescriptor(Object object);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * creates a new <code>ObjectDescriptor</code> for the given object and
	 * registers it to this history object
	 * 
	 * @param object
	 *            the object to register
	 * @return the object descriptor <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	ObjectDescriptor createObjectDescriptor(Object object);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * returns the object history for the given object descriptor
	 * 
	 * @param objectDescriptor
	 *            the object descriptor of the object history tree to retireve
	 * @return the object history tree <!-- end-model-doc -->
	 * @model type=
	 *        "at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree<org.eclipse.emf.ecore.EJavaObject, at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition>"
	 * @generated
	 */
	Tree<ObjectHistoryNode, ObjectHistoryTransition> getObjectHistory(
			ObjectDescriptor objectDescriptor);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void clear();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void registerObjectToDescriptor(ObjectDescriptor objectDescriptor,
			Object object);

} // ModelHistory
