/**
 * 
 * $Id$
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.validation;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;

import org.eclipse.emf.common.util.EList;

/**
 * A sample validator interface for
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode}
 * . This doesn't really do anything, and it's not a real EMF artifact. It was
 * generated by the org.eclipse.emf.examples.generator.validator plug-in to
 * illustrate how EMF's code generator can be extended. This can be disabled
 * with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ObjectHistoryNodeValidator {
	boolean validate();

	boolean validateHistoryData(EList<ObjectHistoryData> value);

	boolean validateObjectDescriptor(ObjectDescriptor value);

	boolean validateObjectInstance(Object value);

	boolean validateState(State value);
}
