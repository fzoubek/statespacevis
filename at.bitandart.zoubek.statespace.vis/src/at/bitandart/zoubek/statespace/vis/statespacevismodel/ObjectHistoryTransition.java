/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Object History Transition</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition#getHistoryData
 * <em>History Data</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition#getStateTransition
 * <em>State Transition</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryTransition()
 * @model
 * @generated
 */
public interface ObjectHistoryTransition extends EObject {
	/**
	 * Returns the value of the '<em><b>History Data</b></em>' reference list.
	 * The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData}
	 * . <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>History Data</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>History Data</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryTransition_HistoryData()
	 * @model
	 * @generated
	 */
	EList<ObjectHistoryData> getHistoryData();

	/**
	 * Returns the value of the '<em><b>State Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>State Transition</em>' reference.
	 * @see #setStateTransition(StateSpaceTransition)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryTransition_StateTransition()
	 * @model
	 * @generated
	 */
	StateSpaceTransition getStateTransition();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition#getStateTransition
	 * <em>State Transition</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>State Transition</em>' reference.
	 * @see #getStateTransition()
	 * @generated
	 */
	void setStateTransition(StateSpaceTransition value);

} // ObjectHistoryTransition
