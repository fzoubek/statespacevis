/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Tree Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeNodeImpl#getChildTransitions
 * <em>Child Transitions</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeNodeImpl#getParentTransition
 * <em>Parent Transition</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeNodeImpl#getNodeValue
 * <em>Node Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TreeNodeImpl<N, T> extends MinimalEObjectImpl.Container implements TreeNode<N, T> {
	/**
	 * The cached value of the '{@link #getChildTransitions()
	 * <em>Child Transitions</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getChildTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<TreeTransition<N, T>>	childTransitions;

	/**
	 * The cached value of the '{@link #getParentTransition()
	 * <em>Parent Transition</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getParentTransition()
	 * @generated
	 * @ordered
	 */
	protected TreeTransition<N, T>			parentTransition;

	/**
	 * The cached value of the '{@link #getNodeValue() <em>Node Value</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNodeValue()
	 * @generated
	 * @ordered
	 */
	protected N								nodeValue;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected TreeNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.TREE_NODE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<TreeTransition<N, T>> getChildTransitions() {
		if (childTransitions == null) {
			childTransitions = new EObjectWithInverseResolvingEList<TreeTransition<N, T>>(	TreeTransition.class,
																							this,
																							StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS,
																							StatespacevisPackage.TREE_TRANSITION__PREVIOUS_NODE);
		}
		return childTransitions;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public TreeTransition<N, T> getParentTransition() {
		if (parentTransition != null && parentTransition.eIsProxy()) {
			InternalEObject oldParentTransition = (InternalEObject) parentTransition;
			parentTransition = (TreeTransition<N, T>) eResolveProxy(oldParentTransition);
			if (parentTransition != oldParentTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.TREE_NODE__PARENT_TRANSITION,
													oldParentTransition,
													parentTransition));
			}
		}
		return parentTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeTransition<N, T> basicGetParentTransition() {
		return parentTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetParentTransition(
			TreeTransition<N, T> newParentTransition, NotificationChain msgs) {
		TreeTransition<N, T> oldParentTransition = parentTransition;
		parentTransition = newParentTransition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(	this,
																	Notification.SET,
																	StatespacevisPackage.TREE_NODE__PARENT_TRANSITION,
																	oldParentTransition,
																	newParentTransition);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setParentTransition(TreeTransition<N, T> newParentTransition) {
		if (newParentTransition != parentTransition) {
			NotificationChain msgs = null;
			if (parentTransition != null)
				msgs = ((InternalEObject) parentTransition).eInverseRemove(	this,
																			StatespacevisPackage.TREE_TRANSITION__NEXT_NODE,
																			TreeTransition.class,
																			msgs);
			if (newParentTransition != null)
				msgs = ((InternalEObject) newParentTransition).eInverseAdd(	this,
																			StatespacevisPackage.TREE_TRANSITION__NEXT_NODE,
																			TreeTransition.class,
																			msgs);
			msgs = basicSetParentTransition(newParentTransition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.TREE_NODE__PARENT_TRANSITION,
											newParentTransition,
											newParentTransition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public N getNodeValue() {
		return nodeValue;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNodeValue(N newNodeValue) {
		N oldNodeValue = nodeValue;
		nodeValue = newNodeValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.TREE_NODE__NODE_VALUE,
											oldNodeValue,
											nodeValue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void addChildNode(TreeNode<N, T> childNode,
			TreeTransition<N, T> transition) {
		transition.setPreviousNode(this);
		transition.setNextNode(childNode);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS:
				return ((InternalEList<InternalEObject>) (InternalEList<?>) getChildTransitions()).basicAdd(otherEnd,
																											msgs);
			case StatespacevisPackage.TREE_NODE__PARENT_TRANSITION:
				if (parentTransition != null)
					msgs = ((InternalEObject) parentTransition).eInverseRemove(	this,
																				StatespacevisPackage.TREE_TRANSITION__NEXT_NODE,
																				TreeTransition.class,
																				msgs);
				return basicSetParentTransition((TreeTransition<N, T>) otherEnd,
												msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS:
				return ((InternalEList<?>) getChildTransitions()).basicRemove(	otherEnd,
																				msgs);
			case StatespacevisPackage.TREE_NODE__PARENT_TRANSITION:
				return basicSetParentTransition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS:
				return getChildTransitions();
			case StatespacevisPackage.TREE_NODE__PARENT_TRANSITION:
				if (resolve)
					return getParentTransition();
				return basicGetParentTransition();
			case StatespacevisPackage.TREE_NODE__NODE_VALUE:
				return getNodeValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS:
				getChildTransitions().clear();
				getChildTransitions().addAll((Collection<? extends TreeTransition<N, T>>) newValue);
				return;
			case StatespacevisPackage.TREE_NODE__PARENT_TRANSITION:
				setParentTransition((TreeTransition<N, T>) newValue);
				return;
			case StatespacevisPackage.TREE_NODE__NODE_VALUE:
				setNodeValue((N) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS:
				getChildTransitions().clear();
				return;
			case StatespacevisPackage.TREE_NODE__PARENT_TRANSITION:
				setParentTransition((TreeTransition<N, T>) null);
				return;
			case StatespacevisPackage.TREE_NODE__NODE_VALUE:
				setNodeValue((N) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.TREE_NODE__CHILD_TRANSITIONS:
				return childTransitions != null && !childTransitions.isEmpty();
			case StatespacevisPackage.TREE_NODE__PARENT_TRANSITION:
				return parentTransition != null;
			case StatespacevisPackage.TREE_NODE__NODE_VALUE:
				return nodeValue != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatespacevisPackage.TREE_NODE___ADD_CHILD_NODE__TREENODE_TREETRANSITION:
				addChildNode(	(TreeNode<N, T>) arguments.get(0),
								(TreeTransition<N, T>) arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nodeValue: ");
		result.append(nodeValue);
		result.append(')');
		return result.toString();
	}

} // TreeNodeImpl
