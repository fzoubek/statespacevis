/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Space Node</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceNode#getState
 * <em>State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceNode#getOutgoingTransitions
 * <em>Outgoing Transitions</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceNode#getIncomingTransitions
 * <em>Incoming Transitions</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceNode()
 * @model
 * @generated
 */
public interface StateSpaceNode extends EObject {
	/**
	 * Returns the value of the '<em><b>State</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(State)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceNode_State()
	 * @model required="true"
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceNode#getState
	 * <em>State</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Outgoing Transitions</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition}
	 * . It is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState
	 * <em>Prev State</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transitions</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Outgoing Transitions</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceNode_OutgoingTransitions()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getPrevState
	 * @model opposite="prevState"
	 * @generated
	 */
	EList<StateSpaceTransition> getOutgoingTransitions();

	/**
	 * Returns the value of the '<em><b>Incoming Transitions</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition}
	 * . It is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState
	 * <em>Next State</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transitions</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Incoming Transitions</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStateSpaceNode_IncomingTransitions()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition#getNextState
	 * @model opposite="nextState"
	 * @generated
	 */
	EList<StateSpaceTransition> getIncomingTransitions();

} // StateSpaceNode
