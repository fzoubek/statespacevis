/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class StatespacevisFactoryImpl extends EFactoryImpl implements StatespacevisFactory {
	/**
	 * Creates the default factory implementation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static StatespacevisFactory init() {
		try {
			StatespacevisFactory theStatespacevisFactory = (StatespacevisFactory) EPackage.Registry.INSTANCE.getEFactory(StatespacevisPackage.eNS_URI);
			if (theStatespacevisFactory != null) {
				return theStatespacevisFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StatespacevisFactoryImpl();
	}

	/**
	 * Creates an instance of the factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public StatespacevisFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StatespacevisPackage.LIFELINE_GROUP:
				return createLifelineGroup();
			case StatespacevisPackage.OBJECT_LIFELINE:
				return createObjectLifeline();
			case StatespacevisPackage.OBJECT_DESCRIPTOR:
				return createObjectDescriptor();
			case StatespacevisPackage.OBJECT_LIFELINE_STATE:
				return createObjectLifelineState();
			case StatespacevisPackage.TREE:
				return createTree();
			case StatespacevisPackage.TREE_NODE:
				return createTreeNode();
			case StatespacevisPackage.TREE_TRANSITION:
				return createTreeTransition();
			case StatespacevisPackage.STATE:
				return createState();
			case StatespacevisPackage.STATE_PATH:
				return createStatePath();
			case StatespacevisPackage.STATE_PATH_NODE:
				return createStatePathNode();
			case StatespacevisPackage.STATE_PATH_TRANSITION:
				return createStatePathTransition();
			case StatespacevisPackage.STATE_SPACE:
				return createStateSpace();
			case StatespacevisPackage.STATE_SPACE_PATH_VIS:
				return createStateSpacePathVis();
			case StatespacevisPackage.STATE_SPACE_TRANSITION:
				return createStateSpaceTransition();
			case StatespacevisPackage.MODEL_HISTORY:
				return createModelHistory();
			case StatespacevisPackage.OBJECT_HISTORY_TRANSITION:
				return createObjectHistoryTransition();
			case StatespacevisPackage.OBJECT_HISTORY_DIFF:
				return createObjectHistoryDiff();
			case StatespacevisPackage.OBJECT_HISTORY_NODE:
				return createObjectHistoryNode();
			case StatespacevisPackage.STATE_PATH_TREE_NODE_REFERENCE:
				return createStatePathTreeNodeReference();
			case StatespacevisPackage.STATE_PATH_TREE_TRANSITION_REFERENCE:
				return createStatePathTreeTransitionReference();
			default:
				throw new IllegalArgumentException("The class '"
													+ eClass.getName()
													+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case StatespacevisPackage.OBJECT_STATE_TYPE:
				return createObjectStateTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '"
													+ eDataType.getName()
													+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case StatespacevisPackage.OBJECT_STATE_TYPE:
				return convertObjectStateTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '"
													+ eDataType.getName()
													+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LifelineGroup createLifelineGroup() {
		LifelineGroupImpl lifelineGroup = new LifelineGroupImpl();
		return lifelineGroup;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpacePathVis createStateSpacePathVis() {
		StateSpacePathVisImpl stateSpacePathVis = new StateSpacePathVisImpl();
		return stateSpacePathVis;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectLifeline createObjectLifeline() {
		ObjectLifelineImpl objectLifeline = new ObjectLifelineImpl();
		return objectLifeline;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePath createStatePath() {
		StatePathImpl statePath = new StatePathImpl();
		return statePath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public State createState() {
		StateImpl state = new StateImpl();
		return state;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathNode createStatePathNode() {
		StatePathNodeImpl statePathNode = new StatePathNodeImpl();
		return statePathNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTransition createStatePathTransition() {
		StatePathTransitionImpl statePathTransition = new StatePathTransitionImpl();
		return statePathTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpace createStateSpace() {
		StateSpaceImpl stateSpace = new StateSpaceImpl();
		return stateSpace;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpaceTransition createStateSpaceTransition() {
		StateSpaceTransitionImpl stateSpaceTransition = new StateSpaceTransitionImpl();
		return stateSpaceTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModelHistory createModelHistory() {
		ModelHistoryImpl modelHistory = new ModelHistoryImpl();
		return modelHistory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryTransition createObjectHistoryTransition() {
		ObjectHistoryTransitionImpl objectHistoryTransition = new ObjectHistoryTransitionImpl();
		return objectHistoryTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryDiff createObjectHistoryDiff() {
		ObjectHistoryDiffImpl objectHistoryDiff = new ObjectHistoryDiffImpl();
		return objectHistoryDiff;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryNode createObjectHistoryNode() {
		ObjectHistoryNodeImpl objectHistoryNode = new ObjectHistoryNodeImpl();
		return objectHistoryNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTreeNodeReference createStatePathTreeNodeReference() {
		StatePathTreeNodeReferenceImpl statePathTreeNodeReference = new StatePathTreeNodeReferenceImpl();
		return statePathTreeNodeReference;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTreeTransitionReference createStatePathTreeTransitionReference() {
		StatePathTreeTransitionReferenceImpl statePathTreeTransitionReference = new StatePathTreeTransitionReferenceImpl();
		return statePathTreeTransitionReference;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectStateType createObjectStateTypeFromString(EDataType eDataType,
			String initialValue) {
		ObjectStateType result = ObjectStateType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '"
												+ initialValue
												+ "' is not a valid enumerator of '"
												+ eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String convertObjectStateTypeToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectDescriptor createObjectDescriptor() {
		ObjectDescriptorImpl objectDescriptor = new ObjectDescriptorImpl();
		return objectDescriptor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectLifelineState createObjectLifelineState() {
		ObjectLifelineStateImpl objectLifelineState = new ObjectLifelineStateImpl();
		return objectLifelineState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public <N, T> Tree<N, T> createTree() {
		TreeImpl<N, T> tree = new TreeImpl<N, T>();
		return tree;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public <N, T> TreeNode<N, T> createTreeNode() {
		TreeNodeImpl<N, T> treeNode = new TreeNodeImpl<N, T>();
		return treeNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public <N, T> TreeTransition<N, T> createTreeTransition() {
		TreeTransitionImpl<N, T> treeTransition = new TreeTransitionImpl<N, T>();
		return treeTransition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatespacevisPackage getStatespacevisPackage() {
		return (StatespacevisPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StatespacevisPackage getPackage() {
		return StatespacevisPackage.eINSTANCE;
	}

} // StatespacevisFactoryImpl
