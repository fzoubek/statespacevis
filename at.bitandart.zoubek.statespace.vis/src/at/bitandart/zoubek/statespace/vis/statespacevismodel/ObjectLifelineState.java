/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Object Lifeline State</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getState
 * <em>State</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getObjectInstance
 * <em>Object Instance</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getBaseHistoryNode
 * <em>Base History Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectLifelineState()
 * @model
 * @generated
 */
public interface ObjectLifelineState extends EObject {
	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute. The literals
	 * are from the enumeration
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType}
	 * . <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>State</em>' attribute.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * @see #setState(ObjectStateType)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectLifelineState_State()
	 * @model
	 * @generated
	 */
	ObjectStateType getState();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getState
	 * <em>State</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>State</em>' attribute.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType
	 * @see #getState()
	 * @generated
	 */
	void setState(ObjectStateType value);

	/**
	 * Returns the value of the '<em><b>Object Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Instance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Object Instance</em>' attribute.
	 * @see #setObjectInstance(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectLifelineState_ObjectInstance()
	 * @model
	 * @generated
	 */
	Object getObjectInstance();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getObjectInstance
	 * <em>Object Instance</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Object Instance</em>' attribute.
	 * @see #getObjectInstance()
	 * @generated
	 */
	void setObjectInstance(Object value);

	/**
	 * Returns the value of the '<em><b>Base History Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base History Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base History Node</em>' reference.
	 * @see #setBaseHistoryNode(ObjectHistoryNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectLifelineState_BaseHistoryNode()
	 * @model
	 * @generated
	 */
	ObjectHistoryNode getBaseHistoryNode();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState#getBaseHistoryNode
	 * <em>Base History Node</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base History Node</em>' reference.
	 * @see #getBaseHistoryNode()
	 * @generated
	 */
	void setBaseHistoryNode(ObjectHistoryNode value);

} // ObjectLifelineState
