/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import java.lang.Object;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Model History</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ModelHistoryImpl#getObjectHistoryTrees
 * <em>Object History Trees</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ModelHistoryImpl extends MinimalEObjectImpl.Container implements ModelHistory {
	/**
	 * The cached value of the '{@link #getObjectHistoryTrees()
	 * <em>Object History Trees</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectHistoryTrees()
	 * @generated
	 * @ordered
	 */
	protected Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>>	objectHistoryTrees;

	private int																			lastObjectDescriptorId		= 0;

	private Map<Object, ObjectDescriptor>												objectToObjectDescriptorMap	= new IdentityHashMap<>();
	private Map<ObjectDescriptor, List<Object>>											objectDescriptorToObjectMap	= new HashMap<>();
	private Map<Integer, ObjectDescriptor>												idToObjectDescriptorMap		= new HashMap<>();
	private Map<Integer, List<Object>>													idToObjectMap				= new HashMap<>();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModelHistoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.MODEL_HISTORY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>> getObjectHistoryTrees() {
		if (objectHistoryTrees == null) {
			objectHistoryTrees = new HashMap<>();
		}
		return objectHistoryTrees;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setObjectHistoryTrees(
			Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>> newObjectHistoryTrees) {
		Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>> oldObjectHistoryTrees = objectHistoryTrees;
		objectHistoryTrees = newObjectHistoryTrees;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.MODEL_HISTORY__OBJECT_HISTORY_TREES,
											oldObjectHistoryTrees,
											objectHistoryTrees));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void update(final ModelHistoryBuilder historyBuilder,
			final Tree<State, StateSpaceTransition> stateTree) {
		historyBuilder.updateObjectHistory(stateTree, this);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ObjectDescriptor getObjectDescriptor(Object object) {
		return objectToObjectDescriptorMap.get(object);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public ObjectDescriptor createObjectDescriptor(Object object) {
		ObjectDescriptor objectDescriptor = StatespacevisFactoryImpl.eINSTANCE.createObjectDescriptor();
		int id = lastObjectDescriptorId++;
		objectDescriptor.setObjectID(id);
		objectDescriptor.setInstanceName("#" + id);
		if (object instanceof EObject) {
			EObject eObj = (EObject) object;
			EClass eClass = eObj.eClass();
			if (eClass.getInstanceClass() != null) {
				objectDescriptor.setClassName(eClass.getInstanceClass()
													.getSimpleName());
			} else {
				objectDescriptor.setClassName(eClass.getName());
			}
		} else {
			objectDescriptor.setClassName(object.getClass().getSimpleName());
		}

		Tree<ObjectHistoryNode, ObjectHistoryTransition> objHistoryTree = StatespacevisFactory.eINSTANCE.createTree();
		getObjectHistoryTrees().put(objectDescriptor, objHistoryTree);

		List<Object> objects = null;

		if (objectDescriptorToObjectMap.containsKey(objectDescriptor)) {
			objects = objectDescriptorToObjectMap.get(objectDescriptor);
		} else {
			objects = new ArrayList<>();
		}

		objects.add(object);
		objectDescriptorToObjectMap.put(objectDescriptor, objects);
		objectToObjectDescriptorMap.put(object, objectDescriptor);
		idToObjectDescriptorMap.put(id, objectDescriptor);
		idToObjectMap.put(id, objects);

		return objectDescriptor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Tree<ObjectHistoryNode, ObjectHistoryTransition> getObjectHistory(
			final ObjectDescriptor objectDescriptor) {
		return getObjectHistoryTrees().get(objectDescriptor);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void clear() {
		objectDescriptorToObjectMap.clear();
		objectToObjectDescriptorMap.clear();
		idToObjectDescriptorMap.clear();
		idToObjectMap.clear();
		lastObjectDescriptorId = 0;
		getObjectHistoryTrees().clear();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void registerObjectToDescriptor(ObjectDescriptor objectDescriptor,
			Object object) {
		List<Object> objList = objectDescriptorToObjectMap.get(objectDescriptor);
		objList.add(object);
		objectToObjectDescriptorMap.put(object, objectDescriptor);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.MODEL_HISTORY__OBJECT_HISTORY_TREES:
				return getObjectHistoryTrees();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.MODEL_HISTORY__OBJECT_HISTORY_TREES:
				setObjectHistoryTrees((Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.MODEL_HISTORY__OBJECT_HISTORY_TREES:
				setObjectHistoryTrees((Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>>) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.MODEL_HISTORY__OBJECT_HISTORY_TREES:
				return objectHistoryTrees != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatespacevisPackage.MODEL_HISTORY___UPDATE__MODELHISTORYBUILDER_TREE:
				update(	(ModelHistoryBuilder) arguments.get(0),
						(Tree<State, StateSpaceTransition>) arguments.get(1));
				return null;
			case StatespacevisPackage.MODEL_HISTORY___GET_OBJECT_DESCRIPTOR__OBJECT:
				return getObjectDescriptor(arguments.get(0));
			case StatespacevisPackage.MODEL_HISTORY___CREATE_OBJECT_DESCRIPTOR__OBJECT:
				return createObjectDescriptor(arguments.get(0));
			case StatespacevisPackage.MODEL_HISTORY___GET_OBJECT_HISTORY__OBJECTDESCRIPTOR:
				return getObjectHistory((ObjectDescriptor) arguments.get(0));
			case StatespacevisPackage.MODEL_HISTORY___CLEAR:
				clear();
				return null;
			case StatespacevisPackage.MODEL_HISTORY___REGISTER_OBJECT_TO_DESCRIPTOR__OBJECTDESCRIPTOR_OBJECT:
				registerObjectToDescriptor(	(ObjectDescriptor) arguments.get(0),
											arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (objectHistoryTrees: ");
		result.append(objectHistoryTrees);
		result.append(')');
		return result.toString();
	}

} // ModelHistoryImpl
