/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Model History Builder</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc --> Subclasses of this interface are responsible for
 * updating a given <code>ModelHistory</code> (usually for a
 * <code>StateSpacePathVis</code> instance) based on a state path tree.
 * 
 * @see ModelHistory
 * @see StateSpacePathVis <!-- end-model-doc -->
 * 
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getModelHistoryBuilder()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ModelHistoryBuilder extends EObject {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc --> <!-- begin-model-doc -->
	 * updates the given model history based on the given state path tree.
	 * 
	 * @param stateTree
	 *            the state tree to consider when updating the model history
	 * @param modelHistory
	 *            the model history to update <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void updateObjectHistory(Tree<State, StateSpaceTransition> stateTree,
			ModelHistory modelHistory);

} // ModelHistoryBuilder
