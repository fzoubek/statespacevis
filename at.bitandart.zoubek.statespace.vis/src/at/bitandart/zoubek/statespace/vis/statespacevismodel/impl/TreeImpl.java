/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ListIterator;
import java.util.Stack;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Tree</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.TreeImpl#getRootNode
 * <em>Root Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TreeImpl<N, T> extends MinimalEObjectImpl.Container implements Tree<N, T> {
	/**
	 * The cached value of the '{@link #getRootNode() <em>Root Node</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRootNode()
	 * @generated
	 * @ordered
	 */
	protected TreeNode<N, T>	rootNode;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected TreeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.TREE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public TreeNode<N, T> getRootNode() {
		if (rootNode != null && rootNode.eIsProxy()) {
			InternalEObject oldRootNode = (InternalEObject) rootNode;
			rootNode = (TreeNode<N, T>) eResolveProxy(oldRootNode);
			if (rootNode != oldRootNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.TREE__ROOT_NODE,
													oldRootNode,
													rootNode));
			}
		}
		return rootNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeNode<N, T> basicGetRootNode() {
		return rootNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRootNode(TreeNode<N, T> newRootNode) {
		TreeNode<N, T> oldRootNode = rootNode;
		rootNode = newRootNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.TREE__ROOT_NODE,
											oldRootNode,
											rootNode));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<TreeNode<N, T>> getAllNodes() {
		EList<TreeNode<N, T>> allNodes = new BasicEList<>();

		if (rootNode != null) {
			// Start DFS
			Stack<TreeTransition<N, T>> unvisitedTransitions = new Stack<>();
			unvisitedTransitions.addAll(rootNode.getChildTransitions());
			allNodes.add(rootNode);

			while (!unvisitedTransitions.isEmpty()) {
				TreeTransition<N, T> transition = unvisitedTransitions.pop();
				TreeNode<N, T> node = transition.getNextNode();
				allNodes.add(node);

				EList<TreeTransition<N, T>> childTransitions = node.getChildTransitions();
				ListIterator<TreeTransition<N, T>> iterator = childTransitions.listIterator(childTransitions.size());
				while (iterator.hasPrevious()) {
					unvisitedTransitions.push(iterator.previous());
				}
			}
		}

		return allNodes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<TreeTransition<N, T>> getAllTransitions() {
		EList<TreeTransition<N, T>> allTransitions = new BasicEList<>();

		if (rootNode != null) {
			// Start DFS
			Stack<TreeTransition<N, T>> unvisitedTransitions = new Stack<>();
			unvisitedTransitions.addAll(rootNode.getChildTransitions());

			while (!unvisitedTransitions.isEmpty()) {
				TreeTransition<N, T> transition = unvisitedTransitions.pop();
				allTransitions.add(transition);

				TreeNode<N, T> node = transition.getNextNode();
				EList<TreeTransition<N, T>> childTransitions = node.getChildTransitions();
				ListIterator<TreeTransition<N, T>> iterator = childTransitions.listIterator(childTransitions.size());
				while (iterator.hasPrevious()) {
					unvisitedTransitions.push(iterator.previous());
				}
			}
		}

		return allTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * returns if a particular value is contained in a node of the tree using
	 * {@link Object#equals(Object)}
	 * 
	 * @param value
	 *            the value to search for
	 * @return true if the specified value is contained in a node of this tree,
	 *         false otherwise
	 * 
	 *         <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean containsNodeValue(N value) {

		if (rootNode != null) {
			// Start DFS
			Stack<TreeTransition<N, T>> unvisitedTransitions = new Stack<>();
			unvisitedTransitions.addAll(rootNode.getChildTransitions());
			if (rootNode.getNodeValue().equals(value)) {
				return true;
			}

			while (!unvisitedTransitions.isEmpty()) {
				TreeTransition<N, T> transition = unvisitedTransitions.pop();
				TreeNode<N, T> node = transition.getNextNode();
				if (node.getNodeValue().equals(value)) {
					return true;
				}
				unvisitedTransitions.addAll(node.getChildTransitions());
			}
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * returns if a particular value is contained in a transition of the tree
	 * using {@link Object#equals(Object)}
	 * 
	 * @param value
	 *            the value to search for
	 * @return true if the specified value is contained in a transition of this
	 *         tree, false otherwise
	 * 
	 *         <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean containsTransitionValue(T value) {

		if (rootNode != null) {
			// Start DFS
			Stack<TreeTransition<N, T>> unvisitedTransitions = new Stack<>();
			unvisitedTransitions.addAll(rootNode.getChildTransitions());

			while (!unvisitedTransitions.isEmpty()) {
				TreeTransition<N, T> transition = unvisitedTransitions.pop();
				if (transition.getTransitionValue().equals(value)) {
					return true;
				}
				TreeNode<N, T> node = transition.getNextNode();
				unvisitedTransitions.addAll(node.getChildTransitions());
			}
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @return the maximum depth of this tree - the maximum number of nodes
	 *         passed during a depth first iteration over the full tree, which
	 *         means that if the tree does not contain a root node, the depth is
	 *         0.
	 * 
	 *         <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getMaximumDepth() {
		int maxDepth = 0;

		if (rootNode != null) {
			maxDepth = 1;
			int depth = 1;

			// Start DFS
			Stack<TreeTransition<N, T>> unvisitedTransitions = new Stack<>();
			// record depths of the parent node for each transition
			Stack<Integer> depthStack = new Stack<>();
			for (TreeTransition<N, T> transiton : rootNode.getChildTransitions()) {
				unvisitedTransitions.push(transiton);
				depthStack.push(depth);
			}

			while (!unvisitedTransitions.isEmpty()) {
				TreeTransition<N, T> transition = unvisitedTransitions.pop();
				depth = depthStack.pop();

				TreeNode<N, T> node = transition.getNextNode();
				depth++;
				if (depth > maxDepth) {
					maxDepth = depth;
				}

				EList<TreeTransition<N, T>> childTransitions = node.getChildTransitions();
				if (!childTransitions.isEmpty()) {
					ListIterator<TreeTransition<N, T>> iterator = childTransitions.listIterator(childTransitions.size());
					while (iterator.hasPrevious()) {
						unvisitedTransitions.push(iterator.previous());
						depthStack.push(depth);
					}
				}
			}
		}

		return maxDepth;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.TREE__ROOT_NODE:
				if (resolve)
					return getRootNode();
				return basicGetRootNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.TREE__ROOT_NODE:
				setRootNode((TreeNode<N, T>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.TREE__ROOT_NODE:
				setRootNode((TreeNode<N, T>) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.TREE__ROOT_NODE:
				return rootNode != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatespacevisPackage.TREE___GET_ALL_NODES:
				return getAllNodes();
			case StatespacevisPackage.TREE___GET_ALL_TRANSITIONS:
				return getAllTransitions();
			case StatespacevisPackage.TREE___CONTAINS_NODE_VALUE__OBJECT:
				return containsNodeValue((N) arguments.get(0));
			case StatespacevisPackage.TREE___CONTAINS_TRANSITION_VALUE__OBJECT:
				return containsTransitionValue((T) arguments.get(0));
			case StatespacevisPackage.TREE___GET_MAXIMUM_DEPTH:
				return getMaximumDepth();
		}
		return super.eInvoke(operationID, arguments);
	}

} // TreeImpl
