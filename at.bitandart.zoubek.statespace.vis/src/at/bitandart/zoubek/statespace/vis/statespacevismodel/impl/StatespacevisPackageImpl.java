/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class StatespacevisPackageImpl extends EPackageImpl implements StatespacevisPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	lifelineGroupEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	lifelineEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	stateSpacePathVisEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	objectLifelineEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	statePathEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	stateEClass								= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	statePathNodeEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	statePathTransitionEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	stateSpaceEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	stateSpaceTransitionEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	modelHistoryEClass						= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	modelHistoryBuilderEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	lifelineBuilderEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	objectHistoryTransitionEClass			= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	objectHistoryDataEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	objectHistoryDiffEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	objectHistoryNodeEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	statePathTreeNodeReferenceEClass		= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	statePathReferenceEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	statePathTreeTransitionReferenceEClass	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EEnum	objectStateTypeEEnum					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	objectDescriptorEClass					= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	objectLifelineStateEClass				= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	treeEClass								= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	treeNodeEClass							= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	treeTransitionEClass					= null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StatespacevisPackageImpl() {
		super(eNS_URI, StatespacevisFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean	isInited	= false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link StatespacevisPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StatespacevisPackage init() {
		if (isInited)
			return (StatespacevisPackage) EPackage.Registry.INSTANCE.getEPackage(StatespacevisPackage.eNS_URI);

		// Obtain or create and register package
		StatespacevisPackageImpl theStatespacevisPackage = (StatespacevisPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StatespacevisPackageImpl
				? EPackage.Registry.INSTANCE.get(eNS_URI)
				: new StatespacevisPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theStatespacevisPackage.createPackageContents();

		// Initialize created meta-data
		theStatespacevisPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStatespacevisPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(	StatespacevisPackage.eNS_URI,
										theStatespacevisPackage);
		return theStatespacevisPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getLifelineGroup() {
		return lifelineGroupEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getLifelineGroup_Lifelines() {
		return (EReference) lifelineGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getLifelineGroup_Name() {
		return (EAttribute) lifelineGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getLifeline() {
		return lifelineEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getLifeline_ParentLifeline() {
		return (EReference) lifelineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getLifeline_ChildLifelines() {
		return (EReference) lifelineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getLifeline_Visible() {
		return (EAttribute) lifelineEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getLifeline_Collapsed() {
		return (EAttribute) lifelineEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStateSpacePathVis() {
		return stateSpacePathVisEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpacePathVis_LifelineGroups() {
		return (EReference) stateSpacePathVisEClass.getEStructuralFeatures()
													.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpacePathVis_StatePathTree() {
		return (EReference) stateSpacePathVisEClass.getEStructuralFeatures()
													.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpacePathVis_Statespace() {
		return (EReference) stateSpacePathVisEClass.getEStructuralFeatures()
													.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpacePathVis_ModelHistory() {
		return (EReference) stateSpacePathVisEClass.getEStructuralFeatures()
													.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpacePathVis_ModelHistoryBuilder() {
		return (EReference) stateSpacePathVisEClass.getEStructuralFeatures()
													.get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpacePathVis_LifelineBuilders() {
		return (EReference) stateSpacePathVisEClass.getEStructuralFeatures()
													.get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getStateSpacePathVis__UpdateModelHistory() {
		return stateSpacePathVisEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getStateSpacePathVis__UpdateLifelines() {
		return stateSpacePathVisEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getObjectLifeline() {
		return objectLifelineEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectLifeline_ObjectDescriptor() {
		return (EReference) objectLifelineEClass.getEStructuralFeatures()
												.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectLifeline_ObjectStateTree() {
		return (EReference) objectLifelineEClass.getEStructuralFeatures()
												.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStatePath() {
		return statePathEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePath_InitialState() {
		return (EReference) statePathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getState() {
		return stateEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getState_ModelSnapshot() {
		return (EAttribute) stateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getState_RawStateData() {
		return (EAttribute) stateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getState_IncomingTransitions() {
		return (EReference) stateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getState_OutgoingTransitions() {
		return (EReference) stateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStatePathNode() {
		return statePathNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePathNode_State() {
		return (EReference) statePathNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePathNode_NextTransition() {
		return (EReference) statePathNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePathNode_IncomingTransition() {
		return (EReference) statePathNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStatePathTransition() {
		return statePathTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePathTransition_NextStatePathNode() {
		return (EReference) statePathTransitionEClass.getEStructuralFeatures()
														.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getStatePathTransition_TransitionName() {
		return (EAttribute) statePathTransitionEClass.getEStructuralFeatures()
														.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePathTransition_PreviousStatePathNode() {
		return (EReference) statePathTransitionEClass.getEStructuralFeatures()
														.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStateSpace() {
		return stateSpaceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpace_InitialState() {
		return (EReference) stateSpaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getStateSpace__GetAllStateSpaceNodes() {
		return stateSpaceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getStateSpace__GetAllStateSpaceTransitions() {
		return stateSpaceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStateSpaceTransition() {
		return stateSpaceTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpaceTransition_NextState() {
		return (EReference) stateSpaceTransitionEClass.getEStructuralFeatures()
														.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getStateSpaceTransition_TransitionName() {
		return (EAttribute) stateSpaceTransitionEClass.getEStructuralFeatures()
														.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStateSpaceTransition_PrevState() {
		return (EReference) stateSpaceTransitionEClass.getEStructuralFeatures()
														.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getStateSpaceTransition_RawTransitionData() {
		return (EAttribute) stateSpaceTransitionEClass.getEStructuralFeatures()
														.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getModelHistory() {
		return modelHistoryEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getModelHistory_ObjectHistoryTrees() {
		return (EAttribute) modelHistoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getModelHistory__Update__ModelHistoryBuilder_Tree() {
		return modelHistoryEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getModelHistory__GetObjectDescriptor__Object() {
		return modelHistoryEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getModelHistory__CreateObjectDescriptor__Object() {
		return modelHistoryEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getModelHistory__GetObjectHistory__ObjectDescriptor() {
		return modelHistoryEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getModelHistory__Clear() {
		return modelHistoryEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getModelHistory__RegisterObjectToDescriptor__ObjectDescriptor_Object() {
		return modelHistoryEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getModelHistoryBuilder() {
		return modelHistoryBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getModelHistoryBuilder__UpdateObjectHistory__Tree_ModelHistory() {
		return modelHistoryBuilderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getLifelineBuilder() {
		return lifelineBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getLifelineBuilder__BuildLifelines__Tree_ModelHistory() {
		return lifelineBuilderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getObjectHistoryTransition() {
		return objectHistoryTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectHistoryTransition_HistoryData() {
		return (EReference) objectHistoryTransitionEClass.getEStructuralFeatures()
															.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectHistoryTransition_StateTransition() {
		return (EReference) objectHistoryTransitionEClass.getEStructuralFeatures()
															.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getObjectHistoryData() {
		return objectHistoryDataEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getObjectHistoryDiff() {
		return objectHistoryDiffEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectHistoryDiff_NewState() {
		return (EAttribute) objectHistoryDiffEClass.getEStructuralFeatures()
													.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectHistoryDiff_NewObjectInstance() {
		return (EAttribute) objectHistoryDiffEClass.getEStructuralFeatures()
													.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectHistoryDiff_OldObjectInstance() {
		return (EAttribute) objectHistoryDiffEClass.getEStructuralFeatures()
													.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectHistoryDiff_RawDiffData() {
		return (EAttribute) objectHistoryDiffEClass.getEStructuralFeatures()
													.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getObjectHistoryNode() {
		return objectHistoryNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectHistoryNode_HistoryData() {
		return (EReference) objectHistoryNodeEClass.getEStructuralFeatures()
													.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectHistoryNode_ObjectDescriptor() {
		return (EReference) objectHistoryNodeEClass.getEStructuralFeatures()
													.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectHistoryNode_ObjectInstance() {
		return (EAttribute) objectHistoryNodeEClass.getEStructuralFeatures()
													.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectHistoryNode_State() {
		return (EReference) objectHistoryNodeEClass.getEStructuralFeatures()
													.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStatePathTreeNodeReference() {
		return statePathTreeNodeReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePathTreeNodeReference_StatePathTreeNode() {
		return (EReference) statePathTreeNodeReferenceEClass.getEStructuralFeatures()
															.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStatePathReference() {
		return statePathReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getStatePathTreeTransitionReference() {
		return statePathTreeTransitionReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getStatePathTreeTransitionReference_StatePathTreeTransition() {
		return (EReference) statePathTreeTransitionReferenceEClass.getEStructuralFeatures()
																	.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EEnum getObjectStateType() {
		return objectStateTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getObjectDescriptor() {
		return objectDescriptorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectDescriptor_ClassName() {
		return (EAttribute) objectDescriptorEClass.getEStructuralFeatures()
													.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectDescriptor_InstanceName() {
		return (EAttribute) objectDescriptorEClass.getEStructuralFeatures()
													.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectDescriptor_ClassDescriptor() {
		return (EAttribute) objectDescriptorEClass.getEStructuralFeatures()
													.get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getTree() {
		return treeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getTree_RootNode() {
		return (EReference) treeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getTree__GetAllNodes() {
		return treeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getTree__GetAllTransitions() {
		return treeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getTree__ContainsNodeValue__Object() {
		return treeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getTree__ContainsTransitionValue__Object() {
		return treeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getTree__GetMaximumDepth() {
		return treeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getTreeNode() {
		return treeNodeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getTreeNode_ChildTransitions() {
		return (EReference) treeNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getTreeNode_ParentTransition() {
		return (EReference) treeNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getTreeNode_NodeValue() {
		return (EAttribute) treeNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EOperation getTreeNode__AddChildNode__TreeNode_TreeTransition() {
		return treeNodeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getTreeTransition() {
		return treeTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getTreeTransition_NextNode() {
		return (EReference) treeTransitionEClass.getEStructuralFeatures()
												.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getTreeTransition_PreviousNode() {
		return (EReference) treeTransitionEClass.getEStructuralFeatures()
												.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getTreeTransition_TransitionValue() {
		return (EAttribute) treeTransitionEClass.getEStructuralFeatures()
												.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getObjectLifelineState() {
		return objectLifelineStateEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectLifelineState_State() {
		return (EAttribute) objectLifelineStateEClass.getEStructuralFeatures()
														.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectLifelineState_ObjectInstance() {
		return (EAttribute) objectLifelineStateEClass.getEStructuralFeatures()
														.get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EReference getObjectLifelineState_BaseHistoryNode() {
		return (EReference) objectLifelineStateEClass.getEStructuralFeatures()
														.get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EAttribute getObjectDescriptor_ObjectID() {
		return (EAttribute) objectDescriptorEClass.getEStructuralFeatures()
													.get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatespacevisFactory getStatespacevisFactory() {
		return (StatespacevisFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isCreated	= false;

	/**
	 * Creates the meta-model objects for the package. This method is guarded to
	 * have no affect on any invocation but its first. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		lifelineGroupEClass = createEClass(LIFELINE_GROUP);
		createEReference(lifelineGroupEClass, LIFELINE_GROUP__LIFELINES);
		createEAttribute(lifelineGroupEClass, LIFELINE_GROUP__NAME);

		lifelineEClass = createEClass(LIFELINE);
		createEReference(lifelineEClass, LIFELINE__PARENT_LIFELINE);
		createEReference(lifelineEClass, LIFELINE__CHILD_LIFELINES);
		createEAttribute(lifelineEClass, LIFELINE__VISIBLE);
		createEAttribute(lifelineEClass, LIFELINE__COLLAPSED);

		objectLifelineEClass = createEClass(OBJECT_LIFELINE);
		createEReference(	objectLifelineEClass,
							OBJECT_LIFELINE__OBJECT_DESCRIPTOR);
		createEReference(	objectLifelineEClass,
							OBJECT_LIFELINE__OBJECT_STATE_TREE);

		objectDescriptorEClass = createEClass(OBJECT_DESCRIPTOR);
		createEAttribute(objectDescriptorEClass, OBJECT_DESCRIPTOR__OBJECT_ID);
		createEAttribute(objectDescriptorEClass, OBJECT_DESCRIPTOR__CLASS_NAME);
		createEAttribute(	objectDescriptorEClass,
							OBJECT_DESCRIPTOR__INSTANCE_NAME);
		createEAttribute(	objectDescriptorEClass,
							OBJECT_DESCRIPTOR__CLASS_DESCRIPTOR);

		objectLifelineStateEClass = createEClass(OBJECT_LIFELINE_STATE);
		createEAttribute(	objectLifelineStateEClass,
							OBJECT_LIFELINE_STATE__STATE);
		createEAttribute(	objectLifelineStateEClass,
							OBJECT_LIFELINE_STATE__OBJECT_INSTANCE);
		createEReference(	objectLifelineStateEClass,
							OBJECT_LIFELINE_STATE__BASE_HISTORY_NODE);

		treeEClass = createEClass(TREE);
		createEReference(treeEClass, TREE__ROOT_NODE);
		createEOperation(treeEClass, TREE___GET_ALL_NODES);
		createEOperation(treeEClass, TREE___GET_ALL_TRANSITIONS);
		createEOperation(treeEClass, TREE___CONTAINS_NODE_VALUE__OBJECT);
		createEOperation(treeEClass, TREE___CONTAINS_TRANSITION_VALUE__OBJECT);
		createEOperation(treeEClass, TREE___GET_MAXIMUM_DEPTH);

		treeNodeEClass = createEClass(TREE_NODE);
		createEReference(treeNodeEClass, TREE_NODE__CHILD_TRANSITIONS);
		createEReference(treeNodeEClass, TREE_NODE__PARENT_TRANSITION);
		createEAttribute(treeNodeEClass, TREE_NODE__NODE_VALUE);
		createEOperation(	treeNodeEClass,
							TREE_NODE___ADD_CHILD_NODE__TREENODE_TREETRANSITION);

		treeTransitionEClass = createEClass(TREE_TRANSITION);
		createEReference(treeTransitionEClass, TREE_TRANSITION__NEXT_NODE);
		createEReference(treeTransitionEClass, TREE_TRANSITION__PREVIOUS_NODE);
		createEAttribute(	treeTransitionEClass,
							TREE_TRANSITION__TRANSITION_VALUE);

		stateEClass = createEClass(STATE);
		createEAttribute(stateEClass, STATE__MODEL_SNAPSHOT);
		createEAttribute(stateEClass, STATE__RAW_STATE_DATA);
		createEReference(stateEClass, STATE__INCOMING_TRANSITIONS);
		createEReference(stateEClass, STATE__OUTGOING_TRANSITIONS);

		statePathEClass = createEClass(STATE_PATH);
		createEReference(statePathEClass, STATE_PATH__INITIAL_STATE);

		statePathNodeEClass = createEClass(STATE_PATH_NODE);
		createEReference(statePathNodeEClass, STATE_PATH_NODE__STATE);
		createEReference(statePathNodeEClass, STATE_PATH_NODE__NEXT_TRANSITION);
		createEReference(	statePathNodeEClass,
							STATE_PATH_NODE__INCOMING_TRANSITION);

		statePathTransitionEClass = createEClass(STATE_PATH_TRANSITION);
		createEReference(	statePathTransitionEClass,
							STATE_PATH_TRANSITION__NEXT_STATE_PATH_NODE);
		createEAttribute(	statePathTransitionEClass,
							STATE_PATH_TRANSITION__TRANSITION_NAME);
		createEReference(	statePathTransitionEClass,
							STATE_PATH_TRANSITION__PREVIOUS_STATE_PATH_NODE);

		stateSpaceEClass = createEClass(STATE_SPACE);
		createEReference(stateSpaceEClass, STATE_SPACE__INITIAL_STATE);
		createEOperation(	stateSpaceEClass,
							STATE_SPACE___GET_ALL_STATE_SPACE_NODES);
		createEOperation(	stateSpaceEClass,
							STATE_SPACE___GET_ALL_STATE_SPACE_TRANSITIONS);

		stateSpacePathVisEClass = createEClass(STATE_SPACE_PATH_VIS);
		createEReference(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS__LIFELINE_GROUPS);
		createEReference(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS__STATE_PATH_TREE);
		createEReference(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS__STATESPACE);
		createEReference(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS__MODEL_HISTORY);
		createEReference(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS__MODEL_HISTORY_BUILDER);
		createEReference(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS__LIFELINE_BUILDERS);
		createEOperation(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS___UPDATE_MODEL_HISTORY);
		createEOperation(	stateSpacePathVisEClass,
							STATE_SPACE_PATH_VIS___UPDATE_LIFELINES);

		stateSpaceTransitionEClass = createEClass(STATE_SPACE_TRANSITION);
		createEReference(	stateSpaceTransitionEClass,
							STATE_SPACE_TRANSITION__NEXT_STATE);
		createEAttribute(	stateSpaceTransitionEClass,
							STATE_SPACE_TRANSITION__TRANSITION_NAME);
		createEReference(	stateSpaceTransitionEClass,
							STATE_SPACE_TRANSITION__PREV_STATE);
		createEAttribute(	stateSpaceTransitionEClass,
							STATE_SPACE_TRANSITION__RAW_TRANSITION_DATA);

		modelHistoryEClass = createEClass(MODEL_HISTORY);
		createEAttribute(	modelHistoryEClass,
							MODEL_HISTORY__OBJECT_HISTORY_TREES);
		createEOperation(	modelHistoryEClass,
							MODEL_HISTORY___UPDATE__MODELHISTORYBUILDER_TREE);
		createEOperation(	modelHistoryEClass,
							MODEL_HISTORY___GET_OBJECT_DESCRIPTOR__OBJECT);
		createEOperation(	modelHistoryEClass,
							MODEL_HISTORY___CREATE_OBJECT_DESCRIPTOR__OBJECT);
		createEOperation(	modelHistoryEClass,
							MODEL_HISTORY___GET_OBJECT_HISTORY__OBJECTDESCRIPTOR);
		createEOperation(modelHistoryEClass, MODEL_HISTORY___CLEAR);
		createEOperation(	modelHistoryEClass,
							MODEL_HISTORY___REGISTER_OBJECT_TO_DESCRIPTOR__OBJECTDESCRIPTOR_OBJECT);

		modelHistoryBuilderEClass = createEClass(MODEL_HISTORY_BUILDER);
		createEOperation(	modelHistoryBuilderEClass,
							MODEL_HISTORY_BUILDER___UPDATE_OBJECT_HISTORY__TREE_MODELHISTORY);

		lifelineBuilderEClass = createEClass(LIFELINE_BUILDER);
		createEOperation(	lifelineBuilderEClass,
							LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY);

		objectHistoryTransitionEClass = createEClass(OBJECT_HISTORY_TRANSITION);
		createEReference(	objectHistoryTransitionEClass,
							OBJECT_HISTORY_TRANSITION__HISTORY_DATA);
		createEReference(	objectHistoryTransitionEClass,
							OBJECT_HISTORY_TRANSITION__STATE_TRANSITION);

		objectHistoryDataEClass = createEClass(OBJECT_HISTORY_DATA);

		objectHistoryDiffEClass = createEClass(OBJECT_HISTORY_DIFF);
		createEAttribute(	objectHistoryDiffEClass,
							OBJECT_HISTORY_DIFF__NEW_STATE);
		createEAttribute(	objectHistoryDiffEClass,
							OBJECT_HISTORY_DIFF__NEW_OBJECT_INSTANCE);
		createEAttribute(	objectHistoryDiffEClass,
							OBJECT_HISTORY_DIFF__OLD_OBJECT_INSTANCE);
		createEAttribute(	objectHistoryDiffEClass,
							OBJECT_HISTORY_DIFF__RAW_DIFF_DATA);

		objectHistoryNodeEClass = createEClass(OBJECT_HISTORY_NODE);
		createEReference(	objectHistoryNodeEClass,
							OBJECT_HISTORY_NODE__HISTORY_DATA);
		createEReference(	objectHistoryNodeEClass,
							OBJECT_HISTORY_NODE__OBJECT_DESCRIPTOR);
		createEAttribute(	objectHistoryNodeEClass,
							OBJECT_HISTORY_NODE__OBJECT_INSTANCE);
		createEReference(objectHistoryNodeEClass, OBJECT_HISTORY_NODE__STATE);

		statePathTreeNodeReferenceEClass = createEClass(STATE_PATH_TREE_NODE_REFERENCE);
		createEReference(	statePathTreeNodeReferenceEClass,
							STATE_PATH_TREE_NODE_REFERENCE__STATE_PATH_TREE_NODE);

		statePathReferenceEClass = createEClass(STATE_PATH_REFERENCE);

		statePathTreeTransitionReferenceEClass = createEClass(STATE_PATH_TREE_TRANSITION_REFERENCE);
		createEReference(	statePathTreeTransitionReferenceEClass,
							STATE_PATH_TREE_TRANSITION_REFERENCE__STATE_PATH_TREE_TRANSITION);

		// Create enums
		objectStateTypeEEnum = createEEnum(OBJECT_STATE_TYPE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isInitialized	= false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters
		ETypeParameter treeEClass_N = addETypeParameter(treeEClass, "N");
		ETypeParameter treeEClass_T = addETypeParameter(treeEClass, "T");
		ETypeParameter treeNodeEClass_N = addETypeParameter(treeNodeEClass, "N");
		ETypeParameter treeNodeEClass_T = addETypeParameter(treeNodeEClass, "T");
		ETypeParameter treeTransitionEClass_N = addETypeParameter(	treeTransitionEClass,
																	"N");
		ETypeParameter treeTransitionEClass_T = addETypeParameter(	treeTransitionEClass,
																	"T");

		// Set bounds for type parameters

		// Add supertypes to classes
		objectLifelineEClass.getESuperTypes().add(this.getLifeline());
		objectHistoryDiffEClass.getESuperTypes()
								.add(this.getObjectHistoryData());
		statePathTreeNodeReferenceEClass.getESuperTypes()
										.add(this.getStatePathReference());
		statePathReferenceEClass.getESuperTypes()
								.add(this.getObjectHistoryData());
		statePathTreeTransitionReferenceEClass.getESuperTypes()
												.add(this.getStatePathReference());

		// Initialize classes, features, and operations; add parameters
		initEClass(	lifelineGroupEClass,
					LifelineGroup.class,
					"LifelineGroup",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getLifelineGroup_Lifelines(),
						this.getLifeline(),
						null,
						"lifelines",
						null,
						0,
						-1,
						LifelineGroup.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getLifelineGroup_Name(),
						ecorePackage.getEString(),
						"name",
						null,
						0,
						1,
						LifelineGroup.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	lifelineEClass,
					Lifeline.class,
					"Lifeline",
					IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getLifeline_ParentLifeline(),
						this.getLifeline(),
						this.getLifeline_ChildLifelines(),
						"parentLifeline",
						null,
						0,
						1,
						Lifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getLifeline_ChildLifelines(),
						this.getLifeline(),
						this.getLifeline_ParentLifeline(),
						"childLifelines",
						null,
						0,
						-1,
						Lifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getLifeline_Visible(),
						ecorePackage.getEBoolean(),
						"visible",
						"true",
						0,
						1,
						Lifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getLifeline_Collapsed(),
						ecorePackage.getEBoolean(),
						"collapsed",
						"true",
						0,
						1,
						Lifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	objectLifelineEClass,
					ObjectLifeline.class,
					"ObjectLifeline",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getObjectLifeline_ObjectDescriptor(),
						this.getObjectDescriptor(),
						null,
						"objectDescriptor",
						null,
						1,
						1,
						ObjectLifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		EGenericType g1 = createEGenericType(this.getTree());
		EGenericType g2 = createEGenericType(this.getStateSpaceTransition());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getObjectLifelineState());
		g1.getETypeArguments().add(g2);
		initEReference(	getObjectLifeline_ObjectStateTree(),
						g1,
						null,
						"objectStateTree",
						null,
						1,
						1,
						ObjectLifeline.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	objectDescriptorEClass,
					ObjectDescriptor.class,
					"ObjectDescriptor",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(	getObjectDescriptor_ObjectID(),
						ecorePackage.getEInt(),
						"ObjectID",
						null,
						0,
						1,
						ObjectDescriptor.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectDescriptor_ClassName(),
						ecorePackage.getEString(),
						"ClassName",
						null,
						0,
						1,
						ObjectDescriptor.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectDescriptor_InstanceName(),
						ecorePackage.getEString(),
						"InstanceName",
						null,
						0,
						1,
						ObjectDescriptor.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectDescriptor_ClassDescriptor(),
						ecorePackage.getEJavaObject(),
						"classDescriptor",
						null,
						0,
						1,
						ObjectDescriptor.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	objectLifelineStateEClass,
					ObjectLifelineState.class,
					"ObjectLifelineState",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(	getObjectLifelineState_State(),
						this.getObjectStateType(),
						"state",
						null,
						0,
						1,
						ObjectLifelineState.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectLifelineState_ObjectInstance(),
						ecorePackage.getEJavaObject(),
						"objectInstance",
						null,
						0,
						1,
						ObjectLifelineState.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getObjectLifelineState_BaseHistoryNode(),
						this.getObjectHistoryNode(),
						null,
						"baseHistoryNode",
						null,
						0,
						1,
						ObjectLifelineState.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	treeEClass,
					Tree.class,
					"Tree",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getTreeNode());
		g2 = createEGenericType(treeEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(	getTree_RootNode(),
						g1,
						null,
						"rootNode",
						null,
						1,
						1,
						Tree.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		EOperation op = initEOperation(	getTree__GetAllNodes(),
										null,
										"getAllNodes",
										0,
										-1,
										IS_UNIQUE,
										IS_ORDERED);
		g1 = createEGenericType(this.getTreeNode());
		g2 = createEGenericType(treeEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeEClass_T);
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getTree__GetAllTransitions(),
							null,
							"getAllTransitions",
							0,
							-1,
							IS_UNIQUE,
							IS_ORDERED);
		g1 = createEGenericType(this.getTreeTransition());
		g2 = createEGenericType(treeEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeEClass_T);
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getTree__ContainsNodeValue__Object(),
							ecorePackage.getEBoolean(),
							"containsNodeValue",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		g1 = createEGenericType(treeEClass_N);
		addEParameter(op, g1, "value", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTree__ContainsTransitionValue__Object(),
							ecorePackage.getEBoolean(),
							"containsTransitionValue",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		g1 = createEGenericType(treeEClass_T);
		addEParameter(op, g1, "value", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(	getTree__GetMaximumDepth(),
						ecorePackage.getEInt(),
						"getMaximumDepth",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		initEClass(	treeNodeEClass,
					TreeNode.class,
					"TreeNode",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getTreeTransition());
		g2 = createEGenericType(treeNodeEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeNodeEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(	getTreeNode_ChildTransitions(),
						g1,
						this.getTreeTransition_PreviousNode(),
						"childTransitions",
						null,
						0,
						-1,
						TreeNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		g1 = createEGenericType(this.getTreeTransition());
		g2 = createEGenericType(treeNodeEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeNodeEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(	getTreeNode_ParentTransition(),
						g1,
						this.getTreeTransition_NextNode(),
						"parentTransition",
						null,
						0,
						1,
						TreeNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		g1 = createEGenericType(treeNodeEClass_N);
		initEAttribute(	getTreeNode_NodeValue(),
						g1,
						"nodeValue",
						null,
						0,
						1,
						TreeNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		op = initEOperation(getTreeNode__AddChildNode__TreeNode_TreeTransition(),
							null,
							"addChildNode",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		g1 = createEGenericType(this.getTreeNode());
		g2 = createEGenericType(treeNodeEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeNodeEClass_T);
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "childNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getTreeTransition());
		g2 = createEGenericType(treeNodeEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeNodeEClass_T);
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "transition", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(	treeTransitionEClass,
					TreeTransition.class,
					"TreeTransition",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getTreeNode());
		g2 = createEGenericType(treeTransitionEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeTransitionEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(	getTreeTransition_NextNode(),
						g1,
						this.getTreeNode_ParentTransition(),
						"nextNode",
						null,
						1,
						1,
						TreeTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		g1 = createEGenericType(this.getTreeNode());
		g2 = createEGenericType(treeTransitionEClass_N);
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(treeTransitionEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(	getTreeTransition_PreviousNode(),
						g1,
						this.getTreeNode_ChildTransitions(),
						"previousNode",
						null,
						1,
						1,
						TreeTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		g1 = createEGenericType(treeTransitionEClass_T);
		initEAttribute(	getTreeTransition_TransitionValue(),
						g1,
						"transitionValue",
						null,
						0,
						1,
						TreeTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	stateEClass,
					State.class,
					"State",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(	getState_ModelSnapshot(),
						ecorePackage.getEJavaObject(),
						"modelSnapshot",
						null,
						0,
						1,
						State.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getState_RawStateData(),
						ecorePackage.getEJavaObject(),
						"rawStateData",
						null,
						0,
						1,
						State.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getState_IncomingTransitions(),
						this.getStateSpaceTransition(),
						this.getStateSpaceTransition_NextState(),
						"incomingTransitions",
						null,
						0,
						-1,
						State.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getState_OutgoingTransitions(),
						this.getStateSpaceTransition(),
						this.getStateSpaceTransition_PrevState(),
						"outgoingTransitions",
						null,
						0,
						-1,
						State.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	statePathEClass,
					StatePath.class,
					"StatePath",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getStatePath_InitialState(),
						this.getStatePathNode(),
						null,
						"initialState",
						null,
						0,
						1,
						StatePath.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	statePathNodeEClass,
					StatePathNode.class,
					"StatePathNode",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getStatePathNode_State(),
						this.getState(),
						null,
						"state",
						null,
						1,
						1,
						StatePathNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStatePathNode_NextTransition(),
						this.getStatePathTransition(),
						null,
						"nextTransition",
						null,
						0,
						1,
						StatePathNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStatePathNode_IncomingTransition(),
						this.getStatePathTransition(),
						null,
						"incomingTransition",
						null,
						0,
						1,
						StatePathNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	statePathTransitionEClass,
					StatePathTransition.class,
					"StatePathTransition",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getStatePathTransition_NextStatePathNode(),
						this.getStatePathNode(),
						null,
						"nextStatePathNode",
						null,
						0,
						1,
						StatePathTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getStatePathTransition_TransitionName(),
						ecorePackage.getEString(),
						"transitionName",
						null,
						0,
						1,
						StatePathTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStatePathTransition_PreviousStatePathNode(),
						this.getStatePathNode(),
						null,
						"previousStatePathNode",
						null,
						0,
						1,
						StatePathTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	stateSpaceEClass,
					StateSpace.class,
					"StateSpace",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getStateSpace_InitialState(),
						this.getState(),
						null,
						"initialState",
						null,
						1,
						1,
						StateSpace.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEOperation(	getStateSpace__GetAllStateSpaceNodes(),
						this.getState(),
						"getAllStateSpaceNodes",
						0,
						-1,
						IS_UNIQUE,
						IS_ORDERED);

		initEOperation(	getStateSpace__GetAllStateSpaceTransitions(),
						this.getStateSpaceTransition(),
						"getAllStateSpaceTransitions",
						0,
						-1,
						IS_UNIQUE,
						IS_ORDERED);

		initEClass(	stateSpacePathVisEClass,
					StateSpacePathVis.class,
					"StateSpacePathVis",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getStateSpacePathVis_LifelineGroups(),
						this.getLifelineGroup(),
						null,
						"lifelineGroups",
						null,
						0,
						-1,
						StateSpacePathVis.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		g1 = createEGenericType(this.getTree());
		g2 = createEGenericType(this.getState());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getStateSpaceTransition());
		g1.getETypeArguments().add(g2);
		initEReference(	getStateSpacePathVis_StatePathTree(),
						g1,
						null,
						"statePathTree",
						null,
						0,
						1,
						StateSpacePathVis.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStateSpacePathVis_Statespace(),
						this.getStateSpace(),
						null,
						"statespace",
						null,
						0,
						1,
						StateSpacePathVis.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStateSpacePathVis_ModelHistory(),
						this.getModelHistory(),
						null,
						"modelHistory",
						null,
						0,
						1,
						StateSpacePathVis.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStateSpacePathVis_ModelHistoryBuilder(),
						this.getModelHistoryBuilder(),
						null,
						"modelHistoryBuilder",
						null,
						0,
						1,
						StateSpacePathVis.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStateSpacePathVis_LifelineBuilders(),
						this.getLifelineBuilder(),
						null,
						"lifelineBuilders",
						null,
						0,
						-1,
						StateSpacePathVis.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEOperation(	getStateSpacePathVis__UpdateModelHistory(),
						null,
						"updateModelHistory",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		initEOperation(	getStateSpacePathVis__UpdateLifelines(),
						null,
						"updateLifelines",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		initEClass(	stateSpaceTransitionEClass,
					StateSpaceTransition.class,
					"StateSpaceTransition",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getStateSpaceTransition_NextState(),
						this.getState(),
						this.getState_IncomingTransitions(),
						"nextState",
						null,
						1,
						1,
						StateSpaceTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getStateSpaceTransition_TransitionName(),
						ecorePackage.getEString(),
						"transitionName",
						null,
						0,
						1,
						StateSpaceTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getStateSpaceTransition_PrevState(),
						this.getState(),
						this.getState_OutgoingTransitions(),
						"prevState",
						null,
						1,
						1,
						StateSpaceTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getStateSpaceTransition_RawTransitionData(),
						ecorePackage.getEJavaObject(),
						"rawTransitionData",
						null,
						0,
						1,
						StateSpaceTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	modelHistoryEClass,
					ModelHistory.class,
					"ModelHistory",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(this.getObjectDescriptor());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getTree());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(this.getObjectHistoryNode());
		g2.getETypeArguments().add(g3);
		g3 = createEGenericType(this.getObjectHistoryTransition());
		g2.getETypeArguments().add(g3);
		initEAttribute(	getModelHistory_ObjectHistoryTrees(),
						g1,
						"objectHistoryTrees",
						null,
						0,
						1,
						ModelHistory.class,
						IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		op = initEOperation(getModelHistory__Update__ModelHistoryBuilder_Tree(),
							null,
							"update",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		addEParameter(	op,
						this.getModelHistoryBuilder(),
						"historyBuilder",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);
		g1 = createEGenericType(this.getTree());
		g2 = createEGenericType(this.getState());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getStateSpaceTransition());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "stateTree", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getModelHistory__GetObjectDescriptor__Object(),
							this.getObjectDescriptor(),
							"getObjectDescriptor",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		addEParameter(	op,
						ecorePackage.getEJavaObject(),
						"object",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		op = initEOperation(getModelHistory__CreateObjectDescriptor__Object(),
							this.getObjectDescriptor(),
							"createObjectDescriptor",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		addEParameter(	op,
						ecorePackage.getEJavaObject(),
						"object",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		op = initEOperation(getModelHistory__GetObjectHistory__ObjectDescriptor(),
							null,
							"getObjectHistory",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		addEParameter(	op,
						this.getObjectDescriptor(),
						"objectDescriptor",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);
		g1 = createEGenericType(this.getTree());
		g2 = createEGenericType(this.getObjectHistoryNode());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getObjectHistoryTransition());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEOperation(	getModelHistory__Clear(),
						null,
						"clear",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		op = initEOperation(getModelHistory__RegisterObjectToDescriptor__ObjectDescriptor_Object(),
							null,
							"registerObjectToDescriptor",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		addEParameter(	op,
						this.getObjectDescriptor(),
						"objectDescriptor",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);
		addEParameter(	op,
						ecorePackage.getEJavaObject(),
						"object",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		initEClass(	modelHistoryBuilderEClass,
					ModelHistoryBuilder.class,
					"ModelHistoryBuilder",
					IS_ABSTRACT,
					IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getModelHistoryBuilder__UpdateObjectHistory__Tree_ModelHistory(),
							null,
							"updateObjectHistory",
							0,
							1,
							IS_UNIQUE,
							IS_ORDERED);
		g1 = createEGenericType(this.getTree());
		g2 = createEGenericType(this.getState());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getStateSpaceTransition());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "stateTree", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(	op,
						this.getModelHistory(),
						"modelHistory",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		initEClass(	lifelineBuilderEClass,
					LifelineBuilder.class,
					"LifelineBuilder",
					IS_ABSTRACT,
					IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getLifelineBuilder__BuildLifelines__Tree_ModelHistory(),
							this.getLifeline(),
							"buildLifelines",
							0,
							-1,
							IS_UNIQUE,
							IS_ORDERED);
		g1 = createEGenericType(this.getTree());
		g2 = createEGenericType(this.getState());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getStateSpaceTransition());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "stateTree", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(	op,
						this.getModelHistory(),
						"modelHistory",
						0,
						1,
						IS_UNIQUE,
						IS_ORDERED);

		initEClass(	objectHistoryTransitionEClass,
					ObjectHistoryTransition.class,
					"ObjectHistoryTransition",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getObjectHistoryTransition_HistoryData(),
						this.getObjectHistoryData(),
						null,
						"historyData",
						null,
						0,
						-1,
						ObjectHistoryTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getObjectHistoryTransition_StateTransition(),
						this.getStateSpaceTransition(),
						null,
						"stateTransition",
						null,
						0,
						1,
						ObjectHistoryTransition.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	objectHistoryDataEClass,
					ObjectHistoryData.class,
					"ObjectHistoryData",
					IS_ABSTRACT,
					IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		initEClass(	objectHistoryDiffEClass,
					ObjectHistoryDiff.class,
					"ObjectHistoryDiff",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(	getObjectHistoryDiff_NewState(),
						this.getObjectStateType(),
						"newState",
						null,
						0,
						1,
						ObjectHistoryDiff.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectHistoryDiff_NewObjectInstance(),
						ecorePackage.getEJavaObject(),
						"newObjectInstance",
						null,
						0,
						1,
						ObjectHistoryDiff.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectHistoryDiff_OldObjectInstance(),
						ecorePackage.getEJavaObject(),
						"oldObjectInstance",
						null,
						0,
						1,
						ObjectHistoryDiff.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectHistoryDiff_RawDiffData(),
						ecorePackage.getEJavaObject(),
						"rawDiffData",
						null,
						0,
						1,
						ObjectHistoryDiff.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	objectHistoryNodeEClass,
					ObjectHistoryNode.class,
					"ObjectHistoryNode",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		initEReference(	getObjectHistoryNode_HistoryData(),
						this.getObjectHistoryData(),
						null,
						"historyData",
						null,
						0,
						-1,
						ObjectHistoryNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getObjectHistoryNode_ObjectDescriptor(),
						this.getObjectDescriptor(),
						null,
						"objectDescriptor",
						null,
						1,
						1,
						ObjectHistoryNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEAttribute(	getObjectHistoryNode_ObjectInstance(),
						ecorePackage.getEJavaObject(),
						"objectInstance",
						null,
						0,
						1,
						ObjectHistoryNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_UNSETTABLE,
						!IS_ID,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);
		initEReference(	getObjectHistoryNode_State(),
						this.getState(),
						null,
						"state",
						null,
						1,
						1,
						ObjectHistoryNode.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	statePathTreeNodeReferenceEClass,
					StatePathTreeNodeReference.class,
					"StatePathTreeNodeReference",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getTreeNode());
		g2 = createEGenericType(this.getState());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getStateSpaceTransition());
		g1.getETypeArguments().add(g2);
		initEReference(	getStatePathTreeNodeReference_StatePathTreeNode(),
						g1,
						null,
						"statePathTreeNode",
						null,
						1,
						1,
						StatePathTreeNodeReference.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		initEClass(	statePathReferenceEClass,
					StatePathReference.class,
					"StatePathReference",
					IS_ABSTRACT,
					IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		initEClass(	statePathTreeTransitionReferenceEClass,
					StatePathTreeTransitionReference.class,
					"StatePathTreeTransitionReference",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getTreeTransition());
		g2 = createEGenericType(this.getState());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getStateSpaceTransition());
		g1.getETypeArguments().add(g2);
		initEReference(	getStatePathTreeTransitionReference_StatePathTreeTransition(),
						g1,
						null,
						"statePathTreeTransition",
						null,
						1,
						1,
						StatePathTreeTransitionReference.class,
						!IS_TRANSIENT,
						!IS_VOLATILE,
						IS_CHANGEABLE,
						!IS_COMPOSITE,
						IS_RESOLVE_PROXIES,
						!IS_UNSETTABLE,
						IS_UNIQUE,
						!IS_DERIVED,
						IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(	objectStateTypeEEnum,
					ObjectStateType.class,
					"ObjectStateType");
		addEEnumLiteral(objectStateTypeEEnum, ObjectStateType.NULL);
		addEEnumLiteral(objectStateTypeEEnum, ObjectStateType.CHANGED);
		addEEnumLiteral(objectStateTypeEEnum, ObjectStateType.NOTNULL);

		// Create resource
		createResource(eNS_URI);
	}

} // StatespacevisPackageImpl
