/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>State Path</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath#getInitialState
 * <em>Initial State</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePath()
 * @model
 * @generated
 */
public interface StatePath extends EObject {
	/**
	 * Returns the value of the '<em><b>Initial State</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Initial State</em>' reference.
	 * @see #setInitialState(StatePathNode)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getStatePath_InitialState()
	 * @model
	 * @generated
	 */
	StatePathNode getInitialState();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath#getInitialState
	 * <em>Initial State</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Initial State</em>' reference.
	 * @see #getInitialState()
	 * @generated
	 */
	void setInitialState(StatePathNode value);

} // StatePath
