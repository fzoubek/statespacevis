/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Object History Data</b></em>'. <!-- end-user-doc -->
 * 
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getObjectHistoryData()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ObjectHistoryData extends EObject {
} // ObjectHistoryData
