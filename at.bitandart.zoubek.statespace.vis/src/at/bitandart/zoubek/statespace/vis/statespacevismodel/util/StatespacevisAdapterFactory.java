/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.util;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage
 * @generated
 */
public class StatespacevisAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static StatespacevisPackage	modelPackage;

	/**
	 * Creates an instance of the adapter factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public StatespacevisAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = StatespacevisPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatespacevisSwitch<Adapter>	modelSwitch	= new StatespacevisSwitch<Adapter>() {
															@Override
															public Adapter caseLifelineGroup(
																	LifelineGroup object) {
																return createLifelineGroupAdapter();
															}

															@Override
															public Adapter caseLifeline(
																	Lifeline object) {
																return createLifelineAdapter();
															}

															@Override
															public Adapter caseObjectLifeline(
																	ObjectLifeline object) {
																return createObjectLifelineAdapter();
															}

															@Override
															public Adapter caseObjectDescriptor(
																	ObjectDescriptor object) {
																return createObjectDescriptorAdapter();
															}

															@Override
															public Adapter caseObjectLifelineState(
																	ObjectLifelineState object) {
																return createObjectLifelineStateAdapter();
															}

															@Override
															public <N, T> Adapter caseTree(
																	Tree<N, T> object) {
																return createTreeAdapter();
															}

															@Override
															public <N, T> Adapter caseTreeNode(
																	TreeNode<N, T> object) {
																return createTreeNodeAdapter();
															}

															@Override
															public <N, T> Adapter caseTreeTransition(
																	TreeTransition<N, T> object) {
																return createTreeTransitionAdapter();
															}

															@Override
															public Adapter caseState(
																	State object) {
																return createStateAdapter();
															}

															@Override
															public Adapter caseStatePath(
																	StatePath object) {
																return createStatePathAdapter();
															}

															@Override
															public Adapter caseStatePathNode(
																	StatePathNode object) {
																return createStatePathNodeAdapter();
															}

															@Override
															public Adapter caseStatePathTransition(
																	StatePathTransition object) {
																return createStatePathTransitionAdapter();
															}

															@Override
															public Adapter caseStateSpace(
																	StateSpace object) {
																return createStateSpaceAdapter();
															}

															@Override
															public Adapter caseStateSpacePathVis(
																	StateSpacePathVis object) {
																return createStateSpacePathVisAdapter();
															}

															@Override
															public Adapter caseStateSpaceTransition(
																	StateSpaceTransition object) {
																return createStateSpaceTransitionAdapter();
															}

															@Override
															public Adapter caseModelHistory(
																	ModelHistory object) {
																return createModelHistoryAdapter();
															}

															@Override
															public Adapter caseModelHistoryBuilder(
																	ModelHistoryBuilder object) {
																return createModelHistoryBuilderAdapter();
															}

															@Override
															public Adapter caseLifelineBuilder(
																	LifelineBuilder object) {
																return createLifelineBuilderAdapter();
															}

															@Override
															public Adapter caseObjectHistoryTransition(
																	ObjectHistoryTransition object) {
																return createObjectHistoryTransitionAdapter();
															}

															@Override
															public Adapter caseObjectHistoryData(
																	ObjectHistoryData object) {
																return createObjectHistoryDataAdapter();
															}

															@Override
															public Adapter caseObjectHistoryDiff(
																	ObjectHistoryDiff object) {
																return createObjectHistoryDiffAdapter();
															}

															@Override
															public Adapter caseObjectHistoryNode(
																	ObjectHistoryNode object) {
																return createObjectHistoryNodeAdapter();
															}

															@Override
															public Adapter caseStatePathTreeNodeReference(
																	StatePathTreeNodeReference object) {
																return createStatePathTreeNodeReferenceAdapter();
															}

															@Override
															public Adapter caseStatePathReference(
																	StatePathReference object) {
																return createStatePathReferenceAdapter();
															}

															@Override
															public Adapter caseStatePathTreeTransitionReference(
																	StatePathTreeTransitionReference object) {
																return createStatePathTreeTransitionReferenceAdapter();
															}

															@Override
															public Adapter defaultCase(
																	EObject object) {
																return createEObjectAdapter();
															}
														};

	/**
	 * Creates an adapter for the <code>target</code>. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param target
	 *            the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup
	 * <em>Lifeline Group</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup
	 * @generated
	 */
	public Adapter createLifelineGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline
	 * <em>Lifeline</em>}'. <!-- begin-user-doc --> This default implementation
	 * returns null so that we can easily ignore cases; it's useful to ignore a
	 * case when inheritance will catch all the cases anyway. <!-- end-user-doc
	 * -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline
	 * @generated
	 */
	public Adapter createLifelineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis
	 * <em>State Space Path Vis</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis
	 * @generated
	 */
	public Adapter createStateSpacePathVisAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline
	 * <em>Object Lifeline</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline
	 * @generated
	 */
	public Adapter createObjectLifelineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath
	 * <em>State Path</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePath
	 * @generated
	 */
	public Adapter createStatePathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.State
	 * <em>State</em>}'. <!-- begin-user-doc --> This default implementation
	 * returns null so that we can easily ignore cases; it's useful to ignore a
	 * case when inheritance will catch all the cases anyway. <!-- end-user-doc
	 * -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.State
	 * @generated
	 */
	public Adapter createStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode
	 * <em>State Path Node</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathNode
	 * @generated
	 */
	public Adapter createStatePathNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition
	 * <em>State Path Transition</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTransition
	 * @generated
	 */
	public Adapter createStatePathTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace
	 * <em>State Space</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace
	 * @generated
	 */
	public Adapter createStateSpaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition
	 * <em>State Space Transition</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition
	 * @generated
	 */
	public Adapter createStateSpaceTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory
	 * <em>Model History</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory
	 * @generated
	 */
	public Adapter createModelHistoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * <em>Model History Builder</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * @generated
	 */
	public Adapter createModelHistoryBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * <em>Lifeline Builder</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * @generated
	 */
	public Adapter createLifelineBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition
	 * <em>Object History Transition</em>}'. <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition
	 * @generated
	 */
	public Adapter createObjectHistoryTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
	 * <em>Object History Data</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData
	 * @generated
	 */
	public Adapter createObjectHistoryDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff
	 * <em>Object History Diff</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff
	 * @generated
	 */
	public Adapter createObjectHistoryDiffAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode
	 * <em>Object History Node</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode
	 * @generated
	 */
	public Adapter createObjectHistoryNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference
	 * <em>State Path Tree Node Reference</em>}'. <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference
	 * @generated
	 */
	public Adapter createStatePathTreeNodeReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
	 * <em>State Path Reference</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference
	 * @generated
	 */
	public Adapter createStatePathReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference
	 * <em>State Path Tree Transition Reference</em>}'. <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore
	 * cases; it's useful to ignore a case when inheritance will catch all the
	 * cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference
	 * @generated
	 */
	public Adapter createStatePathTreeTransitionReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor
	 * <em>Object Descriptor</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor
	 * @generated
	 */
	public Adapter createObjectDescriptorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState
	 * <em>Object Lifeline State</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState
	 * @generated
	 */
	public Adapter createObjectLifelineStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree
	 * <em>Tree</em>}'. <!-- begin-user-doc --> This default implementation
	 * returns null so that we can easily ignore cases; it's useful to ignore a
	 * case when inheritance will catch all the cases anyway. <!-- end-user-doc
	 * -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree
	 * @generated
	 */
	public Adapter createTreeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode
	 * <em>Tree Node</em>}'. <!-- begin-user-doc --> This default implementation
	 * returns null so that we can easily ignore cases; it's useful to ignore a
	 * case when inheritance will catch all the cases anyway. <!-- end-user-doc
	 * -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode
	 * @generated
	 */
	public Adapter createTreeNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition
	 * <em>Tree Transition</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition
	 * @generated
	 */
	public Adapter createTreeTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case. <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // StatespacevisAdapterFactory
