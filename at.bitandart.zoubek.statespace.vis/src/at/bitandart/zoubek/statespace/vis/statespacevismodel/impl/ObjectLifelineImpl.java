/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.impl;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Object Lifeline</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineImpl#getObjectDescriptor
 * <em>Object Descriptor</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.ObjectLifelineImpl#getObjectStateTree
 * <em>Object State Tree</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ObjectLifelineImpl extends LifelineImpl implements ObjectLifeline {
	/**
	 * the visibility of this lifeline
	 */
	private boolean												visible	= true;
	/**
	 * The cached value of the '{@link #getObjectDescriptor()
	 * <em>Object Descriptor</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectDescriptor()
	 * @generated
	 * @ordered
	 */
	protected ObjectDescriptor									objectDescriptor;
	/**
	 * The cached value of the '{@link #getObjectStateTree()
	 * <em>Object State Tree</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getObjectStateTree()
	 * @generated
	 * @ordered
	 */
	protected Tree<StateSpaceTransition, ObjectLifelineState>	objectStateTree;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectLifelineImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatespacevisPackage.Literals.OBJECT_LIFELINE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectDescriptor getObjectDescriptor() {
		if (objectDescriptor != null && objectDescriptor.eIsProxy()) {
			InternalEObject oldObjectDescriptor = (InternalEObject) objectDescriptor;
			objectDescriptor = (ObjectDescriptor) eResolveProxy(oldObjectDescriptor);
			if (objectDescriptor != oldObjectDescriptor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.OBJECT_LIFELINE__OBJECT_DESCRIPTOR,
													oldObjectDescriptor,
													objectDescriptor));
			}
		}
		return objectDescriptor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectDescriptor basicGetObjectDescriptor() {
		return objectDescriptor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setObjectDescriptor(ObjectDescriptor newObjectDescriptor) {
		ObjectDescriptor oldObjectDescriptor = objectDescriptor;
		objectDescriptor = newObjectDescriptor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_LIFELINE__OBJECT_DESCRIPTOR,
											oldObjectDescriptor,
											objectDescriptor));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public Tree<StateSpaceTransition, ObjectLifelineState> getObjectStateTree() {
		if (objectStateTree != null && objectStateTree.eIsProxy()) {
			InternalEObject oldObjectStateTree = (InternalEObject) objectStateTree;
			objectStateTree = (Tree<StateSpaceTransition, ObjectLifelineState>) eResolveProxy(oldObjectStateTree);
			if (objectStateTree != oldObjectStateTree) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(	this,
													Notification.RESOLVE,
													StatespacevisPackage.OBJECT_LIFELINE__OBJECT_STATE_TREE,
													oldObjectStateTree,
													objectStateTree));
			}
		}
		return objectStateTree;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Tree<StateSpaceTransition, ObjectLifelineState> basicGetObjectStateTree() {
		return objectStateTree;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setObjectStateTree(
			Tree<StateSpaceTransition, ObjectLifelineState> newObjectStateTree) {
		Tree<StateSpaceTransition, ObjectLifelineState> oldObjectStateTree = objectStateTree;
		objectStateTree = newObjectStateTree;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(	this,
											Notification.SET,
											StatespacevisPackage.OBJECT_LIFELINE__OBJECT_STATE_TREE,
											oldObjectStateTree,
											objectStateTree));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_DESCRIPTOR:
				if (resolve)
					return getObjectDescriptor();
				return basicGetObjectDescriptor();
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_STATE_TREE:
				if (resolve)
					return getObjectStateTree();
				return basicGetObjectStateTree();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_DESCRIPTOR:
				setObjectDescriptor((ObjectDescriptor) newValue);
				return;
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_STATE_TREE:
				setObjectStateTree((Tree<StateSpaceTransition, ObjectLifelineState>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_DESCRIPTOR:
				setObjectDescriptor((ObjectDescriptor) null);
				return;
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_STATE_TREE:
				setObjectStateTree((Tree<StateSpaceTransition, ObjectLifelineState>) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_DESCRIPTOR:
				return objectDescriptor != null;
			case StatespacevisPackage.OBJECT_LIFELINE__OBJECT_STATE_TREE:
				return objectStateTree != null;
		}
		return super.eIsSet(featureID);
	}

} // ObjectLifelineImpl
