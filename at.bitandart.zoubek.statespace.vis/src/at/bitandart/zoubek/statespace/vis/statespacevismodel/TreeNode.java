/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel;

import java.lang.Object;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Tree Node</b></em>'. <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getChildTransitions
 * <em>Child Transitions</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getParentTransition
 * <em>Parent Transition</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getNodeValue
 * <em>Node Value</em>}</li>
 * </ul>
 * </p>
 * 
 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeNode()
 * @model
 * @generated
 */
public interface TreeNode<N, T> extends EObject {
	/**
	 * Returns the value of the '<em><b>Child Transitions</b></em>' reference
	 * list. The list contents are of type
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition}
	 * &lt;N, T>. It is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getPreviousNode
	 * <em>Previous Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Transitions</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Child Transitions</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeNode_ChildTransitions()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getPreviousNode
	 * @model opposite="previousNode"
	 * @generated
	 */
	EList<TreeTransition<N, T>> getChildTransitions();

	/**
	 * Returns the value of the '<em><b>Parent Transition</b></em>' reference.
	 * It is bidirectional and its opposite is '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getNextNode
	 * <em>Next Node</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Parent Transition</em>' reference.
	 * @see #setParentTransition(TreeTransition)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeNode_ParentTransition()
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition#getNextNode
	 * @model opposite="nextNode"
	 * @generated
	 */
	TreeTransition<N, T> getParentTransition();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getParentTransition
	 * <em>Parent Transition</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Parent Transition</em>' reference.
	 * @see #getParentTransition()
	 * @generated
	 */
	void setParentTransition(TreeTransition<N, T> value);

	/**
	 * Returns the value of the '<em><b>Node Value</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Value</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Node Value</em>' attribute.
	 * @see #setNodeValue(Object)
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage#getTreeNode_NodeValue()
	 * @model
	 * @generated
	 */
	N getNodeValue();

	/**
	 * Sets the value of the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#getNodeValue
	 * <em>Node Value</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Node Value</em>' attribute.
	 * @see #getNodeValue()
	 * @generated
	 */
	void setNodeValue(N value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @model
	 * @generated
	 */
	void addChildNode(TreeNode<N, T> childNode, TreeTransition<N, T> transition);

} // TreeNode
