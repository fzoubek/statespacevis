/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.editors.inputs;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisEditorInput implements IEditorInput {

	/**
	 * the data used for the state space path visualization
	 */
	private StateSpacePathVis model;

	public SSPVisEditorInput(StateSpacePathVis model) {
		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class adapter) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		// TODO replace with a more appropriate name
		return "SSPVis";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		// TODO replace with a more appropriate tooltip text
		return "";
	}

	/* Getter and Setter */

	/**
	 * @return the model
	 */
	public StateSpacePathVis getModel() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(StateSpacePathVis model) {
		this.model = model;
	}

}
