package at.bitandart.zoubek.statespace.vis.editors;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import at.bitandart.zoubek.statespace.vis.StateSpaceVisPlugin;
import at.bitandart.zoubek.statespace.vis.editors.inputs.SSPVisEditorInput;
import at.bitandart.zoubek.statespace.vis.gef.factories.SSPVisEditPartFactory;
import at.bitandart.zoubek.statespace.vis.outline.SSPVisOutline;
import at.bitandart.zoubek.statespace.vis.sspvis.listener.SSPVisEditorListener;
import at.bitandart.zoubek.statespace.vis.sspvis.listener.SSPVisEvent;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class StateSpacePathVisEditor extends EditorPart implements ITabbedPropertySheetPageContributor {

	public static final String		EDITOR_ID	= "at.bitandart.zoubek.statespace.vis.editors.StateSpacePathVis";

	private static Logger			LOG			= Logger.getLogger(StateSpacePathVisEditor.class.getCanonicalName());

	/**
	 * the GEF-<code>EditDomain</code>
	 */
	private EditDomain				editDomain;

	/**
	 * the GEF Viewer component
	 */
	private GraphicalViewer			viewer;

	/**
	 * the GEF EditPart factory
	 */
	private SSPVisEditPartFactory	editPartFactory;

	@Override
	public void doSave(IProgressMonitor monitor) {
		LOG.info("State space path visualization can't be saved.");
	}

	@Override
	public void doSaveAs() {
		LOG.info("State space path visualization can't be saved.");
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setInput(input);
		setSite(site);

	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		viewer = createGraphicalViewer(parent);
	}

	@Override
	public void setFocus() {
		if (viewer != null) {
			viewer.setFocus(viewer.getRootEditPart());
		}
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {

		// handle created GEF elements
		if (adapter == GraphicalViewer.class || adapter == EditPartViewer.class)
			return getGraphicalViewer();
		else if (adapter == CommandStack.class)
			return getEditDomain().getCommandStack();
		else if (adapter == EditDomain.class)
			return getEditDomain();
		else if (adapter == IContentOutlinePage.class)
			return new SSPVisOutline(getModel());
		else if (adapter == IPropertySheetPage.class)
			return new TabbedPropertySheetPage(this);

		return super.getAdapter(adapter);
	}

	/**
	 * creates and initializes a new GraphicalViewer and assigns it as a child
	 * of <code>parent</code>.
	 * 
	 * @param parent
	 *            the parent SWT component
	 * @return the viewer instance
	 */
	private GraphicalViewer createGraphicalViewer(Composite parent) {

		GraphicalViewer viewer = new ScrollingGraphicalViewer();
		viewer.createControl(parent);

		// configure the viewer
		viewer.getControl().setBackground(parent.getBackground());
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		// hook the viewer into the EditDomain
		getEditDomain().addViewer(viewer);

		// context menu
		MenuManager menuProvider = new MenuManager();
		viewer.setContextMenu(menuProvider);
		getSite().registerContextMenu(menuProvider, viewer);

		// set viewer as selection Provider
		getSite().setSelectionProvider(viewer);

		// set the edit part factory
		viewer.setEditPartFactory(getEditPartFactory());

		// initialize the viewer with the the model
		getModel().getStatePathTree().eAdapters().add(new EContentAdapter() {
			@Override
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				fireStateSpaceTreePathChangedEvent();
				updateModelHistoryAndLifelines();
			}
		});
		installTreeNotifier(getModel().getStatePathTree());
		fireStateSpaceTreePathChangedEvent();
		fireFilterDefinitionChangedEvent();
		updateModelHistoryAndLifelines();
		viewer.setContents(getModel());

		return viewer;
	}

	private void installTreeNotifier(
			Tree<State, StateSpaceTransition> statePathTree) {
		for (TreeNode<State, StateSpaceTransition> node : statePathTree.getAllNodes()) {
			node.eAdapters().add(treeUpdateListener);
		}

		for (TreeTransition<State, StateSpaceTransition> transition : statePathTree.getAllTransitions()) {
			transition.eAdapters().add(treeUpdateListener);
		}
	}

	private EContentAdapter	treeUpdateListener	= new TreeUpdateListener();

	class TreeUpdateListener extends EContentAdapter {
		@Override
		public void notifyChanged(Notification notification) {
			Object value = notification.getNewValue();
			if (notification.getEventType() == Notification.ADD
				|| notification.getEventType() == Notification.ADD_MANY
				|| notification.getEventType() == Notification.SET) {

				if (value instanceof TreeNode<?, ?>) {
					((EObject) value).eAdapters().add(treeUpdateListener);
				}
				if (value instanceof TreeTransition<?, ?>
					&& ((TreeTransition<?, ?>) value).getNextNode() != null) {
					((TreeTransition<?, ?>) value).getNextNode().eAdapters()
													.add(treeUpdateListener);
				}
			}
			fireStateSpaceTreePathChangedEvent();
			updateModelHistoryAndLifelines();
		}
	};

	/**
	 * forces an update of the model history and all lifelines
	 */
	private void updateModelHistoryAndLifelines() {
		LOG.log(Level.FINE,
				"Update of model history and lifelines requested...");
		StateSpacePathVis model = getModel();
		LOG.log(Level.FINER, "Performing model history update...");
		model.updateModelHistory();
		LOG.log(Level.FINER, "Updating lifelines...");
		model.updateLifelines();
		LOG.log(Level.FINE, "Finished update.");
	}

	/**
	 * notifies all listeners bound to the extension point
	 * <code>StateSpaceVisPlugin.EXT_POINT_SSPVIS_LISTENER_ID</code> that the
	 * state space tree path has been changed
	 */
	private void fireStateSpaceTreePathChangedEvent() {
		fireEvent(new SSPVisEvent(	this,
									SSPVisEvent.ET_STATE_SPACE_TREE_PATH_CHANGED));
	}

	/**
	 * notifies all listeners bound to the extension point
	 * <code>StateSpaceVisPlugin.EXT_POINT_SSPVIS_LISTENER_ID</code> that the
	 * filter definition for this editor has been changed
	 */
	private void fireFilterDefinitionChangedEvent() {
		fireEvent(new SSPVisEvent(	this,
									SSPVisEvent.ET_FILTER_DEFINITION_CHANGED));
	}

	/**
	 * notifies all listeners bound to the extension point
	 * <code>StateSpaceVisPlugin.EXT_POINT_SSPVIS_LISTENER_ID</code> of the
	 * given event
	 * 
	 * @param event
	 *            the event to fire
	 */
	private void fireEvent(SSPVisEvent event) {
		LOG.log(Level.FINER,
				"Firing new event of type \"{0}\" ...",
				event.getEventType());
		IConfigurationElement[] config = Platform.getExtensionRegistry()
													.getConfigurationElementsFor(StateSpaceVisPlugin.EXT_POINT_SSPVIS_LISTENER_ID);
		try {
			for (IConfigurationElement e : config) {

				final Object o = e.createExecutableExtension("class");
				if (o instanceof SSPVisEditorListener) {

					// make sure exceptions thrown in listeners
					// won't mess up the whole visualization
					SafeSSPVisListenerEvaluator runnable = new SafeSSPVisListenerEvaluator(	(SSPVisEditorListener) o,
																							event);
					SafeRunner.run(runnable);
				}
			}
		} catch (CoreException ex) {
			LOG.log(Level.SEVERE,
					"Exception during executeable creation for extension "
							+ StateSpaceVisPlugin.EXT_POINT_SSPVIS_LISTENER_ID,
					ex);
		}
	}

	/**
	 * 
	 * @return the GEF Viewer component
	 */
	private GraphicalViewer getGraphicalViewer() {
		return viewer;
	}

	/**
	 * 
	 * @return the model instance for this editor, may be null
	 */
	public StateSpacePathVis getModel() {
		IEditorInput input = getEditorInput();
		if (input instanceof SSPVisEditorInput) {
			return ((SSPVisEditorInput) input).getModel();
		}
		return null;
	}

	/**
	 * 
	 * @return the <code>EditDomain</code> used by this editor
	 */
	private EditDomain getEditDomain() {
		if (editDomain == null) {
			editDomain = new DefaultEditDomain(this);
		}
		return editDomain;
	}

	/**
	 * 
	 * @return the <code>EditPart</code>-Factory used by this editor
	 */
	private EditPartFactory getEditPartFactory() {
		if (editPartFactory == null) {
			editPartFactory = new SSPVisEditPartFactory();
		}
		return editPartFactory;
	}

	@Override
	public String getContributorId() {
		return EDITOR_ID;
	}

}

/**
 * Utility class that executes the <code>handleEvent()</code> method of a
 * <code>SSPVisEditorListener</code> safely. Thrown exceptions will be logged
 * with the default logger.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
class SafeSSPVisListenerEvaluator implements ISafeRunnable {

	private static Logger			LOG	= Logger.getLogger(SafeSSPVisListenerEvaluator.class.getCanonicalName());
	private SSPVisEditorListener	listener;
	private SSPVisEvent				event;

	/**
	 * @param listener
	 *            the listener to execute
	 * @param event
	 *            the event to pass to the listener
	 */
	public SafeSSPVisListenerEvaluator(SSPVisEditorListener listener,
			SSPVisEvent event) {
		super();
		this.listener = listener;
		this.event = event;
	}

	@Override
	public void handleException(Throwable e) {
		LOG.log(Level.SEVERE,
				"Exception thrown in SSPVis Listener "
						+ listener.getClass().getCanonicalName()
						+ "\n"
						+ ((e.getLocalizedMessage() != null)
								? e.getLocalizedMessage() : ""),
				e);
	}

	@Override
	public void run() throws Exception {
		listener.handleEvent(event);
	}
}
