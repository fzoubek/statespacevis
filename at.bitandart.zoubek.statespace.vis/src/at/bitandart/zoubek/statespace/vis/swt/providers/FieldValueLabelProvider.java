package at.bitandart.zoubek.statespace.vis.swt.providers;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import at.bitandart.zoubek.statespace.vis.swt.providers.data.FieldDescriptor;
import at.bitandart.zoubek.statespace.vis.swt.providers.data.RowDescriptor;

/**
 * A {@link ColumnLabelProvider} that uses the value of a
 * {@link FieldDescriptor}.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class FieldValueLabelProvider extends ColumnLabelProvider {

	@Override
	public String getText(Object element) {
		Object value = element;
		if (element instanceof RowDescriptor) {
			String valueString = ((RowDescriptor) element).getValueString(1);
			if(valueString != null){
				return valueString;
			}else{
				return "<null>";
			}
		}
		if (value != null) {
			return value.toString();
		}
		return "<null>";
	}
}