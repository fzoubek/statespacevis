/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.swt.providers.data;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public interface RowDescriptor {

	/**
	 * 
	 * @param index
	 * @return a string describing the value with the given id, or null if there
	 *         exists no value with the given index
	 */
	String getValueString(int index);

}
