/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.swt.providers.data;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A {@link RowDescriptor} representing a row describing a
 * {@link EStructuralFeature} and its associated values.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class EStructuralFeatureDescriptor implements RowDescriptor, ValuedDescriptor {

	private EStructuralFeature	feature;
	private Object				value;

	/* (non-Javadoc)
	 * @see at.bitandart.zoubek.statespace.vis.swt.providers.data.RowDescriptor#getValueString(int)
	 */
	@Override
	public String getValueString(int index) {
		switch (index) {
			case 0:
				if (feature != null) {
					return feature.getName()
							+ " : " + feature.getEType().getName();
				} else if (value != null) {
					if (value instanceof EObject) {
						return ((EObject) value).eClass().getName();
					} else {
						return value.getClass().getSimpleName();
					}

				}
			case 1:
				if (value != null) {
					return value.toString();
				} else {
					return "<null>";
				}
		}
		return null;
	}

	/**
	 * @return the feature
	 */
	public EStructuralFeature getFeature() {
		return feature;
	}

	/**
	 * @param feature
	 *            the feature to set
	 */
	public void setFeature(EStructuralFeature feature) {
		this.feature = feature;
	}

	@Override
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

}
