package at.bitandart.zoubek.statespace.vis.swt.providers;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import at.bitandart.zoubek.statespace.vis.swt.providers.data.FieldDescriptor;
import at.bitandart.zoubek.statespace.vis.swt.providers.data.RowDescriptor;

/**
 * A {@link ColumnLabelProvider} that uses the field of a
 * {@link FieldDescriptor}
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class FieldNameLabelProvider extends ColumnLabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof RowDescriptor) {
			String valueString = ((RowDescriptor) element).getValueString(0);
			if(valueString != null){
				return valueString;
			}
		} else if (element != null) {
			return element.getClass().getSimpleName();
		}
		return "<null>";
	}
}