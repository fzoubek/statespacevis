/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.swt.providers.data;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public interface ValuedDescriptor {

	/**
	 * 
	 * @return the value associated to this descriptor
	 */
	public Object getValue();
}
