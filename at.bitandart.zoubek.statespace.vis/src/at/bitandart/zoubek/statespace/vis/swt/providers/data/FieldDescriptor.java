package at.bitandart.zoubek.statespace.vis.swt.providers.data;

import java.lang.reflect.Field;

import org.eclipse.emf.ecore.EObject;

/**
 * A {@link RowDescriptor} representing a row describing a field and its
 * associated values.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class FieldDescriptor implements RowDescriptor, ValuedDescriptor {
	private Field	field;
	private Object	value;

	public FieldDescriptor() {
	}

	public FieldDescriptor(Field field, Object value) {
		this.field = field;
		this.value = value;
	}

	/**
	 * @return the field
	 */
	public Field getField() {
		return field;
	}

	/**
	 * @param field
	 *            the field to set
	 */
	public void setField(Field field) {
		this.field = field;
	}

	@Override
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String getValueString(int index) {
		switch (index) {
			case 0:
				if (field != null) {
					return field.getName()
							+ " : " + field.getType().getSimpleName();
				} else if (value != null) {
					if (value instanceof EObject) {
						((EObject) value).eClass().getName();
					} else {
						return value.getClass().getSimpleName();
					}
				}
			case 1:
				if (value != null) {
					return value.toString();
				} else {
					return "<null>";
				}
		}
		return null;
	}
}