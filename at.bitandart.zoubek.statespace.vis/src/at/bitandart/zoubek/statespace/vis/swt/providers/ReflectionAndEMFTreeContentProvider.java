package at.bitandart.zoubek.statespace.vis.swt.providers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILazyTreeContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import at.bitandart.zoubek.statespace.vis.swt.providers.data.CollectionItemDescriptor;
import at.bitandart.zoubek.statespace.vis.swt.providers.data.EStructuralFeatureDescriptor;
import at.bitandart.zoubek.statespace.vis.swt.providers.data.FieldDescriptor;
import at.bitandart.zoubek.statespace.vis.swt.providers.data.ValuedDescriptor;

/**
 * a {@link ITreeContentProvider} that creates {@link FieldDescriptor}s from the
 * given Input using the Java reflection API
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class ReflectionAndEMFTreeContentProvider implements ILazyTreeContentProvider {

	private TreeViewer			viewer;
	private Object				input;

	private static final Logger	LOG	= Logger.getLogger(ReflectionAndEMFTreeContentProvider.class.getCanonicalName());

	public ReflectionAndEMFTreeContentProvider(TreeViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		input = newInput;
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public void updateElement(Object parent, int index) {
		LOG.log(Level.FINE, "Updating element {0}...", parent);
		Object element = parent;

		// root element is an object and not an valued descriptor, so test what we have in our case		
		if (parent instanceof ValuedDescriptor) {
			element = ((ValuedDescriptor) parent).getValue();
		} else if (parent == input) {
			// root element
			CollectionItemDescriptor descriptor = new CollectionItemDescriptor(parent);
			viewer.replace(parent, 0, descriptor);
			if (getChildCount(descriptor) > 0) {
				viewer.setHasChildren(descriptor, true);
			}
			LOG.log(Level.FINE, "Update of element {0}  finished", parent);

			return;
		}

		// null objects don't have children
		if (element == null) {
			LOG.log(Level.FINE, "Element is null, nothing to update");
			return;
		}

		if (element instanceof Object[]) {
			// element is an array
			Object[] array = (Object[]) element;
			for (int i = 0; i < array.length; i++) {
				CollectionItemDescriptor descriptor = new CollectionItemDescriptor(array[i]);
				viewer.replace(parent, i, descriptor);
				if (getChildCount(descriptor) > 0) {
					viewer.setHasChildren(descriptor, true);
				}
			}
		} else if (element instanceof Collection<?>) {
			// element is a collection
			Collection<?> collection = (Collection<?>) element;
			int i = 0;
			for (Object collItem : collection) {
				CollectionItemDescriptor descriptor = new CollectionItemDescriptor(collItem);
				viewer.replace(parent, i, descriptor);
				if (getChildCount(descriptor) > 0) {
					viewer.setHasChildren(descriptor, true);
				}
				i++;
			}
		} else if (element instanceof EObject) {
			// element is an EObject
			EObject eObj = (EObject) element;
			EClass eClass = eObj.eClass();
			int i = 0;
			for (EStructuralFeature feature : eClass.getEAllStructuralFeatures()) {
				EStructuralFeatureDescriptor descriptor = new EStructuralFeatureDescriptor();
				descriptor.setFeature(feature);
				descriptor.setValue(eObj.eGet(feature));
				viewer.replace(parent, i, descriptor);
				if (getChildCount(descriptor) > 0) {
					viewer.setHasChildren(descriptor, true);
				}
				i++;
			}
		} else {
			// "normal" object
			Class<?> clazz = element.getClass();
			Field[] fields = getAllFields(clazz);
			for (int i = 0; i < fields.length; i++) {
				FieldDescriptor descriptor = new FieldDescriptor();
				Field field = fields[i];
				descriptor.setField(field);
				field.setAccessible(true);
				try {
					descriptor.setValue(field.get(element));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					LOG.log(Level.SEVERE,
							"Couldn't access field value of field " + field,
							e);
				}
				field.setAccessible(false);
				viewer.replace(parent, i, descriptor);
				if (getChildCount(descriptor) > 0) {
					viewer.setHasChildren(descriptor, true);
				}
			}
		}
		viewer.setChildCount(parent, getChildCount(parent));
		LOG.log(Level.FINE, "Update of element {0}  finished", parent);
	}

	@Override
	public void updateChildCount(Object parentElement, int currentChildCount) {
		viewer.setChildCount(parentElement, getChildCount(parentElement));
	}

	private Field[] getAllFields(Class<?> clazz) {
		List<Field> fields = new ArrayList<>();
		Class<?> currentClass = clazz;
		while (currentClass != null) {
			for (Field field : currentClass.getDeclaredFields()) {
				fields.add(field);
			}
			currentClass = currentClass.getSuperclass();
		}

		return fields.toArray(new Field[0]);
	}

	private int getChildCount(Object parentElement) {
		Object element = parentElement;
		if (parentElement instanceof ValuedDescriptor) {
			element = ((ValuedDescriptor) parentElement).getValue();
		} else if (parentElement == input) {
			return 1;
		}
		if (element instanceof Object[]) {
			return ((Object[]) element).length;
		}
		if (element instanceof Collection<?>) {
			return ((Collection<?>) element).size();
		}
		if (element instanceof EObject) {
			return ((EObject) element).eClass().getEAllStructuralFeatures()
										.size();
		}

		if (element == null) {
			return 0;
		}

		Class<?> clazz = element.getClass();
		return getAllFields(clazz).length;
	}

}