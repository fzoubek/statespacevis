/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.swt.providers.data;

import org.eclipse.emf.ecore.EObject;

/**
 * A {@link RowDescriptor} representing a row describing a single collection
 * item and its associated values.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class CollectionItemDescriptor implements RowDescriptor, ValuedDescriptor {

	private Object	value;

	public CollectionItemDescriptor(Object value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see at.bitandart.zoubek.statespace.vis.swt.providers.data.RowDescriptor#getValueString(int)
	 */
	@Override
	public String getValueString(int index) {
		switch (index) {
			case 0:
				if (value != null) {
					if (value instanceof EObject) {
						return ((EObject) value).eClass().getName();
					}
					return value.getClass().getSimpleName();
				}
				return "";
			case 1:
				if (value != null) {
					return value.toString();
				} else {
					return "<null>";
				}
		}
		return null;
	}

	@Override
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

}
