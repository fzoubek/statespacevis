/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.utils;

import org.eclipse.draw2d.geometry.Point;

/**
 * Represents a vector in 2D space. Components are stored with double precision.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class Vector2D {

	/**
	 * x component
	 */
	private double	x;

	/**
	 * y component
	 */
	private double	y;

	/**
	 * creates a new vector at the origin (0,0)
	 */
	public Vector2D() {
		this.x = 0;
		this.y = 0;
	}

	/**
	 * @param x
	 *            the x component
	 * @param y
	 *            the y component
	 */
	public Vector2D(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * creates a new vector using the given point.
	 * 
	 * @param point
	 */
	public Vector2D(Point point) {
		this.x = point.preciseX();
		this.y = point.preciseY();
	}

	/**
	 * creates a new vector using the same components as the given vector
	 * 
	 * @param other
	 */
	public Vector2D(Vector2D other) {
		this.x = other.getX();
		this.y = other.getY();
	}

	/**
	 * adds the given vector to the current
	 * 
	 * @param other
	 *            the vector to add
	 * @return this
	 */
	public Vector2D add(Vector2D other) {
		this.x += other.getX();
		this.y += other.getY();
		return this;
	}

	/**
	 * creates a new vector which is the result from the addition of the given
	 * vector to the current
	 * 
	 * @param other
	 *            the vector to add
	 * @return a new vector, which is the result from the addition of the given
	 */
	public Vector2D getAdded(Vector2D other) {
		Vector2D result = new Vector2D(this);
		result.subtract(other);
		return result;
	}

	/**
	 * subtracts the given vector from the current
	 * 
	 * @param other
	 *            the vector to substract
	 * @return this
	 */
	public Vector2D subtract(Vector2D other) {
		this.x -= other.getX();
		this.y -= other.getY();
		return this;
	}

	/**
	 * creates a new vector which is the result from the subtraction of the
	 * given vector from the current
	 * 
	 * @param other
	 *            the vector to substract
	 * @return a new vector, which is the result from the subtraction of the
	 *         given vector from the current
	 */
	public Vector2D getSubtracted(Vector2D other) {
		Vector2D result = new Vector2D(this);
		result.subtract(other);
		return result;
	}

	/**
	 * mutiplies this vector with the given scalar
	 * 
	 * @param scalar
	 * @return this
	 */
	public Vector2D multiply(double scalar) {
		this.x *= scalar;
		this.y *= scalar;
		return this;
	}

	/**
	 * creates a new vector which is the result from the multiplication of the
	 * given vector with the current
	 * 
	 * @param scalar
	 * @return a new vector, which is the result from the multiplication of the
	 *         given vector with the current
	 */
	public Vector2D getMultiplied(double scalar) {
		Vector2D result = new Vector2D(this);
		result.multiply(scalar);
		return result;
	}

	/**
	 * normalizes the vector
	 * 
	 * @return this
	 */
	public Vector2D normalize() {
		double length = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
		x /= length;
		y /= length;
		return this;
	}

	/**
	 * creates a new vector representing the normalized version of the current
	 * vector
	 * 
	 * @return a new vector, representing the normalized version of the current
	 */
	public Vector2D getNormalized() {
		Vector2D result = new Vector2D(this);
		result.normalize();
		return result;
	}
	
	public double getLength(){
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}

	// static methods

	/**
	 * calculates the distance between the given vectors
	 * 
	 * @param vecA
	 *            the first vector
	 * @param vecB
	 *            the second vector
	 * @return the distance between the given vectors
	 */
	public static double distance(Vector2D vecA, Vector2D vecB) {
		return Math.sqrt(Math.pow(vecA.getX() - vecB.getX(), 2)
							+ Math.pow(vecA.getY() - vecB.getY(), 2));
	}

	// Getter/Setter

	/**
	 * @return the x component
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x value to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y component
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y value to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Vector2D [x=" + x + ", y=" + y + "]";
	}
}
