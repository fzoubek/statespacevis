/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.utils;

import java.util.ListIterator;
import java.util.Stack;

import org.eclipse.emf.common.util.EList;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * Utility class for {@link Tree}s.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class TreeUtils {

	/**
	 * creates a string representation of the tree structure
	 * 
	 * @param tree
	 * @return a string representation of the given tree structure
	 */
	public static String toStructureString(Tree<?, ?> tree) {
		StringBuilder sb = new StringBuilder();

		TreeNode<?, ?> rootNode = tree.getRootNode();

		if (rootNode != null) {
			sb.append("o");
			
			// Start DFS
			Stack<Pair<Integer, TreeTransition<?, ?>>> unvisitedTransitions = new Stack<>();

			EList<?> rootChildren = rootNode.getChildTransitions();
			@SuppressWarnings("unchecked")
			ListIterator<TreeTransition<?, ?>> rootChildIterator = (ListIterator<TreeTransition<?, ?>>) rootChildren.listIterator(rootChildren.size());
			while (rootChildIterator.hasPrevious()) {
				unvisitedTransitions.push(new Pair<Integer, TreeTransition<?, ?>>(	0,
																					rootChildIterator.previous()));
			}

			while (!unvisitedTransitions.isEmpty()) {
				Pair<Integer, TreeTransition<?, ?>> pair = unvisitedTransitions.pop();
				int level = pair.value1.intValue();
				TreeTransition<?, ?> transition = pair.value2;
				TreeNode<?, ?> node = transition.getNextNode();
				if (sb.length() > 0 && sb.charAt(sb.length()-1) == '\n'){
					sb.append(" ");
					for(int i = 0; i < level; i++ ){
						if(containsPairWithLevel(unvisitedTransitions, i)){
							sb.append(" |  ");
						}else{
							sb.append("    ");
						}
					}
					sb.append(" |\n");
					sb.append(" ");
					for(int i = 0; i < level; i++ ){
						if(containsPairWithLevel(unvisitedTransitions, i)){
							sb.append(" |  ");
						}else{
							sb.append("    ");
						}
					}
					sb.append(" +-");
				}else if (node.getParentTransition() != null) {
					if (node.getParentTransition().getPreviousNode().getChildTransitions().size() > 1) {
						sb.append("-+-");
					}else{
						sb.append("---");
					}
				}
				sb.append("o");

				EList<?> childTransitions = node.getChildTransitions();
				if (childTransitions.isEmpty()) {
					sb.append("\n");
				} else {
					@SuppressWarnings("unchecked")
					ListIterator<TreeTransition<?, ?>> iterator = (ListIterator<TreeTransition<?, ?>>) childTransitions.listIterator(childTransitions.size());
					while (iterator.hasPrevious()) {
						unvisitedTransitions.push(new Pair<Integer, TreeTransition<?, ?>>(	level + 1,
																							iterator.previous()));
					}
				}
			}
		}
		return sb.toString();
	}
	
	private static boolean containsPairWithLevel(Stack<Pair<Integer, TreeTransition<?, ?>>> unvisitedTransitions, int level){
		for(Pair<Integer, ?> tmpPair : unvisitedTransitions){
			if(tmpPair.value1.intValue() == level){
				return true;
			}
		}
		return false;
	}

}

class Pair<S, T> {

	public Pair(S value1, T value2) {
		super();
		this.value1 = value1;
		this.value2 = value2;
	}

	public S	value1;
	public T	value2;
}
