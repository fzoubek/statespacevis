/**
 * 
 */
package at.bitandart.zoubek.statespace.vis;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;

/**
 * The default logging utility class for this plugin. Log messages are written
 * through the standard log mechanism from eclipse.
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 * @deprecated use the java logging API instead as it provides better logging
 *             capabilities than the log mechanism from eclipse
 * 
 */
public class StateSpaceVisLogger {

	public static final int	ERROR	= IStatus.ERROR;
	public static final int	INFO	= IStatus.INFO;
	public static final int	WARNING	= IStatus.WARNING;

	/**
	 * logs the given message with the given severity
	 * 
	 * @param severity
	 *            the severity, may be either INFO, WARNING or ERROR
	 * @param message
	 *            the message to log
	 */
	public static void log(int severity, String message) {
		Plugin plugin = StateSpaceVisPlugin.getInstance();
		ILog log = plugin.getLog();
		log.log(new Status(	severity,
							StateSpaceVisPlugin.getInstance().getBundle()
												.getSymbolicName(),
							message));
	}

	/**
	 * logs the given message with severity ERROR
	 * 
	 * @param message
	 *            the message to log
	 */
	public static void logError(String message) {
		log(ERROR, message);
	}

	/**
	 * logs the given message with severity WARNING
	 * 
	 * @param message
	 *            the message to log
	 */
	public static void logWarning(String message) {
		log(WARNING, message);
	}

	/**
	 * logs the given message with severity INFO
	 * 
	 * @param message
	 *            the message to log
	 */
	public static void logInfo(String message) {
		log(INFO, message);
	}
}
