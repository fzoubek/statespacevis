package at.bitandart.zoubek.statespace.vis;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import at.bitandart.zoubek.statespace.vis.views.StateSpaceView;

/**
 * Default perspective for the SSPVis visualization
 * 
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisPerspective implements IPerspectiveFactory {

	public static final String PERSPECTIVE_ID = "at.bitandart.zoubek.statespace.vis.SSPVisPerspective"; 
	
	@Override
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.addView("org.eclipse.ui.views.ContentOutline", IPageLayout.LEFT, 0.2f, editorArea);
		layout.addView(StateSpaceView.VIEW_ID, IPageLayout.BOTTOM, 0.5f, editorArea);
		layout.addView("org.eclipse.ui.views.PropertySheet", IPageLayout.LEFT, 0.5f, StateSpaceView.VIEW_ID);
	}

}
