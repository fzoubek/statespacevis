/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Model History</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#update(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder, at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree)
 * <em>Update</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectDescriptor(java.lang.Object)
 * <em>Get Object Descriptor</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#createObjectDescriptor(java.lang.Object)
 * <em>Create Object Descriptor</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor)
 * <em>Get Object History</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#clear()
 * <em>Clear</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#registerObjectToDescriptor(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor, java.lang.Object)
 * <em>Register Object To Descriptor</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class ModelHistoryTest extends TestCase {

	/**
	 * The fixture for this Model History test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModelHistory	fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ModelHistoryTest.class);
	}

	/**
	 * Constructs a new Model History test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModelHistoryTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Model History test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(ModelHistory fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Model History test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModelHistory getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatespacevisFactory.eINSTANCE.createModelHistory());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#update(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder, at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree)
	 * <em>Update</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#update(at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree)
	 * @generated
	 */
	public void testUpdate__ModelHistoryBuilder_Tree() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	private static int		TEST_GET_OBJECT_DESCRIPTOR_NUM_OBJECTS	= 30;
	private static Class<?>	TEST_GET_OBJECT_DESCRIPTOR_OBJECT_CLASS	= String.class;

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectDescriptor(java.lang.Object)
	 * <em>Get Object Descriptor</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectDescriptor(java.lang.Object)
	 * @generated NOT
	 */
	public void testGetObjectDescriptor__Object() throws InstantiationException,
			IllegalAccessException {
		ModelHistory history = getFixture();
		Object[] testObjs = new Object[TEST_GET_OBJECT_DESCRIPTOR_NUM_OBJECTS];
		ObjectDescriptor[] descriptors = new ObjectDescriptor[TEST_GET_OBJECT_DESCRIPTOR_NUM_OBJECTS];

		assertNull(history.getObjectDescriptor(TEST_GET_OBJECT_DESCRIPTOR_OBJECT_CLASS.newInstance()));

		for (int i = 0; i < TEST_GET_OBJECT_DESCRIPTOR_NUM_OBJECTS; i++) {
			testObjs[i] = TEST_GET_OBJECT_DESCRIPTOR_OBJECT_CLASS.newInstance();
			descriptors[i] = history.createObjectDescriptor(testObjs[i]);
		}

		for (int i = 0; i < TEST_GET_OBJECT_DESCRIPTOR_NUM_OBJECTS; i++) {
			ObjectDescriptor descriptor = history.getObjectDescriptor(testObjs[i]);
			assertNotNull(descriptor);
			assertEquals(descriptors[i], descriptor);
		}
	}

	private static Class<?>	TEST_CREATE_OBJECT_DESCRIPTOR_OBJECT_CLASS	= String.class;

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#createObjectDescriptor(java.lang.Object)
	 * <em>Create Object Descriptor</em>}' operation. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#createObjectDescriptor(java.lang.Object)
	 * @generated NOT
	 */
	public void testCreateObjectDescriptor__Object() throws InstantiationException,
			IllegalAccessException {
		ModelHistory history = getFixture();

		Object testObj = TEST_CREATE_OBJECT_DESCRIPTOR_OBJECT_CLASS.newInstance();
		ObjectDescriptor descriptor = history.createObjectDescriptor(testObj);

		assertNotNull(descriptor);
		assertNotNull(descriptor.getClassName());
		assertNotNull(descriptor.getInstanceName());

		ObjectDescriptor registeredDescriptor = history.getObjectDescriptor(testObj);
		assertNotNull(registeredDescriptor);
		assertEquals(descriptor, registeredDescriptor);
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor)
	 * <em>Get Object History</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#getObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor)
	 * @generated
	 */
	public void testGetObjectHistory__ObjectDescriptor() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#clear()
	 * <em>Clear</em>}' operation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#clear()
	 * @generated
	 */
	public void testClear() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#registerObjectToDescriptor(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor, java.lang.Object)
	 * <em>Register Object To Descriptor</em>}' operation. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory#registerObjectToDescriptor(at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor,
	 *      java.lang.Object)
	 * @generated
	 */
	public void testRegisterObjectToDescriptor__ObjectDescriptor_Object() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} // ModelHistoryTest
