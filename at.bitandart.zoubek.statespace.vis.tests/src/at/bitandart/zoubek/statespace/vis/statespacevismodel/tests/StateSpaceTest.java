/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>State Space</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceNodes()
 * <em>Get All State Space Nodes</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceTransitions()
 * <em>Get All State Space Transitions</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class StateSpaceTest extends TestCase {

	private List<State>					allNodes;
	private List<StateSpaceTransition>	allTransitions;
	/**
	 * The fixture for this State Space test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected StateSpace				fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StateSpaceTest.class);
	}

	/**
	 * Constructs a new State Space test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StateSpaceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this State Space test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(StateSpace fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this State Space test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StateSpace getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated NOT
	 */
	@Override
	protected void setUp() throws Exception {
		StatespacevisFactory modelFactory = StatespacevisFactory.eINSTANCE;
		StateSpace stateSpace = modelFactory.createStateSpace();
		setFixture(stateSpace);

		// create states & state space nodes

		State initialState = modelFactory.createState();
		stateSpace.setInitialState(initialState);

		State state1 = modelFactory.createState();

		State state2 = modelFactory.createState();

		State state3 = modelFactory.createState();

		allNodes = new ArrayList<>();
		allNodes.add(initialState);
		allNodes.add(state1);
		allNodes.add(state2);
		allNodes.add(state3);

		// create state space Transitions
		// ------------------------------

		// iS -> s1

		StateSpaceTransition initialStateToState1 = modelFactory.createStateSpaceTransition();
		initialStateToState1.setNextState(state1);
		initialStateToState1.setPrevState(initialState);

		// s1 -> s2

		StateSpaceTransition state1ToState2 = modelFactory.createStateSpaceTransition();
		state1ToState2.setNextState(state2);
		state1ToState2.setPrevState(state1);

		// s2 -> s3

		StateSpaceTransition state2ToState3 = modelFactory.createStateSpaceTransition();
		state2ToState3.setNextState(state3);
		state2ToState3.setPrevState(state2);

		// s1 -> s1

		StateSpaceTransition state1ToState1 = modelFactory.createStateSpaceTransition();
		state1ToState1.setNextState(state1);
		state1ToState1.setPrevState(state1);

		// s1 -> iS

		StateSpaceTransition state1ToInitialState = modelFactory.createStateSpaceTransition();
		state1ToInitialState.setNextState(initialState);
		state1ToInitialState.setPrevState(state1);

		allTransitions = new ArrayList<>();
		allTransitions.add(initialStateToState1);
		allTransitions.add(state1ToInitialState);
		allTransitions.add(state1ToState1);
		allTransitions.add(state2ToState3);
		allTransitions.add(state1ToState2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceNodes()
	 * <em>Get All State Space Nodes</em>}' operation. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceNodes()
	 * @generated NOT
	 */
	public void testGetAllStateSpaceNodes() {
		EList<State> nodes = getFixture().getAllStateSpaceNodes();
		assert (allNodes.containsAll(nodes));
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceTransitions()
	 * <em>Get All State Space Transitions</em>}' operation. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace#getAllStateSpaceTransitions()
	 * @generated NOT
	 */
	public void testGetAllStateSpaceTransitions() {
		EList<StateSpaceTransition> transitions = getFixture().getAllStateSpaceTransitions();
		assert (allTransitions.containsAll(transitions));
	}

} // StateSpaceTest
