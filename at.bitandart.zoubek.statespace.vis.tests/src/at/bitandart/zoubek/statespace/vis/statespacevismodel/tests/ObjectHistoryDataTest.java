/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Object History Data</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public abstract class ObjectHistoryDataTest extends TestCase {

	/**
	 * The fixture for this Object History Data test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectHistoryData	fixture	= null;

	/**
	 * Constructs a new Object History Data test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryDataTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Object History Data test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(ObjectHistoryData fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Object History Data test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectHistoryData getFixture() {
		return fixture;
	}

} // ObjectHistoryDataTest
