/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Tree Node</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#addChildNode(at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode, at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition)
 * <em>Add Child Node</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TreeNodeTest extends TestCase {

	/**
	 * The fixture for this Tree Node test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected TreeNode<?, ?>	fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TreeNodeTest.class);
	}

	/**
	 * Constructs a new Tree Node test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeNodeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Tree Node test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(TreeNode<?, ?> fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Tree Node test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected TreeNode<?, ?> getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated NOT
	 */
	@Override
	protected void setUp() throws Exception {
		StatespacevisFactory factory = StatespacevisFactory.eINSTANCE;
		TreeNode<String, String> node = factory.createTreeNode();
		setFixture(node);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#addChildNode(at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode, at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition)
	 * <em>Add Child Node</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode#addChildNode(at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition)
	 * @generated NOT
	 */
	public void testAddChildNode__TreeNode_TreeTransition() {
		StatespacevisFactory factory = StatespacevisFactory.eINSTANCE;
		@SuppressWarnings("unchecked")
		TreeNode<String, String> node = (TreeNode<String, String>) getFixture();
		TreeTransition<String, String> transition = factory.createTreeTransition();
		TreeNode<String, String> childNode = factory.createTreeNode();

		node.addChildNode(childNode, transition);

		assertEquals(childNode.getParentTransition(), transition);
		assertTrue(node.getChildTransitions().contains(transition));
		assertEquals(transition.getPreviousNode(), node);
		assertEquals(transition.getNextNode(), childNode);
	}

} // TreeNodeTest
