/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Tree</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllNodes()
 * <em>Get All Nodes</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllTransitions()
 * <em>Get All Transitions</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsNodeValue(java.lang.Object)
 * <em>Contains Node Value</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsTransitionValue(java.lang.Object)
 * <em>Contains Transition Value</em>}</li>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getMaximumDepth()
 * <em>Get Maximum Depth</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TreeTest extends TestCase {

	private List<TreeNode<String, String>>			allNodes;
	private List<TreeTransition<String, String>>	allTransitions;

	/**
	 * The fixture for this Tree test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected Tree<?, ?>							fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TreeTest.class);
	}

	/**
	 * Constructs a new Tree test case with the given name. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public TreeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Tree test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(Tree<?, ?> fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Tree test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected Tree<?, ?> getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated NOT
	 */
	@Override
	protected void setUp() throws Exception {
		StatespacevisFactory modelFactory = StatespacevisFactory.eINSTANCE;
		Tree<String, String> tree = modelFactory.createTree();
		setFixture(tree);

		TreeNode<String, String> rootNode = modelFactory.createTreeNode();
		rootNode.setNodeValue("rootNode");
		tree.setRootNode(rootNode);

		TreeNode<String, String> node1 = modelFactory.createTreeNode();
		rootNode.setNodeValue("node1");

		TreeNode<String, String> node2 = modelFactory.createTreeNode();
		rootNode.setNodeValue("node2");

		TreeNode<String, String> node3 = modelFactory.createTreeNode();
		rootNode.setNodeValue("node3");

		TreeNode<String, String> node4 = modelFactory.createTreeNode();
		rootNode.setNodeValue("node4");

		allNodes = new ArrayList<>();
		allNodes.add(rootNode);
		allNodes.add(node1);
		allNodes.add(node2);
		allNodes.add(node3);
		allNodes.add(node4);

		TreeTransition<String, String> rTo1 = modelFactory.createTreeTransition();
		rTo1.setTransitionValue("rTo1");
		rTo1.setNextNode(node1);
		rTo1.setPreviousNode(rootNode);
		rootNode.getChildTransitions().add(rTo1);
		node1.setParentTransition(rTo1);

		TreeTransition<String, String> rTo2 = modelFactory.createTreeTransition();
		rTo2.setTransitionValue("rTo2");
		rTo2.setNextNode(node2);
		rTo2.setPreviousNode(rootNode);
		rootNode.getChildTransitions().add(rTo2);
		node2.setParentTransition(rTo2);

		TreeTransition<String, String> rTo3 = modelFactory.createTreeTransition();
		rTo3.setTransitionValue("rTo3");
		rTo3.setNextNode(node3);
		rTo3.setPreviousNode(rootNode);
		rootNode.getChildTransitions().add(rTo3);
		node3.setParentTransition(rTo3);

		TreeTransition<String, String> n2To4 = modelFactory.createTreeTransition();
		n2To4.setTransitionValue("2To4");
		n2To4.setNextNode(node4);
		n2To4.setPreviousNode(node2);
		node2.getChildTransitions().add(n2To4);
		node4.setParentTransition(n2To4);

		allTransitions = new ArrayList<>();
		allTransitions.add(rTo1);
		allTransitions.add(rTo2);
		allTransitions.add(rTo3);
		allTransitions.add(n2To4);

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllNodes()
	 * <em>Get All Nodes</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllNodes()
	 * @generated NOT
	 */
	public void testGetAllNodes() {
		EList<?> nodes = getFixture().getAllNodes();
		assertTrue(allNodes.containsAll(nodes));
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllTransitions()
	 * <em>Get All Transitions</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getAllTransitions()
	 * @generated NOT
	 */
	public void testGetAllTransitions() {
		EList<?> transitions = getFixture().getAllTransitions();
		assertTrue(allTransitions.containsAll(transitions));
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsNodeValue(java.lang.Object)
	 * <em>Contains Node Value</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsNodeValue(java.lang.Object)
	 * @generated
	 */
	public void testContainsNodeValue__Object() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsTransitionValue(java.lang.Object)
	 * <em>Contains Transition Value</em>}' operation. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#containsTransitionValue(java.lang.Object)
	 * @generated
	 */
	public void testContainsTransitionValue__Object() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getMaximumDepth()
	 * <em>Get Maximum Depth</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree#getMaximumDepth()
	 * @generated
	 */
	public void testGetMaximumDepth() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} // TreeTest
