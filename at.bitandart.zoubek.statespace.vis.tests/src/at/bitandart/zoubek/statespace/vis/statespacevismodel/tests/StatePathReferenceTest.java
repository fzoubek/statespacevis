/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathReference;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>State Path Reference</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public abstract class StatePathReferenceTest extends TestCase {

	/**
	 * The fixture for this State Path Reference test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathReference	fixture	= null;

	/**
	 * Constructs a new State Path Reference test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathReferenceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this State Path Reference test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(StatePathReference fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this State Path Reference test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathReference getFixture() {
		return fixture;
	}

} // StatePathReferenceTest
