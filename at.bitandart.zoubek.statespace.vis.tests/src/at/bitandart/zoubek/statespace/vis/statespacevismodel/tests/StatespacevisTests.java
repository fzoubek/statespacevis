/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test suite for the '
 * <em><b>statespacevismodel</b></em>' package. <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatespacevisTests extends TestSuite {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new StatespacevisTests("statespacevismodel Tests");
		suite.addTestSuite(TreeTest.class);
		suite.addTestSuite(TreeNodeTest.class);
		suite.addTestSuite(StateSpaceTest.class);
		suite.addTestSuite(StateSpacePathVisTest.class);
		suite.addTestSuite(ModelHistoryTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatespacevisTests(String name) {
		super(name);
	}

} // StatespacevisTests
