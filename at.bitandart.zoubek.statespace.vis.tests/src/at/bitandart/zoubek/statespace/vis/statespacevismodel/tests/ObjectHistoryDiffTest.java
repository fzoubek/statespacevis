/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Object History Diff</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class ObjectHistoryDiffTest extends TestCase {

	/**
	 * The fixture for this Object History Diff test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectHistoryDiff	fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ObjectHistoryDiffTest.class);
	}

	/**
	 * Constructs a new Object History Diff test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ObjectHistoryDiffTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Object History Diff test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(ObjectHistoryDiff fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Object History Diff test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ObjectHistoryDiff getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatespacevisFactory.eINSTANCE.createObjectHistoryDiff());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} // ObjectHistoryDiffTest
