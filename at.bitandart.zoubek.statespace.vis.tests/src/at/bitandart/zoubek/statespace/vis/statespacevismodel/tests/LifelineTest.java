/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Lifeline</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public abstract class LifelineTest extends TestCase {

	/**
	 * The fixture for this Lifeline test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected Lifeline	fixture	= null;

	/**
	 * Constructs a new Lifeline test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LifelineTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Lifeline test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(Lifeline fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Lifeline test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected Lifeline getFixture() {
		return fixture;
	}

} // LifelineTest
