/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Lifeline Group</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class LifelineGroupTest extends TestCase {

	/**
	 * The fixture for this Lifeline Group test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected LifelineGroup	fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LifelineGroupTest.class);
	}

	/**
	 * Constructs a new Lifeline Group test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LifelineGroupTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Lifeline Group test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(LifelineGroup fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Lifeline Group test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected LifelineGroup getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatespacevisFactory.eINSTANCE.createLifelineGroup());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} // LifelineGroupTest
