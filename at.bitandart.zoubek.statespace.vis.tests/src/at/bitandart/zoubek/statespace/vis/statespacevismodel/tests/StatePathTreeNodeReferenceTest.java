/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>State Path Tree Node Reference</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatePathTreeNodeReferenceTest extends TestCase {

	/**
	 * The fixture for this State Path Tree Node Reference test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathTreeNodeReference	fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StatePathTreeNodeReferenceTest.class);
	}

	/**
	 * Constructs a new State Path Tree Node Reference test case with the given
	 * name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTreeNodeReferenceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this State Path Tree Node Reference test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(StatePathTreeNodeReference fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this State Path Tree Node Reference test case.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathTreeNodeReference getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatespacevisFactory.eINSTANCE.createStatePathTreeNodeReference());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} // StatePathTreeNodeReferenceTest
