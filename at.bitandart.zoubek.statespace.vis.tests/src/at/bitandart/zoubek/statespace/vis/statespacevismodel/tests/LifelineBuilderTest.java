/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Lifeline Builder</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
 * <em>Build Lifelines</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class LifelineBuilderTest extends TestCase {

	/**
	 * The fixture for this Lifeline Builder test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected LifelineBuilder	fixture	= null;

	/**
	 * Constructs a new Lifeline Builder test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LifelineBuilderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Lifeline Builder test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(LifelineBuilder fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Lifeline Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected LifelineBuilder getFixture() {
		return fixture;
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * <em>Build Lifelines</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder#buildLifelines(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * @generated
	 */
	public void testBuildLifelines__Tree_ModelHistory() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} // LifelineBuilderTest
