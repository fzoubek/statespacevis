/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>State Path Tree Transition Reference</b></em>'. <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatePathTreeTransitionReferenceTest extends TestCase {

	/**
	 * The fixture for this State Path Tree Transition Reference test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathTreeTransitionReference	fixture	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StatePathTreeTransitionReferenceTest.class);
	}

	/**
	 * Constructs a new State Path Tree Transition Reference test case with the
	 * given name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public StatePathTreeTransitionReferenceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this State Path Tree Transition Reference test case.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(StatePathTreeTransitionReference fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this State Path Tree Transition Reference test
	 * case. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected StatePathTreeTransitionReference getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatespacevisFactory.eINSTANCE.createStatePathTreeTransitionReference());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} // StatePathTreeTransitionReferenceTest
