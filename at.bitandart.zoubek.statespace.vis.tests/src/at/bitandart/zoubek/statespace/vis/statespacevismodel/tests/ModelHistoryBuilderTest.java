/**
 */
package at.bitandart.zoubek.statespace.vis.statespacevismodel.tests;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Model History Builder</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>
 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
 * <em>Update Object History</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public abstract class ModelHistoryBuilderTest extends TestCase {

	/**
	 * The fixture for this Model History Builder test case. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModelHistoryBuilder	fixture	= null;

	/**
	 * Constructs a new Model History Builder test case with the given name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ModelHistoryBuilderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Model History Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(ModelHistoryBuilder fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Model History Builder test case. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ModelHistoryBuilder getFixture() {
		return fixture;
	}

	/**
	 * Tests the '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree, at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * <em>Update Object History</em>}' operation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder#updateObjectHistory(at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree,
	 *      at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory)
	 * @generated
	 */
	public void testUpdateObjectHistory__Tree_ModelHistory() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} // ModelHistoryBuilderTest
