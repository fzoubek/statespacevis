# StateSpaceVis plugins for Eclipse

The StateSpaceVis plugins provide a visualization for objects and expressions of paths within a state space.

## LICENSE

The StateSpaceVis plugins and sources are provided under the terms of the Eclipse Public Licence version 1.0. 
The full text of the EPLv1.0 can be found at http://www.eclipse.org/org/documents/epl-v10.html