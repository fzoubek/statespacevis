/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>EMF Object Lifeline Builder</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc --> This builder builds lifelines from model snapshots
 * that completly consists of instances of <code>EObject</code>s. <!--
 * end-model-doc -->
 * 
 * 
 * @see at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage#getEMFObjectLifelineBuilder()
 * @model
 * @generated
 */
public interface EMFObjectLifelineBuilder extends LifelineBuilder {

} // EMFObjectLifelineBuilder
