/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EMF Lifeline Builder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This builder builds lifelines from model snapshots that completly consists of instances of <code>EObject</code>s.
 * <!-- end-model-doc -->
 *
 *
 * @see at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage#getEMFLifelineBuilder()
 * @model
 * @generated
 */
public interface EMFLifelineBuilder extends LifelineBuilder {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Lifeline> buildLifelines(ModelHistory modelHistory);
} // EMFLifelineBuilder
