/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * 
 * @see at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage
 * @generated
 */
public interface SSPVisEMFFactory extends EFactory {
	/**
	 * The singleton instance of the factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	SSPVisEMFFactory	eINSTANCE	= at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>EMF Object Lifeline Builder</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>EMF Object Lifeline Builder</em>'.
	 * @generated
	 */
	EMFObjectLifelineBuilder createEMFObjectLifelineBuilder();

	/**
	 * Returns a new object of class '<em>EMF Compare History Builder</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>EMF Compare History Builder</em>'.
	 * @generated
	 */
	EMFCompareHistoryBuilder createEMFCompareHistoryBuilder();

	/**
	 * Returns the package supported by this factory. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the package supported by this factory.
	 * @generated
	 */
	SSPVisEMFPackage getSSPVisEMFPackage();

} // SSPVisEMFFactory
