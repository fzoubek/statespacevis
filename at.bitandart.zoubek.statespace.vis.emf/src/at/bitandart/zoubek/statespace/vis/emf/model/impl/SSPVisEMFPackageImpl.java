/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.impl;

import at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder;
import at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFFactory;
import at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class SSPVisEMFPackageImpl extends EPackageImpl implements SSPVisEMFPackage {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	emfObjectLifelineBuilderEClass	= null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private EClass	emfCompareHistoryBuilderEClass	= null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SSPVisEMFPackageImpl() {
		super(eNS_URI, SSPVisEMFFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static boolean	isInited	= false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link SSPVisEMFPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SSPVisEMFPackage init() {
		if (isInited)
			return (SSPVisEMFPackage) EPackage.Registry.INSTANCE.getEPackage(SSPVisEMFPackage.eNS_URI);

		// Obtain or create and register package
		SSPVisEMFPackageImpl theSSPVisEMFPackage = (SSPVisEMFPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SSPVisEMFPackageImpl
				? EPackage.Registry.INSTANCE.get(eNS_URI)
				: new SSPVisEMFPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StatespacevisPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSSPVisEMFPackage.createPackageContents();

		// Initialize created meta-data
		theSSPVisEMFPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSSPVisEMFPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(	SSPVisEMFPackage.eNS_URI,
										theSSPVisEMFPackage);
		return theSSPVisEMFPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getEMFObjectLifelineBuilder() {
		return emfObjectLifelineBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EClass getEMFCompareHistoryBuilder() {
		return emfCompareHistoryBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SSPVisEMFFactory getSSPVisEMFFactory() {
		return (SSPVisEMFFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isCreated	= false;

	/**
	 * Creates the meta-model objects for the package. This method is guarded to
	 * have no affect on any invocation but its first. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		emfObjectLifelineBuilderEClass = createEClass(EMF_OBJECT_LIFELINE_BUILDER);

		emfCompareHistoryBuilderEClass = createEClass(EMF_COMPARE_HISTORY_BUILDER);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private boolean	isInitialized	= false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StatespacevisPackage theStatespacevisPackage = (StatespacevisPackage) EPackage.Registry.INSTANCE.getEPackage(StatespacevisPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		emfObjectLifelineBuilderEClass.getESuperTypes()
										.add(theStatespacevisPackage.getLifelineBuilder());
		emfCompareHistoryBuilderEClass.getESuperTypes()
										.add(theStatespacevisPackage.getModelHistoryBuilder());

		// Initialize classes, features, and operations; add parameters
		initEClass(	emfObjectLifelineBuilderEClass,
					EMFObjectLifelineBuilder.class,
					"EMFObjectLifelineBuilder",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		initEClass(	emfCompareHistoryBuilderEClass,
					EMFCompareHistoryBuilder.class,
					"EMFCompareHistoryBuilder",
					!IS_ABSTRACT,
					!IS_INTERFACE,
					IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} // SSPVisEMFPackageImpl
