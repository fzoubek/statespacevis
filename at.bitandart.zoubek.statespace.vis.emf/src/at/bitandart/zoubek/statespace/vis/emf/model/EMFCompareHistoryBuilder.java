/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>EMF Compare History Builder</b></em>'. <!-- end-user-doc -->
 * 
 * <!-- begin-model-doc --> This class represents a history builder for model
 * snapshots consisting of <code>EObject</code>s. EMF Compare is used for
 * matching of objects in two model snapshots. <!-- end-model-doc -->
 * 
 * 
 * @see at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage#getEMFCompareHistoryBuilder()
 * @model
 * @generated
 */
public interface EMFCompareHistoryBuilder extends ModelHistoryBuilder {
} // EMFCompareHistoryBuilder
