/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.ResourceAttachmentChange;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder;
import at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Lifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifelineState;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>EMF Object Lifeline Builder</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class EMFObjectLifelineBuilderImpl extends MinimalEObjectImpl.Container implements EMFObjectLifelineBuilder {

	private static final Logger	LOG	= Logger.getLogger(EMFObjectLifelineBuilderImpl.class.getCanonicalName());

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EMFObjectLifelineBuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SSPVisEMFPackage.Literals.EMF_OBJECT_LIFELINE_BUILDER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<Lifeline> buildLifelines(
			Tree<State, StateSpaceTransition> stateTree,
			ModelHistory modelHistory) {

		EList<Lifeline> lifelines = new BasicEList<Lifeline>();

		Map<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>> objectHistoryTrees = modelHistory.getObjectHistoryTrees();

		for (ObjectDescriptor objDescriptor : objectHistoryTrees.keySet()) {

			LOG.log(Level.FINE,
					"building lifeline for instance {0}",
					objDescriptor.getInstanceName());

			// create object lifeline
			ObjectLifeline lifeline = StatespacevisFactory.eINSTANCE.createObjectLifeline();
			lifeline.setObjectDescriptor(objDescriptor);
			Tree<StateSpaceTransition, ObjectLifelineState> objectStateTree = StatespacevisFactory.eINSTANCE.createTree();
			lifeline.setObjectStateTree(objectStateTree);

			// create child object lifelines, if possible

			List<ObjectLifeline> childLifelines = null;
			EList<EStructuralFeature> structuralFeatures = null;
			Map<EStructuralFeature, ObjectLifeline> featureToLifelineMap = new HashMap<>();
			Map<EStructuralFeature, Map<TreeNode<ObjectHistoryNode, ObjectHistoryTransition>, TreeNode<StateSpaceTransition, ObjectLifelineState>>> childNodeMap = new HashMap<>();

			if (objDescriptor.getClassDescriptor() instanceof EClass) {
				EClass eClass = (EClass) objDescriptor.getClassDescriptor();
				structuralFeatures = eClass.getEAllStructuralFeatures();
				childLifelines = new ArrayList<>(structuralFeatures.size());
				for (EStructuralFeature feature : structuralFeatures) {
					ObjectDescriptor childObjDescriptor = StatespacevisFactory.eINSTANCE.createObjectDescriptor();
					childObjDescriptor.setInstanceName(feature.getName());
					childObjDescriptor.setClassName(feature.getEType()
															.getName());
					childObjDescriptor.setObjectID(-1);

					ObjectLifeline childObjectLifeline = StatespacevisFactory.eINSTANCE.createObjectLifeline();
					childObjectLifeline.setParentLifeline(lifeline);
					childObjectLifeline.setObjectDescriptor(childObjDescriptor);

					Tree<StateSpaceTransition, ObjectLifelineState> childTree = StatespacevisFactory.eINSTANCE.createTree();
					childObjectLifeline.setObjectStateTree(childTree);

					featureToLifelineMap.put(feature, childObjectLifeline);
				}
			}

			// retrieve object history

			Tree<ObjectHistoryNode, ObjectHistoryTransition> objHistTree = objectHistoryTrees.get(objDescriptor);

			// # build state tree
			// The object history tree contains of nodes that represent object states and transitions that represent 
			// transitions between object states. The object lifeline state tree contains of nodes that represent state 
			// transitions and transitions that represent object states. Therefore, the state tree needs additional 
			// start and end states, where the object instances change from null to the initial state or form the end
			// state to null.

			TreeNode<StateSpaceTransition, ObjectLifelineState> parentObjStateNode = null;

			Stack<TreeNode<ObjectHistoryNode, ObjectHistoryTransition>> histNodesToVisit = new Stack<>();
			histNodesToVisit.push(objHistTree.getRootNode());

			// This map hold track of state tree nodes that have been created after visiting a history node. Its primary
			// use is to find the predecessor of a certain state node by its history nodes predecessor.
			Map<TreeNode<ObjectHistoryNode, ObjectHistoryTransition>, TreeNode<StateSpaceTransition, ObjectLifelineState>> nodeMap = new HashMap<>();

			// iterate over history tree
			while (!histNodesToVisit.isEmpty()) {

				TreeNode<ObjectHistoryNode, ObjectHistoryTransition> visitedNode = histNodesToVisit.pop();

				// retrieve the object history node and the previous object history transition
				ObjectHistoryNode histNodeValue = visitedNode.getNodeValue();
				TreeTransition<ObjectHistoryNode, ObjectHistoryTransition> histTransition = visitedNode.getParentTransition();

				ObjectHistoryTransition histTransitionValue = null;
				TreeTransition<StateSpaceTransition, ObjectLifelineState> newObjStateTransition = null;

				// For each node in the object history we have to create at least one node for the actual state tree of the object lifeline.
				TreeNode<StateSpaceTransition, ObjectLifelineState> newObjStateNode = StatespacevisFactory.eINSTANCE.createTreeNode();

				// We have to check if we have a root node or a normal node of the object history, as we build the object 
				// lifeline state tree from left to right but with respect to the different state tree structure. So for
				// a root node we create just the root node in the state tree, for a normal node we also have to create a 
				// transition.
				if (histTransition != null) {

					// we have a "normal" node -> create also a transition (representing a new state)

					histTransitionValue = histTransition.getTransitionValue();
					newObjStateTransition = StatespacevisFactory.eINSTANCE.createTreeTransition();

					// update the node map
					nodeMap.put(visitedNode, newObjStateNode);

					// We are interested in creating the state tree node and its transition that corresponds to the 
					// transition between the currently visited history node and its successor history node. So retrieve
					// also the previous history node
					TreeNode<ObjectHistoryNode, ObjectHistoryTransition> histNode = histTransition.getPreviousNode();

					// retrieve the predecessor in the state tree using the predecessor history node and assign the new
					// node as child to it  
					parentObjStateNode = nodeMap.get(histNode);
					parentObjStateNode.addChildNode(newObjStateNode,
													newObjStateTransition);

					// create the object state value for the transition
					ObjectLifelineState objectLifelineState = StatespacevisFactory.eINSTANCE.createObjectLifelineState();
					newObjStateTransition.setTransitionValue(objectLifelineState);

					for (ObjectHistoryData data : histNode.getNodeValue()
															.getHistoryData()) {
						// search for the difference data in the history node and assign extract the corresponding state information
						if (data instanceof ObjectHistoryDiff) {
							ObjectHistoryDiff diffData = (ObjectHistoryDiff) data;
							objectLifelineState.setState(diffData.getNewState());
							LOG.log(Level.FINEST, "{0}", diffData.getNewState());

							objectLifelineState.setObjectInstance(diffData.getNewObjectInstance());
							objectLifelineState.setBaseHistoryNode(histNode.getNodeValue());
						}
					}

					// assign the state (space) transition to the new node
					if (histTransitionValue != null) {
						newObjStateNode.setNodeValue(histTransitionValue.getStateTransition());
					}

					if (structuralFeatures != null) {

						// create nodes and transitions for child lifelines

						for (EStructuralFeature feature : structuralFeatures) {

							// create node and transition for a specific feature

							TreeNode<StateSpaceTransition, ObjectLifelineState> newChildObjStateNode = StatespacevisFactory.eINSTANCE.createTreeNode();
							TreeTransition<StateSpaceTransition, ObjectLifelineState> newChildObjStateTransition = StatespacevisFactory.eINSTANCE.createTreeTransition();

							// This map hold track of state tree nodes of the current feature that have been created after visiting a history node. 
							// Its primary use is to find the predecessor of a certain state node by its history nodes predecessor.
							Map<TreeNode<ObjectHistoryNode, ObjectHistoryTransition>, TreeNode<StateSpaceTransition, ObjectLifelineState>> featureNodeMap = childNodeMap.get(feature);

							// update the node map for this feature
							featureNodeMap.put(	visitedNode,
												newChildObjStateNode);

							// retrieve the predecessor in the feature's state tree using the predecessor history node and assign the new
							// node as child to it  
							TreeNode<StateSpaceTransition, ObjectLifelineState> parentChildObjStateNode = featureNodeMap.get(histNode);
							parentChildObjStateNode.addChildNode(	newChildObjStateNode,
																	newChildObjStateTransition);

							ObjectLifelineState childObjectLifelineState = StatespacevisFactory.eINSTANCE.createObjectLifelineState();
							newChildObjStateTransition.setTransitionValue(childObjectLifelineState);

							// determine new State
							determineAndSetFeatureState(histNode.getNodeValue(),
														feature,
														childObjectLifelineState);

						}
					}

					if (visitedNode.getChildTransitions().isEmpty()) {
						// visitedNode is a leaf -> create "end" transition

						ObjectLifelineState endState = StatespacevisFactory.eINSTANCE.createObjectLifelineState();
						TreeTransition<StateSpaceTransition, ObjectLifelineState> endObjStateTransition = StatespacevisFactory.eINSTANCE.createTreeTransition();
						TreeNode<StateSpaceTransition, ObjectLifelineState> endObjStateNode = StatespacevisFactory.eINSTANCE.createTreeNode();
						newObjStateNode.addChildNode(	endObjStateNode,
														endObjStateTransition);

						for (ObjectHistoryData data : histNodeValue.getHistoryData()) {
							if (data instanceof ObjectHistoryDiff) {
								ObjectHistoryDiff diffData = (ObjectHistoryDiff) data;
								endState.setState(diffData.getNewState());
								LOG.log(Level.FINEST,
										"{0}",
										diffData.getNewState());

								endState.setObjectInstance(diffData.getNewObjectInstance());
								endState.setBaseHistoryNode(histNodeValue);
							}
						}
						endObjStateTransition.setTransitionValue(endState);

						if (structuralFeatures != null) {

							// create end nodes and transitions for child lifelines

							for (EStructuralFeature feature : structuralFeatures) {

								// create node and transition for a specific feature

								ObjectLifelineState childEndState = StatespacevisFactory.eINSTANCE.createObjectLifelineState();
								TreeTransition<StateSpaceTransition, ObjectLifelineState> endChildObjStateTransition = StatespacevisFactory.eINSTANCE.createTreeTransition();
								TreeNode<StateSpaceTransition, ObjectLifelineState> endChildObjStateNode = StatespacevisFactory.eINSTANCE.createTreeNode();

								Map<TreeNode<ObjectHistoryNode, ObjectHistoryTransition>, TreeNode<StateSpaceTransition, ObjectLifelineState>> featureNodeMap = childNodeMap.get(feature);

								featureNodeMap.get(visitedNode)
												.addChildNode(	endChildObjStateNode,
																endChildObjStateTransition);

								// determine new State
								determineAndSetFeatureState(histNodeValue,
															feature,
															childEndState);
								endChildObjStateTransition.setTransitionValue(childEndState);
							}

						}

					}

				} else {

					// we are at the root node -> create only the initial node

					objectStateTree.setRootNode(newObjStateNode);
					nodeMap.put(visitedNode, newObjStateNode);

					if (structuralFeatures != null) {

						// create root nodes for child lifelines

						for (EStructuralFeature feature : structuralFeatures) {
							Tree<StateSpaceTransition, ObjectLifelineState> childObjectStateTree = featureToLifelineMap.get(feature)
																														.getObjectStateTree();

							TreeNode<StateSpaceTransition, ObjectLifelineState> newChildObjStateNode = StatespacevisFactory.eINSTANCE.createTreeNode();
							childObjectStateTree.setRootNode(newChildObjStateNode);

							Map<TreeNode<ObjectHistoryNode, ObjectHistoryTransition>, TreeNode<StateSpaceTransition, ObjectLifelineState>> featureNodeMap = new HashMap<>();

							featureNodeMap.put(	visitedNode,
												newChildObjStateNode);
							childNodeMap.put(feature, featureNodeMap);

						}

					}

				}

				// add child history nodes to stack for iteration
				EList<TreeTransition<ObjectHistoryNode, ObjectHistoryTransition>> childTransitions = visitedNode.getChildTransitions();
				ListIterator<TreeTransition<ObjectHistoryNode, ObjectHistoryTransition>> iterator = childTransitions.listIterator(childTransitions.size());
				while (iterator.hasPrevious()) {
					histNodesToVisit.push(iterator.previous().getNextNode());
				}

			}

			lifelines.add(lifeline);
			if (childLifelines != null) {
				lifelines.addAll(childLifelines);
			}
		}

		return lifelines;
	}

	/**
	 * determines the object state for a feature based on the given history node
	 * and assigns it to the given object lifeline state
	 * 
	 * @param histNode
	 *            the history node used to determine the object state
	 * @param feature
	 *            the {@link EStructuralFeature} which's state must be
	 *            determined
	 * @param childObjectLifelineState
	 *            the object lifeline state to assign the state
	 */
	private void determineAndSetFeatureState(ObjectHistoryNode histNode,
			EStructuralFeature feature,
			ObjectLifelineState childObjectLifelineState) {
		for (ObjectHistoryData data : histNode.getHistoryData()) {
			if (data instanceof ObjectHistoryDiff) {
				ObjectHistoryDiff diffData = (ObjectHistoryDiff) data;
				EObject oldObj = (EObject) diffData.getOldObjectInstance();
				EObject newObj = (EObject) diffData.getNewObjectInstance();
				Object rawDiffData = diffData.getRawDiffData();

				// try to determine state based on EMF Compare match data 

				if (oldObj != null && newObj != null) {

					Object oldVal = oldObj.eGet(feature);
					Object newVal = newObj.eGet(feature);

					if (newVal != null && oldVal != null) {
						// state can only be "not null" or "changed", assume "not null" and check if its is "changed"
						childObjectLifelineState.setState(ObjectStateType.NOTNULL);

						if (rawDiffData != null && rawDiffData instanceof Match) {
							Match match = (Match) rawDiffData;
							for (Diff diff : match.getDifferences()) {
								if (diff instanceof ReferenceChange) {
									if (feature.equals(((ReferenceChange) diff).getReference())) {
										childObjectLifelineState.setState(ObjectStateType.CHANGED);
									}
								} else if (diff instanceof AttributeChange) {
									if (feature.equals(((AttributeChange) diff).getAttribute())) {
										childObjectLifelineState.setState(ObjectStateType.CHANGED);
									}
								} else if (diff instanceof ResourceAttachmentChange) {
									// Not supported yet, so log a warning
									LOG.log(Level.WARNING,
											"Resource attachment change found, this type of difference is currently not supported. Instance: {0}",
											diff);
								}
							}
						}
						// fallback mechanism if the match instance is not set, however it relies on a correctly overridden 
						// equals method, so this is not recommended
						else if (!oldVal.equals(newVal)) {
							childObjectLifelineState.setState(ObjectStateType.CHANGED);
						}
					} else if (newVal == null) {
						childObjectLifelineState.setState(ObjectStateType.NULL);
					}
					childObjectLifelineState.setObjectInstance(newVal);

				} else if (newObj == null) {

					childObjectLifelineState.setState(ObjectStateType.NULL);
					childObjectLifelineState.setObjectInstance(null);

				} else if (newObj != null) {
					// State changes from NULL only to NOTNULL, so we don't have to do some sophisticated checks here
					Object newVal = newObj.eGet(feature);
					if (newVal == null) {
						childObjectLifelineState.setState(ObjectStateType.NULL);
					} else {
						childObjectLifelineState.setState(ObjectStateType.NOTNULL);
					}
					childObjectLifelineState.setObjectInstance(newVal);

				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SSPVisEMFPackage.EMF_OBJECT_LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY:
				return buildLifelines(	(Tree<State, StateSpaceTransition>) arguments.get(0),
										(ModelHistory) arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} // EMFObjectLifelineBuilderImpl
