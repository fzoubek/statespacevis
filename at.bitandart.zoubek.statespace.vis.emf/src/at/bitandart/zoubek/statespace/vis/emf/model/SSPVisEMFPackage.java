/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFFactory
 * @model kind="package"
 * @generated
 */
public interface SSPVisEMFPackage extends EPackage {
	/**
	 * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String				eNAME																	= "model";

	/**
	 * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String				eNS_URI																	= "http://zoubek.bitandart.at/sspvisemf";

	/**
	 * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	String				eNS_PREFIX																= "sspvisemf";

	/**
	 * The singleton instance of the package. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	SSPVisEMFPackage	eINSTANCE																= at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFPackageImpl.init();

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFObjectLifelineBuilderImpl
	 * <em>EMF Object Lifeline Builder</em>}' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFObjectLifelineBuilderImpl
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFPackageImpl#getEMFObjectLifelineBuilder()
	 * @generated
	 */
	int					EMF_OBJECT_LIFELINE_BUILDER												= 0;

	/**
	 * The number of structural features of the '
	 * <em>EMF Object Lifeline Builder</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int					EMF_OBJECT_LIFELINE_BUILDER_FEATURE_COUNT								= StatespacevisPackage.LIFELINE_BUILDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Build Lifelines</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int					EMF_OBJECT_LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY		= StatespacevisPackage.LIFELINE_BUILDER___BUILD_LIFELINES__TREE_MODELHISTORY;

	/**
	 * The number of operations of the '<em>EMF Object Lifeline Builder</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int					EMF_OBJECT_LIFELINE_BUILDER_OPERATION_COUNT								= StatespacevisPackage.LIFELINE_BUILDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFCompareHistoryBuilderImpl
	 * <em>EMF Compare History Builder</em>}' class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFCompareHistoryBuilderImpl
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFPackageImpl#getEMFCompareHistoryBuilder()
	 * @generated
	 */
	int					EMF_COMPARE_HISTORY_BUILDER												= 1;

	/**
	 * The number of structural features of the '
	 * <em>EMF Compare History Builder</em>' class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int					EMF_COMPARE_HISTORY_BUILDER_FEATURE_COUNT								= StatespacevisPackage.MODEL_HISTORY_BUILDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Update Object History</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int					EMF_COMPARE_HISTORY_BUILDER___UPDATE_OBJECT_HISTORY__TREE_MODELHISTORY	= StatespacevisPackage.MODEL_HISTORY_BUILDER___UPDATE_OBJECT_HISTORY__TREE_MODELHISTORY;

	/**
	 * The number of operations of the '<em>EMF Compare History Builder</em>'
	 * class. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int					EMF_COMPARE_HISTORY_BUILDER_OPERATION_COUNT								= StatespacevisPackage.MODEL_HISTORY_BUILDER_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder
	 * <em>EMF Object Lifeline Builder</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>EMF Object Lifeline Builder</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder
	 * @generated
	 */
	EClass getEMFObjectLifelineBuilder();

	/**
	 * Returns the meta object for class '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder
	 * <em>EMF Compare History Builder</em>}'. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the meta object for class '<em>EMF Compare History Builder</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder
	 * @generated
	 */
	EClass getEMFCompareHistoryBuilder();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SSPVisEMFFactory getSSPVisEMFFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each operation of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFObjectLifelineBuilderImpl
		 * <em>EMF Object Lifeline Builder</em>}' class. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFObjectLifelineBuilderImpl
		 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFPackageImpl#getEMFObjectLifelineBuilder()
		 * @generated
		 */
		EClass	EMF_OBJECT_LIFELINE_BUILDER	= eINSTANCE.getEMFObjectLifelineBuilder();

		/**
		 * The meta object literal for the '
		 * {@link at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFCompareHistoryBuilderImpl
		 * <em>EMF Compare History Builder</em>}' class. <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * 
		 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.EMFCompareHistoryBuilderImpl
		 * @see at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFPackageImpl#getEMFCompareHistoryBuilder()
		 * @generated
		 */
		EClass	EMF_COMPARE_HISTORY_BUILDER	= eINSTANCE.getEMFCompareHistoryBuilder();

	}

} // SSPVisEMFPackage
