/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder;
import at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryData;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryDiff;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectHistoryTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectStateType;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeNodeReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatePathTreeTransitionReference;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;
import at.bitandart.zoubek.statespace.vis.utils.TreeUtils;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>EMF Compare History Builder</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated
 */
public class EMFCompareHistoryBuilderImpl extends MinimalEObjectImpl.Container implements EMFCompareHistoryBuilder {

	/**
	 * the default logger for this class
	 */
	private static final Logger	LOG	= Logger.getLogger(EMFCompareHistoryBuilderImpl.class.getCanonicalName());

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected EMFCompareHistoryBuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SSPVisEMFPackage.Literals.EMF_COMPARE_HISTORY_BUILDER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void updateObjectHistory(
			final Tree<State, StateSpaceTransition> stateTree,
			final ModelHistory modelHistory) {

		TreeNode<State, StateSpaceTransition> currentNode = stateTree.getRootNode();

		// TODO: don't clear history, only add or delete new nodes
		modelHistory.clear();

		Stack<TreeTransition<State, StateSpaceTransition>> transitionsToVisit = new Stack<>();
		for (TreeTransition<State, StateSpaceTransition> transition : currentNode.getChildTransitions()) {
			transitionsToVisit.push(transition);
		}

		while (!transitionsToVisit.isEmpty()) {
			TreeTransition<State, StateSpaceTransition> transition = transitionsToVisit.pop();

			processTransition(transition, modelHistory);

			TreeNode<State, StateSpaceTransition> nextNode = transition.getNextNode();
			EList<TreeTransition<State, StateSpaceTransition>> childTransitions = nextNode.getChildTransitions();
			ListIterator<TreeTransition<State, StateSpaceTransition>> iterator = childTransitions.listIterator(childTransitions.size());
			while (iterator.hasPrevious()) {
				transitionsToVisit.push(iterator.previous());
			}
		}
		if (LOG.isLoggable(Level.FINE)) {
			StringBuilder sb = new StringBuilder();
			sb.append("Tree structure summary:\n");
			for (Entry<ObjectDescriptor, Tree<ObjectHistoryNode, ObjectHistoryTransition>> entry : modelHistory.getObjectHistoryTrees()
																												.entrySet()) {
				sb.append(entry.getKey().getInstanceName());
				sb.append("\n");
				sb.append(TreeUtils.toStructureString(entry.getValue()));
				sb.append("\n\n");
			}
			LOG.fine(sb.toString());
		}

	}

	/**
	 * processes a given transition and updates the given model history
	 * 
	 * @param transition
	 * @param modelHistory
	 */
	private void processTransition(
			TreeTransition<State, StateSpaceTransition> transition,
			ModelHistory modelHistory) {
		State prevState = transition.getPreviousNode().getNodeValue();
		State nextState = transition.getNextNode().getNodeValue();

		Notifier prevNotifier = getResourceSet(prevState.getModelSnapshot());
		Notifier nextNotifier = getResourceSet(nextState.getModelSnapshot());

		// if no resource set could be found for both, use the model snapshot instead
		if (prevNotifier == null || nextNotifier == null) {
			Object snapshot = prevState.getModelSnapshot();
			if (snapshot instanceof Notifier) {
				prevNotifier = (Notifier) snapshot;
			} else {
				LOG.log(Level.SEVERE,
						"Could not extract a compareable EMF Notifier from model snapshot");
			}
			snapshot = nextState.getModelSnapshot();
			if (snapshot instanceof Notifier) {
				nextNotifier = (Notifier) snapshot;
			} else {
				LOG.log(Level.SEVERE,
						"Could not extract a compareable EMF Notifier from model snapshot");
			}
		}

		Comparison comparison = compare(prevNotifier, nextNotifier);

		processAllMatches(comparison.getMatches(), transition, modelHistory);
	}

	private void processAllMatches(List<Match> matches,
			TreeTransition<State, StateSpaceTransition> transition,
			ModelHistory modelHistory) {
		for (Match match : matches) {
			processMatch(transition, match, modelHistory);
			processAllMatches(match.getSubmatches(), transition, modelHistory);
		}
	}

	private void processMatch(
			TreeTransition<State, StateSpaceTransition> pathTransition,
			Match match, ModelHistory modelHistory) {

		State prevState = pathTransition.getPreviousNode().getNodeValue();
		State nextState = pathTransition.getNextNode().getNodeValue();

		EObject prevObj = match.getLeft();
		EObject nextObj = match.getRight();

		if (prevObj == null && nextObj != null) {

			// nextObj is a new object -> add new node and fill previous
			// nodes in history tree (including branches) with "null" object
			// history

			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> nextTreeNode = createObjectHistoryTreeNode(nextState,
																											nextObj,
																											prevObj,
																											modelHistory,
																											ObjectStateType.NOTNULL,
																											pathTransition.getNextNode(),
																											match);
			Tree<ObjectHistoryNode, ObjectHistoryTransition> objectHistoryTree = modelHistory.getObjectHistory(nextTreeNode.getNodeValue()
																															.getObjectDescriptor());
			TreeTransition<State, StateSpaceTransition> lastSSTT = pathTransition;

			// go back the tree until the root is reached

			while (prevState != null) {

				// create previous node

				TreeNode<ObjectHistoryNode, ObjectHistoryTransition> prevTreeNode = createObjectHistoryTreeNode(prevState,
																												null,
																												null,
																												modelHistory,
																												ObjectStateType.NULL,
																												lastSSTT.getPreviousNode(),
																												null);

				// create transition

				TreeTransition<ObjectHistoryNode, ObjectHistoryTransition> newTransition = createObjectHistoryTreeTransition(	prevTreeNode,
																																nextTreeNode,
																																lastSSTT.getTransitionValue(),
																																lastSSTT);

				// fill all branches with null object states

				EList<TreeTransition<State, StateSpaceTransition>> childPathTransitions = lastSSTT.getPreviousNode()
																									.getChildTransitions();
				int index = 0;
				for (TreeTransition<State, StateSpaceTransition> childPathTransition : childPathTransitions) {
					if (!childPathTransition.equals(lastSSTT)) {
						createNullObjectHistory(childPathTransition,
												prevTreeNode,
												modelHistory);
					} else {
						// maintain the same transition order as in the path tree
						EList<TreeTransition<ObjectHistoryNode, ObjectHistoryTransition>> prevChildTransitions = prevTreeNode.getChildTransitions();
						prevChildTransitions.move(index, newTransition);
					}
					index++;
				}

				// go back in tree

				nextState = prevState;
				nextTreeNode = prevTreeNode;

				lastSSTT = lastSSTT.getPreviousNode().getParentTransition();
				if (lastSSTT != null) {
					prevState = lastSSTT.getPreviousNode().getNodeValue();
				} else {

					// root node reached

					objectHistoryTree.setRootNode(nextTreeNode);
					prevState = null;
				}

			}

		} else if (prevObj != null) {

			ObjectDescriptor objDescriptor = getOrCreateObjectDescriptor(	prevObj,
																			modelHistory);
			Tree<ObjectHistoryNode, ObjectHistoryTransition> historyTree = modelHistory.getObjectHistory(objDescriptor);

			// get history TreeNode for prevObj

			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> prevHistTreeNode = historyTree.getRootNode();

			if (prevHistTreeNode == null) {
				// root node does not exist -> create it and attach as root

				prevHistTreeNode = createObjectHistoryTreeNode(	prevState,
																prevObj,
																null,
																modelHistory,
																ObjectStateType.NOTNULL,
																pathTransition.getPreviousNode(),
																null);
				historyTree.setRootNode(prevHistTreeNode);

			} else {
				prevHistTreeNode = getTreeNodeForState(	prevHistTreeNode,
														pathTransition.getPreviousNode());
			}
			if (prevHistTreeNode == null) {
				// TODO implement error message
				LOG.severe("Inconsistent model history: Couldn't find an object history tree node for the state: "
							+ prevState.toString());
			}

			ObjectStateType type = ObjectStateType.NULL;
			if (nextObj != null) {
				if (!match.getDifferences().iterator().hasNext()) {
					type = ObjectStateType.NOTNULL;
				} else {
					type = ObjectStateType.CHANGED;
				}
				// register nextObj to object descriptor
				modelHistory.registerObjectToDescriptor(objDescriptor, nextObj);
			}

			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> nextTreeNode = createObjectHistoryTreeNode(nextState,
																											nextObj,
																											prevObj,
																											modelHistory,
																											type,
																											pathTransition.getNextNode(),
																											match);
			TreeTransition<ObjectHistoryNode, ObjectHistoryTransition> newTransition = createObjectHistoryTreeTransition(	prevHistTreeNode,
																															nextTreeNode,
																															pathTransition.getTransitionValue(),
																															pathTransition);
			int pathTransitionIndex = pathTransition.getPreviousNode()
													.getChildTransitions()
													.indexOf(pathTransition);
			EList<TreeTransition<ObjectHistoryNode, ObjectHistoryTransition>> childPrevHistNodeTransitions = prevHistTreeNode.getChildTransitions();
			int newTransitionIndex = childPrevHistNodeTransitions.indexOf(newTransition);

			// maintain index of transition -> update transition index if possible
			if (pathTransitionIndex != newTransitionIndex
				&& pathTransitionIndex < childPrevHistNodeTransitions.size()) {
				childPrevHistNodeTransitions.move(	pathTransitionIndex,
													newTransitionIndex);
			}

			if (nextObj == null) {
				// fill subsequent path with "dead" object history nodes

				EList<TreeTransition<State, StateSpaceTransition>> childTransitions = pathTransition.getNextNode()
																									.getChildTransitions();

				for (TreeTransition<State, StateSpaceTransition> childTransition : childTransitions) {
					createNullObjectHistory(childTransition,
											nextTreeNode,
											modelHistory);
				}
			}

		}
	}

	/**
	 * creates a null-object-history for all subsequent tree nodes.
	 * 
	 * @param stateSpacePathTreeTransition
	 *            the state space path tree transition used to determine the
	 *            subsequent states
	 * @param objectHistoryTreeNode
	 *            the accompanying parent object history tree node
	 */
	private void createNullObjectHistory(
			TreeTransition<State, StateSpaceTransition> stateSpacePathTreeTransition,
			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> objectHistoryTreeNode,
			ModelHistory modelHistory) {

		Stack<Pair<TreeTransition<State, StateSpaceTransition>, TreeNode<ObjectHistoryNode, ObjectHistoryTransition>>> pairsToVisit = new Stack<>();
		pairsToVisit.push(new Pair<TreeTransition<State, StateSpaceTransition>, TreeNode<ObjectHistoryNode, ObjectHistoryTransition>>(	stateSpacePathTreeTransition,
																																		objectHistoryTreeNode));

		while (!pairsToVisit.isEmpty()) {

			Pair<TreeTransition<State, StateSpaceTransition>, TreeNode<ObjectHistoryNode, ObjectHistoryTransition>> pair = pairsToVisit.pop();

			TreeTransition<State, StateSpaceTransition> nextSSPTT = pair.value1;
			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> parentHTN = pair.value2;

			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> nextTreeNode = createObjectHistoryTreeNode(nextSSPTT.getNextNode()
																														.getNodeValue(),
																											null,
																											null,
																											modelHistory,
																											ObjectStateType.NULL,
																											nextSSPTT.getNextNode(),
																											null);
			createObjectHistoryTreeTransition(	parentHTN,
												nextTreeNode,
												nextSSPTT.getTransitionValue(),
												nextSSPTT);

			EList<TreeTransition<State, StateSpaceTransition>> childTransitions = nextSSPTT.getNextNode()
																							.getChildTransitions();
			ListIterator<TreeTransition<State, StateSpaceTransition>> iterator = childTransitions.listIterator(childTransitions.size());
			while (iterator.hasPrevious()) {
				pairsToVisit.push(new Pair<TreeTransition<State, StateSpaceTransition>, TreeNode<ObjectHistoryNode, ObjectHistoryTransition>>(	iterator.previous(),
																																				nextTreeNode));
			}
		}

	}

	private class Pair<S, T> {

		public Pair(S value1, T value2) {
			super();
			this.value1 = value1;
			this.value2 = value2;
		}

		public S	value1;
		public T	value2;
	}

	/**
	 * creates a new ObjectHistoryTreeTransition.
	 * 
	 * @param nextTreeNode
	 * @param prevTreeNode
	 * @param stateSpaceTransition
	 * @param statePathTreeTransition
	 * @return
	 */
	private TreeTransition<ObjectHistoryNode, ObjectHistoryTransition> createObjectHistoryTreeTransition(
			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> prevTreeNode,
			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> nextTreeNode,
			StateSpaceTransition stateSpaceTransition,
			TreeTransition<State, StateSpaceTransition> statePathTreeTransition) {
		TreeTransition<ObjectHistoryNode, ObjectHistoryTransition> transition = StatespacevisFactory.eINSTANCE.createTreeTransition();

		transition.setNextNode(nextTreeNode);
		transition.setPreviousNode(prevTreeNode);

		ObjectHistoryTransition objectHistTransition = StatespacevisFactory.eINSTANCE.createObjectHistoryTransition();
		objectHistTransition.setStateTransition(stateSpaceTransition);

		StatePathTreeTransitionReference sPTTRef = StatespacevisFactory.eINSTANCE.createStatePathTreeTransitionReference();
		sPTTRef.setStatePathTreeTransition(statePathTreeTransition);
		objectHistTransition.getHistoryData().add(sPTTRef);

		return transition;
	}

	/**
	 * creates a new TreeNode for the object history tree.
	 * 
	 * @param state
	 * @param obj
	 * @param oldObj
	 * @param modelHistory
	 *            may not be null
	 * @param objStateType
	 * @param statePathTreeNode
	 * @param match
	 *            the match instance if there is one, null otherwise
	 * @return the new TreeNode
	 */
	private TreeNode<ObjectHistoryNode, ObjectHistoryTransition> createObjectHistoryTreeNode(
			State state, EObject obj, EObject oldObj,
			ModelHistory modelHistory, ObjectStateType objStatetype,
			TreeNode<State, StateSpaceTransition> statePathTreeNode, Match match) {

		ObjectDescriptor nextObjDescriptor = null;
		if (obj != null) {
			nextObjDescriptor = getOrCreateObjectDescriptor(obj, modelHistory);
		}

		TreeNode<ObjectHistoryNode, ObjectHistoryTransition> nextHistTreeNode = StatespacevisFactory.eINSTANCE.createTreeNode();
		ObjectHistoryNode nextObjHistNode = StatespacevisFactory.eINSTANCE.createObjectHistoryNode();
		nextObjHistNode.setObjectInstance(obj);
		nextObjHistNode.setObjectDescriptor(nextObjDescriptor);
		nextObjHistNode.setState(state);

		ObjectHistoryDiff objHistDiff = StatespacevisFactory.eINSTANCE.createObjectHistoryDiff();
		objHistDiff.setNewState(objStatetype);
		objHistDiff.setNewObjectInstance(obj);
		objHistDiff.setOldObjectInstance(oldObj);
		objHistDiff.setRawDiffData(match);
		nextObjHistNode.getHistoryData().add(objHistDiff);

		nextHistTreeNode.setNodeValue(nextObjHistNode);

		StatePathTreeNodeReference sPTTRef = StatespacevisFactory.eINSTANCE.createStatePathTreeNodeReference();
		sPTTRef.setStatePathTreeNode(statePathTreeNode);
		nextObjHistNode.getHistoryData().add(sPTTRef);

		return nextHistTreeNode;
	}

	/**
	 * searches for the TreeNode associated to the given state path node.
	 * 
	 * @param startNode
	 *            the first node of the tree
	 * @param statePathNode
	 *            the state path node to search for
	 * @return the node or null if the node does not exist
	 */
	private TreeNode<ObjectHistoryNode, ObjectHistoryTransition> getTreeNodeForState(
			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> startNode,
			TreeNode<State, StateSpaceTransition> statePathNode) {
		TreeNode<ObjectHistoryNode, ObjectHistoryTransition> foundNode = null;

		Stack<TreeNode<ObjectHistoryNode, ObjectHistoryTransition>> nodesToVisit = new Stack<>();
		if (startNode != null) {
			nodesToVisit.push(startNode);
		}

		while (!nodesToVisit.isEmpty()) {

			TreeNode<ObjectHistoryNode, ObjectHistoryTransition> node = nodesToVisit.pop();

			for (ObjectHistoryData data : node.getNodeValue().getHistoryData()) {
				if (data instanceof StatePathTreeNodeReference) {
					TreeNode<State, StateSpaceTransition> refNode = ((StatePathTreeNodeReference) data).getStatePathTreeNode();
					if (refNode.equals(statePathNode)) {
						foundNode = node;
						break;
					}
				}
			}
			EList<TreeTransition<ObjectHistoryNode, ObjectHistoryTransition>> childTransitions = node.getChildTransitions();
			ListIterator<TreeTransition<ObjectHistoryNode, ObjectHistoryTransition>> iterator = childTransitions.listIterator(childTransitions.size());
			while (iterator.hasPrevious()) {
				nodesToVisit.push(iterator.previous().getNextNode());
			}

		}

		return foundNode;

	}

	/**
	 * retrieves the object descriptor for the given object, if no descriptor
	 * exists, a new descriptor will be created.
	 * 
	 * @param obj
	 *            the object associated to the descriptor
	 * @param modelHistory
	 *            the model history
	 * @return the object descriptor for the given object
	 */
	private ObjectDescriptor getOrCreateObjectDescriptor(EObject obj,
			ModelHistory modelHistory) {
		ObjectDescriptor objDescriptor = modelHistory.getObjectDescriptor(obj);
		if (objDescriptor == null) {
			objDescriptor = modelHistory.createObjectDescriptor(obj);
			objDescriptor.setClassDescriptor(obj.eClass());
		}
		return objDescriptor;
	}

	/**
	 * 
	 * @param snapshot
	 *            the snapshot the snapshot contained in the resource set
	 * @return the resource set for the model in the given snapshot, or
	 *         <code>null</code> if no resource set is available
	 */
	private ResourceSet getResourceSet(Object snapshot) {
		ResourceSet resourceSet = null;

		if (snapshot instanceof EObject) {
			EObject rootObject = (EObject) snapshot;
			resourceSet = rootObject.eResource().getResourceSet();
		} else if (snapshot instanceof ResourceSet) {
			resourceSet = (ResourceSet) snapshot;
		} else if (snapshot instanceof Resource) {
			resourceSet = ((Resource) snapshot).getResourceSet();
		}

		return resourceSet;
	}

	/**
	 * compares to notifier instances using EMF compare.
	 * 
	 * @param left
	 *            the first notifier
	 * @param right
	 *            the second notifier
	 * @return the comparison created by EMF Compare
	 */
	private Comparison compare(Notifier left, Notifier right) {

		// Configure EMF Compare
		IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.WHEN_AVAILABLE);
		IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
		IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(	matcher,
																				comparisonFactory);
		matchEngineFactory.setRanking(20);
		IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
		matchEngineRegistry.add(matchEngineFactory);
		EMFCompare comparator = EMFCompare.builder()
											.setMatchEngineFactoryRegistry(matchEngineRegistry)
											.build();

		// Compare the two models
		IComparisonScope scope = EMFCompare.createDefaultScope(left, right);
		return comparator.compare(scope);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SSPVisEMFPackage.EMF_COMPARE_HISTORY_BUILDER___UPDATE_OBJECT_HISTORY__TREE_MODELHISTORY:
				updateObjectHistory((Tree<State, StateSpaceTransition>) arguments.get(0),
									(ModelHistory) arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} // EMFCompareHistoryBuilderImpl
