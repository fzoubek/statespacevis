/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.util;

import at.bitandart.zoubek.statespace.vis.emf.model.*;

import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * 
 * @see at.bitandart.zoubek.statespace.vis.emf.model.SSPVisEMFPackage
 * @generated
 */
public class SSPVisEMFAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static SSPVisEMFPackage	modelPackage;

	/**
	 * Creates an instance of the adapter factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public SSPVisEMFAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SSPVisEMFPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SSPVisEMFSwitch<Adapter>	modelSwitch	= new SSPVisEMFSwitch<Adapter>() {
														@Override
														public Adapter caseEMFObjectLifelineBuilder(
																EMFObjectLifelineBuilder object) {
															return createEMFObjectLifelineBuilderAdapter();
														}

														@Override
														public Adapter caseEMFCompareHistoryBuilder(
																EMFCompareHistoryBuilder object) {
															return createEMFCompareHistoryBuilderAdapter();
														}

														@Override
														public Adapter caseLifelineBuilder(
																LifelineBuilder object) {
															return createLifelineBuilderAdapter();
														}

														@Override
														public Adapter caseModelHistoryBuilder(
																ModelHistoryBuilder object) {
															return createModelHistoryBuilderAdapter();
														}

														@Override
														public Adapter defaultCase(
																EObject object) {
															return createEObjectAdapter();
														}
													};

	/**
	 * Creates an adapter for the <code>target</code>. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param target
	 *            the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder
	 * <em>EMF Object Lifeline Builder</em>}'. <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.EMFObjectLifelineBuilder
	 * @generated
	 */
	public Adapter createEMFObjectLifelineBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder
	 * <em>EMF Compare History Builder</em>}'. <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.emf.model.EMFCompareHistoryBuilder
	 * @generated
	 */
	public Adapter createEMFCompareHistoryBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * <em>Lifeline Builder</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineBuilder
	 * @generated
	 */
	public Adapter createLifelineBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * <em>Model History Builder</em>}'. <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.statespacevismodel.ModelHistoryBuilder
	 * @generated
	 */
	public Adapter createModelHistoryBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case. <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // SSPVisEMFAdapterFactory
