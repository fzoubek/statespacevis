/**
 */
package at.bitandart.zoubek.statespace.vis.emf.model.impl;

import at.bitandart.zoubek.statespace.vis.emf.model.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class SSPVisEMFFactoryImpl extends EFactoryImpl implements SSPVisEMFFactory {
	/**
	 * Creates the default factory implementation. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public static SSPVisEMFFactory init() {
		try {
			SSPVisEMFFactory theSSPVisEMFFactory = (SSPVisEMFFactory) EPackage.Registry.INSTANCE.getEFactory(SSPVisEMFPackage.eNS_URI);
			if (theSSPVisEMFFactory != null) {
				return theSSPVisEMFFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SSPVisEMFFactoryImpl();
	}

	/**
	 * Creates an instance of the factory. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	public SSPVisEMFFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SSPVisEMFPackage.EMF_OBJECT_LIFELINE_BUILDER:
				return createEMFObjectLifelineBuilder();
			case SSPVisEMFPackage.EMF_COMPARE_HISTORY_BUILDER:
				return createEMFCompareHistoryBuilder();
			default:
				throw new IllegalArgumentException("The class '"
													+ eClass.getName()
													+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EMFObjectLifelineBuilder createEMFObjectLifelineBuilder() {
		EMFObjectLifelineBuilderImpl emfObjectLifelineBuilder = new EMFObjectLifelineBuilderImpl();
		return emfObjectLifelineBuilder;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EMFCompareHistoryBuilder createEMFCompareHistoryBuilder() {
		EMFCompareHistoryBuilderImpl emfCompareHistoryBuilder = new EMFCompareHistoryBuilderImpl();
		return emfCompareHistoryBuilder;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public SSPVisEMFPackage getSSPVisEMFPackage() {
		return (SSPVisEMFPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SSPVisEMFPackage getPackage() {
		return SSPVisEMFPackage.eINSTANCE;
	}

} // SSPVisEMFFactoryImpl
