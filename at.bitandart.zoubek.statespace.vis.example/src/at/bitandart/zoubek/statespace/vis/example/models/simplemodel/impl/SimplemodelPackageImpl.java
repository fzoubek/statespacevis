/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseInterface;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelFactory;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimplemodelPackageImpl extends EPackageImpl implements
		SimplemodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseInterfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subClassAEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subClassBEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subClassCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum myEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType myStringEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SimplemodelPackageImpl() {
		super(eNS_URI, SimplemodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SimplemodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SimplemodelPackage init() {
		if (isInited)
			return (SimplemodelPackage) EPackage.Registry.INSTANCE
					.getEPackage(SimplemodelPackage.eNS_URI);

		// Obtain or create and register package
		SimplemodelPackageImpl theSimplemodelPackage = (SimplemodelPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof SimplemodelPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new SimplemodelPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theSimplemodelPackage.createPackageContents();

		// Initialize created meta-data
		theSimplemodelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSimplemodelPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SimplemodelPackage.eNS_URI,
				theSimplemodelPackage);
		return theSimplemodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractClass() {
		return abstractClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractClass_MyStringAttribute() {
		return (EAttribute) abstractClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleClass() {
		return simpleClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleClass_RefBaseClass() {
		return (EReference) simpleClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimpleClass_DoubleAttribute() {
		return (EAttribute) simpleClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseClass() {
		return baseClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseClass_RefSimpleClass() {
		return (EReference) baseClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseClass_StringAttribute() {
		return (EAttribute) baseClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseInterface() {
		return baseInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubClassA() {
		return subClassAEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubClassA_ConSimpleClass() {
		return (EReference) subClassAEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubClassA_MyEnumAttribute() {
		return (EAttribute) subClassAEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubClassB() {
		return subClassBEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubClassB_IntAttribute() {
		return (EAttribute) subClassBEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubClassC() {
		return subClassCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubClassC_FloatAttribute() {
		return (EAttribute) subClassCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMyEnum() {
		return myEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getMyString() {
		return myStringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimplemodelFactory getSimplemodelFactory() {
		return (SimplemodelFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		abstractClassEClass = createEClass(ABSTRACT_CLASS);
		createEAttribute(abstractClassEClass,
				ABSTRACT_CLASS__MY_STRING_ATTRIBUTE);

		simpleClassEClass = createEClass(SIMPLE_CLASS);
		createEReference(simpleClassEClass, SIMPLE_CLASS__REF_BASE_CLASS);
		createEAttribute(simpleClassEClass, SIMPLE_CLASS__DOUBLE_ATTRIBUTE);

		baseClassEClass = createEClass(BASE_CLASS);
		createEReference(baseClassEClass, BASE_CLASS__REF_SIMPLE_CLASS);
		createEAttribute(baseClassEClass, BASE_CLASS__STRING_ATTRIBUTE);

		baseInterfaceEClass = createEClass(BASE_INTERFACE);

		subClassAEClass = createEClass(SUB_CLASS_A);
		createEReference(subClassAEClass, SUB_CLASS_A__CON_SIMPLE_CLASS);
		createEAttribute(subClassAEClass, SUB_CLASS_A__MY_ENUM_ATTRIBUTE);

		subClassBEClass = createEClass(SUB_CLASS_B);
		createEAttribute(subClassBEClass, SUB_CLASS_B__INT_ATTRIBUTE);

		subClassCEClass = createEClass(SUB_CLASS_C);
		createEAttribute(subClassCEClass, SUB_CLASS_C__FLOAT_ATTRIBUTE);

		// Create enums
		myEnumEEnum = createEEnum(MY_ENUM);

		// Create data types
		myStringEDataType = createEDataType(MY_STRING);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		abstractClassEClass.getESuperTypes().add(this.getBaseInterface());
		baseClassEClass.getESuperTypes().add(this.getBaseInterface());
		subClassAEClass.getESuperTypes().add(this.getAbstractClass());
		subClassBEClass.getESuperTypes().add(this.getBaseClass());
		subClassCEClass.getESuperTypes().add(this.getBaseClass());

		// Initialize classes, features, and operations; add parameters
		initEClass(abstractClassEClass, AbstractClass.class, "AbstractClass",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractClass_MyStringAttribute(),
				this.getMyString(), "myStringAttribute", null, 0, 1,
				AbstractClass.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(simpleClassEClass, SimpleClass.class, "SimpleClass",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimpleClass_RefBaseClass(), this.getBaseClass(),
				this.getBaseClass_RefSimpleClass(), "refBaseClass", null, 0, 1,
				SimpleClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimpleClass_DoubleAttribute(),
				ecorePackage.getEDouble(), "doubleAttribute", null, 0, 1,
				SimpleClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseClassEClass, BaseClass.class, "BaseClass", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseClass_RefSimpleClass(), this.getSimpleClass(),
				this.getSimpleClass_RefBaseClass(), "refSimpleClass", null, 0,
				-1, BaseClass.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBaseClass_StringAttribute(),
				ecorePackage.getEString(), "stringAttribute", null, 0, 1,
				BaseClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(baseInterfaceEClass, BaseInterface.class, "BaseInterface",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(subClassAEClass, SubClassA.class, "SubClassA", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubClassA_ConSimpleClass(), this.getSimpleClass(),
				null, "conSimpleClass", null, 0, 1, SubClassA.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getSubClassA_MyEnumAttribute(), this.getMyEnum(),
				"myEnumAttribute", null, 0, 1, SubClassA.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(subClassBEClass, SubClassB.class, "SubClassB", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSubClassB_IntAttribute(), ecorePackage.getEInt(),
				"intAttribute", null, 0, 1, SubClassB.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(subClassCEClass, SubClassC.class, "SubClassC", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSubClassC_FloatAttribute(), ecorePackage.getEFloat(),
				"floatAttribute", null, 0, 1, SubClassC.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(myEnumEEnum, MyEnum.class, "MyEnum");
		addEEnumLiteral(myEnumEEnum, MyEnum.VAL1);
		addEEnumLiteral(myEnumEEnum, MyEnum.VAL2);
		addEEnumLiteral(myEnumEEnum, MyEnum.VAL3);

		// Initialize data types
		initEDataType(myStringEDataType, String.class, "MyString",
				IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //SimplemodelPackageImpl
