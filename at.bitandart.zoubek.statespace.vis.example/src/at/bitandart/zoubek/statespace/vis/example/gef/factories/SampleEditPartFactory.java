/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.example.gef.factories;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SampleEditPartFactory implements EditPartFactory {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	@Override
	public EditPart createEditPart(EditPart context, Object object) {

		// TODO create your own edit part here

		return null; // we don't contribute new edit parts, so return null
	}

}
