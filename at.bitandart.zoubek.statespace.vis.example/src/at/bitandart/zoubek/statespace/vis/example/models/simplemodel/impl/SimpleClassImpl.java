/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimpleClassImpl#getRefBaseClass <em>Ref Base Class</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimpleClassImpl#getDoubleAttribute <em>Double Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SimpleClassImpl extends MinimalEObjectImpl.Container implements
		SimpleClass {
	/**
	 * The cached value of the '{@link #getRefBaseClass() <em>Ref Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefBaseClass()
	 * @generated
	 * @ordered
	 */
	protected BaseClass refBaseClass;

	/**
	 * The default value of the '{@link #getDoubleAttribute() <em>Double Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoubleAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final double DOUBLE_ATTRIBUTE_EDEFAULT = 0.0;
	/**
	 * The cached value of the '{@link #getDoubleAttribute() <em>Double Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoubleAttribute()
	 * @generated
	 * @ordered
	 */
	protected double doubleAttribute = DOUBLE_ATTRIBUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplemodelPackage.Literals.SIMPLE_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseClass getRefBaseClass() {
		if (refBaseClass != null && refBaseClass.eIsProxy()) {
			InternalEObject oldRefBaseClass = (InternalEObject) refBaseClass;
			refBaseClass = (BaseClass) eResolveProxy(oldRefBaseClass);
			if (refBaseClass != oldRefBaseClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS,
							oldRefBaseClass, refBaseClass));
			}
		}
		return refBaseClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseClass basicGetRefBaseClass() {
		return refBaseClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRefBaseClass(BaseClass newRefBaseClass,
			NotificationChain msgs) {
		BaseClass oldRefBaseClass = refBaseClass;
		refBaseClass = newRefBaseClass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS,
					oldRefBaseClass, newRefBaseClass);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefBaseClass(BaseClass newRefBaseClass) {
		if (newRefBaseClass != refBaseClass) {
			NotificationChain msgs = null;
			if (refBaseClass != null)
				msgs = ((InternalEObject) refBaseClass).eInverseRemove(this,
						SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS,
						BaseClass.class, msgs);
			if (newRefBaseClass != null)
				msgs = ((InternalEObject) newRefBaseClass).eInverseAdd(this,
						SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS,
						BaseClass.class, msgs);
			msgs = basicSetRefBaseClass(newRefBaseClass, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS,
					newRefBaseClass, newRefBaseClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDoubleAttribute() {
		return doubleAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoubleAttribute(double newDoubleAttribute) {
		double oldDoubleAttribute = doubleAttribute;
		doubleAttribute = newDoubleAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.SIMPLE_CLASS__DOUBLE_ATTRIBUTE,
					oldDoubleAttribute, doubleAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS:
			if (refBaseClass != null)
				msgs = ((InternalEObject) refBaseClass).eInverseRemove(this,
						SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS,
						BaseClass.class, msgs);
			return basicSetRefBaseClass((BaseClass) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS:
			return basicSetRefBaseClass(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS:
			if (resolve)
				return getRefBaseClass();
			return basicGetRefBaseClass();
		case SimplemodelPackage.SIMPLE_CLASS__DOUBLE_ATTRIBUTE:
			return getDoubleAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS:
			setRefBaseClass((BaseClass) newValue);
			return;
		case SimplemodelPackage.SIMPLE_CLASS__DOUBLE_ATTRIBUTE:
			setDoubleAttribute((Double) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS:
			setRefBaseClass((BaseClass) null);
			return;
		case SimplemodelPackage.SIMPLE_CLASS__DOUBLE_ATTRIBUTE:
			setDoubleAttribute(DOUBLE_ATTRIBUTE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS:
			return refBaseClass != null;
		case SimplemodelPackage.SIMPLE_CLASS__DOUBLE_ATTRIBUTE:
			return doubleAttribute != DOUBLE_ATTRIBUTE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (doubleAttribute: ");
		result.append(doubleAttribute);
		result.append(')');
		return result.toString();
	}

} //SimpleClassImpl
