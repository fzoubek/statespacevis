/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getBaseInterface()
 * @model
 * @generated
 */
public interface BaseInterface extends EObject {
} // BaseInterface
