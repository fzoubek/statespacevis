/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getRefSimpleClass <em>Ref Simple Class</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getStringAttribute <em>String Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getBaseClass()
 * @model
 * @generated
 */
public interface BaseClass extends BaseInterface {
	/**
	 * Returns the value of the '<em><b>Ref Simple Class</b></em>' reference list.
	 * The list contents are of type {@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass}.
	 * It is bidirectional and its opposite is '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getRefBaseClass <em>Ref Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Simple Class</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Simple Class</em>' reference list.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getBaseClass_RefSimpleClass()
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getRefBaseClass
	 * @model opposite="refBaseClass"
	 * @generated
	 */
	EList<SimpleClass> getRefSimpleClass();

	/**
	 * Returns the value of the '<em><b>String Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Attribute</em>' attribute.
	 * @see #setStringAttribute(String)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getBaseClass_StringAttribute()
	 * @model
	 * @generated
	 */
	String getStringAttribute();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getStringAttribute <em>String Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Attribute</em>' attribute.
	 * @see #getStringAttribute()
	 * @generated
	 */
	void setStringAttribute(String value);

} // BaseClass
