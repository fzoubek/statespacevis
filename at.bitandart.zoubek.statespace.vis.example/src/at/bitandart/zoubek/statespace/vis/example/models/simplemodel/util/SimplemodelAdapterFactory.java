/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.util;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage
 * @generated
 */
public class SimplemodelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SimplemodelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimplemodelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SimplemodelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimplemodelSwitch<Adapter> modelSwitch = new SimplemodelSwitch<Adapter>() {
		@Override
		public Adapter caseAbstractClass(AbstractClass object) {
			return createAbstractClassAdapter();
		}

		@Override
		public Adapter caseSimpleClass(SimpleClass object) {
			return createSimpleClassAdapter();
		}

		@Override
		public Adapter caseBaseClass(BaseClass object) {
			return createBaseClassAdapter();
		}

		@Override
		public Adapter caseBaseInterface(BaseInterface object) {
			return createBaseInterfaceAdapter();
		}

		@Override
		public Adapter caseSubClassA(SubClassA object) {
			return createSubClassAAdapter();
		}

		@Override
		public Adapter caseSubClassB(SubClassB object) {
			return createSubClassBAdapter();
		}

		@Override
		public Adapter caseSubClassC(SubClassC object) {
			return createSubClassCAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass <em>Abstract Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass
	 * @generated
	 */
	public Adapter createAbstractClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass <em>Simple Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass
	 * @generated
	 */
	public Adapter createSimpleClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass
	 * @generated
	 */
	public Adapter createBaseClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseInterface <em>Base Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseInterface
	 * @generated
	 */
	public Adapter createBaseInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA <em>Sub Class A</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA
	 * @generated
	 */
	public Adapter createSubClassAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB <em>Sub Class B</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB
	 * @generated
	 */
	public Adapter createSubClassBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC <em>Sub Class C</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC
	 * @generated
	 */
	public Adapter createSubClassCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SimplemodelAdapterFactory
