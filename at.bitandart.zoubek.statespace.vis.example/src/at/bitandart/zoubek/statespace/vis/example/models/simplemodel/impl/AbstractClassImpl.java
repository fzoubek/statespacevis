/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.AbstractClassImpl#getMyStringAttribute <em>My String Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class AbstractClassImpl extends BaseInterfaceImpl implements
		AbstractClass {
	/**
	 * The default value of the '{@link #getMyStringAttribute() <em>My String Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMyStringAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final String MY_STRING_ATTRIBUTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMyStringAttribute() <em>My String Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMyStringAttribute()
	 * @generated
	 * @ordered
	 */
	protected String myStringAttribute = MY_STRING_ATTRIBUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplemodelPackage.Literals.ABSTRACT_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMyStringAttribute() {
		return myStringAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMyStringAttribute(String newMyStringAttribute) {
		String oldMyStringAttribute = myStringAttribute;
		myStringAttribute = newMyStringAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.ABSTRACT_CLASS__MY_STRING_ATTRIBUTE,
					oldMyStringAttribute, myStringAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SimplemodelPackage.ABSTRACT_CLASS__MY_STRING_ATTRIBUTE:
			return getMyStringAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SimplemodelPackage.ABSTRACT_CLASS__MY_STRING_ATTRIBUTE:
			setMyStringAttribute((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.ABSTRACT_CLASS__MY_STRING_ATTRIBUTE:
			setMyStringAttribute(MY_STRING_ATTRIBUTE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.ABSTRACT_CLASS__MY_STRING_ATTRIBUTE:
			return MY_STRING_ATTRIBUTE_EDEFAULT == null ? myStringAttribute != null
					: !MY_STRING_ATTRIBUTE_EDEFAULT.equals(myStringAttribute);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (myStringAttribute: ");
		result.append(myStringAttribute);
		result.append(')');
		return result.toString();
	}

} //AbstractClassImpl
