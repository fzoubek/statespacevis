/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelFactory
 * @model kind="package"
 * @generated
 */
public interface SimplemodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "simplemodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://zoubek.bitandart.at/sspvis/example/simplemodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "simp";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SimplemodelPackage eINSTANCE = at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseInterfaceImpl <em>Base Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseInterfaceImpl
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getBaseInterface()
	 * @generated
	 */
	int BASE_INTERFACE = 3;

	/**
	 * The number of structural features of the '<em>Base Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Base Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.AbstractClassImpl <em>Abstract Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.AbstractClassImpl
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getAbstractClass()
	 * @generated
	 */
	int ABSTRACT_CLASS = 0;

	/**
	 * The feature id for the '<em><b>My String Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CLASS__MY_STRING_ATTRIBUTE = BASE_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Abstract Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CLASS_FEATURE_COUNT = BASE_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CLASS_OPERATION_COUNT = BASE_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimpleClassImpl <em>Simple Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimpleClassImpl
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSimpleClass()
	 * @generated
	 */
	int SIMPLE_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Ref Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_CLASS__REF_BASE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Double Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_CLASS__DOUBLE_ATTRIBUTE = 1;

	/**
	 * The number of structural features of the '<em>Simple Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_CLASS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Simple Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_CLASS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseClassImpl <em>Base Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseClassImpl
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getBaseClass()
	 * @generated
	 */
	int BASE_CLASS = 2;

	/**
	 * The feature id for the '<em><b>Ref Simple Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS__REF_SIMPLE_CLASS = BASE_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>String Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS__STRING_ATTRIBUTE = BASE_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Base Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_FEATURE_COUNT = BASE_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Base Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_OPERATION_COUNT = BASE_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassAImpl <em>Sub Class A</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassAImpl
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSubClassA()
	 * @generated
	 */
	int SUB_CLASS_A = 4;

	/**
	 * The feature id for the '<em><b>My String Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_A__MY_STRING_ATTRIBUTE = ABSTRACT_CLASS__MY_STRING_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Con Simple Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_A__CON_SIMPLE_CLASS = ABSTRACT_CLASS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>My Enum Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_A__MY_ENUM_ATTRIBUTE = ABSTRACT_CLASS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sub Class A</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_A_FEATURE_COUNT = ABSTRACT_CLASS_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Sub Class A</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_A_OPERATION_COUNT = ABSTRACT_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassBImpl <em>Sub Class B</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassBImpl
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSubClassB()
	 * @generated
	 */
	int SUB_CLASS_B = 5;

	/**
	 * The feature id for the '<em><b>Ref Simple Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_B__REF_SIMPLE_CLASS = BASE_CLASS__REF_SIMPLE_CLASS;

	/**
	 * The feature id for the '<em><b>String Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_B__STRING_ATTRIBUTE = BASE_CLASS__STRING_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Int Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_B__INT_ATTRIBUTE = BASE_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sub Class B</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_B_FEATURE_COUNT = BASE_CLASS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Sub Class B</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_B_OPERATION_COUNT = BASE_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassCImpl <em>Sub Class C</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassCImpl
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSubClassC()
	 * @generated
	 */
	int SUB_CLASS_C = 6;

	/**
	 * The feature id for the '<em><b>Ref Simple Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_C__REF_SIMPLE_CLASS = BASE_CLASS__REF_SIMPLE_CLASS;

	/**
	 * The feature id for the '<em><b>String Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_C__STRING_ATTRIBUTE = BASE_CLASS__STRING_ATTRIBUTE;

	/**
	 * The feature id for the '<em><b>Float Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_C__FLOAT_ATTRIBUTE = BASE_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sub Class C</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_C_FEATURE_COUNT = BASE_CLASS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Sub Class C</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_C_OPERATION_COUNT = BASE_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum <em>My Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getMyEnum()
	 * @generated
	 */
	int MY_ENUM = 7;

	/**
	 * The meta object id for the '<em>My String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getMyString()
	 * @generated
	 */
	int MY_STRING = 8;

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass <em>Abstract Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Class</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass
	 * @generated
	 */
	EClass getAbstractClass();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass#getMyStringAttribute <em>My String Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>My String Attribute</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass#getMyStringAttribute()
	 * @see #getAbstractClass()
	 * @generated
	 */
	EAttribute getAbstractClass_MyStringAttribute();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass <em>Simple Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Class</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass
	 * @generated
	 */
	EClass getSimpleClass();

	/**
	 * Returns the meta object for the reference '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getRefBaseClass <em>Ref Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref Base Class</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getRefBaseClass()
	 * @see #getSimpleClass()
	 * @generated
	 */
	EReference getSimpleClass_RefBaseClass();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getDoubleAttribute <em>Double Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Double Attribute</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getDoubleAttribute()
	 * @see #getSimpleClass()
	 * @generated
	 */
	EAttribute getSimpleClass_DoubleAttribute();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Class</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass
	 * @generated
	 */
	EClass getBaseClass();

	/**
	 * Returns the meta object for the reference list '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getRefSimpleClass <em>Ref Simple Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ref Simple Class</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getRefSimpleClass()
	 * @see #getBaseClass()
	 * @generated
	 */
	EReference getBaseClass_RefSimpleClass();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getStringAttribute <em>String Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Attribute</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getStringAttribute()
	 * @see #getBaseClass()
	 * @generated
	 */
	EAttribute getBaseClass_StringAttribute();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseInterface <em>Base Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Interface</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseInterface
	 * @generated
	 */
	EClass getBaseInterface();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA <em>Sub Class A</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Class A</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA
	 * @generated
	 */
	EClass getSubClassA();

	/**
	 * Returns the meta object for the containment reference '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getConSimpleClass <em>Con Simple Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Con Simple Class</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getConSimpleClass()
	 * @see #getSubClassA()
	 * @generated
	 */
	EReference getSubClassA_ConSimpleClass();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getMyEnumAttribute <em>My Enum Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>My Enum Attribute</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getMyEnumAttribute()
	 * @see #getSubClassA()
	 * @generated
	 */
	EAttribute getSubClassA_MyEnumAttribute();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB <em>Sub Class B</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Class B</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB
	 * @generated
	 */
	EClass getSubClassB();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB#getIntAttribute <em>Int Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Attribute</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB#getIntAttribute()
	 * @see #getSubClassB()
	 * @generated
	 */
	EAttribute getSubClassB_IntAttribute();

	/**
	 * Returns the meta object for class '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC <em>Sub Class C</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Class C</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC
	 * @generated
	 */
	EClass getSubClassC();

	/**
	 * Returns the meta object for the attribute '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC#getFloatAttribute <em>Float Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Float Attribute</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC#getFloatAttribute()
	 * @see #getSubClassC()
	 * @generated
	 */
	EAttribute getSubClassC_FloatAttribute();

	/**
	 * Returns the meta object for enum '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum <em>My Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>My Enum</em>'.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum
	 * @generated
	 */
	EEnum getMyEnum();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>My String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>My String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getMyString();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SimplemodelFactory getSimplemodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.AbstractClassImpl <em>Abstract Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.AbstractClassImpl
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getAbstractClass()
		 * @generated
		 */
		EClass ABSTRACT_CLASS = eINSTANCE.getAbstractClass();

		/**
		 * The meta object literal for the '<em><b>My String Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_CLASS__MY_STRING_ATTRIBUTE = eINSTANCE
				.getAbstractClass_MyStringAttribute();

		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimpleClassImpl <em>Simple Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimpleClassImpl
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSimpleClass()
		 * @generated
		 */
		EClass SIMPLE_CLASS = eINSTANCE.getSimpleClass();

		/**
		 * The meta object literal for the '<em><b>Ref Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_CLASS__REF_BASE_CLASS = eINSTANCE
				.getSimpleClass_RefBaseClass();

		/**
		 * The meta object literal for the '<em><b>Double Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_CLASS__DOUBLE_ATTRIBUTE = eINSTANCE
				.getSimpleClass_DoubleAttribute();

		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseClassImpl <em>Base Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseClassImpl
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getBaseClass()
		 * @generated
		 */
		EClass BASE_CLASS = eINSTANCE.getBaseClass();

		/**
		 * The meta object literal for the '<em><b>Ref Simple Class</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_CLASS__REF_SIMPLE_CLASS = eINSTANCE
				.getBaseClass_RefSimpleClass();

		/**
		 * The meta object literal for the '<em><b>String Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_CLASS__STRING_ATTRIBUTE = eINSTANCE
				.getBaseClass_StringAttribute();

		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseInterfaceImpl <em>Base Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseInterfaceImpl
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getBaseInterface()
		 * @generated
		 */
		EClass BASE_INTERFACE = eINSTANCE.getBaseInterface();

		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassAImpl <em>Sub Class A</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassAImpl
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSubClassA()
		 * @generated
		 */
		EClass SUB_CLASS_A = eINSTANCE.getSubClassA();

		/**
		 * The meta object literal for the '<em><b>Con Simple Class</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_CLASS_A__CON_SIMPLE_CLASS = eINSTANCE
				.getSubClassA_ConSimpleClass();

		/**
		 * The meta object literal for the '<em><b>My Enum Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_CLASS_A__MY_ENUM_ATTRIBUTE = eINSTANCE
				.getSubClassA_MyEnumAttribute();

		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassBImpl <em>Sub Class B</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassBImpl
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSubClassB()
		 * @generated
		 */
		EClass SUB_CLASS_B = eINSTANCE.getSubClassB();

		/**
		 * The meta object literal for the '<em><b>Int Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_CLASS_B__INT_ATTRIBUTE = eINSTANCE
				.getSubClassB_IntAttribute();

		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassCImpl <em>Sub Class C</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassCImpl
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getSubClassC()
		 * @generated
		 */
		EClass SUB_CLASS_C = eINSTANCE.getSubClassC();

		/**
		 * The meta object literal for the '<em><b>Float Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_CLASS_C__FLOAT_ATTRIBUTE = eINSTANCE
				.getSubClassC_FloatAttribute();

		/**
		 * The meta object literal for the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum <em>My Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getMyEnum()
		 * @generated
		 */
		EEnum MY_ENUM = eINSTANCE.getMyEnum();

		/**
		 * The meta object literal for the '<em>My String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelPackageImpl#getMyString()
		 * @generated
		 */
		EDataType MY_STRING = eINSTANCE.getMyString();

	}

} //SimplemodelPackage
