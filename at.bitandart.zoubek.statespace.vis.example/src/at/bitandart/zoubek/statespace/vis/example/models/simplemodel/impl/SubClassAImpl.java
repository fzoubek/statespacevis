/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Class A</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassAImpl#getConSimpleClass <em>Con Simple Class</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassAImpl#getMyEnumAttribute <em>My Enum Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubClassAImpl extends AbstractClassImpl implements SubClassA {
	/**
	 * The cached value of the '{@link #getConSimpleClass() <em>Con Simple Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConSimpleClass()
	 * @generated
	 * @ordered
	 */
	protected SimpleClass conSimpleClass;

	/**
	 * The default value of the '{@link #getMyEnumAttribute() <em>My Enum Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMyEnumAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final MyEnum MY_ENUM_ATTRIBUTE_EDEFAULT = MyEnum.VAL1;

	/**
	 * The cached value of the '{@link #getMyEnumAttribute() <em>My Enum Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMyEnumAttribute()
	 * @generated
	 * @ordered
	 */
	protected MyEnum myEnumAttribute = MY_ENUM_ATTRIBUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubClassAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplemodelPackage.Literals.SUB_CLASS_A;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleClass getConSimpleClass() {
		return conSimpleClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConSimpleClass(
			SimpleClass newConSimpleClass, NotificationChain msgs) {
		SimpleClass oldConSimpleClass = conSimpleClass;
		conSimpleClass = newConSimpleClass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS,
					oldConSimpleClass, newConSimpleClass);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConSimpleClass(SimpleClass newConSimpleClass) {
		if (newConSimpleClass != conSimpleClass) {
			NotificationChain msgs = null;
			if (conSimpleClass != null)
				msgs = ((InternalEObject) conSimpleClass)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS,
								null, msgs);
			if (newConSimpleClass != null)
				msgs = ((InternalEObject) newConSimpleClass)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS,
								null, msgs);
			msgs = basicSetConSimpleClass(newConSimpleClass, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS,
					newConSimpleClass, newConSimpleClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MyEnum getMyEnumAttribute() {
		return myEnumAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMyEnumAttribute(MyEnum newMyEnumAttribute) {
		MyEnum oldMyEnumAttribute = myEnumAttribute;
		myEnumAttribute = newMyEnumAttribute == null ? MY_ENUM_ATTRIBUTE_EDEFAULT
				: newMyEnumAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.SUB_CLASS_A__MY_ENUM_ATTRIBUTE,
					oldMyEnumAttribute, myEnumAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS:
			return basicSetConSimpleClass(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS:
			return getConSimpleClass();
		case SimplemodelPackage.SUB_CLASS_A__MY_ENUM_ATTRIBUTE:
			return getMyEnumAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS:
			setConSimpleClass((SimpleClass) newValue);
			return;
		case SimplemodelPackage.SUB_CLASS_A__MY_ENUM_ATTRIBUTE:
			setMyEnumAttribute((MyEnum) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS:
			setConSimpleClass((SimpleClass) null);
			return;
		case SimplemodelPackage.SUB_CLASS_A__MY_ENUM_ATTRIBUTE:
			setMyEnumAttribute(MY_ENUM_ATTRIBUTE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_A__CON_SIMPLE_CLASS:
			return conSimpleClass != null;
		case SimplemodelPackage.SUB_CLASS_A__MY_ENUM_ATTRIBUTE:
			return myEnumAttribute != MY_ENUM_ATTRIBUTE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (myEnumAttribute: ");
		result.append(myEnumAttribute);
		result.append(')');
		return result.toString();
	}

} //SubClassAImpl
