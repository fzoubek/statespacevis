/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Class C</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC#getFloatAttribute <em>Float Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSubClassC()
 * @model
 * @generated
 */
public interface SubClassC extends BaseClass {
	/**
	 * Returns the value of the '<em><b>Float Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Float Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Float Attribute</em>' attribute.
	 * @see #setFloatAttribute(float)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSubClassC_FloatAttribute()
	 * @model
	 * @generated
	 */
	float getFloatAttribute();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC#getFloatAttribute <em>Float Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Float Attribute</em>' attribute.
	 * @see #getFloatAttribute()
	 * @generated
	 */
	void setFloatAttribute(float value);

} // SubClassC
