package at.bitandart.zoubek.statespace.vis.example.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import at.bitandart.zoubek.statespace.vis.editors.StateSpacePathVisEditor;
import at.bitandart.zoubek.statespace.vis.editors.inputs.SSPVisEditorInput;
import at.bitandart.zoubek.statespace.vis.emf.model.impl.SSPVisEMFFactoryImpl;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.State;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpace;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpaceTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.Tree;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeNode;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.TreeTransition;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisFactoryImpl;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl;

/**
 * A Handler that opens a state space visualization editor with sample data.
 */
public class OpenMockupSSPVis extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public OpenMockupSSPVis() {
	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);

		StatespacevisPackageImpl.init();
		StatespacevisFactory modelFactory = StatespacevisFactoryImpl.eINSTANCE;

		at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis model = modelFactory
				.createStateSpacePathVis();

		// create example state space
		// ==========================

		StateSpace stateSpace = modelFactory.createStateSpace();
		model.setStatespace(stateSpace);

		// create states & state space nodes

		State initialState = modelFactory.createState();
		EObject snapshot = load(
				"platform:/plugin/at.bitandart.zoubek.statespace.vis.example/modelSnapshots/state0.simplemodel",
				new ResourceSetImpl()).getContents().get(0);
		initialState.setModelSnapshot(snapshot);
		stateSpace.setInitialState(initialState);

		State state1 = modelFactory.createState();
		snapshot = load(
				"platform:/plugin/at.bitandart.zoubek.statespace.vis.example/modelSnapshots/state1.simplemodel",
				new ResourceSetImpl()).getContents().get(0);
		state1.setModelSnapshot(snapshot);

		State state2 = modelFactory.createState();
		snapshot = load(
				"platform:/plugin/at.bitandart.zoubek.statespace.vis.example/modelSnapshots/state2.simplemodel",
				new ResourceSetImpl()).getContents().get(0);
		state2.setModelSnapshot(snapshot);

		State state3 = modelFactory.createState();
		snapshot = load(
				"platform:/plugin/at.bitandart.zoubek.statespace.vis.example/modelSnapshots/state3.simplemodel",
				new ResourceSetImpl()).getContents().get(0);
		state3.setModelSnapshot(snapshot);

		// create state space Transitions
		// ------------------------------

		// iS -> s1

		StateSpaceTransition initialStateToState1 = modelFactory
				.createStateSpaceTransition();
		initialStateToState1.setTransitionName("iS -> s1");
		initialStateToState1.setNextState(state1);
		initialStateToState1.setPrevState(initialState);

		// s1 -> s2

		StateSpaceTransition state1ToState2 = modelFactory
				.createStateSpaceTransition();
		state1ToState2.setTransitionName("s1 -> s2");
		state1ToState2.setNextState(state2);
		state1ToState2.setPrevState(state1);

		// s2 -> s3

		StateSpaceTransition state2ToState3 = modelFactory
				.createStateSpaceTransition();
		state2ToState3.setTransitionName("s2 -> s3");
		state2ToState3.setNextState(state3);
		state2ToState3.setPrevState(state2);

		// s1 -> s1

		StateSpaceTransition state1ToState1 = modelFactory
				.createStateSpaceTransition();
		state1ToState1.setTransitionName("s1 -> s1");
		state1ToState1.setNextState(state1);
		state1ToState1.setPrevState(state1);
		// s1 -> iS

		StateSpaceTransition state1ToInitialState = modelFactory
				.createStateSpaceTransition();
		state1ToInitialState.setTransitionName("s1 -> iS");
		state1ToInitialState.setNextState(initialState);
		state1ToInitialState.setPrevState(state1);

		// # create example state space path
		/*
		 * 
		 * iS --> s1n1 --> s1n2 --> s2n1 \-> s1n3
		 */

		Tree<State, StateSpaceTransition> statePathTree = modelFactory
				.createTree();
		model.setStatePathTree(statePathTree);

		// iS -> s1n1

		TreeNode<State, StateSpaceTransition> initialStateTreeNode = modelFactory
				.createTreeNode();
		initialStateTreeNode.setNodeValue(initialState);
		statePathTree.setRootNode(initialStateTreeNode);

		TreeNode<State, StateSpaceTransition> state1TreeNode1 = modelFactory
				.createTreeNode();
		state1TreeNode1.setNodeValue(state1);

		TreeTransition<State, StateSpaceTransition> initialStateToState1Node1TPT = modelFactory
				.createTreeTransition();
		initialStateToState1Node1TPT.setTransitionValue(initialStateToState1);
		initialStateToState1Node1TPT.setNextNode(state1TreeNode1);
		initialStateToState1Node1TPT.setPreviousNode(initialStateTreeNode);
		initialStateTreeNode.getChildTransitions().add(
				initialStateToState1Node1TPT);
		state1TreeNode1.setParentTransition(initialStateToState1Node1TPT);

		// s1n1 -> s1n2

		TreeNode<State, StateSpaceTransition> state1TreeNode2 = modelFactory
				.createTreeNode();
		state1TreeNode2.setNodeValue(state1);

		TreeTransition<State, StateSpaceTransition> state1Node1ToState1Node2TPT = modelFactory
				.createTreeTransition();
		state1Node1ToState1Node2TPT.setTransitionValue(state1ToState1);
		state1Node1ToState1Node2TPT.setNextNode(state1TreeNode2);
		state1Node1ToState1Node2TPT.setPreviousNode(state1TreeNode1);
		state1TreeNode1.getChildTransitions().add(
				state1Node1ToState1Node2TPT);
		state1TreeNode2.setParentTransition(state1Node1ToState1Node2TPT);

		// s1n2 -> s2n1

		TreeNode<State, StateSpaceTransition> state2TreeNode1 = modelFactory
				.createTreeNode();
		state2TreeNode1.setNodeValue(state2);

		TreeTransition<State, StateSpaceTransition> state1Node2ToState2Node1TPT = modelFactory
				.createTreeTransition();
		state1Node2ToState2Node1TPT.setTransitionValue(state1ToState2);
		state1Node2ToState2Node1TPT.setNextNode(state2TreeNode1);
		state1Node2ToState2Node1TPT.setPreviousNode(state1TreeNode2);
		state1TreeNode2.getChildTransitions().add(
				state1Node2ToState2Node1TPT);
		state2TreeNode1.setParentTransition(state1Node2ToState2Node1TPT);

		// s1n2 -> s1n3

		TreeNode<State, StateSpaceTransition> state1TreeNode3 = modelFactory
				.createTreeNode();
		state1TreeNode3.setNodeValue(state1);

		TreeTransition<State, StateSpaceTransition> state1Node2ToState1Node3TPT = modelFactory
				.createTreeTransition();
		state1Node2ToState1Node3TPT.setTransitionValue(state1ToState1);
		state1Node2ToState1Node3TPT.setNextNode(state1TreeNode3);
		state1Node2ToState1Node3TPT.setPreviousNode(state1TreeNode2);
		state1TreeNode2.getChildTransitions().add(
				state1Node2ToState1Node3TPT);
		state1TreeNode3.setParentTransition(state1Node2ToState1Node3TPT);

		// # assign builders
		
		model.setModelHistoryBuilder(SSPVisEMFFactoryImpl.eINSTANCE.createEMFCompareHistoryBuilder());
		model.getLifelineBuilders().add(SSPVisEMFFactoryImpl.eINSTANCE.createEMFObjectLifelineBuilder());
		
		// create example lifelines
		// ========================

//		LifelineGroup group = modelFactory.createLifelineGroup();
//		model.getLifelineGroups().add(group);

		// ## lifeline 1

//		ObjectLifeline lf1 = modelFactory.createObjectLifeline();
//		group.getLifelines().add(lf1);
//
//		ObjectDescriptor lf1ObjectDescriptor = modelFactory
//				.createObjectDescriptor();
//		lf1ObjectDescriptor.setClassName("ObjectClassA");
//		lf1ObjectDescriptor.setObjectID(0);
//		lf1.setObjectDescriptor(lf1ObjectDescriptor);
//
//		Tree<StateSpaceTransition, ObjectLifelineState> tree = modelFactory
//				.createTree();
//		lf1.setObjectStateTree(tree);

		// ### lifeline 1 nodes

		/*
		 * 1 --> 2 --> 3 --> 4 --> 5 \-> 6 --> 7
		 */

//		TreeNode<StateSpaceTransition, ObjectLifelineState> node1 = modelFactory
//				.createTreeNode();
//		tree.setRootNode(node1);
//
//		TreeTransition<StateSpaceTransition, ObjectLifelineState> t1to2 = modelFactory
//				.createTreeTransition();
//		ObjectLifelineState oST1to2 = modelFactory.createObjectLifelineState();
//		oST1to2.setState(ObjectStateType.NULL);
//		t1to2.setTransitionValue(oST1to2);
//
//		TreeNode<StateSpaceTransition, ObjectLifelineState> node2 = modelFactory
//				.createTreeNode();
//		node1.addChildNode(node2, t1to2);
//
//		TreeTransition<StateSpaceTransition, ObjectLifelineState> t2to3 = modelFactory
//				.createTreeTransition();
//		ObjectLifelineState oST2to3 = modelFactory.createObjectLifelineState();
//		oST2to3.setState(ObjectStateType.NOTNULL);
//		t2to3.setTransitionValue(oST2to3);
//
//		TreeNode<StateSpaceTransition, ObjectLifelineState> node3 = modelFactory
//				.createTreeNode();
//		node2.addChildNode(node3, t2to3);
//
//		TreeTransition<StateSpaceTransition, ObjectLifelineState> t3to4 = modelFactory
//				.createTreeTransition();
//		ObjectLifelineState oST3to4 = modelFactory.createObjectLifelineState();
//		oST3to4.setState(ObjectStateType.NOTNULL);
//		t3to4.setTransitionValue(oST3to4);
//
//		TreeNode<StateSpaceTransition, ObjectLifelineState> node4 = modelFactory
//				.createTreeNode();
//		node3.addChildNode(node4, t3to4);
//
//		TreeTransition<StateSpaceTransition, ObjectLifelineState> t4to5 = modelFactory
//				.createTreeTransition();
//		ObjectLifelineState oST4to5 = modelFactory.createObjectLifelineState();
//		oST4to5.setState(ObjectStateType.CHANGED);
//		t4to5.setTransitionValue(oST4to5);
//
//		TreeNode<StateSpaceTransition, ObjectLifelineState> node5 = modelFactory
//				.createTreeNode();
//		node4.addChildNode(node5, t4to5);
//
//		TreeTransition<StateSpaceTransition, ObjectLifelineState> t3to6 = modelFactory
//				.createTreeTransition();
//		ObjectLifelineState oST3to6 = modelFactory.createObjectLifelineState();
//		oST3to6.setState(ObjectStateType.NOTNULL);
//		t3to6.setTransitionValue(oST3to6);
//
//		TreeNode<StateSpaceTransition, ObjectLifelineState> node6 = modelFactory
//				.createTreeNode();
//		node3.addChildNode(node6, t3to6);
//
//		TreeTransition<StateSpaceTransition, ObjectLifelineState> t6to7 = modelFactory
//				.createTreeTransition();
//		ObjectLifelineState oST6to7 = modelFactory.createObjectLifelineState();
//		oST6to7.setState(ObjectStateType.NULL);
//		t6to7.setTransitionValue(oST6to7);
//
//		TreeNode<StateSpaceTransition, ObjectLifelineState> node7 = modelFactory
//				.createTreeNode();
//		node6.addChildNode(node7, t6to7);

		// # Open Editor

		try {
			window.getActivePage().openEditor(new SSPVisEditorInput(model),
					StateSpacePathVisEditor.EDITOR_ID);
		} catch (PartInitException e) {
			MessageDialog
					.openError(
							window.getShell(),
							"Couldn't open editor",
							"Couldn't open the state space visualization editor, see message log for further details");
			e.printStackTrace();
		}
		return null;
	}

	private Resource load(String absolutePath, ResourceSet resourceSet) {
		URI uri = URI.createURI(absolutePath);
		return resourceSet.getResource(uri, true);
	}
}
