/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass#getMyStringAttribute <em>My String Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getAbstractClass()
 * @model abstract="true"
 * @generated
 */
public interface AbstractClass extends BaseInterface {
	/**
	 * Returns the value of the '<em><b>My String Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>My String Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>My String Attribute</em>' attribute.
	 * @see #setMyStringAttribute(String)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getAbstractClass_MyStringAttribute()
	 * @model dataType="at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyString"
	 * @generated
	 */
	String getMyStringAttribute();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.AbstractClass#getMyStringAttribute <em>My String Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>My String Attribute</em>' attribute.
	 * @see #getMyStringAttribute()
	 * @generated
	 */
	void setMyStringAttribute(String value);

} // AbstractClass
