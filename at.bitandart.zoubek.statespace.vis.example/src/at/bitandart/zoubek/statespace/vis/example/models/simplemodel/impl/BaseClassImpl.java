/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseClassImpl#getRefSimpleClass <em>Ref Simple Class</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.BaseClassImpl#getStringAttribute <em>String Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BaseClassImpl extends BaseInterfaceImpl implements BaseClass {
	/**
	 * The cached value of the '{@link #getRefSimpleClass() <em>Ref Simple Class</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefSimpleClass()
	 * @generated
	 * @ordered
	 */
	protected EList<SimpleClass> refSimpleClass;

	/**
	 * The default value of the '{@link #getStringAttribute() <em>String Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final String STRING_ATTRIBUTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStringAttribute() <em>String Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringAttribute()
	 * @generated
	 * @ordered
	 */
	protected String stringAttribute = STRING_ATTRIBUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplemodelPackage.Literals.BASE_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimpleClass> getRefSimpleClass() {
		if (refSimpleClass == null) {
			refSimpleClass = new EObjectWithInverseResolvingEList<SimpleClass>(
					SimpleClass.class, this,
					SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS,
					SimplemodelPackage.SIMPLE_CLASS__REF_BASE_CLASS);
		}
		return refSimpleClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStringAttribute() {
		return stringAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStringAttribute(String newStringAttribute) {
		String oldStringAttribute = stringAttribute;
		stringAttribute = newStringAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.BASE_CLASS__STRING_ATTRIBUTE,
					oldStringAttribute, stringAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRefSimpleClass())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS:
			return ((InternalEList<?>) getRefSimpleClass()).basicRemove(
					otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS:
			return getRefSimpleClass();
		case SimplemodelPackage.BASE_CLASS__STRING_ATTRIBUTE:
			return getStringAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS:
			getRefSimpleClass().clear();
			getRefSimpleClass().addAll(
					(Collection<? extends SimpleClass>) newValue);
			return;
		case SimplemodelPackage.BASE_CLASS__STRING_ATTRIBUTE:
			setStringAttribute((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS:
			getRefSimpleClass().clear();
			return;
		case SimplemodelPackage.BASE_CLASS__STRING_ATTRIBUTE:
			setStringAttribute(STRING_ATTRIBUTE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.BASE_CLASS__REF_SIMPLE_CLASS:
			return refSimpleClass != null && !refSimpleClass.isEmpty();
		case SimplemodelPackage.BASE_CLASS__STRING_ATTRIBUTE:
			return STRING_ATTRIBUTE_EDEFAULT == null ? stringAttribute != null
					: !STRING_ATTRIBUTE_EDEFAULT.equals(stringAttribute);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (stringAttribute: ");
		result.append(stringAttribute);
		result.append(')');
		return result.toString();
	}

} //BaseClassImpl
