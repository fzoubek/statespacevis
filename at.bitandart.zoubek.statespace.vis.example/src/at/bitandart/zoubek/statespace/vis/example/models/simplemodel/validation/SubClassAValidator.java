/**
 *
 * $Id$
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.validation;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass;

/**
 * A sample validator interface for {@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface SubClassAValidator {
	boolean validate();

	boolean validateConSimpleClass(SimpleClass value);

	boolean validateMyEnumAttribute(MyEnum value);
}
