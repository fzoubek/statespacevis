/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Class C</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassCImpl#getFloatAttribute <em>Float Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubClassCImpl extends BaseClassImpl implements SubClassC {
	/**
	 * The default value of the '{@link #getFloatAttribute() <em>Float Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFloatAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final float FLOAT_ATTRIBUTE_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getFloatAttribute() <em>Float Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFloatAttribute()
	 * @generated
	 * @ordered
	 */
	protected float floatAttribute = FLOAT_ATTRIBUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubClassCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplemodelPackage.Literals.SUB_CLASS_C;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getFloatAttribute() {
		return floatAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFloatAttribute(float newFloatAttribute) {
		float oldFloatAttribute = floatAttribute;
		floatAttribute = newFloatAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.SUB_CLASS_C__FLOAT_ATTRIBUTE,
					oldFloatAttribute, floatAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_C__FLOAT_ATTRIBUTE:
			return getFloatAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_C__FLOAT_ATTRIBUTE:
			setFloatAttribute((Float) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_C__FLOAT_ATTRIBUTE:
			setFloatAttribute(FLOAT_ATTRIBUTE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_C__FLOAT_ATTRIBUTE:
			return floatAttribute != FLOAT_ATTRIBUTE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (floatAttribute: ");
		result.append(floatAttribute);
		result.append(')');
		return result.toString();
	}

} //SubClassCImpl
