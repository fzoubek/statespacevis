/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Class B</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassBImpl#getIntAttribute <em>Int Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubClassBImpl extends BaseClassImpl implements SubClassB {
	/**
	 * The default value of the '{@link #getIntAttribute() <em>Int Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final int INT_ATTRIBUTE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntAttribute() <em>Int Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntAttribute()
	 * @generated
	 * @ordered
	 */
	protected int intAttribute = INT_ATTRIBUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubClassBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplemodelPackage.Literals.SUB_CLASS_B;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntAttribute() {
		return intAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntAttribute(int newIntAttribute) {
		int oldIntAttribute = intAttribute;
		intAttribute = newIntAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SimplemodelPackage.SUB_CLASS_B__INT_ATTRIBUTE,
					oldIntAttribute, intAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_B__INT_ATTRIBUTE:
			return getIntAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_B__INT_ATTRIBUTE:
			setIntAttribute((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_B__INT_ATTRIBUTE:
			setIntAttribute(INT_ATTRIBUTE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SimplemodelPackage.SUB_CLASS_B__INT_ATTRIBUTE:
			return intAttribute != INT_ATTRIBUTE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (intAttribute: ");
		result.append(intAttribute);
		result.append(')');
		return result.toString();
	}

} //SubClassBImpl
