/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Class B</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB#getIntAttribute <em>Int Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSubClassB()
 * @model
 * @generated
 */
public interface SubClassB extends BaseClass {
	/**
	 * Returns the value of the '<em><b>Int Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Int Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Int Attribute</em>' attribute.
	 * @see #setIntAttribute(int)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSubClassB_IntAttribute()
	 * @model
	 * @generated
	 */
	int getIntAttribute();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB#getIntAttribute <em>Int Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Int Attribute</em>' attribute.
	 * @see #getIntAttribute()
	 * @generated
	 */
	void setIntAttribute(int value);

} // SubClassB
