/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Class A</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getConSimpleClass <em>Con Simple Class</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getMyEnumAttribute <em>My Enum Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSubClassA()
 * @model
 * @generated
 */
public interface SubClassA extends AbstractClass {
	/**
	 * Returns the value of the '<em><b>Con Simple Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Con Simple Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Con Simple Class</em>' containment reference.
	 * @see #setConSimpleClass(SimpleClass)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSubClassA_ConSimpleClass()
	 * @model containment="true"
	 * @generated
	 */
	SimpleClass getConSimpleClass();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getConSimpleClass <em>Con Simple Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Con Simple Class</em>' containment reference.
	 * @see #getConSimpleClass()
	 * @generated
	 */
	void setConSimpleClass(SimpleClass value);

	/**
	 * Returns the value of the '<em><b>My Enum Attribute</b></em>' attribute.
	 * The literals are from the enumeration {@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>My Enum Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>My Enum Attribute</em>' attribute.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum
	 * @see #setMyEnumAttribute(MyEnum)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSubClassA_MyEnumAttribute()
	 * @model
	 * @generated
	 */
	MyEnum getMyEnumAttribute();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA#getMyEnumAttribute <em>My Enum Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>My Enum Attribute</em>' attribute.
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum
	 * @see #getMyEnumAttribute()
	 * @generated
	 */
	void setMyEnumAttribute(MyEnum value);

} // SubClassA
