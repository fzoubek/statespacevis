/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.example;

import at.bitandart.zoubek.statespace.vis.editors.StateSpacePathVisEditor;
import at.bitandart.zoubek.statespace.vis.sspvis.listener.AbstractSSPVisListener;
import at.bitandart.zoubek.statespace.vis.sspvis.listener.SSPVisEvent;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class SSPVisEditorListener extends AbstractSSPVisListener {

	/**
	 * 
	 */
	public SSPVisEditorListener() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.bitandart.zoubek.statespace.vis.sspvis.listener.AbstractSSPVisListener
	 * #stateSpacePathChanged(at.bitandart.zoubek.statespace.vis.editors.
	 * StateSpacePathVis)
	 */
	@Override
	public void stateSpaceTreePathChanged(StateSpacePathVisEditor editorInstance) {
		// TODO handle state space path changed events here
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.bitandart.zoubek.statespace.vis.sspvis.listener.AbstractSSPVisListener
	 * #filterDefinitionChanged(at.bitandart.zoubek.statespace.vis.editors.
	 * StateSpacePathVis)
	 */
	@Override
	public void filterDefinitionChanged(StateSpacePathVisEditor editorInstance) {
		// TODO handle filter definition changes here
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.bitandart.zoubek.statespace.vis.sspvis.listener.AbstractSSPVisListener
	 * #unknownEventOccured(at.bitandart.zoubek.statespace.vis.sspvis.listener.
	 * SSPVisEvent)
	 */
	@Override
	public void unknownEventOccured(SSPVisEvent event) {
		// TODO handle unknown events here
	}

}
