/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimplemodelFactoryImpl extends EFactoryImpl implements
		SimplemodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SimplemodelFactory init() {
		try {
			SimplemodelFactory theSimplemodelFactory = (SimplemodelFactory) EPackage.Registry.INSTANCE
					.getEFactory(SimplemodelPackage.eNS_URI);
			if (theSimplemodelFactory != null) {
				return theSimplemodelFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SimplemodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimplemodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case SimplemodelPackage.SIMPLE_CLASS:
			return createSimpleClass();
		case SimplemodelPackage.BASE_CLASS:
			return createBaseClass();
		case SimplemodelPackage.BASE_INTERFACE:
			return createBaseInterface();
		case SimplemodelPackage.SUB_CLASS_A:
			return createSubClassA();
		case SimplemodelPackage.SUB_CLASS_B:
			return createSubClassB();
		case SimplemodelPackage.SUB_CLASS_C:
			return createSubClassC();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case SimplemodelPackage.MY_ENUM:
			return createMyEnumFromString(eDataType, initialValue);
		case SimplemodelPackage.MY_STRING:
			return createMyStringFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case SimplemodelPackage.MY_ENUM:
			return convertMyEnumToString(eDataType, instanceValue);
		case SimplemodelPackage.MY_STRING:
			return convertMyStringToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleClass createSimpleClass() {
		SimpleClassImpl simpleClass = new SimpleClassImpl();
		return simpleClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseClass createBaseClass() {
		BaseClassImpl baseClass = new BaseClassImpl();
		return baseClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseInterface createBaseInterface() {
		BaseInterfaceImpl baseInterface = new BaseInterfaceImpl();
		return baseInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubClassA createSubClassA() {
		SubClassAImpl subClassA = new SubClassAImpl();
		return subClassA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubClassB createSubClassB() {
		SubClassBImpl subClassB = new SubClassBImpl();
		return subClassB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubClassC createSubClassC() {
		SubClassCImpl subClassC = new SubClassCImpl();
		return subClassC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MyEnum createMyEnumFromString(EDataType eDataType,
			String initialValue) {
		MyEnum result = MyEnum.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMyEnumToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createMyStringFromString(EDataType eDataType,
			String initialValue) {
		return (String) super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMyStringToString(EDataType eDataType,
			Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimplemodelPackage getSimplemodelPackage() {
		return (SimplemodelPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SimplemodelPackage getPackage() {
		return SimplemodelPackage.eINSTANCE;
	}

} //SimplemodelFactoryImpl
