/**
 * 
 */
package at.bitandart.zoubek.statespace.vis.example.handlers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.MyEnum;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelFactory;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassA;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassB;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SubClassC;
import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SubClassAImpl;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.LifelineGroup;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectDescriptor;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.ObjectLifeline;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StateSpacePathVis;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.StatespacevisFactory;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisFactoryImpl;
import at.bitandart.zoubek.statespace.vis.statespacevismodel.impl.StatespacevisPackageImpl;

/**
 * @author Florian Zoubek <zoubek@bitandart.at>
 * 
 */
public class GenerateModelSnapshots extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		/*
		 * StatespacevisPackageImpl.init(); StatespacevisFactory modelFactory =
		 * StatespacevisFactoryImpl.eINSTANCE;
		 * 
		 * List<EObject> objects = new ArrayList<>(); StateSpacePathVis sspvis =
		 * modelFactory.createStateSpacePathVis(); objects.add(sspvis);
		 * 
		 * LifelineGroup lGroup1 = modelFactory.createLifelineGroup();
		 * objects.add(lGroup1); sspvis.getLifelineGroups().add(lGroup1);
		 * 
		 * ObjectLifeline oLifeline1 = modelFactory.createObjectLifeline();
		 * objects.add(oLifeline1); lGroup1.getLifelines().add(oLifeline1);
		 * 
		 * ObjectDescriptor o1Desc = modelFactory.createObjectDescriptor();
		 * objects.add(o1Desc); o1Desc.setClassName("Class1");
		 * o1Desc.setInstanceName("instance1"); o1Desc.setObjectID(0);
		 * oLifeline1.setObjectDescriptor(o1Desc);
		 * 
		 * 
		 * ObjectLifeline oLifeline2 = modelFactory.createObjectLifeline();
		 * objects.add(oLifeline2); lGroup1.getLifelines().add(oLifeline2);
		 * 
		 * ObjectDescriptor o2Desc = modelFactory.createObjectDescriptor();
		 * objects.add(o2Desc); o2Desc.setClassName("Class1");
		 * o2Desc.setInstanceName("instance2"); o2Desc.setObjectID(1);
		 * oLifeline2.setObjectDescriptor(o2Desc);
		 * 
		 * saveModel("sspvis_example"+File.separator+"state1.sspvis",objects);
		 * 
		 * o2Desc.setClassName("Class2");
		 * 
		 * saveModel("sspvis_example"+File.separator+"state2.sspvis",objects);
		 * 
		 * MessageDialog.openInformation(null, "Saved",
		 * "saved snapshots in the current working directory");
		 */

		SimplemodelFactory modelFactory = SimplemodelFactory.eINSTANCE;
		List<EObject> objects = new ArrayList<>();

		BaseClass baseClass1 = modelFactory.createBaseClass();
		objects.add(baseClass1);

		SubClassA subclassA1 = modelFactory.createSubClassA();
		objects.add(subclassA1);

		SubClassB subclassB1 = modelFactory.createSubClassB();
		objects.add(subclassB1);

		SubClassC subclassC1 = modelFactory.createSubClassC();
		objects.add(subclassC1);

		SimpleClass simpleClass1 = modelFactory.createSimpleClass();
		objects.add(simpleClass1);

		SimpleClass simpleClass2 = modelFactory.createSimpleClass();
		objects.add(simpleClass2);

		SimpleClass simpleClass3 = modelFactory.createSimpleClass();
		objects.add(simpleClass3);

		// # State 0

		baseClass1.setStringAttribute("BC1 - String 0");
		baseClass1.getRefSimpleClass().add(simpleClass1);
		baseClass1.getRefSimpleClass().add(simpleClass2);

		subclassA1.setConSimpleClass(simpleClass3);
		subclassA1.setMyEnumAttribute(MyEnum.VAL1);
		subclassA1.setMyStringAttribute("SCA1 - String 0");

		subclassB1.setIntAttribute(0);
		subclassB1.setStringAttribute("SCB1 - String 0");
		subclassB1.getRefSimpleClass().add(simpleClass3);

		subclassC1.setFloatAttribute(0.0f);
		subclassC1.setStringAttribute("SCC1 - String 0");

		simpleClass1.setDoubleAttribute(0.0);
		simpleClass2.setDoubleAttribute(0.0);
		simpleClass3.setDoubleAttribute(0.0);

		saveModel("sspvis_example" + File.separator + "state0.simplemodel",
				objects);

		// # State 1

		baseClass1.setStringAttribute("BC1 - String 1");
		subclassB1.getRefSimpleClass().remove(simpleClass3);
		objects.remove(simpleClass3);

		saveModel("sspvis_example" + File.separator + "state1.simplemodel",
				objects);

		// # State 2

		simpleClass3.setDoubleAttribute(3.0);
		subclassA1.setMyEnumAttribute(MyEnum.VAL2);

		saveModel("sspvis_example" + File.separator + "state2.simplemodel",
				objects);

		// # State 3

		subclassC1.setStringAttribute("SCC1 - String 3");

		SubClassA subclassA2 = modelFactory.createSubClassA();
		objects.add(subclassA2);

		subclassA1.setConSimpleClass(simpleClass1);
		subclassA1.setMyEnumAttribute(MyEnum.VAL3);
		subclassA1.setMyStringAttribute("SCA2 - String 3");

		saveModel("sspvis_example" + File.separator + "state3.simplemodel",
				objects);

		MessageDialog.openInformation(null, "Saved",
				"saved snapshots in the current working directory");

		return null;
	}

	private void saveModel(String file, Collection<EObject> objects) {

		// Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		// Map<String, Object> m = reg.getExtensionToFactoryMap();
		// m.put("sspvis", new XMIResourceFactoryImpl());

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		// create a resource
		Resource resource = resSet.createResource(URI.createURI(file));

		// set model

		for (EObject object : objects) {
			resource.getContents().add(object);
		}

		// save
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
