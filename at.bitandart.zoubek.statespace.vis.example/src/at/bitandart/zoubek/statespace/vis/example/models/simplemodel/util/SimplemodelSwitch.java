/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel.util;

import at.bitandart.zoubek.statespace.vis.example.models.simplemodel.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage
 * @generated
 */
public class SimplemodelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SimplemodelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimplemodelSwitch() {
		if (modelPackage == null) {
			modelPackage = SimplemodelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case SimplemodelPackage.ABSTRACT_CLASS: {
			AbstractClass abstractClass = (AbstractClass) theEObject;
			T result = caseAbstractClass(abstractClass);
			if (result == null)
				result = caseBaseInterface(abstractClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SimplemodelPackage.SIMPLE_CLASS: {
			SimpleClass simpleClass = (SimpleClass) theEObject;
			T result = caseSimpleClass(simpleClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SimplemodelPackage.BASE_CLASS: {
			BaseClass baseClass = (BaseClass) theEObject;
			T result = caseBaseClass(baseClass);
			if (result == null)
				result = caseBaseInterface(baseClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SimplemodelPackage.BASE_INTERFACE: {
			BaseInterface baseInterface = (BaseInterface) theEObject;
			T result = caseBaseInterface(baseInterface);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SimplemodelPackage.SUB_CLASS_A: {
			SubClassA subClassA = (SubClassA) theEObject;
			T result = caseSubClassA(subClassA);
			if (result == null)
				result = caseAbstractClass(subClassA);
			if (result == null)
				result = caseBaseInterface(subClassA);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SimplemodelPackage.SUB_CLASS_B: {
			SubClassB subClassB = (SubClassB) theEObject;
			T result = caseSubClassB(subClassB);
			if (result == null)
				result = caseBaseClass(subClassB);
			if (result == null)
				result = caseBaseInterface(subClassB);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SimplemodelPackage.SUB_CLASS_C: {
			SubClassC subClassC = (SubClassC) theEObject;
			T result = caseSubClassC(subClassC);
			if (result == null)
				result = caseBaseClass(subClassC);
			if (result == null)
				result = caseBaseInterface(subClassC);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractClass(AbstractClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleClass(SimpleClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseClass(BaseClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseInterface(BaseInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub Class A</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub Class A</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubClassA(SubClassA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub Class B</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub Class B</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubClassB(SubClassB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub Class C</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub Class C</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubClassC(SubClassC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SimplemodelSwitch
