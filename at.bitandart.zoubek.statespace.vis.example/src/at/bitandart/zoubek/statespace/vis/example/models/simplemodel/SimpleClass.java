/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getRefBaseClass <em>Ref Base Class</em>}</li>
 *   <li>{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getDoubleAttribute <em>Double Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSimpleClass()
 * @model
 * @generated
 */
public interface SimpleClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Ref Base Class</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getRefSimpleClass <em>Ref Simple Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Base Class</em>' reference.
	 * @see #setRefBaseClass(BaseClass)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSimpleClass_RefBaseClass()
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.BaseClass#getRefSimpleClass
	 * @model opposite="refSimpleClass"
	 * @generated
	 */
	BaseClass getRefBaseClass();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getRefBaseClass <em>Ref Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Base Class</em>' reference.
	 * @see #getRefBaseClass()
	 * @generated
	 */
	void setRefBaseClass(BaseClass value);

	/**
	 * Returns the value of the '<em><b>Double Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Double Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Double Attribute</em>' attribute.
	 * @see #setDoubleAttribute(double)
	 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage#getSimpleClass_DoubleAttribute()
	 * @model
	 * @generated
	 */
	double getDoubleAttribute();

	/**
	 * Sets the value of the '{@link at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimpleClass#getDoubleAttribute <em>Double Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Double Attribute</em>' attribute.
	 * @see #getDoubleAttribute()
	 * @generated
	 */
	void setDoubleAttribute(double value);

} // SimpleClass
