/**
 */
package at.bitandart.zoubek.statespace.vis.example.models.simplemodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see at.bitandart.zoubek.statespace.vis.example.models.simplemodel.SimplemodelPackage
 * @generated
 */
public interface SimplemodelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SimplemodelFactory eINSTANCE = at.bitandart.zoubek.statespace.vis.example.models.simplemodel.impl.SimplemodelFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Simple Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Class</em>'.
	 * @generated
	 */
	SimpleClass createSimpleClass();

	/**
	 * Returns a new object of class '<em>Base Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Class</em>'.
	 * @generated
	 */
	BaseClass createBaseClass();

	/**
	 * Returns a new object of class '<em>Base Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Interface</em>'.
	 * @generated
	 */
	BaseInterface createBaseInterface();

	/**
	 * Returns a new object of class '<em>Sub Class A</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub Class A</em>'.
	 * @generated
	 */
	SubClassA createSubClassA();

	/**
	 * Returns a new object of class '<em>Sub Class B</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub Class B</em>'.
	 * @generated
	 */
	SubClassB createSubClassB();

	/**
	 * Returns a new object of class '<em>Sub Class C</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub Class C</em>'.
	 * @generated
	 */
	SubClassC createSubClassC();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SimplemodelPackage getSimplemodelPackage();

} //SimplemodelFactory
